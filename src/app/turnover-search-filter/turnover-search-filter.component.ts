import { Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { City } from 'app/models/city.model';
import { ClientSmallSearchFilter } from 'app/models/client-small-search-filter.model';
import { Country } from 'app/models/country.model';
import { County } from 'app/models/county.model';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { Post } from 'app/models/post.model';
import { TurnoverSearchFilter } from 'app/models/turnoverSearchFilter.model';
import { AuthService } from 'app/services/auth.service';
import { PartnerService } from 'app/services/partner.service';
import { TranslateService } from 'app/services/translate';
import { UsersService } from 'app/services/user.service';
import { DxValidatorComponent } from 'devextreme-angular';
import { environment } from 'environments/environment';
import * as _ from 'lodash';

@Component({
  selector: 'app-turnover-search-filter',
  templateUrl: './turnover-search-filter.component.html',
  styleUrls: ['./turnover-search-filter.component.css']
})
export class TurnoverSearchFilterComponent implements OnInit, OnChanges {
  @Input() posts: Post[];
  @Input() items: any[];
  @Input() departments: any;

  @Input() cities: City[];
  @Input() citiesDS: any;

  @Input() counties: County[];
  @Input() countiesDS: any;

  @Input() countries: Country[];
  @Input() countriesDS: any;

  @Input() selectedItemCode: number;
  @Input() distinctSupplierAgentIds: number[];
  @Input() filteredPartners: any;

  @Output() searchEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() refreshEvent: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('validationSearchGroup') validationSearchGroup: DxValidatorComponent;
  turnoverSearchFilter: TurnoverSearchFilter;
  postsDS: Post[] = [];
  userPost: Post;
  isOwner: boolean;
  isFromMngDepartment: boolean = false;
  partners: any;

  constructor(private translationService: TranslateService, private partnerService: PartnerService,
    private userService: UsersService, private authService: AuthService) {
    this.authService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
    
    this.turnoverSearchFilter = new TurnoverSearchFilter();
  }

  ngOnInit(): void {
    this.partners = {
      paginate: true,
      pageSize: 15,
      store: []
    };

    if (this.selectedItemCode) {
      this.turnoverSearchFilter.code = this.selectedItemCode;     
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.posts || changes.departments) {
      this.userService.getByUsernameOrEmail(this.authService.getUserUserName(), null).then(user => {
        if (user && user.length > 0 && this.departments && this.departments.length > 0) {
          if (user[0] && user[0].postId) {
            this.userPost = this.posts.find(x => Number(x.id) === user[0].postId);
            let dep = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Management);
            const officesFromMngDep = (dep && dep.length > 0 ) ? dep.map(x => x.offices).reduce((x, y) => x.concat(y)) : null; 
            if (officesFromMngDep && officesFromMngDep.length && (officesFromMngDep.find(x => x.id === this.userPost.officeId) !== null)) {
              this.isFromMngDepartment = true;
            }
            this.getPosts();
          }
        }
      });
    }
  }

  @HostListener('document:keydown', ['$event'])
    onKeydown(event: KeyboardEvent) {
      if (event.key === 'F5') {
        event.preventDefault(); 
        this.searchTurnover();
      }
  
      if (event.key === 'Delete') {
        event.preventDefault(); 
        this.resetFilters();
      }
    }

  getPosts() {
    this.postsDS = [];
    if (this.userPost) {
      if (this.userPost.isLocationSuperior || this.userPost.isGeneralManager || this.isFromMngDepartment || this.isOwner) {
        this.postsDS = this.departments.map(x => x.offices).reduce((x, y) => x.concat(y)).map(p => p.posts).reduce((g, h) => g.concat(h));
        this.postsDS = _.orderBy(this.postsDS.filter(f => this.distinctSupplierAgentIds?.includes(f.id) && f.companyPost !== 'DEZACTIVAT'), ['companyPost'], ['asc']);
      } else {
        this.postsDS = [];      
      }
    }
  }

  searchTurnover() {
    if (this.validationSearchGroup.instance.validate().isValid) {
      this.searchEvent.emit(this.turnoverSearchFilter);
    }
  }

  resetFilters() {
    this.turnoverSearchFilter = new TurnoverSearchFilter();

    this.refreshEvent.emit(true);
  }

  onCitiesChanged(e: any) {
    if (e.value && e.value.length > 0) {
      var countryIds = this.cities.filter(x => e.value.includes(x.id)).map(y => y.countryId);
      var countiesIds = this.cities.filter(x => e.value.includes(x.id)).map(y => y.countyId);
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries.filter(i => countryIds.includes(i.id))
      };

      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties.filter(i => countiesIds.includes(i.id))
      };
    } else {
      this.restoreDS();
    }
  }

  onCountiesChanged(e: any) {
    if (e.value && e.value.length) {
      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities.filter(i => e.value.includes(i.countyId))
      };

      var countryIds = this.counties.filter(x => e.value.includes(x.id)).map(y => y.countryId);
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries.filter(i => countryIds.includes(i.id))
      };
    } else {
      this.restoreDS();
    }
  }

  onCountriesChanged(e: any) {
    if (e.value && e.value.length) {
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties.filter(i => e.value.includes(i.countryId))
      };

      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities.filter(i => e.value.includes(i.countryId))
      };
    }
    else {
      this.restoreDS();
    }
  }

  restoreDS() {
    this.citiesDS = {
      paginate: true,
      pageSize: 15,
      store: this.cities
    };
    this.countiesDS = {
      paginate: true,
      pageSize: 15,
      store: this.counties
    };
    this.countriesDS = {
      paginate: true,
      pageSize: 15,
      store: this.countries
    };
  }

  getDisplayExprCities(item) {
    if (!item) {
      return '';
    }
    return item.name + ' - ' + item.countyName + ' - ' + item.countryName;
  }

  getDisplayExprCounties(item) {
    if (!item) {
      return '';
    }
    return item.name;
  }

  getDisplayExprCountries(item) {
    if (!item) {
      return '';
    }
    return item.name;
  }

  getDisplayExprItems(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  onMultiTagCitiesChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagCountieChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagItemChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagPostChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }
  
  onPartnerChanged(e) {
    if (e && e.length > 4) {
      let searchFilter = new PartnerSearchFilter();
      searchFilter.name = e;
      searchFilter.partnerType = [];
      searchFilter.partnerType.push(2);
      searchFilter.isActive = true;
      this.partnerService.getPartnersByFilter(searchFilter).then(items => {
        this.filteredPartners = (items && items.length > 0) ? items : [];
      })
    }
  }

  partnerChange(e) {
    if (e && e.value) {
      this.turnoverSearchFilter.partnerId = e.value;     
      setTimeout(() => {
       
      }, 100);
    }
  }

  getDisplayExprPartners(item) {
    if (!item) {
        return '';
      }

      return item.code + ' - ' + item.name;
  }

  onSuppliersFilterSearch(searchTerm) {
    if (searchTerm && searchTerm.length >= 4) {
      let searchFilter = new ClientSmallSearchFilter();
      searchFilter.nameOrCode = searchTerm;

      this.partnerService.getPartnersSmallAsync(searchFilter).then(items => {
        this.filteredPartners = (items && items.length > 0) ? items : [];
        this.filteredPartners = this.filteredPartners.map(m => ({ ...m, codeAndName: `${m.code} - ${m.name}` }));
      });
    } else {
      this.filteredPartners = [];
    }
  }

  onSuppliersItemSelected(event) {
    if (event && event.selectedItem) {
      this.turnoverSearchFilter.partnerId = event.selectedItem.id;
    }
  }

  onTurnoverAnalysisClick() {
    var url = environment.SRMPrimaware + '/' + Constants.itemTurnoverAnalysis;
    window.open(url, '_blank');
  }
}