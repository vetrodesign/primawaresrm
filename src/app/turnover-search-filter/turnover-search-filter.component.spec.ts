import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnoverSearchFilterComponent } from './turnover-search-filter.component';

describe('TurnoverSearchFilterComponent', () => {
  let component: TurnoverSearchFilterComponent;
  let fixture: ComponentFixture<TurnoverSearchFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnoverSearchFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnoverSearchFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
