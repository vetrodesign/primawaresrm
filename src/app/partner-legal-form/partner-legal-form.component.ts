import { Component, OnInit, ViewChild, AfterViewInit, Input, OnChanges } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { Constants } from 'app/constants';
import { PageActionService } from 'app/services/page-action.service';
import { PartnerLegalForm } from 'app/models/partnerlegalform.model';
import { PartnerLegalFormService } from 'app/services/partner-legal-form.service';

@Component({
  selector: 'app-partner-legal-form',
  templateUrl: './partner-legal-form.component.html',
  styleUrls: ['./partner-legal-form.component.css']
})
export class PartnerLegalFormComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;

  actions: IPageActions;
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  isOwner: boolean = false;
  partnerLegalForms: PartnerLegalForm[];
  loaded: boolean;
  groupedText: string;
  constructor(private authenticationService: AuthService,
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private partnerLegalFormService: PartnerLegalFormService,
    private pageActionService: PageActionService) {
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();
    this.getPartnerLegalForms();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  async getPartnerLegalForms() {
    this.partnerLegalForms = [];
    this.loaded = true;
    await this.partnerLegalFormService.getAllPartnerLegalFormAsync().then(items => {
      this.partnerLegalForms = items;
      this.loaded = false;
    });
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.partnerLegalForm).then(result => {
      this.actions = result;
    });
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  canExport() {
    return this.actions.CanExport;
  }

  canDelete() {
    return this.actions.CanDelete;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
}

  public add() {
    this.dataGrid.instance.addRow();
  }

  public refreshDataGrid() {
    this.getPartnerLegalForms();
    this.dataGrid.instance.refresh();
  }

  public deleteRecords(data: any) {
    this.partnerLegalFormService.deletePartnerLegalFormAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (!response) {
        this.notificationService.alert('top', 'center', 'Datele au fost sterse cu succes!', NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost sterse!', NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
    this.rowIndex = row.rowIndex;

  }

  public async onRowUpdated(event: any): Promise<void> {
    let item = new PartnerLegalForm();
    item = this.partnerLegalForms.find(g => g.id === event.key.id);
    if (item) {
      await this.partnerLegalFormService.updatePartnerLegalFormAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Forma legala partener - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Forma legala partener - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshDataGrid();
      });
    }
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onRowInserting(event: any): Promise<void> {
    let item = new PartnerLegalForm();
    item = event.data;
    if (item) {
      await this.partnerLegalFormService.createPartnerLegalFormAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Forma legala partener - Datele au fost inserate cu succes!',
           NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Forma legala partener - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshDataGrid();
      });
    }
  }
}
