import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { City } from 'app/models/city.model';
import { Country } from 'app/models/country.model';
import { County } from 'app/models/county.model';
import { Department } from 'app/models/department.model';
import { Post } from 'app/models/post.model';
import { AuthService } from 'app/services/auth.service';
import { CityService } from 'app/services/city.service';
import { CountryService } from 'app/services/country.service';
import { CountyService } from 'app/services/county.service';
import { DepartmentsService } from 'app/services/departments.service';
import { ItemTurnoverService } from 'app/services/item-turnover.service';
import { ItemService } from 'app/services/item.service';
import { PostService } from 'app/services/post.service';
import * as moment from 'moment';
import { TurnoverItemHistory } from 'app/models/turnover-item-history.model';
import { TurnoverSearchFilter } from 'app/models/turnoverSearchFilter.model';
import { TurnoverPartner } from 'app/models/turnoverPartner.model';
import { TurnoverHistoryFilter } from 'app/models/turnoverFilter.model';
import { TurnoverHistoryMonthsRow, TurnoverMonths, TurnoverPartnerMonthQuantity, TurnoverPartnerMonthQuantityFilter } from 'app/models/turnoveritem.model';

@Component({
  selector: 'app-item-turnover-history',
  templateUrl: './item-turnover-history.component.html',
  styleUrls: ['./item-turnover-history.component.css']
})
export class ItemTurnoverHistoryComponent implements OnInit {
  @Input() itemId: number;
  @Input() turnoverOperationId: number;
  @Input() itemCode: string;
  @Input() itemName: string;
  @Output() displayItemTurnoverHistoryPopup: EventEmitter<any> = new EventEmitter<any>();

  loaded: boolean;
  isOwner: boolean;
  countries: Country[] = [];
  countriesDS: any;
  counties: County[] = [];
  countiesDS: any;
  cities: City[] = [];
  citiesDS: any;
  turnoverSearchFilter: TurnoverSearchFilter;
  items: any;
  posts: Post[] = [];
  departments: Department[] = [];
  turnoverItemHistory: TurnoverItemHistory[] = [];
  turnoverMonths: TurnoverHistoryMonthsRow[] = [];
  turnoverSuppliers: TurnoverPartner[] = [];
  currentSupplierIndex: number = 0;
  currentSupplierDisplayIndex: number = 0;
  monthsToDisplay: Date[];
  turnoverHistoryFilter: TurnoverHistoryFilter;
  monthLabels: string[] = [];
  isMonthQuantityPopupOpen: boolean;
  isDataLoading: boolean;
  monthQuantityData: TurnoverPartnerMonthQuantity[];
  monthQuantityPartnerName: string;
  monthQuantityMonthAndYear: string;

  constructor(private itemsService: ItemService, private itemTurnoverService: ItemTurnoverService, private postService: PostService, private cityService: CityService, private countyService: CountyService,
    private countryService: CountryService, private departmentsService: DepartmentsService, private authenticationService: AuthService, private route: ActivatedRoute) {
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.itemId || changes.turnoverOperationId) {
      this.initializeFilter();
    }
  }

  initializeFilter() {
    this.turnoverHistoryFilter = new TurnoverHistoryFilter();
    this.turnoverHistoryFilter.itemId = this.itemId;
    this.turnoverHistoryFilter.itemTurnoverOperationId = this.turnoverOperationId;
  }

  loadData() {
    this.getData();
  }

  getData() {
    this.loaded = true;
    this.displayItemTurnoverHistoryPopup.emit(false);
    Promise.all([this.getItemTurnoverDetailXPartnerByFilter()]).then(async x => {


      this.displayItemTurnoverHistoryPopup.emit(true);
      this.loaded = false
    });
  }

  async getItemTurnoverDetailXPartnerByFilter() {
    if (this.turnoverHistoryFilter) {
      await this.itemTurnoverService.getItemTurnoverDetailXPartnerByFilter(this.turnoverHistoryFilter).then((items) => {
        if (items && items.length > 0) {
          this.turnoverItemHistory = items;
          this.calculateTurnoverMonths();
        }
      })
    }
  }

  calculateTurnoverMonths() {
    this.turnoverMonths = [];
    this.monthsToDisplay = [];

    const labels = [];
    let currentMonth = moment();
    let firstMonth = moment().add(-12, 'months');

    for (let i = 12; i >= 1; i--) {
      labels.push(`L-${i}`);
    }

    labels.push('L-c');

    while (currentMonth >= firstMonth) {
      this.monthsToDisplay.push(currentMonth.toDate());
      currentMonth.add(-1, 'months');
    }

    this.monthLabels = labels;
    this.monthsToDisplay = this.monthsToDisplay.sort((a, b) => (a.getTime() - b.getTime()));

    let distinctOperations = this.turnoverItemHistory.reduce((prev, { partnerId, partnerName, caenCode, caenDescription, partnerActivity, partnerResponsableAgent }) =>
      prev.some(x => x.partnerId === partnerId) ?
        prev : [...prev, { partnerId, partnerName, caenCode, caenDescription, partnerActivity, partnerResponsableAgent }], []);
  
    this.turnoverMonths = distinctOperations.map(partner => {
      var row = new TurnoverHistoryMonthsRow();
      var totalQuantity = 0;

      row.partnerId = partner.partnerId;
      row.partnerName = partner.partnerName;
      row.caenCode = partner.caenCode;
      row.caenDescription = partner.caenDescription;
      row.partnerActivity = partner.partnerActivity;
      row.partnerResponsableAgent = partner.partnerResponsableAgent;
      row.monthsData = this.monthsToDisplay.map(monthDate => {
        var month = new TurnoverMonths();
        month.month = monthDate;
  
        var itemMonthData = this.turnoverItemHistory.filter(a => a.partnerId === partner.partnerId
          && a.year === monthDate.getFullYear() && a.month === (monthDate.getMonth() + 1));
  
        if (itemMonthData.length > 0) {
          month.amount = itemMonthData.reduce((a, b) => a + b.amount, 0);
          month.quantity = itemMonthData.reduce((a, b) => a + b.quantity, 0);
          totalQuantity += month.quantity;
        }
  
        return month;
      }).sort((a, b) => (a.month.getTime() - b.month.getTime()));
  
      row.Total = totalQuantity;
      return row;
    });
  } 

  async onMonthQuantityClick(row: any, monthData: any) {
    this.monthQuantityPartnerName = row.partnerName;
    this.monthQuantityMonthAndYear = moment(monthData.month).format('MMM YYYY');

    this.isDataLoading = true;

    let filter = new TurnoverPartnerMonthQuantityFilter();
    filter.itemId = this.itemId;
    filter.partnerId = row.partnerId;
    filter.month = new Date(monthData.month).getMonth() + 1;
    filter.year = new Date(monthData.month).getFullYear();

    await this.itemTurnoverService.getItemTurnoverMonthQuantityXPartner(filter).then(result => {
      this.monthQuantityData = (result && result.length > 0) ? result : [];

      this.isDataLoading = false;
      this.isMonthQuantityPopupOpen = true;
    })
  }
}
