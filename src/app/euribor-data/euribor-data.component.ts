import { Component, OnInit, ViewChild } from '@angular/core';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { ApisOverviewService } from 'app/services/apis-overview.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-euribor-data',
  templateUrl: './euribor-data.component.html',
  styleUrls: ['./euribor-data.component.css']
})
export class EuriborDataComponent implements OnInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;

  loaded: boolean;
  displayData: boolean;
  actions: IPageActions;
  groupedText: string;

  data: any[] = [];
  shouldDisplayRobor9M: boolean = false;

  constructor(
    private apiOverViewService: ApisOverviewService,
    private translationService: TranslateService
  ) {

    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();
    this.getData();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: false,
      CanUpdate: false,
      CanDelete: false,
      CanPrint: false,
      CanExport: false,
      CanImport: false,
      CanDuplicate: false
    };
  }

  public refreshDataGrid() {
    this.getData();
  }

  getData() {
    this.loaded = true;
    Promise.all([this.GetEuriborData()]).then(x => { 
      this.loaded = false
    });
  }

  async GetEuriborData(): Promise<any> {
    await this.apiOverViewService.getEuriborData().then(response => {
      if (response) {
        const observations = response.dataSets[0].series["0:0:0:0:0:0:0"].observations;
        const timePeriods = response.structure.dimensions.observation[0].values;

        this.data = timePeriods.map((period, index) => ({
          value: observations[index] ? parseFloat(observations[index][0].toFixed(2)) : null,
          start: period.start,
          end: period.end
        }));
      }
    });
  }
}
