import { Component, OnInit, ViewChild, AfterViewInit, Input, OnChanges } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { BarCodeInterval } from 'app/models/barcodeinterval.model';
import { BarCodeIntervalService } from 'app/services/bar-code-interval.service';
import { CountryService } from 'app/services/country.service';
import { Country } from 'app/models/country.model';
import { PageActionService } from 'app/services/page-action.service';
import { Constants } from 'app/constants';
import { BarCodeStandardTypeEnum } from 'app/enums/barcodeStandardTypeEnum';
import { BarCodeDetailsComponent } from 'app/bar-code-details/bar-code-details.component';
import { ActivatedRoute } from '@angular/router';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';
import { PartnerService } from 'app/services/partner.service';
import { environment } from 'environments/environment';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { ItemService } from 'app/services/item.service';
import { BarCodeItemAllocationService } from 'app/services/bar-code-item-allocation.service';

@Component({
  selector: 'app-bar-code',
  templateUrl: './bar-code.component.html',
  styleUrls: ['./bar-code.component.css']
})
export class BarCodeComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('barcodeDetails') barcodeDetails: BarCodeDetailsComponent;

  actions: IPageActions;
  barCodes: BarCodeInterval[];
  selectedBarCode: BarCodeInterval;
  partnerLocations: PartnerLocation[] = [];
  countrys: Country[];
  standardTypes: { id: number; name: string }[] = [];
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  loaded: boolean;
  detailsVisibility: boolean;
  isOwner: boolean = false;
  customerId: string;
  partnersDS: any;
  groupedText: string;
  groupedByOwnerCode: boolean = true;
  showNonAvailableCodes: boolean = false;
  infoButton: string;
  glnDS: any[];
  items: any[];
  barCodeItemAllocations: any[];
  numberList: any[] = [];

  constructor(private authenticationService: AuthService,
    private translationService: TranslateService, 
    private barCodeIntervalService: BarCodeIntervalService, 
    private countryService: CountryService,
    private itemService: ItemService,
    private barCodeItemAllocationService: BarCodeItemAllocationService,
    private notificationService: NotificationService,
    private pageActionService: PageActionService,
    private route: ActivatedRoute, private partnerService: PartnerService,
    private partnerLocationService: PartnerLocationService,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService) {
    this.validateBarcodeRange = this.validateBarcodeRange.bind(this);
    this.validateBarcodePartner = this.validateBarcodePartner.bind(this);

    for (let n in BarCodeStandardTypeEnum) {
      if (typeof BarCodeStandardTypeEnum[n] === 'number') {
        this.standardTypes.push({
          id: <any>BarCodeStandardTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    this.setActions();
    this.customerId = this.authenticationService.getUserCustomerId();
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
    this.barCodes = [];
    this.countrys = [];
    this.groupedText = this.translationService.instant('groupedText');
    this.getData();

     
  }

 

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getCountrys(), this.getBarCodes()]).then(x => {
      this.loaded = false;
    });
  }

  async getCountrys() {
    await this.countryService.getAllCountrysAsync().then(c => {
      this.countrys = c;
    });
  }

  async getItems() {
    if (this.items == null) {
      await this.itemService.getAllItemSmallAsync().then(c => {
        this.items = c;
      });
    }
  }

  async getAllocations() {
    await this.barCodeItemAllocationService.getBarCodesAllocationsAsyncByID().then(c => {
      if (c && c.length > 0) {
        this.barCodeItemAllocations = c.filter(x => x.barCode != null || x.barCode != undefined);
      }
    });
  }

  async getBarCodes() {
    if (this.isOwner) {
      await this.barCodeIntervalService.getAllBarCodeIntervalsAsync().then(i => {
        this.barCodes = i;
        this.getPartners(this.barCodes);
      });
    } else {
      await this.barCodeIntervalService.getBarCodeIntervalsAsyncByID().then(i => {
        this.barCodes = i;
        this.getPartners(this.barCodes);
      });
    }
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.barcode).then(result => {
      this.actions = result;
    });
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  canExport() {
    return this.actions.CanExport;
  }

  canDelete() {
    return this.actions.CanDelete;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public add() {
    this.dataGrid.instance.addRow();
  }

  public refreshDataGrid() {
    this.getData();

    this.dataGrid.instance.refresh();
  }

  changeVisible() {
    this.detailsVisibility = false;
    this.refreshDataGrid();
  }

  public deleteRecords(data: any) {
    this.barCodeIntervalService.deleteBarCodeIntervalsAsync(this.selectedRows.map(x => x.id)).then(response => {
      this.refreshDataGrid();
    });
  }

  async getPartners(barCodes: any[]) {
    if (barCodes && barCodes.length > 0) {
      await this.partnerService.getPartnerNamesAndIdsByPartnerIdsAsync(barCodes.map(x => x.partnerId)).then(results => {
        this.partnersDS = results;
      })

      await this.partnerLocationService.getPartnerLocatonsByPartnerIds(barCodes.map(x => x.partnerId)).then(results => {
        this.partnerLocations = results;
        if (this.partnerLocations && this.partnerLocations.length > 0) {
          this.barCodes.forEach(x => {
            x.partnerLocationGln = this.partnerLocations.find(y => y.partnerId == x.partnerId)?.gln;
            this.glnDS = this.partnerLocations.filter(y => y.gln != null).map(x => x.gln);
        });
        }
      })
    }
  }

  public async openDetails(row: any) {
    this.selectedBarCode = row;
    setTimeout(() => {
      this.barcodeDetails.loadData().then(function () {
        this.detailsVisibility = true;
      }.bind(this));
    }, 0);
  }

  public async onRowUpdated(event: any): Promise<void> {
    let item = new BarCodeInterval();
    item = this.barCodes.find(g => g.id === event.key.id);
    if (item && item.customerId && this.isValid(item.from, item.to)) {
      await this.barCodeIntervalService.updateBarCodeIntervalAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Coduri de bare - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Coduri de bare - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }

        this.refreshDataGrid();
      });
    } else {
      this.notificationService.alert('top', 'center', 'Coduri de bare - Datele nu au fost modificate! Campul de la trebuie sa fie mai mic ca cel de pana la!',
        NotificationTypeEnum.Red, true);

      this.refreshDataGrid();
    }
  }

  validateBarcodePartner(data: any) {
    if (data && data.value) {
      var foundBarcodes = this.barCodes.map(x => x.partnerId).filter(x => x === data.value);
      if (data.data.id && foundBarcodes.length === 1) {
        return true;
      } else if ((data.data.id === undefined || data.data.id === null) && foundBarcodes.length > 0) {
        return false;
      }
    }

    return true;
  }

  validateBarcodeRange(data: any) {
    if (data && data.value) {
      if (data.data.from && data.data.to && this.barCodes.filter(x => x.from > data.data.from && x.to < data.data.to)?.length > 1) {
        return false;
      }
      var foundBarcodes = this.barCodes.map(x => { return { id: x.id, from: x.from, to: x.to }; }).filter(x => {
        return data.value >= x.from && data.value <= x.to;
      });
      if (data.data.id && foundBarcodes.length === 1 && foundBarcodes[0].id !== data.data.id) {
        return false;
      } else if ((data.data.id === undefined || data.data.id === null) && foundBarcodes.length > 0) {
        return false;
      }

      return true;
    }
  }

  isValid(from: number, to: number): boolean {
    return from < to;
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onRowInserting(event: any): Promise<void> {
    let item = new BarCodeInterval();
    item = event.data;
    item.customerId = Number(this.customerId);
    if (item && item.customerId && this.isValid(item.from, item.to)) {
      await this.barCodeIntervalService.createBarCodeIntervalAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Coduri de bare - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Coduri de bare - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }

        this.refreshDataGrid();
      });
    } else {
      this.notificationService.alert('top', 'center', 'Coduri de bare - Datele nu au fost modificate! Campul de la trebuie sa fie mai mic ca cel de pana la!',
        NotificationTypeEnum.Red, true);

      this.refreshDataGrid();
    }
  }

  getDisplayExprCountries(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  getDisplayExpritems(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  gs1Redirect(data) {
    if (data && data.start) {
      var url = 'https://gs1.org/services/verifified-by/' + data.start;
      window.open(url, '_blank').focus();
    }
  }

  addPartner() {
    var url = environment.MRKPrimaware + '/' + Constants.partner
      window.open(url, '_blank');
  }

  rangeNumberExpr(e) {
    if (e && e.end && e.start) {
      return this.countNumbersBetween(e.start, e.end)
    }
  }

  countNumbersBetween(start: string, end: string): number {
    // Convert the start and end strings to integers
    const startNum = Number(start);
    const endNum = Number(end);

    // Calculate the number of integers between the two (inclusive)
    const count = endNum - startNum + Number(1);

    // Return the count as a number (can convert BigInt to number if safe range)
    return Number(count);  // Will work safely if numbers are within the range of JS Number
   }

   onGlnValueChanged(e) {
    if (this.dataGrid && this.dataGrid.instance && e.value) {
      this.dataGrid.instance.filter(['partnerLocationGln', '=', e.value]);
    } else {
      this.dataGrid.instance.clearFilter();
    }
   }

   onPartnerValueChanged(e) {
    if (this.dataGrid && this.dataGrid.instance && e.value) {
      this.dataGrid.instance.filter(['partnerId', '=', e.value]);
    } else {
      this.dataGrid.instance.clearFilter();
    }
   }

   onGroupedByOwnerCodeChanged(e) {
    if (e && e.value == false) {
      this.loaded = true;
      Promise.all([this.getItems(), this.getAllocations()]).then(x => {
        this.loaded = false;
      });
    }
   }

   fromRangeExpr(e) {
    if (e && e.barCodeIntervalId && this.barCodes && this.barCodes.length > 0) {
      var interval = this.barCodes.find(x => x.id == e.barCodeIntervalId);
      if (interval != null) {
        return interval.start;
      } else {
        return "";
      }
    }
   }

   toRangeExpr(e) {
    if (e && e.barCodeIntervalId && this.barCodes && this.barCodes.length > 0) {
      var interval = this.barCodes.find(x => x.id == e.barCodeIntervalId);
      if (interval != null) {
        return interval.end;
      } else {
        return "";
      }
    }
   }

   partnerExpr(e) {
    if (e && e.barCodeIntervalId && this.barCodes && this.barCodes.length > 0) {
      var interval = this.barCodes.find(x => x.id == e.barCodeIntervalId);
      if (interval != null && interval.partnerId && this.partnersDS != null) {
        var partnerName = this.partnersDS.find(x => x.id == interval.partnerId)?.name;
        return partnerName;
      } else {
        return "";
      }
    }
   }

   onNonAvailableCodesChanged(e) {
    if (e && e.value) {
      this.numberList = [];
      this.barCodes.forEach((obj) => {
        const start = parseInt(obj.start, 10);
        const end = parseInt(obj.end, 10);
        var existing = this.barCodeItemAllocations.map(x => x.barCode);
      
        if (!isNaN(start) && !isNaN(end)) {
          // Generate the range of numbers between start and end

          if (end - start > 4000000 || this.numberList.length > 3500000) {
            return;
          }

          for (let i = start; i <= end; i++) {
            if (!existing.includes(i)) {
              this.numberList.push(i);
            }
           
          }
        }
      });
      this.numberList = this.numberList.map((num) => ({ value: num }));
    }
   }
}
