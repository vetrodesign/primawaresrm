import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { Department } from 'app/models/department.model';
import { IPageActions } from 'app/models/ipageactions';
import { Office } from 'app/models/office.model';
import { Partner } from 'app/models/partner.model';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { Post } from 'app/models/post.model';
import { User } from 'app/models/user.model';
import { AuthService } from 'app/services/auth.service';
import { DepartmentsService } from 'app/services/departments.service';
import { NotificationService } from 'app/services/notification.service';
import { PartnerObservationService } from 'app/services/partner-observation.service';
import { PageActionService } from 'app/services/page-action.service';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { TranslateService } from 'app/services/translate';
import { UsersService } from 'app/services/user.service';
import { DxDataGridComponent } from 'devextreme-angular';
import { PostService } from 'app/services/post.service';
import { PartnerObservation } from 'app/models/partner-observation';

@Component({
  selector: 'app-partner-observations',
  templateUrl: './partner-observations.component.html',
  styleUrls: ['./partner-observations.component.css']
})
export class PartnerObservationsComponent implements OnInit {
  partnerObservations: PartnerObservation[] = [];
  partnerContactPersons: PartnerLocationContact[] = [];
  salesAgentUsers: User[] = [];

  isOwner: boolean;
  actions: IPageActions;
  groupedText: string;

  selectedRows: any[];
  selectedRowIndex = -1;

  loaded: boolean = false;

  partnerObservationsInfo: string;
  isPopupVisible: boolean = false;
  displayValue: string = '';
  
  @Input() partner: Partner;

  @ViewChild('partnerObservationsGridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('partnerObservationsDataGrid') dataGrid: DxDataGridComponent;

  constructor(
    private authenticationService: AuthService,
    private translationService: TranslateService,
    private pageActionService: PageActionService,
    private departmentsService: DepartmentsService,
    private partnerObservationService: PartnerObservationService,
    private partnerLocationContactService: PartnerLocationContactService,
    private usersService: UsersService,
    private notificationService: NotificationService,
    private postService: PostService,
  ) {
    this.isOwner = this.authenticationService.isUserOwner();
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();

    this.partnerObservationsInfo = this.translationService.instant("partnerObservationsInfo");

  }

  async loadPartnerObservationDetailsData(clientId: number) {
    if (clientId) {
      if (!this.partner) {
        this.partner = new Partner();
        this.partner.id = clientId;
      } else {
        this.partner.id = clientId;
      }
    }

    await this.loadData();
  }

  async loadData() {
    await this.setPartnerContactPersons().then(() => {
      this.refreshPartnerObservationsDataGrid();
    });
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  private async setObservations() {
    this.loaded = true;

    await this.partnerObservationService.getByPartnerID(this.partner.id).then(async partnerObservations => {
      this.partnerObservations = partnerObservations;

      if (this.partnerObservations && this.partnerObservations.length > 0) {
        await this.setPostsNames();
        
      }
    });

    await this.setSalesAgentUsers();

    this.loaded = false;
  }

  private async setPartnerContactPersons() {
    await this.partnerLocationContactService.getPartnerLocationContactByPartnerID(this.partner.id).then(pcs => {
      this.partnerContactPersons = pcs;
    });
  }

  async setPostsNames() {
    let posts;
    await this.postService.getPostsByCustomerId().then(items => {posts = items;});

    this.partnerObservations.forEach(observation => {
      let post = posts.find(element => element.id === observation.postId)
      if (post) {
        observation.companyPost = post.companyPost;
      } else {
        observation.companyPost = "";
      }
    });
  }
  
  async setSalesAgentUsers() {
    let departments = [];
    let salesPosts: Post[] = [];

    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(items => {
        if (items && items.length > 0) {
          departments = items;
          salesPosts = this.getAllSalesPosts(departments);  
        }
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(items => {
        if (items && items.length > 0) {
          departments = items;
          salesPosts = this.getAllSalesPosts(departments)
        };
      });
    }

    await this.usersService.getUsersAsync().then((users) => {
      if (users && users.length > 0) {
        const salesAgentUsers = users;
        this.salesAgentUsers = salesAgentUsers;

        const saleAgentUsersOfficeSuperiorOrDepartmentSuperiorOrGeneralManager = salesAgentUsers.filter(sau => salesPosts.some(post => (post.id === sau.postId) && (post.isOfficeSuperior || post.isDepartmentSuperior || post.isGeneralManager)));

        const currentUserId: number = Number(this.authenticationService.getUserId());
        const currentUserIsSalesAgent: boolean = salesAgentUsers.some(u => u.id === currentUserId);
        const currentUserIsOfficeSuperiorOrDepartmentSuperiorOrGeneralManager: boolean = saleAgentUsersOfficeSuperiorOrDepartmentSuperiorOrGeneralManager.some(u => u.id === currentUserId);

        this.actions.CanAdd = currentUserIsSalesAgent || this.authenticationService.isUserAdmin();
        this.actions.CanDelete = currentUserIsOfficeSuperiorOrDepartmentSuperiorOrGeneralManager;
      }
    });
  }

  private getAllSalesPosts(departments: Department[]): Post[] {
    if (!departments || departments.length == 0) 
      return [];

    const salesDepartments: Department[] = departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Sales); 
    const salesOffices: Office[] = salesDepartments.reduce((acc: Office[], dept: Department) => acc.concat(dept.offices), []);
    const salesPosts: Post[] = salesOffices.reduce((acc: Post[], office: Office) => acc.concat(office.posts), []);
    const activeSalesPosts = salesPosts.filter((post) => post.isActive === true);

    return activeSalesPosts;
  }


  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.partner).then(result => {
      this.actions = result;
      this.actions.CanAdd = false;
      this.actions.CanDelete = false;
    });
  }

  canExport() {
    return this.actions.CanExport;
  }

  canDelete(rowSiteId) {
    if (rowSiteId && rowSiteId !== Number(this.authenticationService.getUserSiteId())) {
      return false;
    }
    return this.actions.CanDelete;
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  addPartnerObservation() {
    this.dataGrid.instance.addRow();
  }

  refreshPartnerObservationsDataGrid() {
    if (this.partner && this.partner.id) {
      this.loaded = true;
      this.setObservations().then(() => {
        this.dataGrid.instance.refresh();
        this.loaded = false;
      });
    }
  }

  public deleteRecords(data: any) {
    this.loaded = true;
    this.partnerObservationService.deleteAsync(this.selectedRows).then(r => {
      if (r) {
        this.notificationService.alert('top', 'center', 'Observatii - Datele au fost inactivate cu succes!',
        NotificationTypeEnum.Green, true);
      } else {
        this.notificationService.alert('top', 'center', 'Observatii - Datele nu au fost inactivate! A aparut o eroare!',
        NotificationTypeEnum.Red, true);
      }

      this.refreshPartnerObservationsDataGrid();
      this.loaded = false;
    });
  }

  public async onRowInserting(event: any): Promise<void> {
    this.loaded = true;

    let partnerObservation = new PartnerObservation();
    partnerObservation = event.data;
    partnerObservation.isActive = true;
    partnerObservation.customerId = Number(this.authenticationService.getUserCustomerId());
    partnerObservation.partnerId = this.partner.id;
    partnerObservation.salesAgentUserId = this.authenticationService.getUserId();
    partnerObservation.postId = Number(this.authenticationService.getUserPostId());

    if (partnerObservation) {
      await this.partnerObservationService.createAsync(partnerObservation).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Observatii Partener - Datele au fost inserate cu succes!',
          NotificationTypeEnum.Green, true);
          this.refreshPartnerObservationsDataGrid();
          this.loaded = false;
        } else {
          this.notificationService.alert('top', 'center', 'Observatii Partener - Datele nu au fost inserate! A aparut o eroare!',
          NotificationTypeEnum.Red, true);
          this.refreshPartnerObservationsDataGrid();
          this.loaded = false;
        }
      })
    } else {
      this.loaded = false;
    }
  }

  public async onRowUpdating(event: any): Promise<void> {
    this.loaded = true;

    const itemUpdating = { ...event.oldData, ...event.newData };

    if (itemUpdating) {
      await this.partnerObservationService.updateAsync(itemUpdating).then (r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Observatii Partener - Datele au fost updatate cu succes!',
          NotificationTypeEnum.Green, true);
          this.refreshPartnerObservationsDataGrid();
          this.loaded = false;
        } else {
          this.notificationService.alert('top', 'center', 'Observatii Partener - Datele nu au fost updatate! A aparut o eroare!',
          NotificationTypeEnum.Red, true);
          this.refreshPartnerObservationsDataGrid();
          this.loaded = false;
        }
      });
    } else {
      this.loaded = false;
    }
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onInitNewRow(data: any) {
    if (data && data.data) {
      data.data.isActive = true;
    }
  }
  public deletePartnerObservationRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  getDisplayExprPartnerContactPersons(item) {
    if (!item) {
      return '';
    }
    
    const firstName = item.firstName;
    const lastName = (item.lastName === undefined || item.lastName === '') ? '' : ' - ' + item.lastName;

    return firstName + lastName;
  }

  getDisplayExprSalesAgentUsers(item) {
    if (!item) {
      return '';
    }
    
    const firstName = item.firstName;
    const lastName = (item.lastName === undefined || item.lastName === '') ? '' : ' - ' + item.lastName;

    return firstName + lastName;
  }

  openDescriptionPopup(data) {
    this.isPopupVisible = true;
    this.displayValue = data.data.description;
  }

  onRowPrepared(event) {
    if (event.rowType == "data" && event.data.description == "Nu are Site Online") {
      event.rowElement.style.color = 'green';
    }
  }
}
