import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { Partner } from 'app/models/partner.model';
import { PartnerLocationXSalePriceList } from 'app/models/partnerLocationXSalePriceList.model';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { PartnerLocationXSecondaryActivityAllocation } from 'app/models/partnerlocationxsecondaryactivityallocation.model';
import { SalePriceListFilterVM } from 'app/models/salepricelistfilter.model';
import { CurrencyRateService } from 'app/services/currency-rate.service';
import { NotificationService } from 'app/services/notification.service';
import { PartnerLocationSalePriceListService } from 'app/services/partner-location-sale-price-list.service';
import { PartnerLocationSecondaryActivityAllocationService } from 'app/services/partner-location-secondary-activity-allocation.service';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { PartnerService } from 'app/services/partner.service';
import { SalePriceListService } from 'app/services/sale-price-list.service';
import { SupplierPriceListPartnerLocationService } from 'app/services/supplier-price-list-partner-location.service';
import { DxValidatorComponent } from 'devextreme-angular';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-partner-sales-price-list',
  templateUrl: './partner-sales-price-list.component.html',
  styleUrls: ['./partner-sales-price-list.component.css']
})
export class PartnerSalesPriceListComponent implements OnInit {
  @Input() partner: Partner;
  @Input() salePriceListDS: any;
  @Input() supplierSalePriceListDS: any;
  @Input() currencies: any;
  @Input() selectedPartnerLocation: PartnerLocation;
  @Input() partnerLocations: any;
  @Input() partnerActivity: any;
  @Input() partnerActivityAllocation: any;
  @Input() initialPartnerLocation: any;
  @ViewChild('salesPriceListValidationGroup') salesPriceListValidationGroup: DxValidatorComponent;

  loaded: boolean;
  selectedSalesPriceListPartnerLocation: number;
  salesPriceList: any;
  activities: any;
  selectedPartnerActivityAllocationId: number;
  salesPriceListByLocation: { [key: string]: number[] } = {};
  allSupplierSalesPriceList: any[] = [];
  activitiesByLocation: { [key: string]: number[] } = {};
  isOnSavePartnerLocation: boolean = true;
  implicitPostId: number;
  isOnSave: boolean = true;
  allLocationNames: string;
  implicitLocation: any;
  sortedSalePriceListDS: any;
  sortedPartnerActivityAllocation: any;
  sortedPartnerLocations: any;
  sortedSupplierSalePriceListDS: any;

  constructor(private readonly partnerLocationSalePriceListService: PartnerLocationSalePriceListService,
    private readonly notificationService: NotificationService,
    private readonly currencyService: CurrencyRateService,
    private readonly partnerLocationService: PartnerLocationService,
    private readonly partnerLocationSecondaryActivityAllocationService: PartnerLocationSecondaryActivityAllocationService,
    private readonly partnerService: PartnerService,
    private readonly salePriceListService: SalePriceListService
  ) { 
    this.getDisplayExprSalePriceLists = this.getDisplayExprSalePriceLists.bind(this);
    this.getDisplayExprPartnerActivity = this.getDisplayExprPartnerActivity.bind(this);
    this.getDisplayExprSupplierSalePriceLists = this.getDisplayExprSupplierSalePriceLists.bind(this);
  }

  async ngOnInit() {
    await this.getActivitiesAndSalePriceList();
  }

  ngAfterViewInit() {
    this.implicitLocation = this.partnerLocations.find(f => f.isImplicit === true) || this.partnerLocations.find(f => f.isActive === true);
  }

  async getActivitiesAndSalePriceList() {
    this.sortedPartnerLocations = this.partnerLocations.sort((a, b) => {
      if (a.isImplicit) return -1;
      if (b.isImplicit) return 1;
      return 0;
    });

    const promises = [];
  
    //this.loaded = true; -- temporary hidden because it covers some UI

    for (const location of this.sortedPartnerLocations) {
      const locationId = location.id;

      const supplierSalePriceListPromise = this.salePriceListService
      .getAllByPartnerLocationAsync(locationId)
      .then(items => {
        if (items) {
          const supplierSalesPriceListIds = items.map(m => m.supplierPriceListId);
          this.allSupplierSalesPriceList.push(...supplierSalesPriceListIds);

          this.partnerLocations.map(m => {
            if (m.id === locationId) {
              m.existingSupplierSalesPriceListIds = items.map(m => m.supplierPriceListId);
            }

            return m;
          });
        }
      });

      const salePriceListPromise = this.partnerLocationSalePriceListService
        .getAllByPartnerLocationAsync(locationId)
        .then(items => {
          if (items) {
            const salesPriceListIds = items.map(m => m.salePriceListId);
            this.salesPriceListByLocation[locationId] = salesPriceListIds;

            this.partnerLocations.map(m => {
              if (m.id === locationId) {
                m.existingSalesPriceListIds = items.map(m => m.salePriceListId);
              }

              return m;
            });
          }
        });

        const activityPromise = this.partnerLocationSecondaryActivityAllocationService
        .getAllByPartnerLocationIdAsync(locationId)
        .then(items => {
          if (items) {
            const activitiesIds = items.map(m => m.partnerActivityAllocationId);
            this.activitiesByLocation[locationId] = activitiesIds;

            this.partnerLocations.map(m => {
              if (m.id === locationId) {
                m.existingPartnerSecondaryActivityAllocationIds = items.map(m => m.partnerActivityAllocationId);
              }

              return m;
            });
          }
        });

      promises.push(supplierSalePriceListPromise, salePriceListPromise, activityPromise);
    }

    await Promise.all(promises).then(() => {
      this.loaded = false;
    })
  }

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    this.sortedSalePriceListDS = [...this.salePriceListDS];
    this.sortedSupplierSalePriceListDS = [...this.supplierSalePriceListDS];
    if (this.partner.id) {
      this.isOnSave = false;
    }

    if (changes.partnerLocations && this.partnerLocations && this.partnerLocations.length > 0) {

      this.allLocationNames = this.partnerLocations.map(m => m.name).join(", ");
      this.implicitLocation = this.partnerLocations.find(f => f.isImplicit === true) || this.partnerLocations.find(f => f.isActive === true);

      this.sortedPartnerActivityAllocation = [ ...this.partnerActivityAllocation ];

      this.selectedSalesPriceListPartnerLocation = this.partnerLocations[0].id;
      this.selectedPartnerActivityAllocationId = this.partnerLocations[0].partnerActivityAllocationId;

      await this.getActivitiesAndSalePriceList();
    }
  }

  getDisplayExprPartnerActivity(value: any) {
    if (value && this.partnerActivity) {
      const item = this.partnerActivity.find(x => x.id === value.partnerActivityId);
      if (item) {
        return value.saleZoneCode + ' - ' + item.name + ' - ' + value.partnerActivityClassification;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  getDisplayExprSalePriceLists(item) {
    if (!item) {
      return '';
    }
    let c = (this.currencies && this.currencies.length > 0) ? this.currencies.find(i => i.id === item.currencyId) : null;
    return item.code + ' - ' + item.name  + ' - ' + c?.name;
  }

  getDisplayExprSupplierSalePriceLists(item) {
    if (!item) {
      return '';
    }
    let c = (this.currencies && this.currencies.length > 0) ? this.currencies.find(i => i.id === item.currencyId) : null;
    return item.name  + ' - ' + c?.name;
  }

  async getBaseCurrency(): Promise<any> {
    await this.currencyService.getBaseCurrency().then(x => {
      this.currencies = x;
    });
  }

  async getSalesPriceList() { 
    if (this.partner && this.partner.id) {
      if (this.partner.isPrincipalActivityUniqueForAllLocations) {
        await this.partnerLocationSalePriceListService.getAllByPartnerAsync(this.partner.id).then(items => {
          if (items) {
            var arr = items.map(m => m.salePriceListId);
            this.salesPriceList = Array.from(new Set(arr));
          }
        })
      } else {     
        await this.partnerLocationSalePriceListService.getAllByPartnerLocationAsync(this.selectedSalesPriceListPartnerLocation).then(items => {
          if (items) {         
            var arr = items.map(m => m.salePriceListId);
            this.salesPriceList = Array.from(new Set(arr));
          }
        })
      }
    }
  }

  async getActivities() {
    if (this.partner && this.partner.id) {
      if (this.partner.isPrincipalActivityUniqueForAllLocations) {
        await this.partnerLocationSecondaryActivityAllocationService.getAllByPartnerLocationIdAsync(this.partner.id).then(items => {
          if (items) {
            var arr = items.map(m => m.partnerActivityAllocationId);
            this.activities = Array.from(new Set(arr)).push(this.selectedPartnerActivityAllocationId);
          }
        })
      } else {
        await this.partnerLocationSecondaryActivityAllocationService.getAllByPartnerLocationIdAsync(this.selectedSalesPriceListPartnerLocation).then(items => {
          if (items) {
            
            var arr = items.map(m => m.partnerActivityAllocationId);
            this.activities = [ this.selectedPartnerActivityAllocationId, ...arr ];
          }
        })
      }
    }
  }

  async onSalesPriceListSaveClick() {
    this.loaded = true;

    const processLocation = async (item) => {
      let selectedPl = this.partnerLocations.find(f => f.id === item.id);

      if (this.partner.isPrincipalActivityUniqueForAllLocations) {
        item.partnerActivityAllocationId = this.implicitLocation.partnerActivityAllocationId;
      }
      const r = await this.partnerLocationService.updatePartnerLocationPartnerActivityAllocationAsync(item);
      if (r) {
        item.rowVersion = r.rowVersion;
       
        // Create SalePriceListXPartnerLocation
        const existingSalePriceListIds = selectedPl.existingSalesPriceListIds;
        let deleteSalePriceListPartnerLocationAllocation: PartnerLocationXSalePriceList[] = [];
        existingSalePriceListIds?.filter(x => !this.salesPriceListByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].includes(x)).forEach(id => {
          const c = new PartnerLocationXSalePriceList();
          c.partnerLocationId = item.id;
          c.salePriceListId = id;
          deleteSalePriceListPartnerLocationAllocation.push(c);
        });

        let createSalePriceListIds: PartnerLocationXSalePriceList[] = [];
        this.salesPriceListByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].filter(x => !existingSalePriceListIds.includes(x)).forEach(id => {
          const c = new PartnerLocationXSalePriceList();
          c.partnerLocationId = Number(item.id);
          c.salePriceListId = id;
          createSalePriceListIds.push(c);
        });

        if (createSalePriceListIds.length > 0 || deleteSalePriceListPartnerLocationAllocation.length > 0) {
          await this.partnerLocationSalePriceListService.updateMultipleAsync(Number(item.id), createSalePriceListIds.map(m => m.salePriceListId), deleteSalePriceListPartnerLocationAllocation.map(m => m.salePriceListId));
        }

        // Create PartnerSecondaryActivityAllocation
        const existingSecondaryActivityAllocationIds = selectedPl.existingPartnerSecondaryActivityAllocationIds;
        let deleteSecondaryActivityAllocation: PartnerLocationXSecondaryActivityAllocation[] = [];
        existingSecondaryActivityAllocationIds?.filter(x => !this.activitiesByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].includes(x)).forEach(id => {
          const c = new PartnerLocationXSecondaryActivityAllocation();
          c.partnerLocationId = item.id;
          c.partnerActivityAllocationId = id;
          deleteSecondaryActivityAllocation.push(c);
        });
        if (deleteSecondaryActivityAllocation.length > 0) {
          await this.partnerLocationSecondaryActivityAllocationService.deleteMultipleAsync(deleteSecondaryActivityAllocation);
        }
        let createSecondaryActivityAllocationIds: PartnerLocationXSecondaryActivityAllocation[] = [];
        this.activitiesByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].filter(x => !existingSecondaryActivityAllocationIds.includes(x)).forEach(id => {
          const c = new PartnerLocationXSecondaryActivityAllocation();
          c.partnerLocationId = Number(item.id);
          c.partnerActivityAllocationId = id;
          createSecondaryActivityAllocationIds.push(c);
        });
        if (createSecondaryActivityAllocationIds.length > 0) {
          await this.partnerLocationSecondaryActivityAllocationService.createMultipleAsync(createSecondaryActivityAllocationIds).then(() => {});
        }
      }
    };

    for (const location of this.partnerLocations) {
      await processLocation(location).then(() => {})
    }

    await this.getActivitiesAndSalePriceList().then(() => {
      this.notificationService.alert('top', 'center', 'Liste de pret - Datele au fost modificate cu succes!', NotificationTypeEnum.Green, true);
      this.loaded = false;
    })
  }

  partnerLocationsDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.name;
  }

  async onSelectedLocationValueChanged(event: any) {
    if (event && event.value && this.salesPriceList) {
      let selectedPl = this.partnerLocations.find(f => f.id === event.value);
      if (selectedPl) {
        await this.getSalesPriceList();
      }
    }
  }

  isOnSavePartnerLocationOutputChange(event: any) {
    this.isOnSavePartnerLocation = event;
  }

  partnerActivityChange(e: any) {
    if (this.isOnSave && e && e.value) {
      let pa = this.partnerActivityAllocation.find(y => y.id === e.value);
      if (pa) {
       
        this.sortedSalePriceListDS = this.salePriceListDS.filter(x => x.salePriceListXProductConventionList && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId));
        if (this.initialPartnerLocation.countryId) {
          let currency = 1;
          currency = this.initialPartnerLocation.countryId == 33 ? 19 : currency;
          currency = this.initialPartnerLocation.countryId == 99 ? 61 : currency;
          this.initialPartnerLocation.salePriceListIds = [];
          this.initialPartnerLocation.salePriceListIds = this.salePriceListDS.filter(x => x.salePriceListXProductConventionList && x.currencyId == currency && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId) && x.isImplicit && x.status).map(y => y.id);
          }
      }
    }
  }

  async selectedPartnerActivityChanged(e: any) {
    if (e && e.value && this.partnerActivityAllocation && this.partnerActivityAllocation.length) {
      let pa = this.partnerActivityAllocation.find(y => y.id === e.value);
      if (pa) {
        if (this.isOnSavePartnerLocation) {
          this.sortedSalePriceListDS = this.salePriceListDS.filter(x => x.salePriceListXProductConventionList && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId));
          if (this.implicitLocation.countryId) {
            let currency = 1;
            currency = this.implicitLocation.countryId == 33 ? 19 : currency;
            currency = this.implicitLocation.countryId == 99 ? 61 : currency;
            this.implicitLocation.salePriceListIds = [];
            this.implicitLocation.salePriceListIds = this.salePriceListDS.filter(x => x.salePriceListXProductConventionList && x.currencyId == currency && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId) && x.isImplicit && x.status).map(y => y.id);
          }
         
        } else {
          let salePriceListFilter = new SalePriceListFilterVM();
          salePriceListFilter.productConventionIds = [pa.productConventionId];
          if (this.implicitLocation && this.implicitLocation.baseCurrencyId && this.implicitLocation.alternativeCurrencyId) {
            salePriceListFilter.currencyIds = [this.implicitLocation.baseCurrencyId, this.implicitLocation.alternativeCurrencyId];
          } else if (this.initialPartnerLocation && this.initialPartnerLocation.baseCurrencyId && this.initialPartnerLocation.alternativeCurrencyId) {
            salePriceListFilter.currencyIds = [this.initialPartnerLocation.baseCurrencyId, this.initialPartnerLocation.alternativeCurrencyId];
          } else {
            salePriceListFilter.currencyIds = [1];
          }
  
          await this.salePriceListService.getSalePriceListByFilter(salePriceListFilter).then(items => {
            if (items && items.length > 0) {
              this.sortedSalePriceListDS = items;
            } 
          })
        }
      }
    }
  }

  onInitialLocationSalePriceListOpen(event: any) {
    let arr = this.salePriceListDS.slice().sort((a, b) => {
      const aSelected = this.initialPartnerLocation.salePriceListIds && this.initialPartnerLocation.salePriceListIds.includes(a.id);
      const bSelected = this.initialPartnerLocation.salePriceListIds && this.initialPartnerLocation.salePriceListIds.includes(b.id);
      if (aSelected && !bSelected) return -1;
      if (!aSelected && bSelected) return 1;
      return 0;
    });
  }

  onInitialLocationSupplierSalePriceListOpen(event: any) {
    let arr = this.supplierSalePriceListDS.slice().sort((a, b) => {
      const aSelected = this.initialPartnerLocation.supplierSalePriceListIds && this.initialPartnerLocation.supplierSalePriceListIds.includes(a.id);
      const bSelected = this.initialPartnerLocation.supplierSalePriceListIds && this.initialPartnerLocation.supplierSalePriceListIds.includes(b.id);
      if (aSelected && !bSelected) return -1;
      if (!aSelected && bSelected) return 1;
      return 0;
    });
  }

  onSupplierPriceListClick() {
    if (this.allSupplierSalesPriceList && this.allSupplierSalesPriceList.length > 0) {
      let listId = this.allSupplierSalesPriceList[0];

      var url = environment.MRKPrimaware + '/' + Constants.supplierPriceList + '?id=' + listId;
      window.open(url, '_blank');  
    }
  }
}
