import { Component, Input, OnChanges, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { IPageActions } from 'app/models/ipageactions';
import { NotificationService } from 'app/services/notification.service';
import { TranslateService } from 'app/services/translate';
import { ItemService } from 'app/services/item.service';
import * as _ from 'lodash';
import { Item } from 'app/models/item.model';
import { ItemXRelated, RelatedItemType } from 'app/models/itemxrelated.model';
import { ItemRelatedService } from 'app/services/item-related.service';
import { AuthService } from 'app/services/auth.service';
import { Constants } from 'app/constants';
import { PageActionService } from 'app/services/page-action.service';
import { CaenCodeSpecialization } from 'app/models/caencodespecialization.model';
import { CaenCodeSpecializationService } from 'app/services/caen-code-specialization.service';
import { DxDataGridComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { ItemCaenCodeSpecializationService } from 'app/services/item-caen-code-specialization.service';
import { ItemGroupService } from 'app/services/item-group.service';

@Component({
  selector: 'app-item-related',
  templateUrl: './item-related.component.html',
  styleUrls: ['./item-related.component.css']
})
export class ItemRelatedComponent implements OnInit, OnChanges {
  @Input() item: Item;

  @ViewChildren(DxDataGridComponent) itemRelatedDataGrids: QueryList<DxDataGridComponent>;
  @ViewChildren(GridToolbarComponent) itemRelatedGridToolbars: QueryList<GridToolbarComponent>;

  groupedText: string;
  actions: IPageActions;
  itemsRelated: ItemXRelated[] = [];
  selectedRows: any[];
  loaded: boolean;
  isOwner: boolean = true;
  selectedRowIndex = -1;
  selectedRelatedItem: ItemXRelated;
  items: any;
  relatedItemTypes: any;
  showAdd: boolean;
  caenCodeSpecializations: CaenCodeSpecialization[] = [];
  currentTooltip: any = null;
  itemCaenCodeSpecializations: any;
  isCodeFour: boolean = false;
  itemGroups: any;
  selectedItems: any;
  selectedItemGroups: any;

  constructor(
    private notificationService: NotificationService,
    private translationService: TranslateService,
    private itemXReleatedService: ItemRelatedService,
    private itemsService: ItemService,
    private authService: AuthService,
    private pageActionService: PageActionService,
    private caenCodeSpecializationService: CaenCodeSpecializationService,
    private itemCaenCodeSpecializationService: ItemCaenCodeSpecializationService,
    private itemGroupService: ItemGroupService) {
    this.setActions();
    this.selectedRelatedItem = new ItemXRelated();
    this.calculateSpecCell = this.calculateSpecCell.bind(this);
    this.relatedItemTypes = [];
    for (let n in RelatedItemType) {
      if (typeof RelatedItemType[n] === 'number') {
        const id = <any>RelatedItemType[n];

        let icon = '';
        switch(id) {
          case 1:
            icon = 'equivalence-icon.png';
            break;
          case 2:
            icon = 'complementarity-icon.png';
            break;
          case 3:
            icon = 'superiority-icon.png';
            break;

          case 4:
            icon = 'inferiority-icon.png';
            break;

          case 5:
            icon = 'concurrency-icon.png';
            break;

          default:
            break;
        }

        this.relatedItemTypes.push({
          id: <any>RelatedItemType[n],
          name: this.translationService.instant(n),
          description: this.translationService.instant(n + 'Description'),
          dataSource: [],
          isVisible: false,
          icon: icon
        });
      }
    }

    this.authService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });

    this.groupedText = this.translationService.instant('groupedText');
    this.getItems();
    this.getItemsGroups();
    this.itemsRelated = [];
  }

  showTooltip(type: any): void {
    this.currentTooltip = type;
  }

  hideTooltip(): void {
    this.currentTooltip = null;
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.item).then(result => {
      this.actions = result;
    });
  }

  ngOnInit(): void {
  }

  getDataGridByIndex(index: number): DxDataGridComponent | undefined {
    return this.itemRelatedDataGrids.toArray()[index];
  }

  getGridToolbarByIndex(index: number): GridToolbarComponent | undefined {
    return this.itemRelatedGridToolbars.toArray()[index];
  }

  openDetails(type) {
     // temporary disable the competitor item related type
    if (type && type.id === RelatedItemType.Competitor) {
      return;
    }

    this.selectedRelatedItem = new ItemXRelated();
    this.relatedItemTypes.forEach(x => { x.isVisible = false; });
    type.isVisible = true;
  }

  async saveData(type) {
    if ((!this.selectedItems || this.selectedItems.length === 0) && (!this.selectedItemGroups || this.selectedItemGroups.length === 0)) {
      this.notificationService.alert('top', 'center', 'Produse sugerate - Selectati cel putin 1 produs simplu sau 1 produs grupat!', NotificationTypeEnum.Red, true);
      return;
    }

    let existingRelatedItemsByType = this.relatedItemTypes.find(f => f.id == type.id);
    let existingRelatedItems = existingRelatedItemsByType?.dataSource?.map(m => m.relatedItemId);

    let simpleItemsFromGroups = this.items.store.filter(f => this.selectedItemGroups?.includes(f.itemGroupId) && !existingRelatedItems.includes(f.id));
    let simpleItems = this.items.store.filter(f => this.selectedItems?.includes(f.id) && !existingRelatedItems.includes(f.id));

    let combinedItems = [...simpleItemsFromGroups, ...simpleItems];
    let uniqueItems = Array.from(new Map(combinedItems.map(item => [item.id, item])).values());

    let newRelatedItems = [];
    uniqueItems?.forEach(m => {
      let item = new ItemXRelated();
      item.relatedItemId = m.id;
      item.description = this.selectedRelatedItem.description;
      item.itemId = this.item.id;
      item.itemType = type.id;
      item.caenCodeSpecializationId = this.selectedRelatedItem.caenCodeSpecializationId;

      newRelatedItems.push(item);
    });

    this.loaded = true;
    await this.itemXReleatedService.createMultipleAsync(newRelatedItems).then(r => {
      this.notificationService.alert('top', 'center', 'Produse sugerate - Datele au fost inserate cu succes!',
        NotificationTypeEnum.Green, true);

        this.selectedItems = [];
        this.selectedItemGroups = [];
        this.loaded = false;
        this.refreshItemsRelated();    
    });

    this.relatedItemTypes.forEach(x => { x.isVisible = false; });
  }

  async loadData() {
    await this.getCaenCodeSpecializations().then(async () => {
      await this.getItemsRelated();
      await this.getItemCaenCodeSpecializations();
    })
  }

  async getItemCaenCodeSpecializations() {
    await this.itemCaenCodeSpecializationService.getAllByItemId(this.item.id).then(items => {
      if (items && items.length > 0) {
        this.itemCaenCodeSpecializations = items;
      }
    });
  }

  async getCaenCodeSpecializations() {
    this.caenCodeSpecializationService.getByItemIdAsync(this.item.id).then(caenCodeSpecializations => {
      if (caenCodeSpecializations && caenCodeSpecializations.length > 0) {
        this.caenCodeSpecializations = caenCodeSpecializations;
      }
    });
  }

  getItemCodeAndName(itemId) {
    if (itemId && this.items && this.items.store && this.items.store.length > 0) {
      let i = this.items.store.find(x => x.id == itemId);
      return i ? i.code + ' - ' + i.nameRO : '';
    }
  }

  getSpecCodeAndName(specId) {
    if (specId && this.caenCodeSpecializations && this.caenCodeSpecializations.length > 0) {
      let s = this.caenCodeSpecializations.find(x => x.id === specId);
      return s ? s.code + ' - ' + s.description : '';
    }
  }

  async getItems(): Promise<any> {
    if (this.isOwner) {
      await this.itemsService.getAllItemsAsync().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    } else {
      await this.itemsService.getItemssAsyncByID().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    }
  }

  async getItemsGroups(): Promise<any> {
    await this.itemGroupService.getItemGroupsAsyncByID().then(items => {
      this.itemGroups = {
        paginate: true,
        pageSize: 15,
        store: items
      };
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.partner) {
      this.getItemsRelated();
    }
  }

  async getItemsRelated() {
    if (this.item && this.item.id) {
      this.itemXReleatedService.getItemXRelatedByItemIdAsync(this.item.id).then(async items => {
        if (items) {
          this.itemsRelated = items;
          this.relatedItemTypes.forEach(i => {
            i.dataSource = this.itemsRelated.filter(x => x.itemType == i.id);
          });

          const promises = this.itemsRelated.filter(f => f.itemType === 2).map(async item => {
            const relatedItemSpecs = await this.itemCaenCodeSpecializationService.getAllByItemId(item.relatedItemId);
            item.relatedItemSpecIds = relatedItemSpecs.map(m => m.caenCodeSpecializationId);
  
            this.relatedItemTypes.forEach(i => {
              i.dataSource = this.itemsRelated
                .filter(x => x.itemType == i.id)
                .map(x => {
                  return {
                    ...x,
                    relatedItemSpecIds: x.relatedItemSpecIds || [] 
                  };
                });
            });
          });

          await Promise.all(promises);
        } else {
          this.itemsRelated = [];
        }
      });
     }
  }

  refreshItemsRelated() {
    this.getItemsRelated();
  }

  add() {

  }


  displayCodeExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  displayCaenCodeSpecExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.description;
  }

  public deleteRow(data: any) {
    this.loaded = true;
    let ids = [];
    ids.push(data.id);
    this.itemXReleatedService.deleteItemXRelatedAsync(ids).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Produse sugerate - Datele au fost sterse cu succes!', NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Produse sugerate - Datele nu fost sterse! A aparut o eroare!', NotificationTypeEnum.Red, true)
      }
      this.loaded = false;
      this.getItemsRelated();
    });
  }

  getDisplayExprItems(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  calculateSpecCell(rowData) {
    if (this.caenCodeSpecializations && rowData) {      
      const baseItemSpecIds = this.itemCaenCodeSpecializations.map(m => m.caenCodeSpecializationId);
      const relatedItemSpecIds = rowData.relatedItemSpecIds;
      const commonSpecIds = new Set(baseItemSpecIds.filter(value => relatedItemSpecIds?.includes(value)));

      return commonSpecIds ? this.caenCodeSpecializations.reduce((acc, caenCodeSpec) => {
          if (commonSpecIds.has(caenCodeSpec.id)) {
              acc.push(caenCodeSpec.code + " - " + caenCodeSpec.description);
          }
          return acc;
      }, []).join(", ") : [];
    }
  }
}
