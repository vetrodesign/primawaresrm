import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemRelatedComponent } from './item-related.component';

describe('ItemRelatedComponent', () => {
  let component: ItemRelatedComponent;
  let fixture: ComponentFixture<ItemRelatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemRelatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemRelatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
