import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { CaenCodeSpecializationXPartnerLocation } from 'app/models/caencodespecializationxpartner.model';
import { Partner } from 'app/models/partner.model';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { PartnerLocationXSecondaryActivityAllocation } from 'app/models/partnerlocationxsecondaryactivityallocation.model';
import { CaenCodeSpecializationPartnerActivityAllocationService } from 'app/services/caen-code-specialization-partner-activity-allocation.service';
import { CaenCodeSpecializationPartnerLocationService } from 'app/services/caen-code-specialization-partner-location.service';
import { NotificationService } from 'app/services/notification.service';
import { PartnerLocationSecondaryActivityAllocationService } from 'app/services/partner-location-secondary-activity-allocation.service';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { PartnerService } from 'app/services/partner.service';
import { DxValidatorComponent } from 'devextreme-angular';

@Component({
  selector: 'app-partner-specializations',
  templateUrl: './partner-specializations.component.html',
  styleUrls: ['./partner-specializations.component.css']
})
export class PartnerSpecializationsComponent implements OnInit {
  @Input() partner: Partner;
  @Input() caenSpecializationsDS: any;
  @Input() selectedPartnerLocation: PartnerLocation;
  @Input() partnerLocations: any;
  @Input() partnerActivity: any;
  @Input() partnerActivityAllocation: any;
  @Input() initialPartnerLocation: any;

  @ViewChild('specializationsValidationGroup') specializationsValidationGroup: DxValidatorComponent;

  loaded: boolean;
  selectedSpecsPartnerLocation: number;
  specializations: any;
  selectedPartnerActivityAllocationId: number;
  activities: any;
  specializationsByLocation: { [key: string]: number[] } = {};
  activitiesByLocation: { [key: string]: number[] } = {};
  sortedCaenSpecializationsDS: any;
  sortedPartnerActivityAllocation: any;
  isOnSavePartnerLocation: boolean = true;
  isOnSave: boolean = true;
  implicitPostId: number;
  allLocationNames: string;
  implicitLocation: any;
  sortedPartnerLocations: any;

  constructor(private readonly caenCodeSpecializationPartnerLocationService: CaenCodeSpecializationPartnerLocationService,
    private readonly notificationService: NotificationService,
    private readonly partnerLocationService: PartnerLocationService,
    private readonly partnerLocationSecondaryActivityAllocationService: PartnerLocationSecondaryActivityAllocationService,
    private readonly caenCodeSpecializationPartnerActivityAllocationService: CaenCodeSpecializationPartnerActivityAllocationService,
    private readonly partnerService: PartnerService
  ) { 
    this.getDisplayExprPartnerActivity = this.getDisplayExprPartnerActivity.bind(this);

  }

  async ngOnInit() {
    this.loaded = true;
    await Promise.all([this.getActivitiesAndSpecializations()]).then(() => {
      this.loaded = false;
    })
  }

async getActivitiesAndSpecializations() {
  this.sortedPartnerLocations = this.partnerLocations.sort((a, b) => {
      if (a.isImplicit) return -1;
      if (b.isImplicit) return 1;
      return 0;
  });

  const promises = [];

  this.loaded = true;
  for (const location of this.sortedPartnerLocations) {
      const locationId = location.id;

      const caenPromise = this.caenCodeSpecializationPartnerLocationService
          .getAllCaenCodeSpecializationPartnerLocationByPartnerLocationAsync(locationId)
          .then(items => {
              if (items) {
                  const specializationIds = items.map(m => m.caenCodeSpecializationId);
                  this.specializationsByLocation[locationId] = specializationIds;

                  this.partnerLocations.map(m => {
                      if (m.id === locationId) {
                          m.existingCaenCodeIds = items.map(m => m.caenCodeSpecializationId);
                      }

                      return m;
                  });
              }
          });

      const activityPromise = this.partnerLocationSecondaryActivityAllocationService
          .getAllByPartnerLocationIdAsync(locationId)
          .then(items => {
              if (items) {
                  const activitiesIds = items.map(m => m.partnerActivityAllocationId);
                  this.activitiesByLocation[locationId] = activitiesIds;

                  this.partnerLocations.map(m => {
                      if (m.id === locationId) {
                          m.existingPartnerSecondaryActivityAllocationIds = items.map(m => m.partnerActivityAllocationId);
                      }

                      return m;
                  });
              }
          });

      promises.push(caenPromise, activityPromise);
  }

  await Promise.all(promises);
  this.loaded = false;
}

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    this.sortedCaenSpecializationsDS = [...this.caenSpecializationsDS.store];
    
    if (this.partner.id) {
      this.isOnSave = false;
    }

    if (changes.partnerLocations && this.partnerLocations && this.partnerLocations.length > 0) {

      this.allLocationNames = this.partnerLocations.map(m => m.name).join(", ");
      this.implicitLocation = this.partnerLocations.find(f => f.isImplicit === true) || this.partnerLocations.find(f => f.isActive === true);

      this.sortedPartnerActivityAllocation = [ ...this.partnerActivityAllocation ];

      this.selectedSpecsPartnerLocation = this.partnerLocations[0].id;
      this.selectedPartnerActivityAllocationId = this.partnerLocations[0].partnerActivityAllocationId;
    }
  }

  getSpecsDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.description;
  }

  getDisplayExprPartnerActivity(value: any) {
    if (value && this.partnerActivity) {
      const item = this.partnerActivity.find(x => x.id === value.partnerActivityId);
      if (item) {
        return value.saleZoneCode + ' - ' + item.name + ' - ' + value.partnerActivityClassification;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  async onSpecializationsSaveClick(location: any) {
    this.loaded = true;

    const processLocation = async (item) => {
      let selectedPl = this.partnerLocations.find(f => f.id === item.id);

      if (this.partner.isPrincipalActivityUniqueForAllLocations) {
        item.partnerActivityAllocationId = this.implicitLocation.partnerActivityAllocationId;
      }
      const r = await this.partnerLocationService.updatePartnerLocationPartnerActivityAllocationAsync(item);
      if (r) {
        item.rowVersion = r.rowVersion;
       
        // Create CaenCodeSpecializationXPartnerLocation
        const existingSpecializationIds = selectedPl.existingCaenCodeIds;
        let deleteSpecializationPartnerLocationAllocation: CaenCodeSpecializationXPartnerLocation[] = [];
        existingSpecializationIds?.filter(x => !this.specializationsByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].includes(x)).forEach(id => {
          const c = new CaenCodeSpecializationXPartnerLocation();
          c.partnerLocationId = item.id;
          c.caenCodeSpecializationId = id;
          deleteSpecializationPartnerLocationAllocation.push(c);
        });
        if (deleteSpecializationPartnerLocationAllocation.length > 0) {
          await this.caenCodeSpecializationPartnerLocationService.deleteMultipleCaenCodeSpecializationPartnerLocationAsync(deleteSpecializationPartnerLocationAllocation).then(() => {})
        }
        let createSpecializationIds: CaenCodeSpecializationXPartnerLocation[] = [];
        this.specializationsByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].filter(x => !existingSpecializationIds.includes(x)).forEach(id => {
          const c = new CaenCodeSpecializationXPartnerLocation();
          c.partnerLocationId = Number(item.id);
          c.caenCodeSpecializationId = id;
          createSpecializationIds.push(c);
        });
        if (createSpecializationIds.length > 0) {
          await this.caenCodeSpecializationPartnerLocationService.createMultipleCaenCodeSpecializationPartnerLocationAsync(createSpecializationIds).then(() => {})
        }

        // Create PartnerSecondaryActivityAllocation
        const existingSecondaryActivityAllocationIds = selectedPl.existingPartnerSecondaryActivityAllocationIds;
        let deleteSecondaryActivityAllocation: PartnerLocationXSecondaryActivityAllocation[] = [];
        existingSecondaryActivityAllocationIds?.filter(x => !this.activitiesByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].includes(x)).forEach(id => {
          const c = new PartnerLocationXSecondaryActivityAllocation();
          c.partnerLocationId = item.id;
          c.partnerActivityAllocationId = id;
          deleteSecondaryActivityAllocation.push(c);
        });
        if (deleteSecondaryActivityAllocation.length > 0) {
          await this.partnerLocationSecondaryActivityAllocationService.deleteMultipleAsync(deleteSecondaryActivityAllocation).then(() => {})
        }
        let createSecondaryActivityAllocationIds: PartnerLocationXSecondaryActivityAllocation[] = [];
        this.activitiesByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].filter(x => !existingSecondaryActivityAllocationIds.includes(x)).forEach(id => {
          const c = new PartnerLocationXSecondaryActivityAllocation();
          c.partnerLocationId = Number(item.id);
          c.partnerActivityAllocationId = id;
          createSecondaryActivityAllocationIds.push(c);
        });
        if (createSecondaryActivityAllocationIds.length > 0) {
          await this.partnerLocationSecondaryActivityAllocationService.createMultipleAsync(createSecondaryActivityAllocationIds).then(() => {})
        }
      }
    };

    for (const location of this.partnerLocations) {
      await processLocation(location).then(() => {})
    }

    await this.getActivitiesAndSpecializations().then(() => {
      this.notificationService.alert('top', 'center', 'Specializari - Datele au fost modificate cu succes!', NotificationTypeEnum.Green, true );
      this.loaded = false;
    })
  }

  partnerLocationsDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.name;
  }

  onSpecsOpen(event: any, locationId: number) {
    let arr = this.caenSpecializationsDS.store.slice().sort((a, b) => {
      const aSelected = this.specializationsByLocation[locationId].includes(a.id);
      const bSelected = this.specializationsByLocation[locationId].includes(b.id);
      if (aSelected && !bSelected) return -1;
      if (!aSelected && bSelected) return 1;
      return 0;
    });
    this.sortedCaenSpecializationsDS = {
      paginate: true,
      pageSize: 15,
      store: arr
    };
  }

  onActivitiesOpen(event: any, locationId: number) {
    this.sortedPartnerActivityAllocation = this.partnerActivityAllocation.slice().sort((a, b) => {
      const aSelected = this.activitiesByLocation[locationId].includes(a.id);
      const bSelected = this.activitiesByLocation[locationId].includes(b.id);
      if (aSelected && !bSelected) return -1;
      if (!aSelected && bSelected) return 1;
      return 0;
    });
  }

  isOnSavePartnerLocationOutputChange(event: any) {
    
  }

  isOnSaveOutputChange(event: any) {
    
  }

  initialPartnerLocationOutputChange(event: any) {
    this.initialPartnerLocation = event;
  }

  partnerActivityChanged(e: any) {
    if (this.isOnSave && e && e.value) {
      this.initialPartnerLocation.caenCodeSpecializationIds = [];

      this.caenCodeSpecializationPartnerActivityAllocationService.getAllCaenCodeSpecializationPartnerActivityAllocationAsync().then(i => {
        if (i) {
          this.initialPartnerLocation.caenCodeSpecializationIds = i.filter(x => x.partnerActivityAllocationId === e.value).map(y => y.caenCodeSpecializationId);
        }
      });
    }
  }

  async selectedPartnerActivityChanged(e: any, location: any) {
    if (this.isOnSavePartnerLocation && e && e.value) {
      this.selectedPartnerLocation.caenCodeSpecializationIds = [];
      this.caenCodeSpecializationPartnerActivityAllocationService.getAllCaenCodeSpecializationPartnerActivityAllocationAsync().then(i => {
        if (i) {
          this.initialPartnerLocation.caenCodeSpecializationIds = i.filter(x => x.partnerActivityAllocationId === e.value).map(y => y.caenCodeSpecializationId);
        }
      });
    }
  }

  addExtraSpecialziations(location: any) {
    if (location.partnerActivityAllocationId) {
      this.caenCodeSpecializationPartnerActivityAllocationService.getAllCaenCodeSpecializationPartnerActivityAllocationAsync().then(i => {
        if (i) {
          let filteredSpecs = i.filter(x => x.partnerActivityAllocationId === location.partnerActivityAllocationId).map(y => y.caenCodeSpecializationId);

          if (this.activitiesByLocation[location.id] && this.activitiesByLocation[location.id].length > 0) { 
            let secondFilteredSpecs = i.filter(x => this.activitiesByLocation[location.id].includes(x.partnerActivityAllocationId) ).map(y => y.caenCodeSpecializationId);
            filteredSpecs.push(...secondFilteredSpecs);
          }

          if (filteredSpecs && filteredSpecs.length > 0) {
            filteredSpecs.forEach(spec => {
              if (!this.specializationsByLocation[location.id].includes(spec)) {
                this.specializationsByLocation[location.id].push(spec);
              }
            });
            this.notificationService.alert('top', 'center', 'Partener - Specializari adaugate!',
            NotificationTypeEnum.Green, true)

          } else {
            this.notificationService.alert('top', 'center', 'Partener - Nu au fost gasite specializari pentru aceasta activitate de partener! Trebuie selectata o alta activitate sau introduse manual specializarile!',
            NotificationTypeEnum.Red, true)
          }
        }
      });
    } else {
      this.notificationService.alert('top', 'center', 'Partener - Nu au putut sa fie adaugate specialziari! Locatia nu are o activitate selectata!',
      NotificationTypeEnum.Red, true)
    }
  }

  onInitialLocationSpecsOpen(event: any) {
    let arr = this.caenSpecializationsDS.store.slice().sort((a, b) => {
      const aSelected = this.initialPartnerLocation.caenCodeSpecializationIds && this.initialPartnerLocation.caenCodeSpecializationIds.includes(a.id);
      const bSelected = this.initialPartnerLocation.caenCodeSpecializationIds && this.initialPartnerLocation.caenCodeSpecializationIds.includes(b.id);
      if (aSelected && !bSelected) return -1;
      if (!aSelected && bSelected) return 1;
      return 0;
    });
    this.sortedCaenSpecializationsDS = {
      paginate: true,
      pageSize: 15,
      store: arr
    };  
  }
}
