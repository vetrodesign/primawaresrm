import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemSearchFilterComponent } from './item-search-filter.component';

describe('ItemSearchFilterComponent', () => {
  let component: ItemSearchFilterComponent;
  let fixture: ComponentFixture<ItemSearchFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemSearchFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemSearchFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
