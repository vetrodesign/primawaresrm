import { Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { SupplierTypeEnum } from 'app/enums/supplierTypeEnum';
import { ItemGroup } from 'app/models/itemgroup.model';
import { ItemListFilter } from 'app/models/itemlistfilter.model';
import { ItemType } from 'app/models/itemtype.model';
import { MeasurementUnit } from 'app/models/measurementunit.model';
import { SupplierSmallSearchFilter } from 'app/models/suppliersmallsearchfilter.model';
import { AuthService } from 'app/services/auth.service';
import { PartnerService } from 'app/services/partner.service';
import { TranslateService } from 'app/services/translate';
import { UsersService } from 'app/services/user.service';
import { DxValidatorComponent } from 'devextreme-angular';
import { Site } from 'app/models/site.model';
import { Tag } from 'app/models/tag.model';
import { Post } from 'app/models/post.model';
import { ItemService } from 'app/services/item.service';
import { ItemsTurnoversCountData } from 'app/models/item.model';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-item-search-filter',
  templateUrl: './item-search-filter.component.html',
  styleUrls: ['./item-search-filter.component.css']
})
export class ItemSearchFilterComponent implements OnInit, OnChanges {
  @Input() sites: Site[];
  @Input() tags: Tag[];
  @Input() itemGroups: ItemGroup[];
  @Input() measurementUnits: MeasurementUnit[];
  @Input() itemTypes: ItemType[];
  @Input() supplyAndSalesPosts: Post[];
  @Input() supplyAgents: Post[];
  @Input() caenSpecializationsDS: any;
  @Input() customCodesDS: any;
  
  @Output() searchEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() refreshEvent: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('validationSearchGroup') validationSearchGroup: DxValidatorComponent;
  itemSearchFilter: ItemListFilter;
  isOwner: boolean;
  suppliers: any;
  supplierSmallSearchFilter: SupplierSmallSearchFilter;
  itemGroupsDS: any;
  itemsTurnovers: any = [{value: 1, label: 'R36'}, {value: 2, label: 'R24'}, {value: 3, label: 'R12'}, {value: 4, label: 'R9'}, {value: 5, label: 'R6'}, {value: 6, label: 'R3'}];
  isItemsTurnoversTooltipVisible: boolean;
  itemsTurnoversTooltipLoaded: boolean;
  itemsTurnoversCountData: ItemsTurnoversCountData[] = [];

  constructor(private translationService: TranslateService, private partnerService: PartnerService,
    private userService: UsersService, private authService: AuthService, private itemService: ItemService, private decimalPipe: DecimalPipe) {
    this.authService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
    this.supplierSmallSearchFilter = new SupplierSmallSearchFilter();
    this.supplierSmallSearchFilter.supplierType = [];
    this.supplierSmallSearchFilter.supplierType.push(SupplierTypeEnum.Active);

    this.suppliers = [];
    this.partnerService.getSuppliersSmallAsync(this.supplierSmallSearchFilter).then(result => {
      if (result && result.length > 0) {
        this.suppliers = {
          paginate: true,
          pageSize: 15,
          store: result
        };
      }
    });

    this.itemSearchFilter = new ItemListFilter();
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.itemGroups) {
      this.itemGroupsDS = {
        paginate: true,
        pageSize: 15,
        store: (this.itemGroups && this.itemGroups.length > 0) ? this.itemGroups : []
      };
    }
  }

  @HostListener('document:keydown', ['$event'])
  onKeydown(event: KeyboardEvent) {
    if (event.key === 'F5') {
      event.preventDefault(); 
      this.searchItems();
    }

    if (event.key === 'Delete') {
      event.preventDefault(); 
      this.resetFilters();
    }
  }

  searchItems(isChartTurnover?: boolean) {
    if (!isChartTurnover) {
      this.itemSearchFilter.chartTurnover = null;
    }

    if (this.validationSearchGroup.instance.validate().isValid) {
      this.searchEvent.emit(this.itemSearchFilter);
    }
  }

  resetFilters() {
    this.itemSearchFilter = new ItemListFilter();

    this.refreshEvent.emit(true);
  }


  getDisplayExprItemGroup(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  getDisplayExprSupplyAgents(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  getDisplayExprMeasurementUnits(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  getDisplayExpCustomCode(item) {
    if (!item) {
      return '';
    }
    return item.cnKey + ' - ' + item.nameRO;
  }

  getDisplayExprItemType(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  getDisplayExprSupplyAndSalesPosts(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  getDisplayExprSpec(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.description;
  }

  getDisplayExprSuppliers(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  onMultiTagItemGroupChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagMUChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagItemTypeChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagSpecChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagCustomCodeChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagSuppliersChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagSitesChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagSupplyAndSalesPostsChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }
  
  onMultiTagSupplyAgentsChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  async getItemsTurnoversTooltip() {
    this.isItemsTurnoversTooltipVisible = true;
    this.itemsTurnoversTooltipLoaded = true;

    await this.itemService.getItemTurnoversCountData().then(resp => {
      if (resp) {
        this.itemsTurnoversCountData = resp;
      } 

      this.itemsTurnoversTooltipLoaded = false;
    })
  }

  hideItemsTurnoversTooltip(): void {
    this.isItemsTurnoversTooltipVisible = false;
  }

  onHidingItemsTurnoversTooltip(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  customizeLabel = (data) => {
    return {
      visible: true, 
      verticalAlignment: 'bottom',
      horizontalAlignment: 'center',
      font: { size: 14, color: '#fff' },
      backgroundColor: 'black',
      customizeText: () => `${this.decimalPipe.transform(data.value, '1.0-0')}`
    };
  };

  customizeText = ({ value }) => `${this.decimalPipe.transform(value, '1.0-0')}`;

  onBarClick(event: any): void {
    this.itemSearchFilter = new ItemListFilter();

    const clickedBarData = event.target;
    let selectedTurnoverBar = "";
    
    switch (clickedBarData.argument) {
      case "3 si 6 luni":
        selectedTurnoverBar = "R3R6";
        break;
      case "6 si 9 luni":
        selectedTurnoverBar = "R6R9";
        break;
      case "9 si 12 luni":
        selectedTurnoverBar = "R9R12";
        break;
      case "12 si 24 luni":
        selectedTurnoverBar = "R12R24";
        break;
      case "24 si 36 luni":
        selectedTurnoverBar = "R24R36";
        break;
      case "peste 36 luni":
        selectedTurnoverBar = "R36";
        break;
      default:
        selectedTurnoverBar = "";
        break;
    }

    this.itemSearchFilter.chartTurnover = selectedTurnoverBar;
    this.searchItems(true);
  }
}