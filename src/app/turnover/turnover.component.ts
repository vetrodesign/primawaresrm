import { DecimalPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { City } from 'app/models/city.model';
import { Country } from 'app/models/country.model';
import { County } from 'app/models/county.model';
import { Department } from 'app/models/department.model';
import { Post } from 'app/models/post.model';
import { TurnoverFilter, TurnoverHistoryFilter } from 'app/models/turnoverFilter.model';
import { TurnoverItem, TurnoverMonths, TurnoverMonthsRow } from 'app/models/turnoveritem.model';
import { TurnoverPartner } from 'app/models/turnoverPartner.model';
import { TurnoverOnlyToSupplyFilter, TurnoverSearchFilter } from 'app/models/turnoverSearchFilter.model';
import { AuthService } from 'app/services/auth.service';
import { CityService } from 'app/services/city.service';
import { CountryService } from 'app/services/country.service';
import { CountyService } from 'app/services/county.service';
import { DepartmentsService } from 'app/services/departments.service';
import { ItemSeoPlanningService } from 'app/services/item-seo-planning.service';
import { ItemTurnoverService } from 'app/services/item-turnover.service';
import { ItemService } from 'app/services/item.service';
import { PostService } from 'app/services/post.service';
import { SpecialPriceRequestItemsService } from 'app/services/special-price-request-items.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { SupplierOriginService } from 'app/services/supplier-origin.service';
import { SupplierOrigin } from 'app/models/supplier-origin.model';
import { ItemTurnoverAnalysisService } from 'app/services/item-turnover-analysis.service';
import { Item } from 'app/models/item.model';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { ItemTurnoverAnalysis } from 'app/models/item-turnover-analysis.model';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { PartnerService } from 'app/services/partner.service';
import { UsersService } from 'app/services/user.service';
import { User } from 'app/models/user.model';

@Component({
  selector: 'app-turnover',
  templateUrl: './turnover.component.html',
  styleUrls: ['./turnover.component.css']
})
export class TurnoverComponent implements OnInit {
  loaded: boolean;
  isOwner: boolean;
  countries: Country[] = [];
  countriesDS: any;
  counties: County[] = [];
  countiesDS: any;
  cities: City[] = [];
  citiesDS: any;
  turnoverSearchFilter: TurnoverSearchFilter;
  items: any;
  posts: Post[] = [];
  departments: Department[] = [];
  turnOverItems: TurnoverItem[] = [];
  turnoverMonths: TurnoverMonthsRow[] = [];
  turnoverSuppliers: TurnoverPartner[] = [];
  currentSupplierIndex: number = 0;
  currentSupplierDisplayIndex: number = 0;
  monthsToDisplay: Date[];
  selectedItemCode: string;
  isFiltersSectionVisible: boolean = true;
  isItemTurnoverHistoryOpened: boolean;
  itemTurnoverOperationName: string;
  turnoverHistoryItemId: number;
  turnoverHistoryOperationId: number;
  isDataLoading: boolean;
  itemName: string;
  itemCode: string;
  pricesIconTooltip: string;
  isPricesTooltipVisible = {};
  seoIconTooltip: any;
  isSeoTooltipVisible = {};
  isOrdersTooltipVisible = {};
  intraCommunitySuppliers: TurnoverPartner[] = [];
  internalSuppliers: TurnoverPartner[] = [];
  externalSuppliers: TurnoverPartner[] = [];
  isSortByGeoEntities: boolean;
  filteredTurnoverItems: { [key: number]: any[] } = {};
  itemsNewTurnover: any;
  onlyToSupplyFilter: TurnoverOnlyToSupplyFilter = new TurnoverOnlyToSupplyFilter();
  onlyToSupplyFilterOptions: any = [{value: 1, label: 'Max(R/R3/6/9/12)'}, {value: 2, label: 'Max(R3/6/9/12)'}, {value: 3, label: 'R'}];
  supplierOrigin: SupplierOrigin[];
  itemTurnoverAnalysisData: any;
  isSolveTurnoverTooltipVisible = {};
  isSolveTurnoverPopupVisible: boolean;
  selectedSolveTurnoverItem: string;
  selectedSolveTurnoverAutomaticTurnover: number;
  selectedSolveTurnverOtherTurnover: number;
  selectedSolveTurnoverItemId: number;
  itemTurnoverAnalysis: any;
  distinctSupplierAgentIds: number[];
  filteredPartners: any;
  selectedAlertTurnover: string;
  users: User[];
  isSupplyDirectorFlag: boolean;

  pageSize = 15;
  currentPage = 0;
  pagedTurnOverItems = [];

  constructor(private itemsService: ItemService, private itemTurnoverService: ItemTurnoverService, private postService: PostService, private cityService: CityService, private countyService: CountyService,
    private countryService: CountryService, private departmentsService: DepartmentsService, private authenticationService: AuthService, private route: ActivatedRoute, private decimalPipe: DecimalPipe,
    private specialPriceRequestItemsService: SpecialPriceRequestItemsService, private itemSeoPlanningService: ItemSeoPlanningService, private supplierOriginService: SupplierOriginService,
    private itemTurnoverAnalysisService: ItemTurnoverAnalysisService, private notificationService: NotificationService, private partnerService: PartnerService, private userService: UsersService) {
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });

    //this.onlyToSupplyFilter.isOnlyToSupply = true;
    //this.onlyToSupplyFilter.onlyToSupplyOption = this.onlyToSupplyFilterOptions[2].value;
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getItems(), this.getPosts(), this.getSupplierOrigin(), this.getItemTurnoverAnalysis(), this.getFilteredPartners(),
    this.getCities(), this.getDepartments(), this.getCounties(), this.getCountries(), this.getDistinctSupplierAgentIds(), this.getUsers()]).then(async x => {
      //await this.getItemsNewTurnover();

      if (this.route.snapshot.queryParamMap.get('itemCode') !== undefined && this.route.snapshot.queryParamMap.get('itemCode') !== null) {
        this.selectedItemCode = this.route.snapshot.queryParamMap.get('itemCode');
        this.isFiltersSectionVisible = false;

        let filter: any = {};
        filter.code = this.selectedItemCode;
        await this.searchEvent(filter);
      }

      this.isSupplyDirectorFlag = this.isSupplyDirector();

      this.loaded = false
    });
  }

  isSupplyDirector() {
    if (this.posts && this.posts.length > 0 && this.users && this.users.length > 0) {
      let post = this.posts?.find(x => x.id === this.users.find(x => x.id === Number(this.authenticationService.getUserId()))?.postId);
      if (post) {
        if (post.code === "DA") {
          return true;
        } else {
          return false;
        }
      }
    }
  }

  async getUsers() {
    if (this.isOwner) {
      await this.userService.getUsersAsync().then(items => {
        this.users = items;
      });
    } else {
      await this.userService.getUserAsyncByID().then(items => {
        this.users = items;
      });
    }
  }

  async getSupplierOrigin() {
    if (this.isOwner) {
      await this.supplierOriginService.getAllAsync().then(items => {
        this.supplierOrigin = (items && items.length > 0) ? items : [];
      });
    } else {
      await this.supplierOriginService.getAllByCustomerIdAsync().then(items => {
        this.supplierOrigin = (items && items.length > 0) ? items : [];
      });
    }
  }

  async getItemsNewTurnover(): Promise<any> {
    await this.itemTurnoverService.getItemsNewTurnover(this.items.store.map(m => m.id)).then((resp) => {
      this.itemsNewTurnover = resp;
    })
  }

  async getItems(): Promise<any> {
    if (this.isOwner) {
      await this.itemsService.getAllItemsAsync().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    } else {
      await this.itemsService.getItemssAsyncByID().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    }
  }

  async getPosts() {
    if (this.isOwner) {
      await this.postService.getAllPostsAsync().then(items => {
        this.posts = items;
      });
    } else {
      await this.postService.getPostsByCustomerId().then(items => {
        this.posts = items;
      });
    }
  }

  async getDepartments() {
    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(items => {
        this.departments = items;
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(items => {
        this.departments = items;
      });
    }
  }

  async getCountries() {
    await this.countryService.getAllCountrysAsync().then(items => {
      this.countries = items;
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries
      };
    });
  }

  async getCities() {
    await this.cityService.getAllCitiesSmallAsync().then(items => {
      this.cities = items;
      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities
      };
    });
  }

  async getCounties() {
    await this.countyService.getAllCountysAsync().then(items => {
      this.counties = items;
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties
      };
    });
  }

  async getDistinctSupplierAgentIds() {
    await this.itemTurnoverService.getDistinctSupplierAgentIds().then(items => {
      this.distinctSupplierAgentIds = items;
    });
  }

  async searchEvent(e: any) {
    this.turnoverSearchFilter = e
    this.loaded = true;
    //send SearchFilter as parameter to the BE
    await this.getTurnOver().then(x => this.loaded = false);
  }

  async getTurnOver(keepCurrentIndex?: boolean) {
    let filter = new TurnoverFilter();

    if (!keepCurrentIndex) {
      this.currentPage = 0;
    }
    
    this.turnOverItems = [];
    this.pagedTurnOverItems = [];

    if (this.turnoverSearchFilter) {
      if (this.turnoverSearchFilter.citiesIds && this.turnoverSearchFilter.citiesIds.length > 0) {
        filter.cityIds = this.turnoverSearchFilter.citiesIds;
      }

      if (this.turnoverSearchFilter.countiesIds && this.turnoverSearchFilter.countiesIds.length > 0) {
        filter.countyIds = this.turnoverSearchFilter.countiesIds;
      }

      if (this.turnoverSearchFilter.countriesIds && this.turnoverSearchFilter.countriesIds.length > 0) {
        filter.countryIds = this.turnoverSearchFilter.countriesIds;
      }

      if (this.turnoverSearchFilter.itemIds && this.turnoverSearchFilter.itemIds.length > 0) {
        filter.itemIds = this.turnoverSearchFilter.itemIds;
      }

      if (this.turnoverSearchFilter.partnerId) {
        filter.supplierIds = [this.turnoverSearchFilter.partnerId];
      }

      if (this.turnoverSearchFilter.postIds) {
        filter.supplierAgentIds = this.turnoverSearchFilter.postIds;
      }

      if (this.turnoverSearchFilter.code) {
        filter.itemCodes = [this.turnoverSearchFilter.code];
      }
    }
    await this.itemTurnoverAnalysisService.getItemTurnoverAnalysisData().then(async (data) => {
      this.itemTurnoverAnalysisData = data;

    await this.itemTurnoverService.getItemTurnoverSuppliers(filter).then(async items => {
      if (items && items.length > 0) {
        this.turnoverSuppliers = items;

        if (this.onlyToSupplyFilter.onlyToSupplyOption) {
            
            if (data) {

              this.turnoverSuppliers.forEach(ts => {
  
                let supplierIds = this.items.store.map(m => m.partnerId).filter(f => f === ts.supplierId);
                let itemIds = this.items.store.filter(f => supplierIds.includes(f.partnerId)).map(m => m.id);
                let selectedData = data.filter(f => itemIds.includes(f.itemId));
  
                ts.smallDataTurnoverItems = selectedData.map(m => {
                  return {
                    itemId: m.itemId,
                    itemTurnover: m.itemTurnover,
                    r3: m.r3,
                    r6: m.r6,
                    r9: m.r9,
                    r12: m.r12,
                    currentStock: m.itemStock
                  }
                })
              })
            
              this.turnoverSuppliers.forEach((ts: any) => {  
                let supplier: any = ts;
              
                let hasValidItem = ts.smallDataTurnoverItems.some(item => {
                  
                  if (!this.onlyToSupplyFilter.isOnlyToSupply || !supplier) {
                    return false;
                  }
                  
                  let stockAlertInternal = this.supplierOrigin.find(s => s.code === 'INT');
                  let stockAlertExternal = this.supplierOrigin.find(s => s.code === 'EXT');
                  let stockAlertIntraCommunity = this.supplierOrigin.find(s => s.code === 'INTRACOM');
      
                  if (!stockAlertInternal && !stockAlertExternal && !stockAlertIntraCommunity) {
                    return false;
                  }
      
                  let maxR3R6R9R12 = Math.max(item.r3, item.r6, item.r9, item.r12);
                  let maxRR3R6R9R12 = Math.max(item.itemTurnover, item.r3, item.r6, item.r9, item.r12);
      
                  let getComparisonProperty = () => {
                    switch (this.onlyToSupplyFilter.onlyToSupplyOption) {
                      case this.onlyToSupplyFilterOptions[2].value:
                        return item.itemTurnover;
                      case this.onlyToSupplyFilterOptions[1].value:
                        return maxR3R6R9R12;
                      case this.onlyToSupplyFilterOptions[0].value:
                        return maxRR3R6R9R12;
                      default:
                        return null;
                    }
                  };
      
                  let comparisonProperty = getComparisonProperty();
                  if (comparisonProperty === null) {
                    return false;
                  }
      
                  let stockToBeSuppliedInternal = stockAlertInternal ? comparisonProperty * stockAlertInternal.stockAlertMultiplier : 0;
                  let stockToBeSuppliedExternal = stockAlertExternal ? comparisonProperty * stockAlertExternal.stockAlertMultiplier : 0;
                  let stockToBeSuppliedIntraCommunity = stockAlertIntraCommunity ? comparisonProperty * stockAlertIntraCommunity.stockAlertMultiplier : 0;
      
                  let itemTrnAnalysis = this.itemTurnoverAnalysis.find(f => f.itemId === item.itemId);
                  return (
                    (!itemTrnAnalysis || (itemTrnAnalysis && (!itemTrnAnalysis.supplyAlertState || itemTrnAnalysis.supplyAlertState === 1))) &&
                    item.itemTurnover >= 2 &&
                    (
                      (stockAlertInternal && supplier.countryId === 180 && item.currentStock < stockToBeSuppliedInternal) ||
                      (stockAlertExternal && supplier.countryId !== 180 && !supplier.isEUMember && item.currentStock < stockToBeSuppliedExternal) ||
                      (stockAlertIntraCommunity && supplier.countryId !== 180 && supplier.isEUMember && item.currentStock < stockToBeSuppliedIntraCommunity)
                    )
                  );
                });
      
                ts.isValidSupplier = hasValidItem;
              });
      
              this.turnoverSuppliers = this.turnoverSuppliers.filter((ts: any) => ts.isValidSupplier);

              this.internalSuppliers = this.turnoverSuppliers.filter(f => f.countryId === 180);
              this.intraCommunitySuppliers = this.turnoverSuppliers.filter((f: any) => f.countryId !== 180 && f.isEUMember);
              this.externalSuppliers = this.turnoverSuppliers.filter((f: any) => f.countryId !== 180 && !f.isEUMember);
      
              this.setCurrentSupplierIndex(keepCurrentIndex ? this.currentSupplierIndex : 0);      
            }
        
        } else {
          this.internalSuppliers = items.filter(f => f.countryId === 180);
          this.intraCommunitySuppliers = items.filter(f => f.countryId !== 180 && f.isEUMember);
          this.externalSuppliers = items.filter(f => f.countryId !== 180 && !f.isEUMember);

          this.setCurrentSupplierIndex(keepCurrentIndex ? this.currentSupplierIndex : 0);  
        }      
      } else {
        this.turnoverSuppliers = [];
        this.setCurrentSupplierIndex(keepCurrentIndex ? this.currentSupplierIndex : 0);  
      }
    });
  })
  }

  getItem(item) {
    if (item && this.items.length > 0) {
      let c = this.items.find(x => x.id === item[0].itemId);
      return c ? c.code + ' - ' + c.nameRO : '';
    } else {
      return '';
    }
  }

  refreshEvent(e: any) {
    this.loaded = true;
    this.getData();
    this.getTurnOver().then(x => this.loaded = false);
  }

  async setCurrentSupplierIndex(index: number) {
    this.loaded = true;
    this.turnOverItems = [];
    this.currentSupplierIndex = index;
    this.currentSupplierDisplayIndex = this.currentSupplierIndex + 1;

    let filter = new TurnoverFilter();

    if (this.turnoverSearchFilter) {
      if (this.turnoverSearchFilter.itemIds && this.turnoverSearchFilter.itemIds.length > 0) {
        filter.itemIds = this.turnoverSearchFilter.itemIds;
      }

      filter.supplierIds = [this.turnoverSuppliers[index].supplierId];

      if (this.turnoverSearchFilter.code) {
        filter.itemCodes = [this.turnoverSearchFilter.code];
      }
    }

    await this.itemTurnoverService.getTurnoverItemsBySupplierIds(filter).then(async items => {
      this.turnOverItems = items;

      await this.itemsService.getAllManagementsItemStocks(this.turnOverItems?.map(m => m.itemId)).then(r => {
        if (r) {
          this.turnOverItems.forEach(i => {
            if (r.hasOwnProperty(i.itemId)) {
              i.currentStock = r[i.itemId];
            } else {
              i.currentStock = 0;
            }
          });
        }
      });

      let stockAlertInternal = this.supplierOrigin.find(s => s.code === 'INT');
      let stockAlertExternal = this.supplierOrigin.find(s => s.code === 'EXT');
      let stockAlertIntraCommunity = this.supplierOrigin.find(s => s.code === 'INTRACOM');

      this.turnOverItems.forEach((f: any) => {
        // let newTurnover = this.itemsNewTurnover.find(g => g.itemId === f.itemId);

        // if (newTurnover && newTurnover.newTurnover) {
        //   f.itemTurnover = newTurnover.newTurnover
        // }

        let analysisData = this.itemTurnoverAnalysisData.find(g => g.itemId === f.itemId);

        let automaticTurnover = 0;
        let bottomMargin = 0; // todo: development for item type
        let topMargin = 0;
        let bottomMarginPercentage = 0;
        let topMarginPercentage = 0;

        switch (analysisData?.itemType) {
          case 2: // extern
            bottomMargin = stockAlertExternal ? Math.round((stockAlertExternal.lowerMargin / 100) * 10) / 10 : 0; // 0.9 rounded up
            topMargin = stockAlertExternal ? Math.round((stockAlertExternal.upperMargin / 100) * 10) / 10 : 0; // 1.25 rounded up

            bottomMarginPercentage = stockAlertExternal.lowerMargin;
            topMarginPercentage = stockAlertExternal.upperMargin;
            break;
          case 3: // intracom
            bottomMargin = stockAlertIntraCommunity ? Math.round((stockAlertIntraCommunity.lowerMargin / 100) * 10) / 10 : 0; // 0.9 rounded up
            topMargin = stockAlertIntraCommunity ? Math.round((stockAlertIntraCommunity.upperMargin / 100) * 10) / 10 : 0; // 1.25 rounded up

            bottomMarginPercentage = stockAlertIntraCommunity.lowerMargin;
            topMarginPercentage = stockAlertIntraCommunity.upperMargin;
            break;
          case 4: // intern
            bottomMargin = stockAlertInternal ? Math.round((stockAlertInternal.lowerMargin / 100) * 10) / 10 : 0; // 0.8 rounded up
            topMargin = stockAlertInternal ? Math.round((stockAlertInternal.upperMargin / 100) * 10) / 10 : 0;;

            bottomMarginPercentage = stockAlertInternal.lowerMargin;
            topMarginPercentage = stockAlertInternal.upperMargin;
            break;

          default:
            break;
        }

        let r3r12Max = Math.max(f.rulajReal3M, f.rulajReal6M, f.rulajReal9M, f.rulajReal12M);
    
        if ((r3r12Max < f.itemTurnover * bottomMargin) || (r3r12Max > f.itemTurnover * topMargin)) {
          automaticTurnover = Math.round(r3r12Max * 1.1);
        }

        f.automaticTurnover = automaticTurnover;
        f.topMargin = topMargin;
        f.bottomMargin = bottomMargin;
        f.topMarginPercentage = topMarginPercentage;
        f.bottomMarginPercentage = bottomMarginPercentage;

        let itemTurnoverAnalysis = this.itemTurnoverAnalysis.find(g => g.itemId === f.itemId);
        if (itemTurnoverAnalysis) {
          if (itemTurnoverAnalysis.alerting && itemTurnoverAnalysis.alerting !== 'Imediat') {
            f.alerting = itemTurnoverAnalysis.alerting;
          } else {
            const today = moment();
            const nextMonth = today.clone().add(1, 'months').endOf('month');
            const nextMonthDate = nextMonth.format('DD.MM.YYYY');
            f.alerting = nextMonthDate;
          }
        }
      })

      let supplier: any = this.turnoverSuppliers.find(f => f.supplierId === this.turnoverSuppliers[index].supplierId);
      
      if (this.onlyToSupplyFilter.onlyToSupplyOption) {
        this.turnOverItems = this.turnOverItems.filter(item => {
          if (!this.onlyToSupplyFilter.isOnlyToSupply || !supplier) {
            return false;
          }
        
          if (!stockAlertInternal && !stockAlertExternal && !stockAlertIntraCommunity) {
            return false;
          }
        
          let maxR3R6R9R12 = Math.max(item.rulajReal3M, item.rulajReal6M, item.rulajReal9M, item.rulajReal12M);
          let maxRR3R6R9R12 = Math.max(item.itemTurnover, item.rulajReal3M, item.rulajReal6M, item.rulajReal9M, item.rulajReal12M);
        
          let getComparisonProperty = () => {
            switch (this.onlyToSupplyFilter.onlyToSupplyOption) {
              case this.onlyToSupplyFilterOptions[2].value:
                return item.itemTurnover;
              case this.onlyToSupplyFilterOptions[1].value:
                return maxR3R6R9R12;
              case this.onlyToSupplyFilterOptions[0].value:
                return maxRR3R6R9R12;
              default:
                return null;
            }
          };
        
          let comparisonProperty = getComparisonProperty();
          if (comparisonProperty === null) {
            return false;
          }
                  
          let stockToBeSuppliedInternal = stockAlertInternal ? comparisonProperty * stockAlertInternal.stockAlertMultiplier : 0;
          let stockToBeSuppliedExternal = stockAlertExternal ? comparisonProperty * stockAlertExternal.stockAlertMultiplier : 0;
          let stockToBeSuppliedIntraCommunity = stockAlertIntraCommunity ? comparisonProperty * stockAlertIntraCommunity.stockAlertMultiplier : 0;
        
          let itemTrnAnalysis = this.itemTurnoverAnalysis.find(f => f.itemId === item.itemId);
          return (
            (!itemTrnAnalysis || (itemTrnAnalysis && (!itemTrnAnalysis.supplyAlertState || itemTrnAnalysis.supplyAlertState === 1))) &&
            item.itemTurnover >= 2 && ((stockAlertInternal && supplier.countryId === 180 && item.currentStock < stockToBeSuppliedInternal) ||
            (stockAlertExternal && supplier.countryId !== 180 && !supplier.isEUMember && item.currentStock < stockToBeSuppliedExternal) ||
            (stockAlertIntraCommunity && supplier.countryId !== 180 && supplier.isEUMember && item.currentStock < stockToBeSuppliedIntraCommunity))
          );
        });  
      }

      this.updatePagedItems();
      //this.calculateTurnoverMonths();
      this.loaded = false;
    });
  }

  calculateTurnoverMonths() {
    this.turnoverMonths = [];
    this.monthsToDisplay = [];
    let currentMonth = moment();
    let firstMonth = moment().add(-13, 'months');

    while (currentMonth >= firstMonth) {
      this.monthsToDisplay.push(currentMonth.toDate());
      currentMonth.add(-1, 'months');
    }

    this.monthsToDisplay = this.monthsToDisplay.sort((a, b) => (a.getTime() - b.getTime()));

    let dataFlatten = this.pagedTurnOverItems.map(x => x.itemDetails.map(y => ({ itemId: x.itemId, ...y }))).reduce((a, b) => { return a.concat(b); }, []);

    let distinctOperations = dataFlatten.reduce((prev, { itemId, turnoverOperationId, turnoverOperationName }) =>
      prev.some(x => x.itemId === itemId && x.turnoverOperationId === turnoverOperationId) ?
        prev : [...prev, { itemId, turnoverOperationId, turnoverOperationName }], []);

    this.turnoverMonths = distinctOperations.map(x => {
      var row = new TurnoverMonthsRow();
      var totalQuantity = 0;
      
      row.itemId = x.itemId;
      row.turnoverOperationName = x.turnoverOperationName;
      row.turnoverOperationId = x.turnoverOperationId;
      row.monthsData = this.monthsToDisplay.map(y => {
        var month = new TurnoverMonths();
        month.month = y;

        var itemMonthData = dataFlatten.filter(a => a.turnoverOperationId === x.turnoverOperationId && a.itemId === x.itemId
          && a.year === y.getFullYear() && a.month === (y.getMonth() + 1));

        if (itemMonthData.length > 0) {
          month.amount = itemMonthData.reduce((a, b) => { return a + b.amount }, 0);
          month.quantity = itemMonthData.reduce((a, b) => { return a + b.quantity }, 0);
          totalQuantity+= month.quantity;
        }

        return month;
      }).sort((a, b) => (a.month.getTime() - b.month.getTime()));

      row.Total = totalQuantity;

      return row;
    });

    this.turnoverMonths.forEach(async element => {
      let filter = new TurnoverHistoryFilter();
      filter.itemId = element.itemId;
      filter.itemTurnoverOperationId = element.turnoverOperationId;

      await this.itemTurnoverService.getCountItemTurnoverDetailXPartner(filter).then(result => {
        element.NoOfPartners = result;
      })
    })

    this.filteredTurnoverItems = this.pagedTurnOverItems.reduce((acc, item) => {
      acc[item.itemId] = this.turnoverMonths
        .filter(x => x.itemId === item.itemId)
        .sort((a, b) => a.turnoverOperationId - b.turnoverOperationId);
      return acc;
    }, {} as { [key: number]: any[] });
  }

  previousSupplier() {
    this.setCurrentSupplierIndex(this.currentSupplierIndex - 1);
    this.currentPage = 0;
  }

  nextSupplier() {
    this.setCurrentSupplierIndex(this.currentSupplierIndex + 1);
    this.currentPage = 0;
  }

  setSupplier(e: any) {
    let element = (e.event.target as HTMLInputElement);
    const newValue = element.value;

    if (parseInt(newValue) > 0 && parseInt(newValue) <= this.turnoverSuppliers.length) {
      this.setCurrentSupplierIndex(parseInt(newValue) - 1);
    }
    else {
      this.currentSupplierDisplayIndex = this.currentSupplierIndex + 1;
      element.value = (this.currentSupplierIndex + 1).toString();
    }
  }

  get displayedPage(): number {
    return this.currentPage + 1;
  }
  
  set displayedPage(value: number) {
    const newPage = Number(value) - 1;
    if (newPage >= 0 && newPage < this.totalPages) {
      this.currentPage = newPage;
      this.updatePagedItems();
    }
  }
  
  setPage(event: any) {
    const newPage = Number(event.value) - 1;
    if (!isNaN(newPage) && newPage >= 0 && newPage < this.totalPages) {
      this.currentPage = newPage;
      this.updatePagedItems();
    }
  }  

  getHttpLink(url: string) {
    if (url === undefined || url == null)
      return '';
    else if (url.startsWith('http'))
      return url;
    else
      return 'https://'.concat(url);
  }

  filterItemIdHasTurnoverMonths(itemId: number) {
    return this.turnoverMonths.filter(x => x.itemId === itemId).length > 0;
  }

  filterTurnoverMonthsByItemId(itemId: number) {
    return this.turnoverMonths.filter(x => x.itemId === itemId).sort((a, b) => (a.turnoverOperationId - b.turnoverOperationId));
  }

  openFiltersSection() {
    this.isFiltersSectionVisible = !this.isFiltersSectionVisible;
  }

  onTurnoverOperationClick(rowData: any) {
    if (rowData) {
      this.itemCode = this.items.store.find(f => f.id === rowData.itemId)?.code;
      this.itemName = this.items.store.find(f => f.id === rowData.itemId)?.nameRO;
      this.itemTurnoverOperationName = rowData.turnoverOperationName;
      this.turnoverHistoryItemId = rowData.itemId;
      this.turnoverHistoryOperationId = rowData.turnoverOperationId;
      this.isItemTurnoverHistoryOpened = true;
    }
  }

  displayItemTurnoverHistoryPopupChange(event: any) {
    event ? this.isDataLoading = true : this.isDataLoading = false;
  }

  async getItemPrices(itemId: number) {
    this.pricesIconTooltip = 'Se incarca...'; 
    this.isPricesTooltipVisible[itemId] = true;

    await this.specialPriceRequestItemsService.getItemPrices(itemId).then(resp => {
      if (resp) {
        const cmp = this.decimalPipe.transform(resp.cmp, '1.2-2') || '0.00';
        const d0 = this.decimalPipe.transform(resp.d0, '1.2-2') || '0.00';
        const c0 = this.decimalPipe.transform(resp.c0, '1.2-2') || '0.00';
        const r0 = this.decimalPipe.transform(resp.r0, '1.2-2') || '0.00';

        this.pricesIconTooltip = `CMP: ${cmp} \n D0: ${d0} \n C0: ${c0} \n R0: ${r0}`;
      } else {
        this.pricesIconTooltip = '';
      }
    })
  }

  hidePricesTooltip(itemId: number): void {
    this.isPricesTooltipVisible[itemId] = false;
  }

  async getItemSeo(itemId: number) {
    this.seoIconTooltip = []; 
    this.isSeoTooltipVisible[itemId] = true;
    
    await this.itemSeoPlanningService.getByIdsAsync([itemId]).then(resp => {
      if (resp && resp.length > 0) {  
        let response = resp[0];

        this.seoIconTooltip = [{
          roValue: !!response.roValue,
          enValue: !!response.enValue,
          deValue: !!response.deValue,
          frValue: !!response.frValue,
          huValue: !!response.huValue,
          bgValue: !!response.bgValue,
          photos: !!response.photos,
          p360: !!response.p360,
          film: !!response.film
        }];
      } else {
        this.seoIconTooltip = [];
      }
    })
  }

  hideSeoTooltip(itemId: number): void {
    this.isSeoTooltipVisible[itemId] = false;
  }

  showOrdersTooltip(itemId: number): void {
    this.isOrdersTooltipVisible[itemId] = true;
  }

  hideOrdersTooltip(itemId: number): void {
    this.isOrdersTooltipVisible[itemId] = false;
  }

  showSolveTurnoverTooltip(itemId: number): void {
    this.isSolveTurnoverTooltipVisible[itemId] = true;
  }

  hideSolveTurnoverTooltip(itemId: number): void {
    this.isSolveTurnoverTooltipVisible[itemId] = false;
  }

  async onSuppliersCountFilterClick(filterType: string) {
    let filter = new TurnoverFilter();
    
    if (filterType === 'internal') {
      let internalSuppliersIds = this.internalSuppliers.map(m => m.supplierId);
      filter.supplierIds = internalSuppliersIds;
    } else if (filterType === 'intracommunity') {
      let intraCommunitySuppliersIds = this.intraCommunitySuppliers.map(m => m.supplierId);
      filter.supplierIds = intraCommunitySuppliersIds;
    } else if (filterType === 'external') {
      let externalSuppliersIds = this.externalSuppliers.map(m => m.supplierId);
      filter.supplierIds = externalSuppliersIds;
    }

    this.loaded = true;
    await this.itemTurnoverService.getItemTurnoverSuppliers(filter).then(items => {
      if (items && items.length > 0) {
        this.turnoverSuppliers = items;

        if (this.isSortByGeoEntities) {
          let copyArr = [...this.turnoverSuppliers];
          this.turnoverSuppliers = _.orderBy(copyArr, ['countryName', 'countyName', 'cityName'], ['asc', 'asc', 'asc']);
        }

        this.setCurrentSupplierIndex(0);
      } else {
        this.turnoverSuppliers = [];
        this.setCurrentSupplierIndex(0);
      }

      this.loaded = false;
    });
  }

  async onSortByGeoEntitiesChange(event: any): Promise<void> {
    const isChecked = event.value; 
    this.isSortByGeoEntities = isChecked;

    if (isChecked === true) {
      let copyArr = [...this.turnoverSuppliers];
      this.turnoverSuppliers = _.orderBy(copyArr, ['countryName', 'countyName', 'cityName'], ['asc', 'asc', 'asc']);

      this.setCurrentSupplierIndex(0);
    }
  }

  async onOnlyToSupplyChange(event: any): Promise<void> {
    const isChecked = event.value; 
    this.onlyToSupplyFilter.isOnlyToSupply = isChecked;

    let filter = new TurnoverFilter();

    if (this.onlyToSupplyFilter.isOnlyToSupply === false) {
      this.onlyToSupplyFilter.onlyToSupplyOption = null;

      this.loaded = true;
      await this.itemTurnoverService.getItemTurnoverSuppliers(filter).then(items => {
        if (items && items.length > 0) {
          this.turnoverSuppliers = items;
  
          if (this.isSortByGeoEntities) {
            let copyArr = [...this.turnoverSuppliers];
            this.turnoverSuppliers = _.orderBy(copyArr, ['countryName', 'countyName', 'cityName'], ['asc', 'asc', 'asc']);
          }
  
          this.internalSuppliers = this.turnoverSuppliers.filter(f => f.countryId === 180);
          this.intraCommunitySuppliers = this.turnoverSuppliers.filter((f: any) => f.countryId !== 180 && f.isEUMember);
          this.externalSuppliers = this.turnoverSuppliers.filter((f: any) => f.countryId !== 180 && !f.isEUMember);

          this.setCurrentSupplierIndex(0);
        } else {
          this.turnoverSuppliers = [];
          this.setCurrentSupplierIndex(0);
        }
  
        this.currentPage = 0;
        this.loaded = false;
      });
    }
  }

  getMonthQuantity(monthsData: any[], columnField: string): number {
    if (!monthsData || !columnField) return 0;

    const monthYear = this.convertToYearMonth(columnField);
    const monthEntry = monthsData.find(entry => this.formatMonth(entry.month) === monthYear);
  
    return monthEntry ? monthEntry.quantity : 0;
  }
  
  convertToYearMonth(displayedMonth: string): string {
    const monthsMap: { [key: string]: string } = {
      'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06',
      'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'
    };
  
    const [shortMonth, year] = displayedMonth.split('-');
    return `${year}-${monthsMap[shortMonth]}`;
  }
  
  formatMonth(month: any): string {
    if (!month) return '';
  
    const date = new Date(month);
    const year = date.getFullYear();
    const monthFormatted = ('0' + (date.getMonth() + 1)).slice(-2);
  
    return `${year}-${monthFormatted}`;
  }

  onSupplyClick() {
    
  }

  async onOnlyToSupplyOptionsChange(event: any) {
    let filter = new TurnoverFilter();

    if (this.onlyToSupplyFilter.isOnlyToSupply === true && event && event.value) {
      this.loaded = true;
      await this.getTurnOver().then(x => this.loaded = false);
    }
  }

  shouldSolveTurnover(turnoverItem: any) {
    let r3r12Max = Math.max(turnoverItem.rulajReal3M, turnoverItem.rulajReal6M, turnoverItem.rulajReal9M, turnoverItem.rulajReal12M);
    let itemTurnoverAnalysis = this.itemTurnoverAnalysis.find(f => f.itemId === turnoverItem.itemId);
    
    if (itemTurnoverAnalysis) {
      if (itemTurnoverAnalysis.manualTurnover && itemTurnoverAnalysis.alerting !== 'Imediat' && new Date(moment(itemTurnoverAnalysis.alerting, 'DD.MM.YYYY').toDate()) < new Date()) {
        return true;
      }
    }

    if ((r3r12Max < turnoverItem.itemTurnover * turnoverItem.bottomMargin) || (r3r12Max > turnoverItem.itemTurnover * turnoverItem.topMargin)) {
      if (itemTurnoverAnalysis) {

        if (itemTurnoverAnalysis.alerting !== 'Imediat' && new Date(itemTurnoverAnalysis.alerting) > new Date()) {
          return false;
        }
      }

      return true;
    }

    return false;
  }

  shouldAlertAboutTurnover(turnoverItem: any) {
    let itemTurnoverAnalysis = this.itemTurnoverAnalysis.find(f => f.itemId === turnoverItem.itemId);
   
    if (itemTurnoverAnalysis && itemTurnoverAnalysis.isModified) { 
      return true;
    }

    return false;
  }

  onSolveTurnoverClick(turnoverItem: any) {
    if (!turnoverItem.itemTurnover && turnoverItem.currentStock && !this.isSupplyDirectorFlag) {
      return;
    }

    if (!this.shouldSolveTurnover(turnoverItem)) {
      this.notificationService.alert('top', 'center', 'Rulajul este OK!', NotificationTypeEnum.Green, true);
      return;
    }

    this.isSolveTurnoverPopupVisible = true;
    this.selectedSolveTurnoverItem = turnoverItem.itemCode + " - " + turnoverItem.itemName;
    this.selectedSolveTurnoverAutomaticTurnover = turnoverItem.automaticTurnover > 1 ? turnoverItem.automaticTurnover : 2;
    this.selectedSolveTurnoverItemId = turnoverItem.itemId;
    this.selectedSolveTurnverOtherTurnover = null;
    this.selectedAlertTurnover = null;

    if (this.shouldAlertAboutTurnover(turnoverItem)) {
      this.selectedAlertTurnover = turnoverItem.alerting;
    }
  }

  async getItemTurnoverAnalysis() {
    await this.itemTurnoverAnalysisService.getAllAsync().then(async analysisItems => { 
      if (analysisItems) {
        this.itemTurnoverAnalysis = analysisItems;
      }
    })
  }

  async onCheckSolveTurnoverClick(solutionType: string) {
    let selectedItem = this.items.store.find(f => f.id === this.selectedSolveTurnoverItemId);
    let item = new Item();
    item = selectedItem;
    item.turnOver = solutionType === 'automatic' ? this.selectedSolveTurnoverAutomaticTurnover : this.selectedSolveTurnverOtherTurnover;

    this.loaded = true;
    await this.itemsService.updateItemAsync(item).then(async r => {
      if (r) { 
       
        let itemTurnoverAnalysis = this.itemTurnoverAnalysis.find(f => f.itemId === item.id);

        if (itemTurnoverAnalysis) {
          itemTurnoverAnalysis.itemId = item.id;
          itemTurnoverAnalysis.manualTurnover = solutionType === 'other' ? this.selectedSolveTurnverOtherTurnover : null;
          itemTurnoverAnalysis.isModified = solutionType === 'other' ? false : true;

          const today = moment();
          const nextMonth = today.clone().add(1, 'months').endOf('month');
          const nextMonthDate = nextMonth.format('DD.MM.YYYY');
          itemTurnoverAnalysis.alerting = nextMonthDate;

          await this.itemTurnoverAnalysisService.updateAsync(itemTurnoverAnalysis).then(async (r) => {
            if (r) {
              await this.getItems();
              await this.getItemTurnoverAnalysis();
            }
           });
        } else {
          let newRow = new ItemTurnoverAnalysis();
          newRow.itemId = item.id;
          newRow.manualTurnover = solutionType === 'other' ? this.selectedSolveTurnverOtherTurnover : null;
          newRow.isModified = solutionType === 'other' ? false : true;

          const today = moment();
          const nextMonth = today.clone().add(1, 'months').endOf('month');
          const nextMonthDate = nextMonth.format('DD.MM.YYYY');
          newRow.alerting = nextMonthDate;

          await this.itemTurnoverAnalysisService.createAsync(newRow).then(async (r) => {
            await this.getItems();
            await this.getItemTurnoverAnalysis();
          });
        } 

        await this.getTurnOver(true).then(x => this.loaded = false);
        this.isSolveTurnoverPopupVisible = false;
        this.notificationService.alert('top', 'center', 'Rulajul a fost rezolvat cu succes!', NotificationTypeEnum.Green, true);
      } else {
        this.notificationService.alert('top', 'center', 'Rulajul nu a fost rezolvat! A aparut o eroare! Verificati Codul / DenumireRO / Denumire ENG sa fie unice!', NotificationTypeEnum.Red, true);
        this.loaded = false;
      }
    });
  }

  async onDontCheckTurnoverClick() { 
    let itemTurnoverAnalysis = this.itemTurnoverAnalysis.find(f => f.itemId === this.selectedSolveTurnoverItemId);

    if (itemTurnoverAnalysis) {
      itemTurnoverAnalysis.itemId = this.selectedSolveTurnoverItemId;
      itemTurnoverAnalysis.isModified = true;
      const today = moment();
      const nextMonth = today.clone().add(1, 'months').endOf('month');
      const nextMonthDate = nextMonth.format('DD.MM.YYYY');
      itemTurnoverAnalysis.alerting = nextMonthDate;

      this.loaded = true;
      await this.itemTurnoverAnalysisService.updateAsync(itemTurnoverAnalysis).then(async (r) => {
        if (r) {
          await this.getItems();
          await this.getItemTurnoverAnalysis();
        }
        });
    } else {
      let newRow = new ItemTurnoverAnalysis();
      newRow.itemId = this.selectedSolveTurnoverItemId;
      newRow.isModified = true;
      const today = moment();
      const nextMonth = today.clone().add(1, 'months').endOf('month');
      const nextMonthDate = nextMonth.format('DD.MM.YYYY');
      newRow.alerting = nextMonthDate;

      this.loaded = true;
      await this.itemTurnoverAnalysisService.createAsync(newRow).then(async (r) => {
        if (r) {
          await this.getItems();
          await this.getItemTurnoverAnalysis();
        }
       });
    } 

    await this.getTurnOver(true).then(x => this.loaded = false);
    this.isSolveTurnoverPopupVisible = false;
    this.notificationService.alert('top', 'center', 'Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
  }

  isSupplyAlertShown(item: any): boolean {  
    let selectedItem = this.items.store.find(f => f.id === item.itemId);
    let supplier: any = this.turnoverSuppliers.find(f => f.supplierId == selectedItem.partnerId);

    let stockAlertInternal = this.supplierOrigin.find(s => s.code === 'INT');
    let stockAlertExternal = this.supplierOrigin.find(s => s.code === 'EXT');
    let stockAlertIntraCommunity = this.supplierOrigin.find(s => s.code === 'INTRACOM');
  
    if (!stockAlertInternal && !stockAlertExternal && !stockAlertIntraCommunity) {
      return false;
    }
  
    let maxR3R6R9R12 = Math.max(item.rulajReal3M, item.rulajReal6M, item.rulajReal9M, item.rulajReal12M);
    let maxRR3R6R9R12 = Math.max(item.itemTurnover, item.rulajReal3M, item.rulajReal6M, item.rulajReal9M, item.rulajReal12M);
  
    let getComparisonProperty = () => {
      return item.itemTurnover;     
    };
  
    let comparisonProperty = getComparisonProperty();
    if (comparisonProperty === null) {
      return false;
    }
            
    let stockToBeSuppliedInternal = stockAlertInternal ? comparisonProperty * stockAlertInternal.stockAlertMultiplier : 0;
    let stockToBeSuppliedExternal = stockAlertExternal ? comparisonProperty * stockAlertExternal.stockAlertMultiplier : 0;
    let stockToBeSuppliedIntraCommunity = stockAlertIntraCommunity ? comparisonProperty * stockAlertIntraCommunity.stockAlertMultiplier : 0;
  
    let itemTurnoverAnalysis = this.itemTurnoverAnalysis.find(f => f.itemId === item.itemId);

    return (
      (!itemTurnoverAnalysis || (itemTurnoverAnalysis && (!itemTurnoverAnalysis.supplyAlertState || itemTurnoverAnalysis.supplyAlertState === 1))) &&
      item.itemTurnover >= 2 &&
      ((stockAlertInternal && supplier.countryId === 180 && item.currentStock < stockToBeSuppliedInternal) ||
      (stockAlertExternal && supplier.countryId !== 180 && !supplier.isEUMember && item.currentStock < stockToBeSuppliedExternal) ||
      (stockAlertIntraCommunity && supplier.countryId !== 180 && supplier.isEUMember && item.currentStock < stockToBeSuppliedIntraCommunity))
    );
  }  

  isDontSupplyAlertShown(item: any): boolean {
    let selectedItem = this.items.store.find(f => f.id === item.itemId);
    let supplier: any = this.turnoverSuppliers.find(f => f.supplierId == selectedItem.partnerId);

    let stockAlertInternal = this.supplierOrigin.find(s => s.code === 'INT');
    let stockAlertExternal = this.supplierOrigin.find(s => s.code === 'EXT');
    let stockAlertIntraCommunity = this.supplierOrigin.find(s => s.code === 'INTRACOM');
  
    if (!stockAlertInternal && !stockAlertExternal && !stockAlertIntraCommunity) {
      return false;
    }
  
    let maxR3R6R9R12 = Math.max(item.rulajReal3M, item.rulajReal6M, item.rulajReal9M, item.rulajReal12M);
    let maxRR3R6R9R12 = Math.max(item.itemTurnover, item.rulajReal3M, item.rulajReal6M, item.rulajReal9M, item.rulajReal12M);
  
    let getComparisonProperty = () => {
      return item.itemTurnover;     
    };
  
    let comparisonProperty = getComparisonProperty();
    if (comparisonProperty === null) {
      return false;
    }
            
    let stockToBeSuppliedInternal = stockAlertInternal ? comparisonProperty * stockAlertInternal.stockAlertMultiplier : 0;
    let stockToBeSuppliedExternal = stockAlertExternal ? comparisonProperty * stockAlertExternal.stockAlertMultiplier : 0;
    let stockToBeSuppliedIntraCommunity = stockAlertIntraCommunity ? comparisonProperty * stockAlertIntraCommunity.stockAlertMultiplier : 0;
  
    let itemTurnoverAnalysis = this.itemTurnoverAnalysis.find(f => f.itemId === item.itemId);

    return (
      (itemTurnoverAnalysis && (itemTurnoverAnalysis.supplyAlertState === 2)) &&
      item.itemTurnover >= 2 &&
      ((stockAlertInternal && supplier.countryId === 180 && item.currentStock < stockToBeSuppliedInternal) ||
      (stockAlertExternal && supplier.countryId !== 180 && !supplier.isEUMember && item.currentStock < stockToBeSuppliedExternal) ||
      (stockAlertIntraCommunity && supplier.countryId !== 180 && supplier.isEUMember && item.currentStock < stockToBeSuppliedIntraCommunity))
    );
  }

  async onSupplyAlertClick(item: any) {
    let itemTurnoverAnalysis = this.itemTurnoverAnalysis.find(f => f.itemId === item.itemId);

    if (itemTurnoverAnalysis) {
      itemTurnoverAnalysis.supplyAlertState = 2; // alert turned off
      const today = new Date();
      const futureDate = new Date();
      futureDate.setDate(today.getDate() + 30);
      itemTurnoverAnalysis.nextSupplyAlertDate = futureDate;

      this.loaded = true;
      await this.itemTurnoverAnalysisService.updateAsync(itemTurnoverAnalysis).then(async (r) => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Alerta de aprovizionare a fost oprita!', NotificationTypeEnum.Green, true);       
        } else {
          this.notificationService.alert('top', 'center', 'Alerta de aprovizionare nu a fost oprita! A aparut o eroare!', NotificationTypeEnum.Red, true);
        }
        await this.getTurnOver(true);
        await this.getItemTurnoverAnalysis();
        this.loaded = false;
      });
    } else {
      let newRow = new ItemTurnoverAnalysis();
      newRow.itemId = item.itemId;
      newRow.supplyAlertState = 2; // // alert turned off
      const today = new Date();
      const futureDate = new Date();
      futureDate.setDate(today.getDate() + 30);
      newRow.nextSupplyAlertDate = futureDate;

      this.loaded = true;
      await this.itemTurnoverAnalysisService.createAsync(newRow).then(async (r) => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Alerta de aprovizionare a fost oprita!', NotificationTypeEnum.Green, true);       
        } else {
          this.notificationService.alert('top', 'center', 'Alerta de aprovizionare nu a fost oprita! A aparut o eroare!', NotificationTypeEnum.Red, true);
        }
        await this.getTurnOver(true);
        await this.getItemTurnoverAnalysis();
        this.loaded = false;
      });
    } 
  }

  async onDontSupplyAlertClick(item: any) {
    let itemTurnoverAnalysis = this.itemTurnoverAnalysis.find(f => f.itemId === item.itemId);

    if (itemTurnoverAnalysis) {
      itemTurnoverAnalysis.supplyAlertState = 1; // alert turned on
      itemTurnoverAnalysis.nextSupplyAlertDate = null;

      this.loaded = true;
      await this.itemTurnoverAnalysisService.updateAsync(itemTurnoverAnalysis).then(async (r) => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Alerta de aprovizionare a fost pornita!', NotificationTypeEnum.Green, true);       
        } else {
          this.notificationService.alert('top', 'center', 'A aparut o eroare!', NotificationTypeEnum.Red, true);
        }
        await this.getTurnOver(true);
        await this.getItemTurnoverAnalysis();
        this.loaded = false;
      });
    } else {
      let newRow = new ItemTurnoverAnalysis();
      newRow.itemId = item.itemId;
      newRow.supplyAlertState = 1; // alert turned on
      newRow.nextSupplyAlertDate = null;

      this.loaded = true;
      await this.itemTurnoverAnalysisService.createAsync(newRow).then(async (r) => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Alerta de aprovizionare a fost pornita!', NotificationTypeEnum.Green, true);       
        } else {
          this.notificationService.alert('top', 'center', 'A aparut o eroare!', NotificationTypeEnum.Red, true);
        }
        await this.getTurnOver(true);
        await this.getItemTurnoverAnalysis();
        this.loaded = false;
      });
    } 
  }

  onSupplierNameClick() {
    var url = environment.SRMPrimaware + '/' + Constants.partner + '?id=' + this.turnoverSuppliers[this.currentSupplierIndex].supplierId;
    window.open(url, '_blank');
  }

  async getFilteredPartners() {
    let searchFilter = new PartnerSearchFilter();
    searchFilter.partnerType = [];
    searchFilter.partnerType.push(2);
    searchFilter.isActive = true;
    this.partnerService.getPartnersByFilter(searchFilter).then(items => {
      this.filteredPartners = {
        paginate: true,
        pageSize: 15,
        store: (items && items.length > 0) ? items : []
      };
    }) 
  }  

  updatePagedItems() {
    const start = this.currentPage * this.pageSize;
    const end = start + this.pageSize;
    this.pagedTurnOverItems = this.turnOverItems.slice(start, end);
    this.calculateTurnoverMonths();
  }

  get totalPages() {
    return Math.ceil(this.turnOverItems.length / this.pageSize);
  }
  
  prevPage() {
    if (this.currentPage > 0) {
      this.currentPage--;
      this.updatePagedItems();
    }
  }
  
  nextPage() {
    if (this.currentPage < this.totalPages - 1) {
      this.currentPage++;
      this.updatePagedItems();
    }
  }
}
