import { Component, OnInit, ViewChild, AfterViewInit, HostListener } from '@angular/core';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { Post } from 'app/models/post.model';
import { PostService } from 'app/services/post.service';
import { DxDataGridComponent, DxTooltipComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { Constants } from 'app/constants';
import { PageActionService } from 'app/services/page-action.service';
import { ItemService } from 'app/services/item.service';
import { ActivatedRoute } from '@angular/router';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';
// import { ContactCampaignService } from 'app/services/contact-campaign-management.service';
// import { ContactCampaign } from 'app/models/contact-campaign.model';
// import { ContactCampaignPartners } from 'app/models/contact-campaign-partners.model';
// import { ContactCampaignPartnersService } from 'app/services/contact-campaign-partners.service';
import { UsersService } from 'app/services/user.service';
import { User } from 'app/models/user.model';
import { HelperService } from 'app/services/helper.service';
// import { ContactCampaignTypeEnum } from 'app/enums/contactCampaignTypeEnum';
import { environment } from 'environments/environment';
import { PartnerTypeEnum } from 'app/enums/partnerTypeEnum';
import { ClientTypeEnum } from 'app/enums/clientTypeEnum';
import { SupplierTypeEnum } from 'app/enums/supplierTypeEnum';
import { CompetitorTypeEnum } from 'app/enums/competitorTypeEnum';
import { PersonTypeEnum } from 'app/enums/personTypeEnum';
import { PublicInstitutionEnum } from 'app/enums/publicInstitutionTypeEnum';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { Country } from 'app/models/country.model';
import { County } from 'app/models/county.model';
import { City } from 'app/models/city.model';
import { Language } from 'app/models/language.model';
import { PartnerActivity } from 'app/models/partneractivity.model';
import { PartnerActivityAllocation } from 'app/models/partneractivityallocation.model';
import { PartnerActivityAllocationService } from 'app/services/partner-activity-allocation.service';
import { PartnerLegalFormService } from 'app/services/partner-legal-form.service';
import { PartnerLegalForm } from 'app/models/partnerlegalform.model';
import { CityService } from 'app/services/city.service';
import { LanguageService } from 'app/services/language.service';
import { CountyService } from 'app/services/county.service';
import { CountryService } from 'app/services/country.service';
import { PartnerActivityService } from 'app/services/partner-activity.service';
import * as moment from 'moment';
import { PartnerService } from 'app/services/partner.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { DepartmentsService } from 'app/services/departments.service';
import { Department } from 'app/models/department.model';
import { Office } from 'app/models/office.model';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { Partner } from 'app/models/partner.model';
import { PartnerObservationsDetailsComponent } from 'app/partner-observations-details/partner-observations-details.component';
import { on } from 'devextreme/events';
import { ContactCampaignPartnersService } from 'app/services/contact-campaign-partners.service';

@Component({
  selector: 'app-contact-campaign-partners',
  templateUrl: './contact-campaign-partners.component.html',
  styleUrls: ['./contact-campaign-partners.component.css']
})
export class ContactCampaignPartnersComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('additionalDataGrid') additionalDataGrid: DxDataGridComponent;
  @ViewChild('additionalGridToolbar') additionalGridToolbar: GridToolbarComponent;
  @ViewChild('defaultDataGrid') defaultDataGrid: DxDataGridComponent;
  @ViewChild('defaultGridToolbar') defaultGridToolbar: GridToolbarComponent;
  @ViewChild('partnerObservationsDetailsComponent') partnerObservationsDetailsComponent: PartnerObservationsDetailsComponent;
  @ViewChild(DxTooltipComponent) tooltip: DxTooltipComponent;

  loaded: boolean;
  contactCampaignPartnersLoaded: boolean;
  displayData: boolean;
  actions: IPageActions;

  partners: any;
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  isOwner: boolean = false;
  detailsVisibility: boolean;
  customerId: string;
  items: any;
  itemsGroup: any;
  groupedText: string;
  posts: Post[];
  // contactCampaigns: ContactCampaign[];
  isUserAdminOrHasSuperiorPostValue: boolean;
  users: User[];
  selectedPost: number;
  selectedPersonTypes: any;
  contactCampaignType: { id: number; name: string }[] = [];
  currentDayContactedHappyBirthDayCampaignPartners: number;
  currentDayContactedLocationCampaignPartners: number;
  partnersTypes: { id: number; name: string }[] = [];
  clientTypes: { id: number; name: string }[] = [];
  supplierTypes: { id: number; name: string }[] = [];
  personTypes: { id: number; name: string }[] = [];
  competitorTypes: { id: number; name: string }[] = [];
  publicInstitutions: { id: number; name: string }[] = [];
  partnerSearchFilter: PartnerSearchFilter;
  countries: Country[] = [];
  countriesDS: any;
  counties: County[] = [];
  countiesDS: any;
  cities: City[] = [];
  language: Language[] = [];
  citiesDS: any;
  partnerActivity: PartnerActivity[] = [];
  partnerActivityAllocation: PartnerActivityAllocation[] = [];
  partnerLegalForms: PartnerLegalForm[] = [];
  shouldActivateContactCampaignPartners: boolean;
  itemSmall: any;
  itemGroupSmall: any;

  infoButton: string;

  dataGridColumnsWidth: any;
  isPopupVisible:boolean = false;
  toolbarData: any = {};
  baseContactCampaignPartners: any;
  additionalContactCampaignPartners: any;
  happyBirthDayCampaignTooltipData: string;
  locationCampaignTooltipData: string;
  isHappyBirthDayCampaignCountTooltipVisible: boolean;
  isLocationCampaignCountTooltipVisible: boolean;
  happyBirthdayCampaignPartners: any;
  locationCampaignPartners: any;
  nirDocumentHIds: string;

  isUserAdminOrHasSuperiorPostSent = this.isUserAdminOrHasSuperiorPost.bind(this);
  getDisplayExprPostsSent = this.getDisplayExprPosts.bind(this);


  constructor(private authenticationService: AuthService,
    private translationService: TranslateService, private pageActionService: PageActionService,
    private notificationService: NotificationService, private itemsService: ItemService, private route: ActivatedRoute, private helperService: HelperService,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService, private postService: PostService, private userService: UsersService, private contactCampaignPartnersService: ContactCampaignPartnersService,
    private partnerActivityAllocationService: PartnerActivityAllocationService, private partnerLegalFormService: PartnerLegalFormService, private cityService: CityService,
    private languageService: LanguageService, private countyService: CountyService, private countryService: CountryService, private partnerActivityService: PartnerActivityService,
    private partnerService: PartnerService, private itemService: ItemService, private departmentsService: DepartmentsService, private usersService: UsersService) {
    this.getDisplayExprPosts = this.getDisplayExprPosts.bind(this);
    this.getDisplayExprContactCampaigns = this.getDisplayExprContactCampaigns.bind(this);
    this.getDisplayExprPartnerActivity = this.getDisplayExprPartnerActivity.bind(this);

    this.shouldActivateContactCampaignPartners = true;

    for (let n in PartnerTypeEnum) {
      if (typeof PartnerTypeEnum[n] === 'number') {
        this.partnersTypes.push({
          id: <any>PartnerTypeEnum[n],
          name: this.translationService.instant('partner.' + n)
        });
      }
    }
    for (let n in ClientTypeEnum) {
      if (typeof ClientTypeEnum[n] === 'number') {
        this.clientTypes.push({
          id: <any>ClientTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    for (let n in SupplierTypeEnum) {
      if (typeof SupplierTypeEnum[n] === 'number') {
        this.supplierTypes.push({
          id: <any>SupplierTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    for (let n in CompetitorTypeEnum) {
      if (typeof CompetitorTypeEnum[n] === 'number') {
        this.competitorTypes.push({
          id: <any>CompetitorTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    for (let n in PersonTypeEnum) {
      if (typeof PersonTypeEnum[n] === 'number') {
        this.personTypes.push({
          id: <any>PersonTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    for (let n in PublicInstitutionEnum) {
      if (typeof PublicInstitutionEnum[n] === 'number') {
        this.publicInstitutions.push({
          id: <any>PublicInstitutionEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    this.partnerSearchFilter = new PartnerSearchFilter();
    this.partnerSearchFilter.isActive = true;

    this.getInitialData();

    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();

    this.partners = [];

    
  }

 

  getInitialData() {
    this.loaded = true;
    Promise.all([this.setSalesAgentUsers(), this.getUsers(), this.getPartnerActicityAllocations(), this.getSeniorNIRDocumentsToGenerate(),
      this.getPartnerLegalForms(), this.getCities(), this.getLanguages(), this.getAllItems(), this.getAllItemGroups()]).then(x => {
      this.getCounties(), this.getCountries(), this.getPartnerActivity()

      if (this.isUserAdminOrHasSuperiorPost()) {
        
        if (this.selectedPost) {
          this.getData(this.selectedPost);
        }
      }
      else {
        let post = this.posts?.find(x => x.id === this.users.find(x => x.id === Number(this.authenticationService.getUserId()))?.postId);
        this.selectedPost = post?.id;
        this.posts = [post];
        this.getData(post?.id);
      }
      this.loaded = false;
    });
  }

  ngOnInit(): void {
   this.setDataGridColumnsWidth();
  }

  @HostListener('document:keydown', ['$event'])
  onKeydown(event: KeyboardEvent) {
    if (event.key === 'F5') {
      event.preventDefault(); 
      this.refreshDataGrid();
    }
  }

  async searchEvent(e: any) {
    this.partnerSearchFilter = e
    this.loaded = true;
    await this.getPartners().then(x => this.loaded = false);
  }

  refreshEvent(e: any) {
    this.getInitialData();
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }

    if (this.dataGrid) {
      this.dataGrid.instance.columnOption('isActive', 'filterValue', ['1']);
    }
  }

  public isUserAdminOrHasSuperiorPost(): boolean {
    if (this.authenticationService.isUserAdmin()) {
      return true;
    } else {
      if (this.posts && this.posts.length > 0 && this.users && this.users.length > 0) {
        let post = this.posts?.find(x => x?.id === this.users.find(x => x.id === Number(this.authenticationService.getUserId()))?.postId);
        if (post && (post.isDepartmentSuperior || post.isGeneralManager || post.isLeadershipPost || post.isOfficeSuperior)) {
          return true;
        } else {
          return false;
        }
      }
      return false;
    }
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && !e.data.isActive) {
      e.rowElement.style.backgroundColor = '#ffaf82';
    }
  }

  getData(postId: number, personTypes?: any) {
    if (postId) {
      this.getAutomaticContactCampaignPartners(postId, personTypes);
    }
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  async getAutomaticContactCampaignPartners(postId?: number, personTypes?: any) {
    // this.partners = [];
    // this.contactCampaignPartnersLoaded = true;
    // await this.contactCampaignPartnersService.getAutomaticContactCampaignPartnersAsync(postId, personTypes === undefined ? 1 : personTypes).then(items => {
    //   if (items && items.contactCampaignPartners) {
    //     this.partners = items.contactCampaignPartners.length > 0 ? items.contactCampaignPartners.map(m => {
    //       let country = this.countries.find(f => f.id === m.countryId);
    //       let county = this.counties.find(f => f.id === m.countyId);
    //       let user = this.users.find(f => f.postId == m.salesAgentId);

    //       return {
    //         ...m,
    //         countryName: country ? country.name : '',
    //         countyName: county ? county.name : '',
    //         userName: user ? user.lastName + ' ' + user.firstName : ''
    //       }
    //     }) : [];

    //     this.baseContactCampaignPartners = this.partners.filter(f => !f.isCallReminder);

    //     this.happyBirthdayCampaignPartners = this.partners.filter(f => f.type === ContactCampaignTypeEnum.HappyBirthDay);
    //     this.locationCampaignPartners = this.partners.filter(f => f.type === ContactCampaignTypeEnum.Location);
    //     this.additionalContactCampaignPartners = this.partners.filter(f => f.isCallReminder);

    //     this.currentDayContactedHappyBirthDayCampaignPartners = items.currentDayContactedHappyBirthDayCampaignPartners;
    //     this.currentDayContactedLocationCampaignPartners = items.currentDayContactedLocationCampaignPartners;
    //   }

    //   for (let n in ContactCampaignTypeEnum) {
    //     if (typeof ContactCampaignTypeEnum[n] === 'number') {
    //       this.contactCampaignType.push({
    //         id: <any>ContactCampaignTypeEnum[n],
    //         name: this.translationService.instant(n)
    //       });
    //     }
    //   }

    //   this.contactCampaignPartnersLoaded = false;
    // });
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.partner).then(result => {
      this.actions = result;
      this.actions.CanAdd = false;
    });
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public async refreshDataGrid() {
    if (this.shouldActivateContactCampaignPartners) {
      if (this.selectedPost) {
        this.getAutomaticContactCampaignPartners(this.selectedPost, this.selectedPersonTypes);
      }
    }

    this.dataGrid.instance.refresh();
  }

  public async refreshAdditionalDataGrid() {
    if (this.shouldActivateContactCampaignPartners) {
      if (this.selectedPost) {
        this.getAutomaticContactCampaignPartners(this.selectedPost, this.selectedPersonTypes);
      }
    } 

    this.additionalDataGrid.instance.refresh();
  }

  changeVisible() {
    this.detailsVisibility = false;
    this.refreshDataGrid();
    this.refreshAdditionalDataGrid();
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  async getContactCampaigns() {
    // if (this.isOwner) {
    //   await this.contactCampaignService.getAllAsync().then(data => {
    //     this.contactCampaigns = data;
    //   });
    // } else {
    //   await this.contactCampaignService.getAllByCustomerIdAsync(Number(this.authenticationService.getUserCustomerId())).then(data => {
    //     this.contactCampaigns = data;
    //   });
    // }
  }


  async setSalesAgentUsers() {
    let departments = [];

    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(async items => {
        if (items && items.length > 0) {
          departments = items;
          this.posts = await this.getAllSupplyPosts(departments); 
        }
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(async items => {
        if (items && items.length > 0) {
          departments = items;
          this.posts = await this.getAllSupplyPosts(departments)
        };
      });
    }
  }

  private async getAllSupplyPosts(departments: Department[]): Promise<Post[]> {
    if (!departments || departments.length == 0) 
      return [];

    let validSupplyPosts: Post[] = [];

    await this.usersService.getUsersAsync().then((users) => {
      if (users && users.length > 0) {
        const supplyDepartments: Department[] = departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Supply); 
        const supplyOffices: Office[] = supplyDepartments.reduce((acc: Office[], dept: Department) => acc.concat(dept.offices), []);
        const supplyPosts: Post[] = supplyOffices.reduce((acc: Post[], office: Office) => acc.concat(office.posts), []);
        const activeSupplyPosts = supplyPosts.filter((post) => post.isActive === true);

        validSupplyPosts = activeSupplyPosts.filter(f => users.map(m => m.postId).includes(f.id));
      }
    });

    return validSupplyPosts;
  }


  async getUsers() {
    if (this.isOwner) {
      await this.userService.getUsersAsync().then(items => {
        this.users = items;
      });
    } else {
      await this.userService.getUserAsyncByID().then(items => {
        this.users = items;
      });
    }
  }

  async getSeniorNIRDocumentsToGenerate() {
    await this.contactCampaignPartnersService.getSeniorNIRDocumentsToGenerateAsync().then(items => {
      this.nirDocumentHIds = (items && items.length > 0) ? items.join(', ') : '';
    });
  }

  getDisplayExprPosts(item) {
    if (!item) {
      return '';
    }

    return item.code + ' - ' + item.companyPost;
  }

  getDisplayExprContactCampaigns(item) {
    if (!item) {
      return '';
    }

    return item.code + ' - ' + item.name;
  }

  onPostChange(event) {
    if (event && event.value) {
      this.selectedPost = event.value;
      this.getData(this.selectedPost, this.selectedPersonTypes);
    }
  }

  onPersonTypeChange(event) {
      this.selectedPersonTypes = event.value;
  }

  onActivateContactCampaignPartnersChange(event) {
    this.partners = [];

    if (event) {
      if (this.isUserAdminOrHasSuperiorPost()) { 
        if (this.selectedPost) {
          this.getData(this.selectedPost);
        }
      }
      else {
        let post = this.posts?.find(x => x.id === this.users.find(x => x.id === Number(this.authenticationService.getUserId()))?.postId);
        this.selectedPost = post?.id;
        this.posts = [post];
        this.getData(post?.id);
      }
    }

    this.shouldActivateContactCampaignPartners = event;
    this.setDataGridColumnsWidth();
  }

  openDetails(row: any) {
    var url = environment.SRMPrimaware + '/' + Constants.partner + '?id=' + row.id;
    window.open(url, '_blank');
  }

  async getPartnerActicityAllocations(): Promise<any> {
    await this.partnerActivityAllocationService.getAllPartnerActivityAllocationAsync().then(items => {
      this.partnerActivityAllocation = items;
    });
  }

  async getPartnerLegalForms() {
    await this.partnerLegalFormService.getAllPartnerLegalFormAsync().then(x => {
      this.partnerLegalForms = x;
    });
  }

  async getCities() {
    await this.cityService.getAllCitiesSmallAsync().then(items => {
      this.cities = items;
      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities
      };
    });
  }

  async getLanguages() {
    await this.languageService.getAllLanguagesAsync().then(items => {
      this.language = items;
    });
  }

  async getCounties() {
    await this.countyService.getAllCountysAsync().then(items => {
      this.counties = items;
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties
      };
    });
  }

  
  async getCountries() {
    await this.countryService.getAllCountrysAsync().then(items => {
      this.countries = items;
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries
      };
    });
  }

  async getPartnerActivity() {
    await this.partnerActivityService.getAllPartnerActivityAsync().then(items => {
      this.partnerActivity = items;
    });
  }

  async getPartners() {
  //   if (this.partnerSearchFilter && this.partnerSearchFilter.lostPartnersOverXDays && this.partnerSearchFilter.partnersOverXDays) {
  //       this.notificationService.alert('top', 'center', "Alegeti un singur tip de filtrare pentru clientii pierduti!",
  //         NotificationTypeEnum.Red, true);
  //       return;
  //   }

  //   if (this.partnerSearchFilter && this.partnerSearchFilter.lostPartnersOverXDays && ((this.partnerSearchFilter.partnersOverXDaysProductIds && this.partnerSearchFilter.partnersOverXDaysProductIds.length > 0) 
  //     || (this.partnerSearchFilter.partnersOverXDaysCode4ProductIds && this.partnerSearchFilter.partnersOverXDaysCode4ProductIds.length > 0))) {
  //     this.notificationService.alert('top', 'center', "Combinatie de filtre gresita!",
  //       NotificationTypeEnum.Red, true);
  //     return;
  // }

  //   if (this.partnerSearchFilter && this.partnerSearchFilter.partnersOverXDays 
  //     && (!this.partnerSearchFilter.partnersOverXDaysProductIds || this.partnerSearchFilter.partnersOverXDaysProductIds.length === 0) 
  //       && (!this.partnerSearchFilter.partnersOverXDaysCode4ProductIds || this.partnerSearchFilter.partnersOverXDaysCode4ProductIds.length === 0)) {
  //         this.notificationService.alert('top', 'center', "Selectati cel putin un produs simplu sau un produs grupat (Cod4)!",
  //           NotificationTypeEnum.Red, true);
  //     return;
  //   } else {
  //     this.loaded = true;
  //     await this.partnerService.getPartnersFromCrmAsync(this.partnerSearchFilter).then(items => {
  //       if (items) {
  //         this.partners = items;
  //       }
  //       this.loaded = false;
  //     });
  //   }
  }

  async getAllItems(): Promise<any> {
    await this.itemService.getAllItemSmallAsync().then(items => {
      if (items && items.length > 0) {
        this.itemSmall = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      } else {
        this.itemSmall = {
          paginate: true,
          pageSize: 15,
          store: []
        };
      }
    });
  }

  async getAllItemGroups(): Promise<any> {
    // await this.itemService.getAllItemGroupSmallAsync().then(items => {
    //   if (items && items.length > 0) {
    //     this.itemGroupSmall = {
    //       paginate: true,
    //       pageSize: 15,
    //       store: items
    //     };
    //   } else {
    //     this.itemGroupSmall = {
    //       paginate: true,
    //       pageSize: 15,
    //       store: []
    //     };
    //   }
    // });
  }

  getDisplayExprPartnerActivity(value: any) {
    if (value && this.partnerActivity) {
      const item = this.partnerActivity.find(x => x.id === value.partnerActivityId);
      if (item) {
        return value.saleZoneCode + ' - ' + item.name + ' - ' + value.partnerActivityClassification;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  getVdLastYearExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.vdLastYear) {
        item.vdLastYear = this.helperService.roundUp(item.vdLastYear);
        return this.helperService.setNumberWithCommas((item.vdLastYear));
      }
      else {
        return '';
      }
    }
  }

  getVdLast2YearsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.vdLast2Years) {
        item.vdLast2Years = this.helperService.roundUp(item.vdLast2Years);
        return this.helperService.setNumberWithCommas((item.vdLast2Years));
      }
      else {
        return '';
      }
    }
  }

  getPfLastYearExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.pfLastYear) {
        item.pfLastYear = this.helperService.roundUp(item.pfLastYear);
        return this.helperService.setNumberWithCommas((item.pfLastYear));
      }
      else {
        return '';
      }
    }
  }

  getPfLast2YearsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.pfLast2Years) {
        item.pfLast2Years = this.helperService.roundUp(item.pfLast2Years);
        return this.helperService.setNumberWithCommas((item.pfLast2Years));
      }
      else {
        return '';
      }
    }
  }

  public async openPartnerObservationsDetails(row: any) {
    var url = environment.SRMPrimaware + '/' + Constants.partnerObservationsDetails + '?clientId=' + row.id + '&partnerName=' + row.name;
    this.partnerObservationsDetailsComponent.clientId = row.id;
    this.partnerObservationsDetailsComponent.partnerName = row.name;
    this.isPopupVisible = true;
  }

  public goToTurnoverDetails(row: any) {
    // var url = environment.CRMPrimaware + '/' + Constants.turnOver + '?clientId=' + row.id;
    // window.open(url, '_blank');
  }

  private setDataGridColumnsWidth() {
    let columnsWithType = {
      tipCampanie: 7,
      nume: 14,
      site: 4,
      email: 5,
      post: 4,
      ca1: 4,
      pf1: 4,
      na1: 3,
      vd1: 4,
      np1: 3,
      ca2: 4,
      pf2: 4,
      na2: 3,
      vd2: 4,
      np2: 3,
      activitate: 11,
      dataContactare: 4,
      observatii: 14,
      actiuni: 5,
      contactFullName: 5
    };

    let columnsWithoutType = {
      tipCampanie: 0,
      nume: 14,
      site: 4,
      email: 5,
      post: 5,
      ca1: 5,
      pf1: 4,
      na1: 3,
      vd1: 4,
      np1: 3,
      ca2: 5,
      pf2: 4,
      na2: 3,
      vd2: 5,
      np2: 3,
      activitate: 11,
      dataContactare: 4,
      observatii: 13,
      actiuni: 5,
      contactFullName: 5
    };

    this.dataGridColumnsWidth = this.shouldActivateContactCampaignPartners ? columnsWithType : columnsWithoutType;
    
  }

  onCellPrepared(event) {
    if (event.rowType === "data" && event.column.dataField == 'salesAgentId') {
      on(event.cellElement, "mouseover", arg => {
        if (event.data['salesAgentId']) {
          this.tooltip.instance.show(arg.target);
          document.getElementById('tooltipPost').innerHTML = this.getPostName(event.data.salesAgentId);
          document.getElementById('tooltipUser').innerHTML = this.getUserName(event.data.salesAgentId);
          arg.target.title = '';
        }
      });

      on(event.cellElement, "mouseout", arg => {
        this.tooltip.instance.hide();
      });
    }
  }

  private getPostName(postId) {
    return this.posts.find(x => x.id === postId)?.companyPost;
  }

  private getUserName(postId) {
    let user = this.users.find(x => x.postId === postId);
    return user?.firstName + ' ' + user?.lastName;
  }

  onPartnerNameClick(data) {
    if (data) {
      var url = environment.SRMPrimaware + '/' + Constants.partner + '?id=' + data.data.id;
      window.open(url, '_blank');
    }
  }

  getCaLastYearExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.caLastYear) {
        item.caLastYear = this.helperService.roundUp(item.caLastYear);
        return this.helperService.setNumberWithCommas((item.caLastYear));
      }
      else {
        return '';
      }
    }
  }

  getCaLast2YearsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.caLast2Years) {
        item.caLast2Years = this.helperService.roundUp(item.caLast2Years);
        return this.helperService.setNumberWithCommas((item.caLast2Years));
      }
      else {
        return '';
      }
    }
  }

  getNaLastYearExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.naLastYear) {
        item.naLastYear = this.helperService.roundUp(item.naLastYear);
        return this.helperService.setNumberWithCommas((item.naLastYear));
      }
      else {
        return '';
      }
    }
  }

  getNaLast2YearsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.naLast2Years) {
        item.naLast2Years = this.helperService.roundUp(item.naLast2Years);
        return this.helperService.setNumberWithCommas((item.naLast2Years));
      }
      else {
        return '';
      }
    }
  }

  getNpLastYearExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.npLastYear) {
        item.npLastYear = this.helperService.roundUp(item.npLastYear);
        return this.helperService.setNumberWithCommas((item.npLastYear));
      }
      else {
        return '';
      }
    }
  }

  getNpLast2YearsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.npLast2Years) {
        item.npLast2Years = this.helperService.roundUp(item.npLast2Years);
        return this.helperService.setNumberWithCommas((item.npLast2Years));
      }
      else {
        return '';
      }
    }
  }

  async onHbdContactCampaignDescriptionClick(event) {
    event.stopPropagation();

    if (!this.selectedPost) {
      this.notificationService.alert('top', 'center', "Selectati un post!", NotificationTypeEnum.Red, true);
      return;
    }

    this.happyBirthDayCampaignTooltipData = 'Se incarca...'; 
    this.isHappyBirthDayCampaignCountTooltipVisible = true;

    // await this.contactCampaignService.getContactCampaignDescriptionDataAsync(this.selectedPost ?? null).then(resp => {
    //   if (resp && resp.length > 0) {
    //     let happyBirthdayCampaignDescriptions = resp.filter(f => f.type === ContactCampaignTypeEnum.HappyBirthDay);
    //     if (happyBirthdayCampaignDescriptions && happyBirthdayCampaignDescriptions.length > 0) {
    //       this.happyBirthDayCampaignTooltipData = happyBirthdayCampaignDescriptions[0].description;
    //     } else {
    //       this.happyBirthDayCampaignTooltipData = '';
    //     }       
    //   } else {
    //     this.happyBirthDayCampaignTooltipData = '';
    //   }
    // })
  }

  async onLocationContactCampaignDescriptionClick(event) {
    event.stopPropagation();

    if (!this.selectedPost) {
      this.notificationService.alert('top', 'center', "Selectati un post!", NotificationTypeEnum.Red, true);
      return;
    }

    this.locationCampaignTooltipData = 'Se incarca...'; 
    this.isLocationCampaignCountTooltipVisible = true;

    // await this.contactCampaignService.getContactCampaignDescriptionDataAsync(this.selectedPost ?? null).then(resp => {
    //   if (resp && resp.length > 0) {
    //     let locationCampaignDescriptions = resp.filter(f => f.type === ContactCampaignTypeEnum.Location);
    //     if (locationCampaignDescriptions && locationCampaignDescriptions.length > 0) {
    //       this.locationCampaignTooltipData = locationCampaignDescriptions.map(f => f.description).join('\n');
    //     } else {
    //       this.locationCampaignTooltipData = '';
    //     }       
    //   } else {
    //     this.locationCampaignTooltipData = '';
    //   }
    // })
  }

  async getHappyBirthdayContactCampaignTooltipData() {
    this.happyBirthDayCampaignTooltipData = 'Se incarca...'; 
    this.isHappyBirthDayCampaignCountTooltipVisible = true;

    // await this.contactCampaignService.getContactCampaignDescriptionDataAsync(this.selectedPost ?? null).then(resp => {
    //   if (resp && resp.length > 0) {
    //     let happyBirthdayCampaignDescriptions = resp.filter(f => f.type === ContactCampaignTypeEnum.HappyBirthDay);
    //     if (happyBirthdayCampaignDescriptions && happyBirthdayCampaignDescriptions.length > 0) {
    //       this.happyBirthDayCampaignTooltipData = happyBirthdayCampaignDescriptions[0].description;
    //     } else {
    //       this.happyBirthDayCampaignTooltipData = '';
    //     }       
    //   } else {
    //     this.happyBirthDayCampaignTooltipData = '';
    //   }
    // })
  }

  hideHappyBirthdayCampaignCountTooltip(): void {
    this.isHappyBirthDayCampaignCountTooltipVisible = false;
  }

  async getLocationContactCampaignTooltipData() {
    this.locationCampaignTooltipData = 'Se incarca...'; 
    this.isLocationCampaignCountTooltipVisible = true;

    // await this.contactCampaignService.getContactCampaignDescriptionDataAsync(this.selectedPost ?? null).then(resp => {
    //   if (resp && resp.length > 0) {
    //     let locationCampaignDescriptions = resp.filter(f => f.type === ContactCampaignTypeEnum.Location);
    //     if (locationCampaignDescriptions && locationCampaignDescriptions.length > 0) {
    //       this.locationCampaignTooltipData = locationCampaignDescriptions.map(f => f.description).join('\n');
    //     } else {
    //       this.locationCampaignTooltipData = '';
    //     }       
    //   } else {
    //     this.locationCampaignTooltipData = '';
    //   }
    // })
  }

  hideLocaitonCampaignCountTooltip(): void {
    this.isLocationCampaignCountTooltipVisible = false;
  }

  getTypeCellData(data: any) {
    if (!data)
      return '';

    let typeName = this.contactCampaignType.find(f => f.id === data.type);

    // if (data.type === ContactCampaignTypeEnum.HappyBirthDay) {
    //   return `${typeName?.name} (${moment(data.birthday).format('D MMM')})`;
    // } else if (data.type === ContactCampaignTypeEnum.Location) {
    //   return `${typeName?.name} (${data.countryName}, ${data.countyName})`;
    // }
  }

  getSalesAgentIdCellData(data: any) {
    if (!data)
      return '';

    let post = this.posts.find(f => f.id === data.salesAgentId);
    
    return `${post ? post.code : ''} (${data.userName ? data.userName : ''})`;
  }
}
