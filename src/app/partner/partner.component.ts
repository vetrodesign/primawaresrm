import { Component, OnInit, ViewChild, AfterViewInit, Input, OnChanges } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { DuplicatePartner, Partner } from 'app/models/partner.model';
import { PartnerService } from 'app/services/partner.service';
import { PartnerTypeEnum } from 'app/enums/partnerTypeEnum';
import { PartnerDetailsComponent } from 'app/partner-details/partner-details.component';
import { Constants } from 'app/constants';
import { PageActionService } from 'app/services/page-action.service';
import { AddressType } from 'app/models/address-type.model';
import { AddressTypeService } from 'app/services/address-type.service';
import { PartnerLegalForm } from 'app/models/partnerlegalform.model';
import { PartnerLegalFormService } from 'app/services/partner-legal-form.service';
import { ClientTypeEnum } from 'app/enums/clientTypeEnum';
import { SupplierTypeEnum } from 'app/enums/supplierTypeEnum';
import { PersonTypeEnum } from 'app/enums/personTypeEnum';
import { Post } from 'app/models/post.model';
import { PostService } from 'app/services/post.service';
import { PaymentInstrumentService } from 'app/services/payment-instrument.service';
import { PaymentInstrument } from 'app/models/paymentinstrument.model';
import { CaenCodeSpecializationService } from 'app/services/caen-code-specialization.service';
import { DepartmentsService } from 'app/services/departments.service';
import { Department } from 'app/models/department.model';
import { CurrencyRateService } from 'app/services/currency-rate.service';
import { BaseCurrency } from 'app/models/base-currency.model';
import { Country } from 'app/models/country.model';
import { County } from 'app/models/county.model';
import { City } from 'app/models/city.model';
import { PartnerActivity } from 'app/models/partneractivity.model';
import { PartnerActivityService } from 'app/services/partner-activity.service';
import { CityService } from 'app/services/city.service';
import { CountyService } from 'app/services/county.service';
import { CountryService } from 'app/services/country.service';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { PartnerActivityAllocation } from 'app/models/partneractivityallocation.model';
import { PartnerActivityAllocationService } from 'app/services/partner-activity-allocation.service';
import { UsersService } from 'app/services/user.service';
import { User } from 'app/models/user.model';
import { MessagingApp } from 'app/models/messaging-app.model';
import { MessagingAppService } from 'app/services/messaging-app.service';
import { Language } from 'app/models/language.model';
import { LanguageService } from 'app/services/language.service';
import { CompetitorTypeEnum } from 'app/enums/competitorTypeEnum';
import { SiteService } from 'app/services/site.service';
import { Site } from 'app/models/site.model';
import { Tag } from 'app/models/tag.model';
import { TagService } from 'app/services/tag.service';
import { ItemGroupCategory } from 'app/models/itemgroupcategory.model';
import { ItemGroupCategoryService } from 'app/services/item-group-category.service';
import { ItemGroupCode } from 'app/models/itemgroupcode.model';
import { ItemGroupCodeService } from 'app/services/item-group-code.service';
import { ActivatedRoute } from '@angular/router';
import { PublicInstitutionEnum } from 'app/enums/publicInstitutionTypeEnum';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';
import { environment } from 'environments/environment';
import * as moment from 'moment';
import { HelperService } from 'app/services/helper.service';
import { PartnerStatusService } from 'app/services/partner-status.service';
import { PartnerStatus } from 'app/models/partner-status.model';
import { CaenCodeService } from 'app/services/caen-code.service';
import { PartnerSubscriptionAllocationService } from 'app/services/partner-subscription-allocation.service';

@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.css']
})
export class PartnerComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('duplicatePartnersGrid') duplicatePartnersGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('partnerDetails') partnerDetails: PartnerDetailsComponent;

  loaded: boolean;

  actions: IPageActions;
  partners: Partner[] = [];
  users: User[] = [];
  partnerLegalForms: PartnerLegalForm[] = [];
  partnerStatuses: PartnerStatus[] = [];
  posts: Post[] = [];
  sites: Site[] = [];
  tags: Tag[] = [];
  caenSpecializationsDS: any;
  paymentInstruments: PaymentInstrument[] = [];
  addressTypes: AddressType[] = [];
  currencies: BaseCurrency[] = [];
  itemGroupCategories: ItemGroupCategory[] = [];
  selectedRows: any[];
  departments: Department[];
  itemGroupCodes: ItemGroupCode[] = [];
  itemGroupCodesDS: any;
  partnersTypes: { id: number; name: string }[] = [];
  clientTypes: { id: number; name: string }[] = [];
  supplierTypes: { id: number; name: string }[] = [];
  personTypes: { id: number; name: string }[] = [];
  competitorTypes: { id: number; name: string }[] = [];
  publicInstitutions: { id: number; name: string }[] = [];
  countries: Country[] = [];
  countriesDS: any;
  counties: County[] = [];
  countiesDS: any;
  cities: City[] = [];
  language: Language[] = [];
  citiesDS: any;
  messagingApps: MessagingApp[] = [];
  partnerActivity: PartnerActivity[] = [];
  partnerActivityAllocation: PartnerActivityAllocation[] = [];
  selectedRowIndex = -1;
  rowIndex: any;
  isOwner: boolean = false;
  customerId: string;
  selectedPartner: Partner;
  detailsVisibility: boolean;
  groupedText: string;
  partnerSearchFilter: PartnerSearchFilter;

  partnerIdToSearch: number;
  caenGridDataSource: any;
  dataGridPosts: Post[] = [];
  dataGridPartnerActivityAllocations: PartnerActivityAllocation[] = [];

  infoButton: string;
  showDuplicatesValue: boolean;
  inactivateSelected: any;
  displayInactivatePopup: boolean;
  
  multipleDuplicatePartnersInactiatePopup: boolean;
  duplicatePartners: DuplicatePartner[];
  processedDuplicateData: DuplicatePartner[];

  constructor(private partnerService: PartnerService,
    private authenticationService: AuthService,
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private pageActionService: PageActionService,
    private partnerLegalFormService: PartnerLegalFormService,
    private partnerStatusService: PartnerStatusService,
    private addressTypeService: AddressTypeService,
    private partnerActivityAllocationService: PartnerActivityAllocationService,
    private itemGroupCategoryService: ItemGroupCategoryService,
    private itemGroupCodeService: ItemGroupCodeService,
    private postService: PostService,
    private siteService: SiteService,
    private tagService: TagService,
    private route: ActivatedRoute,
    private paymentInstrumentService: PaymentInstrumentService,
    private caenCodeSpecializationService: CaenCodeSpecializationService,
    private departmentsService: DepartmentsService,
    private countryService: CountryService,
    private countyService: CountyService,
    private cityService: CityService,
    private userService: UsersService,
    private languageService: LanguageService,
    private partnerActivityService: PartnerActivityService,
    private currencyService: CurrencyRateService,
    private messagingAppService: MessagingAppService,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService,
    private helperService: HelperService,
    private caenCodeService: CaenCodeService,
    private partnerSubscriptionAllocationService: PartnerSubscriptionAllocationService) {

    this.getDisplayExprPartnerActivity = this.getDisplayExprPartnerActivity.bind(this);

    for (let n in PartnerTypeEnum) {
      if (typeof PartnerTypeEnum[n] === 'number') {
        this.partnersTypes.push({
          id: <any>PartnerTypeEnum[n],
          name: this.translationService.instant('partner.' + n)
        });
      }
    }
    for (let n in ClientTypeEnum) {
      if (typeof ClientTypeEnum[n] === 'number') {
        this.clientTypes.push({
          id: <any>ClientTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    for (let n in SupplierTypeEnum) {
      if (typeof SupplierTypeEnum[n] === 'number') {
        this.supplierTypes.push({
          id: <any>SupplierTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    for (let n in CompetitorTypeEnum) {
      if (typeof CompetitorTypeEnum[n] === 'number') {
        this.competitorTypes.push({
          id: <any>CompetitorTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    for (let n in PersonTypeEnum) {
      if (typeof PersonTypeEnum[n] === 'number') {
        this.personTypes.push({
          id: <any>PersonTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    for (let n in PublicInstitutionEnum) {
      if (typeof PublicInstitutionEnum[n] === 'number') {
        this.publicInstitutions.push({
          id: <any>PublicInstitutionEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    this.partnerSearchFilter = new PartnerSearchFilter();
    this.partnerSearchFilter.isActive = true;
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();
    this.customerId = this.authenticationService.getUserCustomerId();
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
    this.getData();

     
  }

 

  getData() {
    this.loaded = true;
    Promise.all([this.getSpecializations(), this.getPartnerActicityAllocations(), this.getItemGroupCategory(), this.getItemGroupCode(),
    this.getPartnerLegalForms(), this.getPartnerStatuses(), this.getCities(), this.getUsers(), this.getLanguages(),
    this.getAddressTypes(), this.getPosts(), this.getPaymentInstruments(), this.getSites(),
    this.getBaseCurrency(), this.getDepartments(), this.getMessagingApps(), this.getCaenCodes()]).then(x => {
      this.getCounties(), this.getCountries(), this.getPartnerActivity(), this.getTags()

      if (this.route.snapshot.queryParamMap.get('phone') !== undefined && this.route.snapshot.queryParamMap.get('phone') !== null) {
        this.partnerSearchFilter.phone = this.route.snapshot.queryParamMap.get('phone');
        this.getPartners().then(i => {
          if (this.partners && this.partners.length === 1) {
            this.openDetails(this.partners[0]);
          }
        });
      }

      if (this.route.snapshot.queryParamMap.get('id') !== undefined && this.route.snapshot.queryParamMap.get('id') !== null) {
        this.partnerIdToSearch = Number(this.route.snapshot.queryParamMap.get('id'));
        this.getPartner().then(i => {
          if (this.partners && this.partners.length === 1) {
            this.openDetails(this.partners[0]);
          }
        });
      }

      this.loaded = false
    });
  }

  ngOnInit(): void {
  }

  canExport() {
    return this.actions.CanExport;
  }

  async searchEvent(e: any) {
    this.partnerSearchFilter = e
    this.loaded = true;
    if (this.partnerSearchFilter.haveSubscriptions) {
      await this.partnerService.getPartnersWithUnresolvedSubscriptions().then(x => {
        this.partners = x;
        this.loaded = false
      });
    } else {
      await this.getPartners().then(x => this.loaded = false);
    }
  }

  refreshEvent(e: any) {
    this.getData();
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  async getCaenCodes() {
    await this.caenCodeService.getAllCaenCodesAsync().then(async caenCodes => {
      this.caenGridDataSource = {
        paginate: true,
        pageSize: 15,
        store: caenCodes
      };
    });
  }

  async getCities() {
    await this.cityService.getAllCitiesSmallAsync().then(items => {
      this.cities = items;
      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities
      };
    });
  }

  async getCounties() {
    await this.countyService.getAllCountysAsync().then(items => {
      this.counties = items;
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties
      };
    });
  }

  async getTags() {
    if (this.isOwner) {
      await this.tagService.getAllTagsAsync().then(items => {
        this.tags = (items && items.length > 0) ? items : [];
      });
    } else {
      await this.tagService.getAllTagsByCustomerIdAsync().then(items => {
        this.tags = (items && items.length > 0) ? items : [];
      });
    }
  }

  async getItemGroupCategory(): Promise<any> {
    if (this.isOwner) {
      await this.itemGroupCategoryService.getAllItemGroupCategorysAsync().then(items => {
        this.itemGroupCategories = items;
      });
    } else {
      await this.itemGroupCategoryService.getItemGroupCategoryAsyncByID().then(items => {
        this.itemGroupCategories = items;
      });
    }
  }

  async getItemGroupCode(): Promise<any> {
    if (this.isOwner) {
      await this.itemGroupCodeService.getAllItemGroupCodesAsync().then(items => {
        this.itemGroupCodes = items;
        this.itemGroupCodesDS = {
          paginate: true,
          pageSize: 15,
          store: this.itemGroupCodes
        };
      });
    } else {
      await this.itemGroupCodeService.getItemGroupCodesAsyncByID().then(items => {
        this.itemGroupCodes = items;
        this.itemGroupCodesDS = {
          paginate: true,
          pageSize: 15,
          store: this.itemGroupCodes
        };
      });
    }
  }

  async getPartnerActivity() {
    await this.partnerActivityService.getAllPartnerActivityAsync().then(items => {
      this.partnerActivity = items;
    });
  }

  async getMessagingApps() {
    await this.messagingAppService.getAllAsync().then(items => {
      this.messagingApps = items;
    });
  }

  async getSites() {
    if (this.isOwner) {
      await this.siteService.getAllCustomerLocationsAsync().then(items => {
        this.sites = items;
      });
    } else {
      await this.siteService.getCustomerLocationsAsyncByID().then(items => {
        this.sites = items;
      });
    }
  }

  async getCountries() {
    await this.countryService.getAllCountrysAsync().then(items => {
      this.countries = items;
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries
      };
    });
  }

  async getSpecializations() {
    await this.caenCodeSpecializationService.getAllCaenCodesSpecializationsAsync().then(items => {
      this.caenSpecializationsDS = {
        paginate: true,
        pageSize: 15,
        store: items
      };
    });
  }

  getSecondDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.description;
  }

  async getBaseCurrency(): Promise<any> {
    await this.currencyService.getBaseCurrency().then(x => {
      this.currencies = x;
    });
  }

  async getUsers(): Promise<any> {
    await this.userService.getUserAsyncByID().then(x => {
      this.users = x;
    });
  }

  async getLanguages() {
    await this.languageService.getAllLanguagesAsync().then(items => {
      this.language = items;
    });
  }

  async getPartnerLegalForms() {
    await this.partnerLegalFormService.getAllPartnerLegalFormAsync().then(x => {
      this.partnerLegalForms = x;
    });
  }

  async getPartnerStatuses() {
    await this.partnerStatusService.getAllPartnerStatusesAsync().then(x => {
      this.partnerStatuses = x;
    });
  }

  async getPartners() {
    this.dataGridPosts = [...this.posts];
    this.dataGridPartnerActivityAllocations = [...this.partnerActivityAllocation];

    this.loaded = true;
    await this.partnerService.getPartnersByFilter(this.partnerSearchFilter).then(items => {
      if (items) {
        this.partners = items;

        let relevantPartnerSupplierAgentIds = items.map(m => m.supplierAgentId);
        this.dataGridPosts = this.dataGridPosts.filter(f => relevantPartnerSupplierAgentIds.includes(f.id));

        let relevantPartnerActivityAllocations = items.map(m => m.partnerActivityAllocationId);
        this.dataGridPartnerActivityAllocations = this.dataGridPartnerActivityAllocations.filter(f => relevantPartnerActivityAllocations.includes(f.id));
      }
      this.loaded = false;
    });
  }

  async getPartner() {
    this.loaded = true;
    await this.partnerService.getByPartnerId(this.partnerIdToSearch).then(items => {
      if (items) {
        this.partners = [items];
      }
      this.loaded = false;
    });
  }

  async getPartnerActicityAllocations(): Promise<any> {
    await this.partnerActivityAllocationService.getAllPartnerActivityAllocationAsync().then(items => {
      this.partnerActivityAllocation = items;
      this.dataGridPartnerActivityAllocations = items;
    });
  }

  async getPosts() {
    if (this.isOwner) {
      await this.postService.getAllPostsAsync().then(items => {
        this.posts = items;
        this.dataGridPosts = items;
      });
    } else {
      await this.postService.getPostsByCustomerId().then(items => {
        this.posts = items;
        this.dataGridPosts = items;
      });
    }
  }

  async getDepartments() {
    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(items => {
        this.departments = items;
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(items => {
        this.departments = items;
      });
    }
  }

  async getPaymentInstruments() {
    if (this.isOwner) {
      await this.paymentInstrumentService.getAllPaymentInstrumentsAsync().then(items => {
        this.paymentInstruments = items;
      });
    } else {
      await this.paymentInstrumentService.getAllPaymentInstrumentsByCustomerIdAsync().then(items => {
        this.paymentInstruments = items;
      });
    }
  }

  async getAddressTypes() {
    await this.addressTypeService.getAllAddressTypesSmallAsync().then(x => {
      this.addressTypes = x;
    });
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.partner).then(result => {
      this.actions = result;
    });
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  canDelete(rowSiteId) {
    if (rowSiteId && rowSiteId !== Number(this.authenticationService.getUserSiteId())) {
      return false;
    }
    return this.actions.CanDelete;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public refreshDataGrid(forced?: boolean) {
    if (forced) {
      this.getPartners();
    }
    this.dataGrid.instance.refresh();
  }

  public deleteRecords(data: any) {
    this.partnerService.deletePartnerAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Parteneri - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Parteneri - Datele nu au fost sterse!', NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid(true);
    });
  }

  public async openDetails(row: any) {
    this.loaded = true;

    // make sure departments are loaded before entering partner details
    await this.getDepartments();
    this.selectedPartner = row;
    setTimeout(async () => {
      await this.partnerDetails.loadData().then(function () {
        this.detailsVisibility = true;
        this.loaded = false;
      }.bind(this));
    }, 0);
  }

  changeVisible() {
    this.detailsVisibility = false;
    //this.getPartners();
    this.dataGrid.instance.refresh();
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async openPartnerObservationsDetails(row: any) {
    var url = environment.SRMPrimaware + '/' + Constants.partnerObservationsDetails + '?clientId=' + row.id + '&partnerName=' + row.name;
    window.open(url, '_blank');
  }

  getDisplayExprPosts(item) {
    if (!item) {
      return '';
    }
    return item.companyPost;
  }

  getDisplayExprPartnerActivity(value: any) {
    if (value && this.partnerActivity) {
      const item = this.partnerActivity.find(x => x.id === value.partnerActivityId);
      if (item) {
        return value.saleZoneCode + ' - ' + item.name + ' - ' + value.partnerActivityClassification;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  getVdLastYearExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.vdLastYear) {
        item.vdLastYear = this.helperService.roundUp(item.vdLastYear);
        return this.helperService.setNumberWithCommas((item.vdLastYear));
      }
      else {
        return '';
      }
    }
  }

  getVdLast2YearsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.vdLast2Years) {
        item.vdLast2Years = this.helperService.roundUp(item.vdLast2Years);
        return this.helperService.setNumberWithCommas((item.vdLast2Years));
      }
      else {
        return '';
      }
    }
  }

  getPfLastYearExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.pfLastYear) {
        item.pfLastYear = this.helperService.roundUp(item.pfLastYear);
        return this.helperService.setNumberWithCommas((item.pfLastYear));
      }
      else {
        return '';
      }
    }
  }

  getPfLast2YearsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.pfLast2Years) {
        item.pfLast2Years = this.helperService.roundUp(item.pfLast2Years);
        return this.helperService.setNumberWithCommas((item.pfLast2Years));
      }
      else {
        return '';
      }
    }
  }

  onPartnerNameClick(data) {
    if (data) {
      var url = environment.SRMPrimaware + '/' + Constants.partner + '?id=' + data.data.id;
      window.open(url, '_blank');
    }
  }

  getCaLastYearExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.caLastYear) {
        item.caLastYear = this.helperService.roundUp(item.caLastYear);
        return this.helperService.setNumberWithCommas((item.caLastYear));
      }
      else {
        return '';
      }
    }
  }

  getCaLast2YearsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.caLast2Years) {
        item.caLast2Years = this.helperService.roundUp(item.caLast2Years);
        return this.helperService.setNumberWithCommas((item.caLast2Years));
      }
      else {
        return '';
      }
    }
  }

  getNaLastYearExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.naLastYear) {
        item.naLastYear = this.helperService.roundUp(item.naLastYear);
        return this.helperService.setNumberWithCommas((item.naLastYear));
      }
      else {
        return '';
      }
    }
  }

  getNaLast2YearsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.naLast2Years) {
        item.naLast2Years = this.helperService.roundUp(item.naLast2Years);
        return this.helperService.setNumberWithCommas((item.naLast2Years));
      }
      else {
        return '';
      }
    }
  }

  getNpLastYearExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.npLastYear) {
        item.npLastYear = this.helperService.roundUp(item.npLastYear);
        return this.helperService.setNumberWithCommas((item.npLastYear));
      }
      else {
        return '';
      }
    }
  }

  getNpLast2YearsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.npLast2Years) {
        item.npLast2Years = this.helperService.roundUp(item.npLast2Years);
        return this.helperService.setNumberWithCommas((item.npLast2Years));
      }
      else {
        return '';
      }
    }
  }

  async showDuplicates() {
    this.showDuplicatesValue = !this.showDuplicatesValue;
    if (this.showDuplicatesValue) {
      this.notificationService.alert('top', 'center', 'Ati intrat pe modul de vizualizare duplicate! Apasati butonul iar pentru a reveni la pagina de parteneri!',
        NotificationTypeEnum.Green, true)
    }
    if (!this.showDuplicatesValue) {
      this.notificationService.alert('top', 'center', 'Ati iesit de pe modul de vizualizare duplicate!',
        NotificationTypeEnum.Green, true)
    }
   await this.getDuplicates();
  }

  async getDuplicates() {
    this.loaded = true;
    await this.partnerService.getAllDuplicates().then(data => {
      this.duplicatePartners = (data && data.length > 0) ? data : [];
      if (this.duplicatePartners && this.duplicatePartners.length > 0) {

        this.partnerService.getERPInvoiceSmallByPartnerIdsAsync(this.duplicatePartners.map(x => x.id)).then(results => {
          if (results && results.length > 0) {
            this.duplicatePartners.forEach(dp => {
              var find = results.find(x => x.id == dp.id);
              if (find != undefined) {
                dp.invoice = find.number;
              } else {
                dp.invoice = null;
              }
            })
          }
        })

        this.partnerSubscriptionAllocationService.getAllAsync().then(subs => {
          if (subs && subs.length > 0) {
            this.duplicatePartners.forEach(dp => {
              var find = subs.find(x => x.partnerId == dp.id && x.partnerSubscriptionId == 10);
              if (find != undefined) {
                dp.subscriptionState = 'Duplicat';
              } else {
                dp.subscriptionState = 'Activ';
              }
            })
          }
        })

        setTimeout(async () => {
          this.processedDuplicateData = this.processDataForHighlighting(this.duplicatePartners);
          var items = this.processedDuplicateData.filter(row => row.isPriority == false);
          this.duplicatePartnersGrid.instance.selectRows(items, true)
          this.duplicatePartnersGrid.instance.refresh();
        }, 2000);
        this.loaded;
      }
    });
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.isPriority == 0) {
      e.rowElement.style.backgroundColor = '#ffbebd';
    }
  }

   processDataForHighlighting(data: DuplicatePartner[]): DuplicatePartner[] {
  // Group rows by 'normalizedFiscalCode'
  const groups = this.groupBy(data, 'normalizedFiscalCode');
  const result: DuplicatePartner[] = [];

  // Loop through each group
  for (const key in groups) {
    if (Object.prototype.hasOwnProperty.call(groups, key)) {
      const rows = groups[key]; // Get rows for the current group
      if (rows.length < 5) {
        // Sort rows based on importance
        rows.sort((a, b) => {
          // 1. Rows with `Invoice` are prioritized
          if (b.invoice && !a.invoice) return 1;
          if (!b.invoice && a.invoice) return -1;

          // 2. Rows with Sync IDs (syncERPExternalId or syncSeniorExternalId) are prioritized
          if ((b.syncERPExternalId || b.syncSeniorExternalId) && !(a.syncERPExternalId || a.syncSeniorExternalId)) return 1;
          if (!(b.syncERPExternalId || b.syncSeniorExternalId) && (a.syncERPExternalId || a.syncSeniorExternalId)) return -1;

          if (b.lastContactDate && a.lastContactDate) {
            const dateA = new Date(a.lastContactDate).getTime();
            const dateB = new Date(b.lastContactDate).getTime();
            return dateB - dateA; // Prefer the more recent date
        }

          return 0; // Equal priority
        });

        // Highlight the most important row in green
        rows[0].isPriority = true;

        // Highlight the least important rows in red
        for (let i = 1; i < rows.length; i++) {
          rows[i].isPriority = false;
        }
      }
      result.push(...rows);
    }
  }

  return result;
}

// Helper function to group rows by a column
 groupBy<T>(data: T[], key: keyof T): Record<string, T[]> {
  const groups: Record<string, T[]> = {};
  data.forEach(item => {
    const groupKey = item[key] as unknown as string; // Ensure key is string
    if (!groups[groupKey]) {
      groups[groupKey] = [];
    }
    groups[groupKey].push(item);
  });
  return groups;
}

invalidateSelectedDuplicates() {
  this.multipleDuplicatePartnersInactiatePopup = true;
}

  inactivateRow(data) {
    this.inactivateSelected = data;
    this.displayInactivatePopup = true;
  }

  inactivatePartner(inactivate: boolean) {
    if (inactivate && this.inactivateSelected) {
      this.partnerService.deletePartnerAsync([this.inactivateSelected.id]).then(response => {
        if (response) {
          this.notificationService.alert('top', 'center', 'Parteneri - Datele au fost inactivate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Parteneri - Datele nu au fost inactivate!', NotificationTypeEnum.Red, true)
        }
        this.getDuplicates();
      });
    }
    this.displayInactivatePopup = false;
    this.inactivateSelected = null;
  }

  inactivateAllPartner(inactivate: boolean) {
    if (inactivate) {

      const selectedRows = this.duplicatePartnersGrid.instance.getSelectedRowsData(); // Get all selected rows
      const visibleRows = this.duplicatePartnersGrid.instance.getVisibleRows(); // Get visible rows on the current page

      // Extract the keys (IDs or unique identifiers) of the visible rows
      const visibleRowKeys = visibleRows?.filter(x => x.rowType == 'data').map(row => row.key).map(z => z.id);
      // Filter the selected rows to include only those on the current page
      const selectedRowsOnCurrentPage = selectedRows.map(z => z.id)?.filter(row => visibleRowKeys?.includes(row)); // Assuming `id` is the unique key

      if (selectedRowsOnCurrentPage && selectedRowsOnCurrentPage.length > 0) {
        this.partnerService.deletePartnerAsync(selectedRowsOnCurrentPage).then(response => {
          if (response) {
            this.notificationService.alert('top', 'center', 'Parteneri - Datele au fost inactivate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Parteneri - Datele nu au fost inactivate!', NotificationTypeEnum.Red, true)
          }
          this.getDuplicates();
        });
      }
    }
    this.multipleDuplicatePartnersInactiatePopup = false;
  }

  onCellPrepared(e: any) {
    if (e) {
        if (e.rowType == "header") {
            const cellElement = e.cellElement;

            if (e.cellElement.ariaLabel === "Column ERP") {
                cellElement.setAttribute("title", "Reprezinta Id-ul de legatura cu Senior ERP. Daca are valoare, atunci partenerul este legat cu Senior.");

                $(cellElement).dxTooltip({
                    target: cellElement,
                    showEvent: "mouseenter",
                    hideEvent: "mouseleave",
                    position: "bottom",
                    contentTemplate: () => $("<div>").text("Reprezinta Id-ul de legatura cu Senior ERP. Daca are valoare, atunci partenerul este legat cu Senior.")
                }).dxTooltip("instance");
            }
        }
    }
}

}
