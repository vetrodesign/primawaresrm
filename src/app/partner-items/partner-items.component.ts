import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { ItemGroupCode } from 'app/models/itemgroupcode.model';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { ItemGroupCategoryCodeService } from 'app/services/item-group-category-code.service';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { PartnerLocationItemGroupCategoryService } from 'app/services/partner-location-item-group-category.service';
import { PartnerLocationItemGroupCodeService } from 'app/services/partner-location-item-group-code.service';
import { PartnerLocationXItemGroupCategory } from 'app/models/partnerlocationxitemgroupcategory.model';
import { PartnerLocationXItemGroupCode } from 'app/models/partnerlocationxitemgroupcode.model';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { ItemService } from 'app/services/item.service';
import { PartnerService } from 'app/services/partner.service';
import { DxDataGridComponent, DxTreeListComponent, DxValidatorComponent } from 'devextreme-angular';
import { AssociatedItemService } from 'app/services/associated-item.service';
import { AssociatedItem } from 'app/models/associated-item.model';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { PageActionService } from 'app/services/page-action.service';
import { ItemGroupService } from 'app/services/item-group.service';
import { ItemGroup } from 'app/models/itemgroup.model';
import { v4 as uuidv4 } from 'uuid';
import { BarCodeIntervalService } from 'app/services/bar-code-interval.service';
import { BarCodeStandardTypeEnum } from 'app/enums/barcodeStandardTypeEnum';

@Component({
  selector: 'app-partner-items',
  templateUrl: './partner-items.component.html',
  styleUrls: ['./partner-items.component.css']
})
export class PartnerItemsComponent implements OnInit {
  @Input() itemGroupCategories: any[];
  @Input() allItemGroupCodes: ItemGroupCode[];
  @Input() posts: any;
  @Input() initialPartnerLocation: PartnerLocation;
  @Input() partner: any;
  @Input() partnerLocations: any;

  @Output() isItemGroupCategoryChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('itemGroupCodeGridToolbar') itemGroupCodeGridToolbar: GridToolbarComponent;
  @ViewChild('itemGroupCodeDataGrid') itemGroupCodeDataGrid: DxDataGridComponent;
  @ViewChild('itemGroupCategoryGridToolbar') itemGroupCategoryGridToolbar: GridToolbarComponent;
  @ViewChild('itemGroupCategoryDataGrid') itemGroupCategoryDataGrid: DxDataGridComponent;
  @ViewChild('associatedItemsGridToolbar') associatedItemsGridToolbar: GridToolbarComponent;
  @ViewChild('associatedItemsDataGrid') associatedItemsDataGrid: DxDataGridComponent;
  @ViewChild('activeItemsDataGrid') activeItemsDataGrid: DxDataGridComponent;
  @ViewChild('inactiveItemsDataGrid') inactiveItemsDataGrid: DxDataGridComponent;
  @ViewChild('itemGroupCategoryValidationGroup') itemGroupCategoryValidationGroup: DxValidatorComponent;
  @ViewChild('itemGroupCodeValidationGroup') itemGroupCodeValidationGroup: DxValidatorComponent;
  @ViewChild(DxTreeListComponent, { static: false }) treeList: DxTreeListComponent;

  isOwner: boolean;
  actions: IPageActions;
  groupedText: string;
  loaded: boolean;
  shouldDisplayPartnerLocations: boolean;
  plXitemGroupCategoriesIds: number[];
  plXItemGroupCategories: any;
  plXitemGroupCodesIds: number[];
  plXItemGroupCodes: any;
  selectedPartnerLocation: PartnerLocation;

  showItemsSection: boolean;
  showTechnologicalProcessSection: boolean;
  showEndUserSection: boolean;
  showAssociatedItemsSection: boolean;
  showActiveItemsSection: boolean;
  showInactiveItemsSection: boolean;
  showItemGroupCategorySection: boolean;
  showItemGroupCodeSection: boolean;
  filterItems: any;
  items: any;
  filteredItems: any;
  activeItems: any;
  inactiveItems: any;
  associatedItems: any;
  associatedItemsIds: number[];
  associatedItemGroupsIds: number[];
  selectedItemGroupCategoriesIds: { [key: number]: any[] } = {};
  selectedItemGroupCodesIds: { [key: number]: any[] } = {};
  selectedAssociatedItemsRows: any[];
  selectedAssociatedItemsRowIndex = -1;
  selectedItemGroupCategoryRows: { [key: number]: any[] } = {};
  selectedItemGroupCategoryRowIndex = -1;
  standardTypes: { id: number; name: string }[] = [];
  barCodeIntervals: any[] = [];
  selectedItemGroupCodeRows: any[];
  selectedItemGroupCodeRowIndex = -1;
  associatedItemsActions: IPageActions;
  itemGroupCategory: any;
  itemGroupCode: any;
  itemGroups: any;
  expandedRowKeysItemGroupCategory = new Set<number>();
  expandedRowKeysItemGroupCode = new Set<number>();
  selectedItemGroupCategoryPartnerLocation: number;
  selectedItemGroupCodePartnerLocation: number;
  itemGroupCodes: any;

  sortedPartnerLocations: any;
  itemGroupCategoriesByLocation: { [key: number]: any[] } = {};
  itemGroupCodesByLocation: { [key: number]: any[] } = {};
  implicitLocation: any;
  allLocationNames: string;

  constructor(
    private authenticationService: AuthService,
    private translationService: TranslateService,
    private partnerLocationService: PartnerLocationService,
    private partnerLocationXItemGroupCategoryService: PartnerLocationItemGroupCategoryService,
    private partnerLocationXItemGroupCodeService: PartnerLocationItemGroupCodeService,
    private cdr: ChangeDetectorRef,
    private notificationService: NotificationService,
    private itemsService: ItemService,
    private partnerService: PartnerService,
    private associatedItemsService: AssociatedItemService,
    private itemGroupService: ItemGroupService,
    private barCodeIntervalService: BarCodeIntervalService
  ) {
    this.getDisplayExprItemGroupCategories = this.getDisplayExprItemGroupCategories.bind(this);
    this.getDisplayExprItemGroupCodes = this.getDisplayExprItemGroupCodes.bind(this);

    this.isOwner = this.authenticationService.isUserOwner();
    this.groupedText = this.translationService.instant('groupedText');

    this.setAssociatedItemsActions();
    this.getAssociatedItems();
  }

  async ngOnInit() {
    for (let n in BarCodeStandardTypeEnum) {
      if (typeof BarCodeStandardTypeEnum[n] === 'number') {
        this.standardTypes.push({
          id: <any>BarCodeStandardTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    this.itemGroupCodes = {
      paginate: true,
      pageSize: 15,
      store: this.allItemGroupCodes
    };
  }

//   async getItemGroupCategory() {
//     this.sortedPartnerLocations = this.partnerLocations.sort((a, b) => {
//       if (a.isImplicit) return -1;
//       if (b.isImplicit) return 1;
//       return 0;
//     });

//     for (const location of this.sortedPartnerLocations) {
//       const locationId = location.id;
//       await this.caenCodeSpecializationPartnerLocationService
//         .getAllCaenCodeSpecializationPartnerLocationByPartnerLocationAsync(locationId)
//         .then(items => {
//           if (items) {
//             const specializationIds = items.map(m => m.caenCodeSpecializationId);
//             this.specializationsByLocation[locationId] = specializationIds;

//             this.partnerLocations.map(m => {
//               if (m.id === locationId) {
//                 m.existingCaenCodeIds = items.map(m => m.caenCodeSpecializationId);
//               }

//               return m;
//             });
//           }
//         });

//         await this.partnerLocationSecondaryActivityAllocationService
//         .getAllByPartnerLocationIdAsync(locationId)
//         .then(items => {
//           if (items) {
//             const activitiesIds = items.map(m => m.partnerActivityAllocationId);
//             this.activitiesByLocation[locationId] = activitiesIds;

//             this.partnerLocations.map(m => {
//               if (m.id === locationId) {
//                 m.existingPartnerSecondaryActivityAllocationIds = items.map(m => m.partnerActivityAllocationId);
//               }

//               return m;
//             });
//           }
//         });
//     }
//  }

  ngAfterViewInit() {
    this.getData();
    if (this.associatedItemsGridToolbar && this.associatedItemsDataGrid) {
      this.associatedItemsGridToolbar.dataGrid = this.associatedItemsDataGrid;
      this.associatedItemsGridToolbar.setGridInstance();
    }
  }

  ngAfterViewChecked(){
    this.cdr.detectChanges();
  }

  setAssociatedItemsActions() {
    this.associatedItemsActions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, 
      CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    
    this.associatedItemsActions.CanAdd = false;
    this.associatedItemsActions.CanDelete = true;
  }

  async getData() {
    this.loaded = true;  
      await Promise.all([ 
        this.getPartnerLocationXItemGroupCategory(), 
        this.getPartnerLocationXItemGroupCode(), 
        this.getItems(),
        this.getItemsGroup()
      ]);
  
      await this.getPartnerActiveItems();
      await this.getPartnerInactiveItems();
      await this.getAssociatedItems();
      await this.getBarCodeIntervals();

      this.implicitLocation = this.partnerLocations.find(f => f.isImplicit === true) || this.partnerLocations.find(f => f.isActive === true);
      this.allLocationNames = this.partnerLocations.map(m => m.name).join(", ");
  
      this.sortedPartnerLocations = this.partnerLocations.sort((a, b) => {
        if (a.isImplicit) return -1;
        if (b.isImplicit) return 1;
        return 0;
      });
  
      for (const location of this.sortedPartnerLocations) {
        const locationId = location.id;
  
        await this.getItemGroupCategory(locationId);
        await this.getItemGroupCode(locationId);
      }
      
      if (this.activeItems && this.inactiveItems && this.associatedItems) {
        let activeItemsIds = this.activeItems.map(m => m.id);
        let inactiveItemIds = this.inactiveItems.map(m => m.id);
  
        let newItemsArr = this.items.store.filter(f => !activeItemsIds.includes(f.id) && !inactiveItemIds.includes(f.id));
        this.items = {
          paginate: true,
          pageSize: 15,
          store: newItemsArr
        };
  
        let newAssociatedItemsArr = this.associatedItems.filter(f => !activeItemsIds.includes(f.itemId) && !inactiveItemIds.includes(f.itemId));
        this.associatedItems = newAssociatedItemsArr;

        await this.partnerLocationXItemGroupCategoryService.getAllPartnerLocationItemGroupCategoryByPartnerAsync(this.partner.id).then(items => {
          if (items) {
            this.itemGroupCategory = items.map(m => {
              let iGC = this.itemGroupCategories.find(f => f.id === m.itemGroupCategoryId);
              let objToRet = {};

              if (iGC != null) {
                objToRet = {
                  ...m,
                  code: iGC.catCode,
                  name: iGC.firstName,
                  parentId: null
                }
              }
  
              return objToRet;
            })
            
            let itemGroups = this.itemGroups?.store?.filter(f => this.itemGroupCategory.map(m => m.itemGroupCategoryId).includes(f.itemGroupCategoryId));

            if (itemGroups) {
              for (let i=0; i<itemGroups.length; i++) {
                let simpleItems = this.items.store.filter(f => f.itemGroupId === itemGroups[i].id);
           
                if (simpleItems) {
                  let filteredSimpleItems = simpleItems.filter(f => this.activeItems.map(m => m.itemId).includes(f.id) || this.inactiveItems.map(m => m.itemId).includes(f.id) 
                    || this.associatedItems.map(m => m.itemId).includes(f.id));
                  
                  for (let j=0; j<filteredSimpleItems.length; j++) {
                    let simpleItem = {
                      id: uuidv4(),
                      parentId: itemGroups[i].itemGroupCategoryId,
                      itemGroupCategoryId: uuidv4(),
                      simpleItemInfo: simpleItems[j].code + " " + simpleItems[j].nameRO
                    };

                    this.itemGroupCategory.push(simpleItem);
                  }
                }
              }
            }
          }

          this.loaded = false;
        })
      }
  }

  getDisplayExprItemGroupCategories(item) {
    if (!item) {
      return '';
    }
    let p = this.posts?.find(p => p.id === item.implicitPostId);
    return item.catCode + ' - ' + item.firstName + (p ? ' - ' + p.code : '');
  }

  getDisplayExprItemGroupCodes(item) {
    if (!item) {
      return '';
    }
    return item.grpCode + ' - ' + item.firstName;
  }

  async getPartnerActiveItems() {
    if (this.items && this.partner && this.partner.id) {
      await this.partnerService.getPartnerActiveItems(this.partner.id).then(activeItems => {
        if (activeItems) {
          this.activeItems = this.items.store.filter(f => activeItems.includes(f.id));
        }
      })
    }
  }

  async getPartnerInactiveItems() {
    if (this.items && this.partner && this.partner.id) {
      await this.partnerService.getPartnerInactiveItems(this.partner.id).then(inactiveItems => {
        if (inactiveItems) {
          this.inactiveItems = this.items.store.filter(f => inactiveItems.includes(f.id));
        }
      })
    }
  }

  async getPartnerLocationXItemGroupCategory() {
    await this.partnerLocationXItemGroupCategoryService.getAllAsync().then(async pls => {
      this.plXItemGroupCategories = pls;
    }); 
  }

  async getPartnerLocationXItemGroupCode() {
    await this.partnerLocationXItemGroupCodeService.getAllAsync().then(async pls => {
      this.plXItemGroupCodes = pls;
    }); 
  }

  onPartnerLocationXItemGroupCodeChange(event: any) {
    if (event && event.itemData && this.plXItemGroupCategories && this.partnerLocations) {
      let selectedPl = event.itemData.title;
      let partnerLocation = this.partnerLocations.find(f => f.name === selectedPl);
      this.selectedPartnerLocation = partnerLocation;

      if (partnerLocation) {
        this.plXitemGroupCategoriesIds = this.plXItemGroupCategories.filter(f => f.partnerLocationId === partnerLocation.id).map(m => m.itemGroupCategoryId);
        this.selectedPartnerLocation.itemGroupCategoryIds = this.plXitemGroupCategoriesIds;
        this.selectedPartnerLocation.existingItemGroupCategoryIds = this.plXItemGroupCategories.filter(f => f.partnerLocationId === partnerLocation.id).map(m => m.itemGroupCategoryId);
      }
    }
  }

  onPartnerLocationXItemGroupCategoryChange(event: any) {
    if (event && event.itemData && this.plXItemGroupCodes && this.partnerLocations) {
      let selectedPl = event.itemData.title;
      let partnerLocation = this.partnerLocations.find(f => f.name === selectedPl);
      this.selectedPartnerLocation = partnerLocation;

      if (partnerLocation) {
        this.plXitemGroupCodesIds = this.plXItemGroupCodes.filter(f => f.partnerLocationId === partnerLocation.id).map(m => m.itemGroupCodeId);
        this.selectedPartnerLocation.itemGroupCodeIds = this.plXitemGroupCodesIds;
        this.selectedPartnerLocation.existingItemGroupCodeIds = this.plXItemGroupCodes.filter(f => f.partnerLocationId === partnerLocation.id).map(m => m.itemGroupCodeId);
      }
    }
  }

  onPartnerLocationXItemGroupCodeItemChange(event: any) {
    if (event && event.value && this.selectedPartnerLocation) {
      this.selectedPartnerLocation.itemGroupCategoryIds = event.value;
    }
  }

  onPartnerLocationXItemGroupCategoryItemChange(event: any) {
    if (event && event.value && this.selectedPartnerLocation) {
      this.selectedPartnerLocation.itemGroupCodeIds = event.value;
    }
  }

  onShowItemsSectionClick() {
    this.showItemsSection = !this.showItemsSection;
  }

  onShowTechnologicalProcessSectionClick() {
    this.showTechnologicalProcessSection = !this.showTechnologicalProcessSection;
  }

  onShowEndUserSectionClick() {
    this.showEndUserSection = !this.showEndUserSection; 
  }

  async getBarCodeIntervals() {
    this.barCodeIntervals = [];
    if (this.partner && this.partner.id) {
      this.loaded = true;
      await this.barCodeIntervalService.getByPartnerIdAsync(this.partner.id).then(items => {
        if (items) {
          this.barCodeIntervals = items;
        }
        this.loaded = false;
      })
    }
  }

  async getAssociatedItems() {
    if (this.partner && this.partner.id) {
      this.loaded = true;
      await this.associatedItemsService.getAllByPartnerIdAsync(this.partner.id).then(items => {
        if (items) {
          this.associatedItems = items.map(m => {
            let item = this.items.store.find(f => f.id === m.itemId);

            let objToRet = {};
            if (item != null) {
              objToRet = { ...m, nameRO: item.nameRO, code: item.code };
            }

            return objToRet;
          })
        }
        this.loaded = false;
      })
    }
  }

  async getItemGroupCategory(locationId: number) {
    if (this.partner && this.partner.id) {
      this.loaded = true;
      await this.partnerLocationXItemGroupCategoryService.getAllPartnerLocationItemGroupCategoryByPartnerLocationAsync(locationId).then(items => {
        if (items) {

          this.selectedItemGroupCategoriesIds[locationId] = items.map(m => m.itemGroupCategoryId);
         
          this.partnerLocations.map(m => {
            if (m.id === locationId) {
              m.existingItemGroupCategoryIds = items.map(m => m.itemGroupCategoryId);
            }

            return m;
          });

          this.itemGroupCategoriesByLocation[locationId] = items.map(m => {
            let iGC = this.itemGroupCategories.find(f => f.id === m.itemGroupCategoryId);
            let objToRet = {};

            if (iGC != null) {
              objToRet = {
                ...m,
                code: iGC.catCode,
                name: iGC.firstName,
                parentId: null
              }
            }

            return objToRet;
          })

          let itemGroups = this.itemGroups?.store?.filter(f => this.itemGroupCategoriesByLocation[locationId].map(m => m.itemGroupCategoryId).includes(f.itemGroupCategoryId));

          if (itemGroups) {
            for (let i=0; i<itemGroups.length; i++) {
              let simpleItems = this.items.store.filter(f => f.itemGroupId === itemGroups[i].id);
            
              if (simpleItems) {
                let filteredSimpleItems = simpleItems.filter(f => this.activeItems.map(m => m.itemId).includes(f.id) || this.inactiveItems.map(m => m.itemId).includes(f.id) 
                  || this.associatedItems.map(m => m.itemId).includes(f.id));
                
                for (let j=0; j<filteredSimpleItems.length; j++) {
                  let simpleItem = {
                    id: uuidv4(),
                    parentId: itemGroups[i].itemGroupCategoryId,
                    itemGroupCategoryId: uuidv4(),
                    simpleItemInfo: simpleItems[j].code + " " + simpleItems[j].nameRO
                  };

                  this.itemGroupCategoriesByLocation[locationId].push(simpleItem);
                }
              }
            }
          }
        }

        this.loaded = false;
      })
    }
  }

  async getItemGroupCode(locationId: number) {
    if (this.partner && this.partner.id) {
      this.loaded = true;
      await this.partnerLocationXItemGroupCodeService.getAllPartnerLocationItemGroupCodeByPartnerLocationAsync(locationId).then(items => {
        if (items) {

          this.selectedItemGroupCodesIds[locationId] = items.map(m => m.itemGroupCodeId);

          this.partnerLocations.map(m => {
            if (m.id === locationId) {
              m.existingItemGroupCodeIds = items.map(m => m.itemGroupCodeId);
            }

            return m;
          });

          this.itemGroupCodesByLocation[locationId] = items.map(m => {
            let iGC = this.itemGroupCodes.store.find(f => f.id === m.itemGroupCodeId);
            let objToRet = {};

            if (iGC != null) {
              objToRet = {
                ...m,
                code: iGC.grpCode,
                name: iGC.firstName,
                parentId: null
              }
            }

            return objToRet;
          })

          let itemGroups = this.itemGroups?.store?.filter(f => this.itemGroupCodesByLocation[locationId].map(m => m.itemGroupCodeId).includes(f.itemGroupCodeId));

          if (itemGroups) {
            for (let i=0; i<itemGroups.length; i++) {
              let simpleItems = this.items.store.filter(f => f.itemGroupId === itemGroups[i].id);
              
              if (simpleItems) {
                let filteredSimpleItems = simpleItems.filter(f => this.activeItems.map(m => m.itemId).includes(f.id) || this.inactiveItems.map(m => m.itemId).includes(f.id) 
                  || this.associatedItems.map(m => m.itemId).includes(f.id));
                
                for (let j=0; j<filteredSimpleItems.length; j++) {
                  let simpleItem = {
                    id: uuidv4(),
                    parentId: itemGroups[i].itemGroupCodeId,
                    itemGroupCodeId: uuidv4(),
                    simpleItemInfo: simpleItems[j].code + " " + simpleItems[j].nameRO
                  };

                  this.itemGroupCodesByLocation[locationId].push(simpleItem);
                }
              }
            }
          }
        }

        this.loaded = false;
      })
    }
  }

  onShowAssociatedItemsSectionClick() {
    this.showAssociatedItemsSection = !this.showAssociatedItemsSection;
  }

  onShowActiveItemsSectionClick() {
    this.showActiveItemsSection = !this.showActiveItemsSection;
  }

  onShowInactiveItemsSectionClick() {
    this.showInactiveItemsSection = !this.showInactiveItemsSection;
  }

  setFilteredItem() {
    this.filterItems = {
      paginate: true,
      pageSize: 15,
      store: this.items.store
    };
  }

  async getItemsGroup(): Promise<any> {
    if (this.isOwner) {
      await this.itemGroupService.getAllItemGroupsAsync().then(items => {
        this.itemGroups = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    } else {
      await this.itemGroupService.getItemGroupsAsyncByID().then(items => {
        this.itemGroups = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    }
  }

  async getItems(): Promise<any> {
    if (this.isOwner) {
      await this.itemsService.getAllItemsAsync().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    } else {
      await this.itemsService.getItemssAsyncByID().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    }
  }

  partnerLocationsDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.name;
  }

  itemDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  itemGroupCategoryDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.catCode + ' - ' + item.firstName;
  }

  itemGroupCodeDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.grpCode + ' - ' + item.firstName;
  }

  async onAssociatedItemsSaveClick() {
    if ((!this.associatedItemsIds || this.associatedItemsIds.length === 0) && (!this.associatedItemGroupsIds || this.associatedItemGroupsIds.length === 0)) {
      this.notificationService.alert( 'top', 'center', 'Produse Asociate - Selectati cel putin un produs simplu sau un produs grupat!', NotificationTypeEnum.Red, true);
      return;
    }

    let newAssociatedItems: AssociatedItem[] = [];
  
    if (this.partner && this.partner.id) {
      let simpleItemsFromGroups = this.items.store.filter(f => this.associatedItemGroupsIds?.includes(f.itemGroupId));

      let activeItemsIds = this.activeItems.map(m => m.id);
      let inactiveItemIds = this.inactiveItems.map(m => m.id);
      let associatedItemIds = this.associatedItems.map(m => m.itemId);

      let filteredSimpleItems = simpleItemsFromGroups.filter(f => !associatedItemIds.includes(f.id) && !activeItemsIds.includes(f.id) && !inactiveItemIds.includes(f.id));

      filteredSimpleItems?.forEach(item => {
        const c = new AssociatedItem();
        c.itemId = Number(item.id);
        c.partnerId = this.partner.id;
        newAssociatedItems.push(c);
      });

      this.associatedItemsIds?.forEach(id => {
        const c = new AssociatedItem();
        c.itemId = Number(id);
        c.partnerId = this.partner.id;
        newAssociatedItems.push(c);
      });

      if (newAssociatedItems && newAssociatedItems.length > 0) {
        let newAssociatedItemIds = newAssociatedItems.map(m => m.itemId);

        let hasCommonItems = newAssociatedItemIds.some(itemId => associatedItemIds.includes(itemId));

        if (this.associatedItems && hasCommonItems) {
          this.notificationService.alert('top', 'center', 'Produse Asociate - Unul dintre produsele selectate este deja adaugat la Produse Asociate!', NotificationTypeEnum.Red, true);
          return;
        }

        await this.associatedItemsService.createMultipleAsync(newAssociatedItems).then(async () => {
        });

        await this.getAssociatedItems().then(() => {
          this.associatedItemsIds = [];
          this.associatedItemGroupsIds = [];
          this.notificationService.alert( 'top', 'center', 'Produse Asociate - Datele au fost modificate cu succes!', NotificationTypeEnum.Green, true);
        })
      } else {
        this.notificationService.alert( 'top', 'center', 'Produse Asociate - Produsul grupat selectat are toate produsele simple deja la produse asociate/active/achizitionate in trecut pe acest partener!', NotificationTypeEnum.Red, true);
      }
    }
  }

  async onItemGroupCategoriesSaveClick(selectedLocation: any) {
    this.loaded = true;

    const processLocation = async (item) => {
      let selectedPl = this.partnerLocations.find(f => f.id === item.id);

      if (this.partner.isPrincipalActivityUniqueForAllLocations) {
        item.partnerActivityAllocationId = this.implicitLocation.partnerActivityAllocationId;
      }
       
      // Create ItemGroupCategoryXPartnerLocation
      const existingItemGroupCategoryIds = selectedPl.existingItemGroupCategoryIds;
      
      let deleteItemGroupCategoryPartnerLocationAllocation: PartnerLocationXItemGroupCategory[] = [];
      existingItemGroupCategoryIds?.filter(x => !this.selectedItemGroupCategoriesIds[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].includes(x)).forEach(id => {
        const c = new PartnerLocationXItemGroupCategory();
        c.partnerLocationId = item.id;
        c.itemGroupCategoryId = id;
        deleteItemGroupCategoryPartnerLocationAllocation.push(c);
      });
      if (deleteItemGroupCategoryPartnerLocationAllocation.length > 0) {
        await this.partnerLocationXItemGroupCategoryService.deletePartnerLocationItemGroupCategoryAsync(deleteItemGroupCategoryPartnerLocationAllocation);
      }
      let createItemGroupCategoryIds: PartnerLocationXItemGroupCategory[] = [];
      this.selectedItemGroupCategoriesIds[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].filter(x => !existingItemGroupCategoryIds.includes(x)).forEach(id => {
        const c = new PartnerLocationXItemGroupCategory();
        c.partnerLocationId = Number(item.id);
        c.itemGroupCategoryId = id;
        createItemGroupCategoryIds.push(c);
      });
      if (createItemGroupCategoryIds.length > 0) {
        await this.partnerLocationXItemGroupCategoryService.createMultiplePartnerLocationItemGroupCategoryAsync(createItemGroupCategoryIds).then(() => {})
      }
    };

    for (const location of this.partnerLocations) {
      await processLocation(location).then(() => {})
    }

    const promises = this.partnerLocations.map(location => 
      this.getItemGroupCategory(location.id)
    );

    await Promise.all(promises).then(() => { this.isItemGroupCategoryChange.emit(true); this.loaded = false });
    this.notificationService.alert('top', 'center', 'Cod Categorie(Proces Tehnologic) - Datele au fost modificate cu succes!', NotificationTypeEnum.Green, true);
  }

  async onItemGroupCodesSaveClick(selectedLocation: any) {
    this.loaded = true;

    const processLocation = async (item) => {
      let selectedPl = this.partnerLocations.find(f => f.id === item.id);

      if (this.partner.isPrincipalActivityUniqueForAllLocations) {
        item.partnerActivityAllocationId = this.implicitLocation.partnerActivityAllocationId;
      }
       
      // Create ItemGroupCategoryXPartnerLocation
      const existingItemGroupCodeIds = selectedPl.existingItemGroupCodeIds;

      let deleteItemGroupCodePartnerLocationAllocation: PartnerLocationXItemGroupCode[] = [];
      existingItemGroupCodeIds?.filter(x => !this.selectedItemGroupCodesIds[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].includes(x)).forEach(id => {
        const c = new PartnerLocationXItemGroupCode();
        c.partnerLocationId = item.id;
        c.itemGroupCodeId = id;
        deleteItemGroupCodePartnerLocationAllocation.push(c);
      });
      if (deleteItemGroupCodePartnerLocationAllocation.length > 0) {
        await this.partnerLocationXItemGroupCodeService.deletePartnerLocationItemGroupCodeAsync(deleteItemGroupCodePartnerLocationAllocation);
      }
      let createItemGroupCodeIds: PartnerLocationXItemGroupCode[] = [];
      this.selectedItemGroupCodesIds[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].filter(x => !existingItemGroupCodeIds?.includes(x)).forEach(id => {
        const c = new PartnerLocationXItemGroupCode();
        c.partnerLocationId = Number(item.id);
        c.itemGroupCodeId = id;
        createItemGroupCodeIds.push(c);
      });
      if (createItemGroupCodeIds.length > 0) {
        await this.partnerLocationXItemGroupCodeService.createMultiplePartnerLocationItemGroupCodeAsync(createItemGroupCodeIds);
      }
    };

    for (const location of this.partnerLocations) {
      await processLocation(location).then(() => {})
    }

    const promises = this.partnerLocations.map(location => 
      this.getItemGroupCode(location.id)
    );

    await Promise.all(promises).then(() => this.loaded = false);

    this.notificationService.alert('top', 'center', 'Grupe Produse(Destinatie Finala) - Datele au fost modificate cu succes!', NotificationTypeEnum.Green, true);
  }

  onAssociatedItemsSelectionChanged(data: any) {
    this.selectedAssociatedItemsRows = data.selectedRowsData;
    this.selectedAssociatedItemsRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  onItemGroupCategorySelectionChanged(data: any, location: any) {
    this.selectedItemGroupCategoryRows[1] = data.selectedRowsData;
    this.selectedItemGroupCategoryRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  onItemGroupCodeSelectionChanged(data: any) {
    this.selectedItemGroupCodeRows = data.selectedRowsData;
    this.selectedItemGroupCodeRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public refreshAssociatedItemsDataGrid() {
    this.getAssociatedItems();
    this.associatedItemsDataGrid.instance.refresh();
    this.showAssociatedItemsSection = true;
  }

  public async refreshItemGroupCategoryDataGrid(event: any, location: any) {
    await this.getItemGroupCategory(location.id);
    this.itemGroupCategoryDataGrid.instance.refresh();
  }

  public async refreshItemGroupCodeDataGrid(event: any, location: any) {
    await this.getItemGroupCode(location.id);
    this.itemGroupCodeDataGrid.instance.refresh();
  }

  public deleteRow(data: any) {
    this.associatedItemsDataGrid.instance.selectRows(data.key, false);
    this.associatedItemsGridToolbar.displayDeleteConfirmation();
  }

  public async deleteAssociatedItems(data: any) {
    await this.associatedItemsService.deleteMultipleAsync(this.selectedAssociatedItemsRows.map(item => item.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Produse Asociate - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Produse Asociate - Datele nu au fost sterse!',
          NotificationTypeEnum.Red, true)
      }

      this.getAssociatedItems();
    });
  }

  public deleteItemGroupCategoryRow(data: any, location: any) {
    this.selectedItemGroupCategoryRows[location.id] = [data.key];
    this.itemGroupCategoryDataGrid.instance.selectRows(data.key, false);
    this.itemGroupCategoryGridToolbar.displayDeleteConfirmation();
  }

  public async deleteItemGroupCategory(data: any, location: any) {
    await this.partnerLocationXItemGroupCategoryService.deletePartnerLocationItemGroupCategoryAsync(this.selectedItemGroupCategoryRows[location.id].map(item => {
      let obj = new PartnerLocationXItemGroupCategory();
      obj.partnerLocationId = location.id;
      obj.itemGroupCategoryId = item.itemGroupCategoryId;

      return obj;
    })).then(async response => {
       
      await this.getItemGroupCategory(location.id);
      this.notificationService.alert('top', 'center', 'Cod Categorie(Proces Tehnologic) - Datele au fost sterse cu succes!', NotificationTypeEnum.Green, true)
    });
  }

  public deleteItemGroupCodeRow(data: any) {
    this.itemGroupCodeDataGrid.instance.selectRows(data.key, false);
    this.itemGroupCodeGridToolbar.displayDeleteConfirmation();
  }

  public async deleteItemGroupCode(data: any) {
    
  }

  isExpandedItemGroupCategory(key) {
    return this.expandedRowKeysItemGroupCategory.has(key);
  }

  toggleExpandItemGroupCategory(data) {
    if (this.expandedRowKeysItemGroupCategory.has(data.key)) {
      this.treeList.instance.collapseRow(data.key);
      this.expandedRowKeysItemGroupCategory.delete(data.key);
    } else {
      this.treeList.instance.expandRow(data.key);
      this.expandedRowKeysItemGroupCategory.add(data.key);
    }
  }

  isExpandedItemGroupCode(key) {
    return this.expandedRowKeysItemGroupCode.has(key);
  }

  toggleExpandItemGroupCode(data) {
    if (this.expandedRowKeysItemGroupCode.has(data.key)) {
      this.treeList.instance.collapseRow(data.key);
      this.expandedRowKeysItemGroupCode.delete(data.key);
    } else {
      this.treeList.instance.expandRow(data.key);
      this.expandedRowKeysItemGroupCode.add(data.key);
    }
  }
}
