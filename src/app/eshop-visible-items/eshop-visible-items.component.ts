import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { NotificationService } from 'app/services/notification.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';
import * as _ from 'lodash';
import { ItemService } from 'app/services/item.service';
import { Language } from 'app/models/language.model';
import { LanguageService } from 'app/services/language.service';
import { EshopLanguageService } from 'app/services/eshop-language.service';
import { EshopVisibleItem, Item } from 'app/models/item.model';

@Component({
  selector: 'app-eshop-visible-items',
  templateUrl: './eshop-visible-items.component.html',
  styleUrls: ['./eshop-visible-items.component.css']
})
export class EshopVisibleItemsComponent implements OnInit, AfterViewInit {

  groupedText: string;
  actions: IPageActions;
  eshopVisibleItems: EshopVisibleItem[] = [];
  loaded: boolean;
  eshopVisibleItemsInfo: string;
  languages: Language[];
  eshopLanguageIds: number[] = [];
  eshopLanguages: Language[];

  @ViewChild('eshopVisibleItemsDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('eshopVisibleItemsGridToolbar') gridToolbar: GridToolbarComponent;

  constructor(private notificationService: NotificationService,
    private translationService: TranslateService, private eshopLanguageService: EshopLanguageService,
    private itemService: ItemService, private languageService: LanguageService) {
    this.setActions();
    this.groupedText = this.translationService.instant('groupedText');

    this.eshopVisibleItemsInfo = this.translationService.instant("eshopVisibleItemsInfo");
    this.getData();
   }

   setActions() {
    this.actions = {
      CanView: true,
      CanAdd: false,
      CanUpdate: false,
      CanDelete: false,
      CanPrint: true,
      CanExport: true,
      CanImport: false,
      CanDuplicate: false
    };
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.setGridInstances();
  }

  setGridInstances() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getLanguages()]).then(async x => {
      await this.getEshopLanguages();
      this.loaded= false;
    })
  }

  refreshEshopVisibleItems() {
    this.getEshopVisibleItems();
    this.dataGrid.instance.refresh();
  }

  async getLanguages(): Promise<any> {
    this.languages = [];
    await this.languageService.getAllLanguagesAsync().then(items => {
      this.languages = (items && items.length > 0) ? items : [];
    });
  }

  async getEshopLanguages() {
    await this.eshopLanguageService.getByCustomerIdAsync().then(items => {
      if (items && items.length > 0) {
        this.eshopLanguages = this.languages.filter(x => items.map(m => m.languageId).includes(x.id))
      }
    });
  }

  async getEshopVisibleItems() {
    this.loaded = true;
    await this.itemService.getEshopVisibleItems(this.eshopLanguageIds).then(items => {
      this.eshopVisibleItems = (items && items.length > 0) ? items : [];
      this.loaded = false;
    })
  }
}
