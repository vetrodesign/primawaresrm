import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { Constants } from 'app/constants';
import { PageActionService } from 'app/services/page-action.service';
import { SalesGroupTarget } from 'app/models/salegrouptarget.model';
import { SalesGroupTargetService } from 'app/services/sales-group-target.service';


@Component({
  selector: 'app-sales-group-target',
  templateUrl: './sales-group-target.component.html',
  styleUrls: ['./sales-group-target.component.css']
})
export class SalesGroupTargetComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;
  
  actions: IPageActions;
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  isOwner: boolean = false;
  salesGroupTargets: SalesGroupTarget[] = [];
  loaded: boolean;
  customerId: string;
  groupedText: string;

  constructor(private authenticationService: AuthService,
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private salesGroupTargetService: SalesGroupTargetService,
    private pageActionService: PageActionService) {
    this.setActions();
    this.customerId = this.authenticationService.getUserCustomerId();
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
    this.salesGroupTargets = [];
    this.groupedText = this.translationService.instant('groupedText');

    this.getData();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getSalesGroupTargets()]).then(x => {
      this.loaded = false;
    });
  }

  async getSalesGroupTargets() {
    if (this.isOwner) {
      await this.salesGroupTargetService.getAllAsync().then(items => {
        this.salesGroupTargets = (items && items.length > 0) ? items : [];
      });
    } else {
      await this.salesGroupTargetService.getByCustomerIdAsync().then(items => {
        this.salesGroupTargets = (items && items.length > 0) ? items : [];
      });
    }
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.salesTarget).then(result => {
      this.actions = result;
    });
  }
  canUpdate() {
    return this.actions.CanUpdate;
  }

  canExport() {
    return this.actions.CanExport;
  }

  canDelete() {
    return this.actions.CanDelete;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  onInitNewRow(data) {
    if (data && data.data) {
      data.data.isActive = true;
    }
  }

  public async refreshDataGrid() {
    this.loaded = true;
    await this.getSalesGroupTargets().then(x => {
      this.loaded = false;
      this.dataGrid.instance.refresh();
    });
  }

  public deleteRecords(data: any) {
    this.salesGroupTargetService.deleteAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Datele au fost inactivate cu succes!', NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Datele nu au inactivate!', NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
    this.rowIndex = row.rowIndex;
  }

  public add() {
    this.dataGrid.instance.addRow();
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onRowInserting(event: any): Promise<void> {
    let item = new SalesGroupTarget();
    item = event.data;
    item.customerId = Number(this.authenticationService.getUserCustomerId());

    if (item) {
      await this.salesGroupTargetService.createAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Target Vanzari - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Target Vanzari - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshDataGrid();
      });
    }
  }

  public async onRowUpdated(event: any): Promise<void> {
    let item = new SalesGroupTarget();
    item = this.salesGroupTargets.find(g => g.id === event.key.id);
    if (item) {
      await this.salesGroupTargetService.updateAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Target Vanzari - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Target Vanzari - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };
        this.refreshDataGrid();
      });
    }
  }
}
