import { Component, OnInit, ViewChild } from '@angular/core';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { BnrRobor, BnrRoborRequest } from 'app/models/bnr-robor.model';
import { IPageActions } from 'app/models/ipageactions';
import { BnrRoborService } from 'app/services/bnr-robor.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-bnr-robor',
  templateUrl: './bnr-robor.component.html',
  styleUrls: ['./bnr-robor.component.css']
})
export class BnrRoborComponent implements OnInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;

  loaded: boolean;
  displayData: boolean;
  actions: IPageActions;
  groupedText: string;

  bnrRoborRequest: BnrRoborRequest = new BnrRoborRequest();
  bnrRobor: BnrRobor;
  shouldDisplayRobor9M: boolean = false;

  constructor(
    private bnrRoborService: BnrRoborService,
    private translationService: TranslateService
  ) {

    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();

    this.bnrRoborRequest.stopDate = new Date();
    const startDateCopy = new Date(this.bnrRoborRequest.stopDate);
    startDateCopy.setMonth(startDateCopy.getMonth() - 12);
    this.bnrRoborRequest.startDate = startDateCopy;

    this.getData();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: false,
      CanUpdate: false,
      CanDelete: false,
      CanPrint: false,
      CanExport: false,
      CanImport: false,
      CanDuplicate: false
    };
  }

  public refreshDataGrid() {
    this.getData();
  }

  getData() {
    this.loaded = true;
    Promise.all([this.GetBnrRoborData()]).then(x => { 
      this.loaded = false
    });
  }

  async GetBnrRoborData(): Promise<any> {
    await this.bnrRoborService.getBnrRoborData(this.bnrRoborRequest).then(response => {
      if (response) {
        this.bnrRobor = response;
        
        /* display the ROBOR 9M column only if there is at least a value for it */
        this.shouldDisplayRobor9M = Array.prototype.some.call(this.bnrRobor, (s: BnrRobor) => s.robor9M !== '-');
      }
    });
  }

  onFilterChange(event: any) {
    if (event && event.value) {
      this.getData();
    }
  }
}
