import { Component, HostListener, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from 'app/constants';
import { ClientModuleMenuItem } from 'app/models/clientmodulemenuitem.model';
import { Language } from 'app/models/language.model';
import { AuthService } from 'app/services/auth.service';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';
import { ClientModuleService } from 'app/services/client-module.service';
import { HelperService } from 'app/services/helper.service';
import { MenuService } from 'app/services/menu/menu.service';
import { SiteService } from 'app/services/site.service';
import { TranslateService } from 'app/services/translate';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {
@Input() title: string;
@Input() public infoId: string = 'tooltip';
@Input() public tooltipElementId: string;

breadcrumb: string; // This will store the generated breadcrumb string
menuItems: any = [];
isPopupVisible: boolean = false;
infoText = 'No Info'
themeColor: string = 'card-header-success';
templateVisible: boolean = false;
searchValue: string = '';
dropdownVisible: boolean = false;
datasource: any[] = [];
emergencySituationsUrl = environment.SSOPrimaware + '/emergencySituations';
currentUser: string;
shouldShowNotifications: boolean;
notifications: string[];
shouldShow: boolean;
selectedLan: string;
languages: Language[];
shouldShowProfile: boolean;
currentSite: string;
weatherPopupVisible: boolean;
currencyPopupVisible: boolean;
isAdmin: boolean;

constructor(private authService: AuthService, private clientModuleMenuItemsService: ClientModuleMenuItemsService, private router: Router,
  private route: ActivatedRoute, private authenticationService: AuthService, private translateService: TranslateService, private siteService: SiteService,
  private clientModuleService : ClientModuleService, private helperService: HelperService, private menuService: MenuService) {
  if (this.authService.getCustomerPageTitleTheme()) {
    this.currentUser = this.authenticationService.getUserFirstNameAndLastName();
    this.notifications = [];
    this.isAdmin = this.authenticationService.isUserAdmin();
    this.menuService.subject.subscribe(n => {
      if (n) {
          this.notifications = n;
      }
    })

    this.languages = this.translateService.getLangs();
    const langCode = JSON.parse(localStorage.getItem(Constants.clientLang));
    this.selectedLan = langCode ? this.translateService.instant(this.languages.find(l => l.code === langCode).name) : this.translateService.instant('english');
    
    this.authenticationService.currentUserSubject.subscribe(token => {
      this.menuItems = this.menuService.getMenuItems();
      this.currentUser = this.authenticationService.getUserFirstNameAndLastName();
      if (token) {
          this.siteService.getCustomerLocationsAsyncByID().then(s => {
              if (s && s.length > 0) {
                  this.currentSite = s.find(x => x.id === Number(this.authenticationService.getUserSiteId())).name;
              }
          })
      } else {
          this.currentSite = null;
      }
    });

    this.themeColor = 'card-header-' + this.authService.getCustomerPageTitleTheme();
   }
 }

  ngOnInit(): void {
    this.clientModuleMenuItemsService.getClientModuleMenuItemsByModuleIDAsync(this.authService.getClientModuleId(), false).then((menuItems) => {
      this.menuItems = menuItems;
      this.generateBreadcrumb();
      this.setItemInfo();
    });
  }

  generateBreadcrumb() {
    // Get the current route path
    const currentUrl = this.router.url;

    // Find the current menu item by matching the path
    const currentItem = this.menuItems.find(item => currentUrl === item.path);

    if (currentItem) {
      this.breadcrumb = 'SRM/' + this.buildBreadcrumb(currentItem);
    } else {
      // Fallback to using the title if no match is found in the menu
      this.breadcrumb = 'SRM/' +  this.title || 'Unknown Page';
    }
  }

  buildBreadcrumb(item: ClientModuleMenuItem): string {
    let breadcrumb = item.title;
    let parentId = item.subMenuId;

    // Recursively find parent items and build the breadcrumb string
    while (parentId) {
      const parent = this.menuItems.find(menuItem => menuItem.id === parentId);
      if (parent) {
        breadcrumb = `${parent.title}/${breadcrumb}`;
        parentId = parent.subMenuId;
      } else {
        break;
      }
    }

    return breadcrumb;
  }


  async setItemInfo() {
    var path = this.route.snapshot.routeConfig.path;
    this.clientModuleMenuItemsService.getInfoButtonText(path).then(i => {
      if (i) {
        this.infoText = i.text;
      }
    });
  }

  toggleTemplate() {
    this.templateVisible = !this.templateVisible;
    this.setTooltipElementId();
    let element = this.tooltipElementId || 'tooltipInfo';
    setTimeout(async () => {
      if (document.getElementById(element)) {
        document.getElementById(element).innerHTML = this.infoText;
      }
    }, 0);

  }

  setTooltipElementId() {
    if (this.infoId) {
      this.tooltipElementId = `${this.infoId}Tooltip`;
    }
  }

  infoClick() {
    this.isPopupVisible = !this.isPopupVisible;
  }

  onShowing() {
    document.getElementById('popupId').innerHTML = this.infoText;
  }

  async searchRoute() {
    this.dropdownVisible = false;
    if (this.searchValue) {
      await this.clientModuleMenuItemsService.getMenuItemsByTextAsync(this.searchValue).then(async items => {
        if (items && items.length > 0) {
          await this.clientModuleService.getAllClientModulesAsync().then(base => {
            if (base && base.length) {
              this.createSource(items, base);
            }
          })
        }
      })
    }
  }

  createSource(items: any, base: any) {
    items.forEach(i => {
      var b = base.find(x => x.id == i.clientModuleId);
      if (b != null) {
        i.baseTitle = b.title;
        i.baseRedirectUri = b.redirectUri;
        i.fullUrl = i.baseRedirectUri + i.path;
        i.displayValue = i.fullUrl;
      }
    });

    this.datasource = items;
    this.dropdownVisible = true;
  }

  onInputChange() {
    this.dropdownVisible = false; 
  }

  onSelectItem(i){
    if (i?.fullUrl) {
        window.open(i.fullUrl, '_blank');
    }
    this.dropdownVisible = false;
  }

  toggleShowNotifications() {
    this.shouldShowNotifications = !this.shouldShowNotifications;
    this.shouldShowProfile = false;
    this.shouldShow = false;
  }

  toggleShow() {
    this.shouldShow = !this.shouldShow;
    this.shouldShowNotifications = false;
    this.shouldShowProfile = false;
  }

  selectCulture(lang: Language) {
    this.translateService.use(lang);
    this.toggleShow();
    this.selectedLan = this.translateService.instant(lang.name);
  }

  toggleShowProfile() {
    this.shouldShowProfile = !this.shouldShowProfile;
    this.shouldShow = false;
    this.shouldShowNotifications = false;
  }

  logout() {
    this.toggleShowProfile();
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  goToSSO() {
    this.authenticationService.removeToken();
    window.location.href = environment.SSOPrimaware;
  }

  @HostListener('document:click', ['$event'])
  handleClickOutside(event: Event): void {
    const targetElement = event.target as HTMLElement;

    if (!targetElement.closest('.notification-container')) {
      this.shouldShowNotifications = false;
    }

    if (!targetElement.closest('.user-container')) {
      this.shouldShowProfile = false;
    }

    if (!targetElement.closest('.lang-container')) {
      this.shouldShow = false;
    }
  }

  showWeather() {
    this.weatherPopupVisible = true;
  }

  showCurrencyExchange() {
    this.currencyPopupVisible = true;
  }
}
