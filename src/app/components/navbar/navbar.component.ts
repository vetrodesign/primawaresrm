import { Component, OnInit, ElementRef, Output, EventEmitter } from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from 'app/services/translate';
import { Constants } from 'app/constants';
import { Language } from 'app/models/language.model';
import { MenuService, RouteInfo } from 'app/services/menu/menu.service';
import { HelperService } from 'app/services/helper.service';
import { AuthService } from 'app/services/auth.service';
import { environment } from 'environments/environment';
import { SiteService } from 'app/services/site.service';
import { ClientModule } from 'app/models/clientmodule.model';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';
import { ClientModuleService } from 'app/services/client-module.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    @Output() visible: EventEmitter<any> = new EventEmitter<any>();
    location: Location;
      mobile_menu_visible: any = 0;
    private toggleButton: any;
    private sidebarVisible: boolean;
    languages: Language[];
    menuItems: RouteInfo[];
    selectedLan: string;
    shouldShow: boolean;
    hidden: boolean;
    shouldShowProfile: boolean;
    shouldShowNotifications: boolean;
    currentUser: string;
    currentSite: string;
    notifications: string[];
    searchValue: string = '';
    clientModule: ClientModule;
    emergencySituationsUrl = environment.SSOPrimaware + '/emergencySituations';
    datasource: any[] = [];
    dropdownVisible: boolean = false;

    constructor(location: Location, route: ActivatedRoute,  private element: ElementRef, private router: Router,
        private translateService: TranslateService, private menuService: MenuService, private siteService: SiteService,
        private helperService: HelperService, private authenticationService: AuthService,
        private clientModuleService : ClientModuleService,
        private clientModuleMenuItemsService: ClientModuleMenuItemsService) {

        this.menuService.menuItems.subscribe(items => {
            this.menuItems = items;
        });

        this.authenticationService.currentUserSubject.subscribe(token => {
            if (token && this.authenticationService.getClientModuleId()) {
                this.menuService.getMenuItems();
                this.currentUser = this.authenticationService.getUserFirstNameAndLastName();
                setTimeout(() => {
                    this.siteService.getCustomerLocationsAsyncByID().then(s => {
                        if (s && s.length > 0) {
                            this.currentSite = s.find(x => x.id === Number(this.authenticationService.getUserSiteId())).name;
                        }
                    })
                }, 1500);
            }
        });
        this.notifications = [];
        this.menuService.subject.subscribe(n => {
            if (n) {
                this.notifications = n;
            }
        })
        this.languages = this.translateService.getLangs();
        const langCode = JSON.parse(localStorage.getItem(Constants.clientLang));
        this.selectedLan = langCode ?
            this.translateService.instant(this.languages.find(l => l.code === langCode).name)
            : this.translateService.instant('english');
        this.location = location;
        this.sidebarVisible = false;
    }

    ngOnInit() {
      const navbar: HTMLElement = this.element.nativeElement;
      this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
      this.router.events.subscribe((event) => {
        this.sidebarClose();
         var $layer: any = document.getElementsByClassName('close-layer')[0];
         if ($layer) {
           $layer.remove();
           this.mobile_menu_visible = 0;
         }
     });
    }

    toggleShow() {
        if (this.helperService.getBrowserName() !== 'chrome') {
          this.shouldShow = !this.shouldShow;
        }
    }

    hideSideBar() {
        this.hidden = !this.hidden;
        this.visible.emit(this.hidden);
    }

    toggleShowProfile() {
        if (this.helperService.getBrowserName() !== 'chrome') {
          this.shouldShowProfile = !this.shouldShowProfile;
        }
    }

    toggleShowNotifications() {
        if (this.helperService.getBrowserName() !== 'chrome') {
          this.shouldShowNotifications = !this.shouldShowNotifications;
        }
    }

    selectCulture(lang: Language) {
        this.translateService.use(lang);
        this.toggleShow();
        this.selectedLan = this.translateService.instant(lang.name);
      }

    logout() {
        this.toggleShowProfile();
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }

    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(function(){
            toggleButton.classList.add('toggled');
        }, 500);

        body.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        var $toggle = document.getElementsByClassName('navbar-toggler')[0];

        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
        const body = document.getElementsByTagName('body')[0];

        if (this.mobile_menu_visible == 1) {
            // $('html').removeClass('nav-open');
            body.classList.remove('nav-open');
            if ($layer) {
                $layer.remove();
            }
            setTimeout(function() {
                $toggle.classList.remove('toggled');
            }, 400);

            this.mobile_menu_visible = 0;
        } else {
            setTimeout(function() {
                $toggle.classList.add('toggled');
            }, 430);

            var $layer = document.createElement('div');
            $layer.setAttribute('class', 'close-layer');


            if (body.querySelectorAll('.main-panel')) {
                document.getElementsByClassName('main-panel')[0].appendChild($layer);
            }else if (body.classList.contains('off-canvas-sidebar')) {
                document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
            }

            setTimeout(function() {
                $layer.classList.add('visible');
            }, 100);

            $layer.onclick = function() { //asign a function
              body.classList.remove('nav-open');
              this.mobile_menu_visible = 0;
              $layer.classList.remove('visible');
              setTimeout(function() {
                  $layer.remove();
                  $toggle.classList.remove('toggled');
              }, 400);
            }.bind(this);

            body.classList.add('nav-open');
            this.mobile_menu_visible = 1;

        }
    };

    async searchRoute() {
        this.dropdownVisible = false;
        if (this.searchValue) {
            await this.clientModuleMenuItemsService.getMenuItemsByTextAsync(this.searchValue).then(async items => {
                if (items && items.length > 0) {
                    await this.clientModuleService.getAllClientModulesAsync().then(base => {
                        if (base && base.length) {
                            this.createSource(items, base);
                        }
                    })
                }
            })
        }
    }

    createSource(items: any, base: any) {
        items.forEach(i => {
            var b = base.find(x => x.id == i.clientModuleId);
            if (b != null) {
                i.baseTitle = b.title;
                i.baseRedirectUri = b.redirectUri;
                i.fullUrl = i.baseRedirectUri + i.path;
                i.displayValue = i.fullUrl;
            }
        });

        this.datasource = items;
        this.dropdownVisible = true;
    }

    onSelectItem(i){
        if (i?.fullUrl) {
            window.open(i.fullUrl, '_blank');
        }
        this.dropdownVisible = false;
    }

    onInputChange() {
        this.dropdownVisible = false; 
    }

    goToPath(items) {
        let path;
        let searchValueRegexp = new RegExp(this.searchValue, "i");

        path = this.searchAfterPath(items, searchValueRegexp);

        if (!path) {
            path = this.searchAfterKeyWords(items, searchValueRegexp);
        }

        if (path) {
            this.router.navigate([path]);
        }
    }

    searchAfterPath(items, searchValueRegexp) {
        let found = false;
        let path;

        items.forEach(item => {
            if (item.path) {
                if (item.path.match(searchValueRegexp) && !found) {
                    path = item.path;
                    found = true;
                }
            }
        })

        return path;
    }

    searchAfterKeyWords(items, searchValueRegexp) {
        let found = false;
        let path;

        items.forEach(item => {
            if (item.searchKeyWords) {
                if(item.searchKeyWords.match(searchValueRegexp) && !found) {
                    this.router.navigate([item.path]);
                    found = true;
                }
            }
        });

        return path;
    }

    goToSSO() {
        this.authenticationService.removeToken();
        window.location.href = environment.SSOPrimaware;
    }
}
