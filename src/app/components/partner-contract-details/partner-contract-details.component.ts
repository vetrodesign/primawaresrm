import { Component, Input, OnInit, OnChanges, AfterViewInit, SimpleChanges, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { ContractSupportTypeEnum } from 'app/enums/contractSupportTypeEnum';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { PersonTypeEnum } from 'app/enums/personTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { Partner } from 'app/models/partner.model';
import { PartnerContract } from 'app/models/partnerContract.model';
import { PaymentInstrument } from 'app/models/paymentinstrument.model';
import { AuthService } from 'app/services/auth.service';
import { NotificationService } from 'app/services/notification.service';
import { PageActionService } from 'app/services/page-action.service';
import { PartnerContractService } from 'app/services/partner-contract.service';
import { PaymentInstrumentService } from 'app/services/payment-instrument.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';
import { SalePriceListService } from 'app/services/sale-price-list.service';
import { SalePriceList } from 'app/models/salepricelist.model';
import { PartnerService } from 'app/services/partner.service';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { DiscountGrid } from 'app/models/discount-grid.model';
import { DiscountGridService } from 'app/services/discount-grid.service';
import { DiscountGridsByProductConventionIdAndCurrencyIdRequestModel } from 'app/models/discountGridsByProductConventionIdAndCurrencyIdRequestModel.model';
import { DiscountGridDetailsService } from 'app/services/discount-grid-details.service';
import { BenefitTypeEnum } from 'app/enums/discountGridDataTypeEnum';
import { PartnerContractGetDataForPDFRequestModel } from 'app/models/partnercontractgetdataforpdfrequestmodel.model';
import { PartnerContractPDFData } from 'app/models/partnercontractpdfdata.model';
import { DocumentTemplate } from 'app/models/document-template.model';
import { DocumentTemplateService } from 'app/services/document-template.service';
import { DocumentTypeEnum } from 'app/enums/documentTypeEnum';
import { DocumentTemplateTypeEnum } from 'app/enums/documentTemplateTypeEnum';
import { PartnerContractDownloadPDFRequestModel } from 'app/models/partnercontractdownloadpdfrequestmodel.model';
import { SaleListTypeEnum } from 'app/enums/saleListTypeEnum';
import { PartnerLocationDTO } from 'app/models/partnerlocationdto.model';
import { ProductConventionEnum } from 'app/enums/productConventionEnum';

const missingConventionPlaceholder: string = '(lipsa conventie)';

@Component({
  selector: 'app-partner-contract-details',
  templateUrl: './partner-contract-details.component.html',
  styleUrls: ['./partner-contract-details.component.css']
})
export class PartnerContractDetailsComponent implements OnInit, OnChanges, AfterViewInit {
  public static paymentTermDictionary: ReadonlyArray<{ key: number, value: string }> = [
    { key: 1, value: "1" },
    { key: 2, value: "2" },
    { key: 3, value: "5" },
    { key: 4, value: "7" },
    { key: 5, value: "10" },
    { key: 6, value: "15" },
    { key: 7, value: "20" },
    { key: 8, value: "30" },
    { key: 9, value: "45" },
    { key: 10, value: "60" },
    { key: 11, value: "75" },
    { key: 12, value: "90" },
    { key: 13, value: "105" },
    { key: 14, value: "120" },
    { key: 15, value: "180" },
    { key: 16, value: "Avans" },
    { key: 17, value: "Numerar livrare" }
  ];

  @Input() partner: Partner;

  downloadPDFPopupVisible: boolean;
  pdfData: PartnerContractPDFData;
  emptyDataList: string[];  
  documentTemplates: DocumentTemplate[] = [];

  isOwner: boolean = false;
  actions: IPageActions;
  groupedText: string;

  partnerContracts: PartnerContract[];
  personTypes: { id: number; name: string }[] = [];
  departmentTypes: { id: number; name: string }[] = [];
  paymentTermTypes: { id: number; value: string }[] = [];
  contractSupportTypes: { id: number; name: string }[] = [];
  saleListTypes: { id: number; name: string }[] = [];
  paymentInstruments: PaymentInstrument[] = [];
  partnerLocations: PartnerLocation[] = [];
  allPartnerLocations: PartnerLocation[] = [];

  displayDiscountGrids: DiscountGrid[] = [];
  displaySalePriceLists: SalePriceList[] = [];
  formDiscountGrids: DiscountGrid[] = [];
  formSalePriceLists: SalePriceList[] = [];

  selectedRows: any[];
  selectedRowIndex = -1;
  isAdmin: boolean;
  loaded: boolean;

  selectedPartnerContract: PartnerContract = new PartnerContract();
  isOnSavePartnerContract: boolean;
  popupVisible: boolean;
  popupRendered: boolean;

  discountGridEditFieldAvailable: boolean = false;
  allSalePriceLists: SalePriceList[] = [];

  @ViewChild('partnerContractsDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('partnerContractsGridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;

  constructor(
    private authenticationService: AuthService,
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private partnerService: PartnerService,
    private partnerContractService: PartnerContractService,
    private pageActionService: PageActionService,
    private paymentInstrumentService: PaymentInstrumentService,
    private documentTemplateService: DocumentTemplateService,
    private partnerLocationService: PartnerLocationService,
    private discountGridService: DiscountGridService,
    private discountGridDetailsService: DiscountGridDetailsService,
    private salePriceListService: SalePriceListService
  ) {
    this.groupedText = this.translationService.instant('groupedText');
    this.partnerContracts = [];
    this.setActions();
    this.isAdmin = this.authenticationService.isUserAdmin();
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
  }

  async loadData() {
    if (this.partner && this.partner.id) {
      await this.setPartnerLocations();
      await this.setPartnerContracts();
    }
  }

  ngOnInit(): void {
    this.loaded = true;
    Promise.all([this.setDocumentTemplatesForPartner(), this.setAllSalePriceLists(), this.setPaymentInstruments(), this.setDisplayDiscountGrids(), this.setDisplaySalePriceLists()]).then(r => {
      this.loaded = false;
    });
    this.setEnumTypes();
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  onPopupHidden() {
    this.isOnSavePartnerContract = undefined;
    this.selectedPartnerContract.discountGridId = undefined;
    this.selectedPartnerContract.salePriceListId = undefined;
    this.popupRendered = false;
    this.refreshPartnerContractsDataGrid();
  }

  private async setDisplayDiscountGrids() {
    await this.discountGridService.GetAllAsync().then(dgs => {
      this.displayDiscountGrids = dgs;
    });
  }

  private async setAllSalePriceLists() {
    await this.salePriceListService.getSalePriceListsAsyncByIDWithProductConventions().then(spls => {
      this.allSalePriceLists = spls;
    })
  }

  private async setDisplaySalePriceLists() {
    await this.salePriceListService.getAllAsync().then(spls => {
      this.displaySalePriceLists = spls;
    })
  }

  private async setPartnerContracts() {
    this.loaded = true;
    await this.partnerContractService.getPartnerContractsByPartnerID(this.partner.id).then(partnerContracts => {
      this.partnerContracts = partnerContracts;
      this.partnerContracts.forEach(i => {
        i.partnerName = this.partner.name;
      })

      this.loaded = false;
    });
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  async saveData() {
    if (this.selectedPartnerContract) {
      if (!this.selectedPartnerContract.salePriceListId) {
        this.selectedPartnerContract.salePriceListId = null;
      }

      if (this.selectedPartnerContract.isForever === false) {
        if (this.selectedPartnerContract.validTo) {
          const dateNow = new Date();
          const dateValidTo = new Date(this.selectedPartnerContract.validTo);
  
          if (dateValidTo < dateNow) {
            this.notificationService.alert('top', 'center', "Data din campul 'Valabil pana la' trebuie sa fie mai mare decat data actuala",
              NotificationTypeEnum.Red, true);
            return;
          }
        }
      }

      if (this.selectedPartnerContract.credit < 1) {
        this.notificationService.alert('top', 'center', "Campul 'Credit' trebuie sa contina o valoare mai mare ca 0",
        NotificationTypeEnum.Red, true);
        return;
      }

      if (this.validationGroup.instance.validate().isValid) {
        this.loaded = true;

        let partnerContractToAdd = new PartnerContract();
        partnerContractToAdd = this.selectedPartnerContract;
        
        const lastOwnerContractNumber: number = await this.partnerContractService.getLastOwnerContractNumberAsync();
        partnerContractToAdd.ownerContractNumber = lastOwnerContractNumber + 1;

        partnerContractToAdd.customerId = Number(this.authenticationService.getUserCustomerId());
        partnerContractToAdd.siteId = Number(this.authenticationService.getUserSiteId());
  
        partnerContractToAdd.partnerId = this.partner.id;

        if (this.selectedPartnerContract.isForever === true) {
          this.selectedPartnerContract.validTo = null;
        }
  
        if (partnerContractToAdd && partnerContractToAdd.customerId) {
          await this.partnerContractService.createPartnerContractAsync(partnerContractToAdd).then(r => {
            if (r) {
              this.notificationService.alert('top', 'center', 'Contract Partener - Datele au fost inserate cu succes!',
                NotificationTypeEnum.Green, true)
    
              this.togglePopup(false);
              this.loaded = false;
            } else {
              this.notificationService.alert('top', 'center', 'Contract Partener - Datele nu au fost inserate! A aparut o eroare!',
                NotificationTypeEnum.Red, true);
              this.togglePopup(false);
              this.loaded = false;
            }
          });
        }  else {
          this.notificationService.alert('top', 'center', 'A aparut o eroare! Datele nu au fost setate!',
          NotificationTypeEnum.Red, true);
          this.loaded = false;
        }
      } else {
        this.notificationService.alert('top', 'center', 'Form-ul nu este valid!',
        NotificationTypeEnum.Red, true);
        this.loaded = false;
      }
    }
  }

  async updateData() {
    if (this.selectedPartnerContract) {
      if (!this.selectedPartnerContract.salePriceListId) {
        this.selectedPartnerContract.salePriceListId = null;
      }

      if (this.selectedPartnerContract.isForever === false) {
        if (this.selectedPartnerContract.validTo) {
          const dateNow = new Date();
          const dateValidTo = new Date(this.selectedPartnerContract.validTo);
  
          if (dateValidTo < dateNow) {
            this.notificationService.alert('top', 'center', "Data din campul 'Valabil pana la' trebuie sa fie mai mare decat data actuala",
              NotificationTypeEnum.Red, true);
            return;
          }
        }
      }

      if (this.selectedPartnerContract.credit < 1) {
        this.notificationService.alert('top', 'center', "Campul 'Credit' trebuie sa contina o valoare mai mare ca 0",
        NotificationTypeEnum.Red, true);
        return;
      }

      if (this.validationGroup.instance.validate().isValid) {
        this.loaded = true;
        let partnerContractToUpdate = new PartnerContract();
        partnerContractToUpdate = this.selectedPartnerContract;

        if (this.selectedPartnerContract.isForever === true) {
          this.selectedPartnerContract.validTo = null;
        }

        await this.partnerContractService.updatePartnerContractAsync(partnerContractToUpdate).then(r => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Contractul de partener a fost updatat cu succes!',
              NotificationTypeEnum.Green, true);

              this.togglePopup(false);
              this.loaded = false;
            } else {
            this.notificationService.alert('top', 'center', 'Contractul de partener nu a fost updatat cu succes. A aparut o eroare!',
              NotificationTypeEnum.Red, true);

              this.togglePopup(false);
              this.loaded = false;
          }
        });
      }
    }
  }

  private setEnumTypes() {
    this.setPersonTypes();
    this.setDepartmentTypes();
    this.setPaymentTermTypes();
    this.setContractSupportTypes();
    this.setSaleListTypes();
  }
  
  private setPersonTypes() {
    for (let n in PersonTypeEnum) {
      if (typeof PersonTypeEnum[n] === 'number') {
        this.personTypes.push({
          id: <any>PersonTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
  }

  private setDepartmentTypes() {
    for (let n in DepartmentTypeEnum) {
      if (typeof DepartmentTypeEnum[n] === 'number') {
        this.departmentTypes.push({
          id: <any>DepartmentTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
  }

  private setPaymentTermTypes() {
    PartnerContractDetailsComponent.paymentTermDictionary.forEach(e => {
      this.paymentTermTypes.push({
        id: e.key,
        value: e.value
      });
    })
  }

  private setContractSupportTypes() {
    for (let n in ContractSupportTypeEnum) {
      if (typeof ContractSupportTypeEnum[n] === 'number') {
        this.contractSupportTypes.push({
          id: <any>ContractSupportTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
  }

  private setSaleListTypes() {
    for (let n in SaleListTypeEnum) {
      if (typeof SaleListTypeEnum[n] === 'number') {
        this.saleListTypes.push({
          id: <any>SaleListTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
  }

  private async setDocumentTemplatesForPartner() {
    await this.documentTemplateService.getByDocumentTypeAsync(DocumentTypeEnum.PartnerContract).then(documentTemplates => {
      this.documentTemplates = documentTemplates.filter(dt => dt.documentTemplateTypeId === DocumentTemplateTypeEnum.Content);
    });
  }

  private async setPaymentInstruments() {
    await this.paymentInstrumentService.getAllPaymentInstrumentsAsync().then(pi => {
      this.paymentInstruments = pi;
    });
  }

  private async setPartnerLocations() {
    await this.partnerLocationService.getPartnerLocationsByPartnerID(this.partner.id).then(pls => {
      this.partnerLocations = pls;
      this.allPartnerLocations = pls;

      this.partnerService.getPartnerProductConventionIdsByLocationsAsync(pls.map(x => x.id)).then((r: PartnerLocationDTO[]) => {
        if (r) {
          pls.forEach(x => {
            const item = r.find(e => e.locationId == x.id);
            if (item) {
              const conventionId = item.conventionId;

              if (conventionId) {
                x.productConventionName = ProductConventionEnum[conventionId];
                x.productConventionId = conventionId;
              } else {
                x.productConventionName = missingConventionPlaceholder;
              }
            } else {
              x.productConventionName = missingConventionPlaceholder;
            }
          });
        }
      });
    });
  }

  refreshPartnerContractsDataGrid() {
    this.setPartnerContracts()
      .then(() => {
        this.dataGrid.instance.refresh();
        this.loaded = false;
      });
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.partner).then(result => {
      this.actions = result;
    });
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  canDelete(rowSiteId) {
    if (rowSiteId && rowSiteId !== Number(this.authenticationService.getUserSiteId())) {
      return false;
    }
    return this.actions.CanDelete;
  }

  canExport() {
    return this.actions.CanExport;
  }

  canExportWord() {
    if (this.actions.CanExport && this.isAdmin) {
      return true;
    } else return false;
  }

  public async editPartnerContract(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
  }

  public addPartnerContract() {
    this.selectedPartnerContract = new PartnerContract();
    this.selectedPartnerContract.isActive = true;
    this.selectedPartnerContract.validTo = null;
    this.selectedPartnerContract.isForever = true;

    this.togglePopup(true);

    this.isOnSavePartnerContract = true;
  }

  public async openDetails(row: any) {
    this.selectedPartnerContract = row;

    if (this.selectedPartnerContract.validTo) {
      this.selectedPartnerContract.isForever = false;
    } else {
      this.selectedPartnerContract.isForever = true;
    }

    this.isOnSavePartnerContract = false;
    this.togglePopup(true);
  }

  private togglePopup(open: boolean) {
    this.popupVisible = open;
    this.popupRendered = open;

    if (open === false) {
      this.onPopupHidden();
      this.isOnSavePartnerContract = undefined;
    }
  }

  public deletePartnerContractRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public deleteRecords(data: any) {
    this.loaded = true;
    this.partnerContractService.deletePartnerContractAsync(this.selectedRows).then(r => {
      if (r) {
        this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele au fost inactivate cu succes, iar listele de pret au fost dealocate!',
        NotificationTypeEnum.Green, true);
      } else {
        this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele nu au fost inactivate! A aparut o eroare!',
        NotificationTypeEnum.Red, true);
      }

      this.refreshPartnerContractsDataGrid();
      this.loaded = false;
    });
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }
  
  dateValidationCallback(params: any): boolean {
    const currentDate = new Date();
    const inputDate = new Date(params.value);
  
    return inputDate.getTime() > currentDate.getTime();
  }

  setCellValue(newData, value) {
    let column = (<any>this);
    column.defaultSetCellValue(newData, value);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  async onPartnerLocationFieldSelectionChanged(event: any) {
    if (event && event.value) {
      if (this.selectedPartnerContract.saleListTypeId) {
        if (this.selectedPartnerContract.saleListTypeId === 2) {
          const partnerLocationId = event.value;
          const partnerLocation: PartnerLocation = this.partnerLocations.find(x => x.id === partnerLocationId);

          if (partnerLocation && (partnerLocation.productConventionId && partnerLocation.productConventionId !== 0)) {
            this.setFormSalePriceLists(partnerLocation);
          } else {
            this.formSalePriceLists = [];
            this.selectedPartnerContract.salePriceListId = undefined;
          }
        }
      }
    }
  }

  private setFormSalePriceLists(partnerLocation: PartnerLocation) {
    const selectedSalePriceLists: SalePriceList[] = this.allSalePriceLists.filter(x =>
      x.currencyId == partnerLocation.alternativeCurrencyId &&
      (x.salePriceListXProductConventionList && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(partnerLocation.productConventionId)));

    this.formSalePriceLists = selectedSalePriceLists;
  }

  async onSaleListTypeFieldSelectionChanged(event: any) {
    if (!event) return;
    if (!event.value || event.value === 1) {
      this.formSalePriceLists = [];
      this.selectedPartnerContract.salePriceListId = undefined;
      return;
    };

    const selectedPartnerLocation: PartnerLocation = this.partnerLocations.find(x => x.id === this.selectedPartnerContract.partnerLocationId);

    if (selectedPartnerLocation && (selectedPartnerLocation.productConventionId && selectedPartnerLocation.productConventionId !== 0)) {
      this.setFormSalePriceLists(selectedPartnerLocation);
    }
  }

  private disableDiscountGridField(showAlert: boolean, reasonMessage: string = ""): void {
    this.selectedPartnerContract.discountGridId = undefined;
    this.formDiscountGrids = [];
    
    if (showAlert) {
      this.notificationService.alert('top', 'center', reasonMessage, NotificationTypeEnum.Red, true);
    }
  }

  private enableDiscountGridField(discountGridsData: DiscountGrid[]): void {
    this.formDiscountGrids = discountGridsData;
  }

  async onDiscountGridFieldSelectionChanged(event: any) {
    if (event.previousValue !== undefined && event.previousValue !== null) {
      this.selectedPartnerContract.salePriceListId = undefined;
    }

    if ((event.value === undefined || event.value === null)) {
      this.disableSalePriceListField(false);
    }

    if (event.value) {
      if (this.formDiscountGrids.length === 0) {
        await this.partnerService.getPartnerProductConventionIdByLocationAsync(this.selectedPartnerContract.partnerLocationId).then(productConventionId => {
          if (productConventionId !== 0) {
  
            const baseCurrencyId = this.partnerLocations.find(pl => pl.id === this.selectedPartnerContract.partnerLocationId).baseCurrencyId;
  
            const discountGridsRequestModel: DiscountGridsByProductConventionIdAndCurrencyIdRequestModel = new DiscountGridsByProductConventionIdAndCurrencyIdRequestModel();
            discountGridsRequestModel.productConventionId = productConventionId;
            discountGridsRequestModel.currencyId = baseCurrencyId;
  
            this.discountGridService.GetDiscountGridsByProductConventionIdAndCurrencyId(discountGridsRequestModel).then(formDiscountGrids => {
              if (formDiscountGrids && formDiscountGrids.length > 0) {
                const filteredByBenefitTypeFormDiscountGrids: DiscountGrid[] = formDiscountGrids.filter(fdg => fdg.benefitType === BenefitTypeEnum.SalePriceList);
                this.enableDiscountGridField(filteredByBenefitTypeFormDiscountGrids);
              } else if (formDiscountGrids && formDiscountGrids.length === 0) {
                this.disableDiscountGridField(false);
              }
            }).then(async () => {
              await this.discountGridDetailsService.GetByDiscountGridsAsync(this.formDiscountGrids.map(fdg => fdg.id)).then(async formDiscountGridsDetails => {
                if (formDiscountGridsDetails && formDiscountGridsDetails.length > 0) {            
                } else {
                  this.disableSalePriceListField(true, "Campul Lista de Pret - Nu au fost gasite intervale pentru grila de discount selectata.");
                }
        
                await this.salePriceListService.getAllByIds(formDiscountGridsDetails.map(fdgd => fdgd.salePriceListId)).then(salePriceLists => {
                  if (salePriceLists && salePriceLists.length > 0) {            
                    const filteredFormDiscountGridDetailsByCurrentDiscountGridId = formDiscountGridsDetails.filter(fdgd => fdgd.discountGridId === this.selectedPartnerContract.discountGridId);
                    const formSalePriceLists: SalePriceList[] = salePriceLists.filter(spl => filteredFormDiscountGridDetailsByCurrentDiscountGridId.map(x => x.salePriceListId).includes(spl.id));
        
                    this.enableSalePriceListField(formSalePriceLists)
                  } else {
                    this.disableSalePriceListField(true, "Campul Lista de Pret - Nu au fost gasite liste de pret pentru grila de discount selectata.");
                  }
                });
              });
            });
  
          } else {
            this.disableDiscountGridField(false);
          }
        });
      } else {
        await this.discountGridDetailsService.GetByDiscountGridsAsync(this.formDiscountGrids.map(fdg => fdg.id)).then(async formDiscountGridsDetails => {
          if (formDiscountGridsDetails && formDiscountGridsDetails.length > 0) {            
          } else {
            this.disableSalePriceListField(true, "Campul Lista de Pret - Nu au fost gasite intervale pentru grila de discount selectata.");
          }
  
          await this.salePriceListService.getAllByIds(formDiscountGridsDetails.map(fdgd => fdgd.salePriceListId)).then(salePriceLists => {
            if (salePriceLists && salePriceLists.length > 0) {            
              const filteredFormDiscountGridDetailsByCurrentDiscountGridId = formDiscountGridsDetails.filter(fdgd => fdgd.discountGridId === this.selectedPartnerContract.discountGridId);
              const formSalePriceLists: SalePriceList[] = salePriceLists.filter(spl => filteredFormDiscountGridDetailsByCurrentDiscountGridId.map(x => x.salePriceListId).includes(spl.id));
  
              this.enableSalePriceListField(formSalePriceLists)
            } else {
              this.disableSalePriceListField(true, "Campul Lista de Pret - Nu au fost gasite liste de pret pentru grila de discount selectata.");
            }
          });
        });
      }
    }
  }


  public onDownloadContractPdfPopupHidden() {
    this.pdfData = null;
    this.emptyDataList = null;
  }

  public async openDownloadContractPdfPopup(data: any) {

    this.loaded = true;

    const requestModel: PartnerContractGetDataForPDFRequestModel = new PartnerContractGetDataForPDFRequestModel();
    requestModel.contractId = data.id;
    requestModel.paymentTerm = PartnerContractDetailsComponent.paymentTermDictionary[data.paymentTermId].value;

    await this.partnerContractService.getDataForPdfAsync(requestModel).then(r => {
      this.pdfData = r.pdfData;
      this.emptyDataList = r.emptyDataList;
      this.downloadPDFPopupVisible = true;
      this.loaded = false;
    });
  }

  public async downloadContractPdf(documentTemplateName: string) {
    this.loaded = true;

    const requestModel: PartnerContractDownloadPDFRequestModel = new PartnerContractDownloadPDFRequestModel();
    requestModel.pdfData = this.pdfData;
    requestModel.documentTemplateName = documentTemplateName;

    await this.partnerContractService.downloadPdfAsync(requestModel).then(r => {
      if (r) {
        // Convert the base64 encoded fileContents to a Uint8Array
        const binaryString = window.atob(r.fileContents);
        const byteArray = new Uint8Array(binaryString.length);
        for (let i = 0; i < binaryString.length; i++) {
          byteArray[i] = binaryString.charCodeAt(i);
        }

        const blob = new Blob([byteArray], { type: r.fileContents }); // Adjust the 'type' as needed

        // const blob = r.fileResult as Blob;
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');

        link.href = url;

        const date: Date = new Date();
        const dateView: string = `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}_${date.getHours()}${date.getMinutes()}${date.getSeconds()}`;
        link.download = `ContractPartener` + '-' + dateView + '.pdf';

        link.click();
        window.URL.revokeObjectURL(url);

        this.loaded = false;
      } else {
        this.loaded = false;
      }
    })
  }

  public async downloadContractWord(documentTemplateName: string) {
    this.loaded = true;
  
    const requestModel: PartnerContractDownloadPDFRequestModel = new PartnerContractDownloadPDFRequestModel();
    requestModel.pdfData = this.pdfData;
    requestModel.documentTemplateName = documentTemplateName;
    try {
      const r = await this.partnerContractService.downloadWordAsync(requestModel);
      if (r && r.fileContents) {
        // Correctly decode Base64 to Uint8Array
        const byteArray = Uint8Array.from(atob(r.fileContents), c => c.charCodeAt(0));
  
        // Create a Blob with the correct MIME type
        const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });
  
        // Generate a timestamp for the filename
        const date: Date = new Date();
        const dateView: string = `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}_${date.getHours()}${date.getMinutes()}${date.getSeconds()}`;
        const filename = `ContractPartener-${dateView}.docx`;
  
        // Create a download link and trigger it
        const link = document.createElement('a');
        link.href = URL.createObjectURL(blob);
        link.download = filename;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        URL.revokeObjectURL(link.href);
      } else {
        console.error('No file content received.');
      }
    } catch (error) {
      console.error('Error downloading file:', error);
    } finally {
      this.loaded = false;
    }
  }

  private disableSalePriceListField(showAlert: boolean, reasonMessage: string = ""): void {
    this.selectedPartnerContract.salePriceListId = undefined;
    this.formSalePriceLists = [];
    
    if (showAlert) {
      this.notificationService.alert('top', 'center', reasonMessage, NotificationTypeEnum.Red, true);
    }
  }

  isSalePriceListsFieldEnabled: boolean = false;

  private enableSalePriceListField(salePriceListsData: SalePriceList[]): void {
    this.formSalePriceLists = salePriceListsData;
    this.isSalePriceListsFieldEnabled = true;
  }

  getDisplayExprOwnerContractNumber(item) {
    if (!item) {
      return '';
    }
    return 'VD-' + item.ownerContractNumber;
  }

  getSortExprOwnerContractNumber(item) {
    return item.ownerContractNumber;
  }

  getDisplayExprPartnerType(item) {
    if (!item) {
      return '';
    }
    
    return item.name;
  }

  getDisplayExprDepartmentType(item) {
    if (!item) {
      return '';
    }
    
    return item.name;
  }

  getDisplayExprPartnerLocations(item: PartnerLocation) {
    if (!item) {
      return '';
    }
    
    return item.name + (item.isImplicit ? ' (locatie implicita)' : '') + ' - ' + item.productConventionName;
  }

  getDisplayExprDiscountGrids(item) {
    if (!item) {
      return '';
    }
  
    return item.name + ' - ' + item.description;
  }

  getDisplayExprPaymentInstrumentType(item) {
    if (!item) {
      return '';
    }
  
    return item.name;
  }

  getDisplayExprPaymentTermType(item) {
    if (!item) {
      return '';
    }
  
    return item.value;
  }

  getDisplayExprContractSupportType(item) {
    if (!item) {
      return '';
    }
  
    return item.name;
  }

  getDisplayExprSalePriceList(item) {
    if (!item) {
      return '';
    }
  
    return item.name;
  }

  getDisplayExprSaleListType(item) {
    if (!item) {
      return '';
    }

    return item.name;
  }
}
