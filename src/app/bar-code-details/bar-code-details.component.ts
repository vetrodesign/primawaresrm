import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BarCodeStandardTypeEnum } from 'app/enums/barcodeStandardTypeEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { BarCodeInterval } from 'app/models/barcodeinterval.model';
import { Country } from 'app/models/country.model';
import { CountryBarCodeStandard } from 'app/models/countrybarcodestandard.model';
import { IPageActions } from 'app/models/ipageactions';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { AuthService } from 'app/services/auth.service';
import { BarCodeIntervalService } from 'app/services/bar-code-interval.service';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';
import { CountryBarCodeStandardService } from 'app/services/country-bar-code-standard.service';
import { CountryService } from 'app/services/country.service';
import { NotificationService } from 'app/services/notification.service';
import { PartnerService } from 'app/services/partner.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';

@Component({
  selector: 'app-bar-code-details',
  templateUrl: './bar-code-details.component.html',
  styleUrls: ['./bar-code-details.component.css']
})
export class BarCodeDetailsComponent implements OnInit {
  @Input() selectedBarCode: BarCodeInterval;

  @Output() visible: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;

  barCode: BarCodeInterval;
  isOnSave: boolean;
  standardTypes: { id: number; name: string }[] = [];
  countrys: Country[];
  countryBarCodeStandard: CountryBarCodeStandard[];
  countryBarCodeStandardByType: CountryBarCodeStandard[];
  digitPattern: any = /^[0-9]*$/;
  loaded: boolean;
  provenanceInfoPopupVisible: boolean;
  actions: IPageActions;
  infoButton: string;
  partners: any = {
    paginate: true,
    pageSize: 15,
    store: []
  };
  selectedRows: any[];
  selectedRowIndex = -1;
  partnerId: any;

  constructor(private translationService: TranslateService, private countryService: CountryService, private authenticationService: AuthService,
    private barCodeIntervalService: BarCodeIntervalService, private notificationService: NotificationService, private countryBarCodeStandardService: CountryBarCodeStandardService,
    private partnerService: PartnerService,
    private route: ActivatedRoute, private clientModuleMenuItemsService: ClientModuleMenuItemsService) {
    this.getCountrys();
    this.getCountrysStandards();
    for (let n in BarCodeStandardTypeEnum) {
      if (typeof BarCodeStandardTypeEnum[n] === 'number') {
        this.standardTypes.push({
          id: <any>BarCodeStandardTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    this.setActions();
     
   }

  ngOnInit(): void {
  }

 

  async getCountrys() {
    await this.countryService.getAllCountrysAsync().then(c => {
      this.countrys = c;
    });
  }

  async getCountrysStandards() {
    await this.countryBarCodeStandardService.getAllCountryBarCodeStandardsAsync().then(c => {
      this.countryBarCodeStandard = c;
    });
  }

  async loadData() {
    this.barCode = new BarCodeInterval();
    if (this.selectedBarCode) {
      this.isOnSave = false;
      this.barCode = new BarCodeInterval(this.selectedBarCode);
      this.barCode.setIntervals();

      this.partnerService.getPartnerByPartnerIdsAsync([this.barCode.partnerId]).then(items => {
        this.partners.store.push(...items.filter(x => !this.partners.store.map(y => y.id).includes(x.id)));
        this.partnerId = items[0].id;
      });
      
    } else {
      this.partnerId = null;
      this.partners.store = [];
      this.isOnSave = true;
    }
  }

  backToBarCode() {
    this.barCode = null;
    this.visible.emit(false);
  }

  getDisplayExprCountries(item) {
    if (!item) {
       return '';
     }
     return item.code + ' - ' + item.name;
  }

  async saveData() {
    if (this.validationGroup.instance.validate().isValid && this.validateBarCode()) {
      this.loaded = true;
      let item = new BarCodeInterval();
      item = this.barCode;
      item.to = Number(item.toString);
      item.from = Number(item.fromString);
      item.customerId = Number(this.authenticationService.getUserCustomerId());
      if (item  && item.customerId) {
        await this.barCodeIntervalService.createBarCodeIntervalAsync(item).then(response => {
          if (response) {
            this.notificationService.alert('top', 'center', 'Coduri de bare - Datele au fost inserate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Coduri de bare - Datele nu au fost inserate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          }
          this.loaded = false;
        });
      } else {
        // Validare
        this.notificationService.alert('top', 'center', 'Alocari coduri de bare - Datele nu au fost inserate! Produsul are deja un cod de bare asignat sau acest cod este pe un alt produs!',
        NotificationTypeEnum.Red, true)
        this.loaded = false;
      }
      this.backToBarCode();
    }
  }

  async updateData() {
    if (this.validationGroup.instance.validate().isValid && this.validateBarCode()) {
      this.loaded = true;
      let item = new BarCodeInterval();
      item = this.barCode;
      item.to = Number(item.toString);
      item.from = Number(item.fromString);
      if (item) {
        await this.barCodeIntervalService.updateBarCodeIntervalAsync(item).then(response => {
          if (response) {
            this.barCode.rowVersion = response.rowVersion;
            this.notificationService.alert('top', 'center', 'Clocari coduri de bare - Datele au fost modificate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Clocari coduri de bare - Datele nu au fost modificate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          }
          this.loaded = false;
        });
        this.backToBarCode();
      } 
    }
  }

  ownerCodeChange(e: any) {
    if (e && e.value) {
      let countryCode = Number(e.value.substring(0, 3));
      if (!isNaN(countryCode)) {
        let standard = this.countryBarCodeStandard.find(x => x.standardType === this.barCode.standardType && x.from <= countryCode && x.to >= countryCode);
        if (standard) {
          this.barCode.countryId = standard.countryId;
        } else {
          this.barCode.countryId = null;
        }
      }

      const { number2, number3 } = this.calculateNumbers(Number(this.barCode.ownerCode));
      this.barCode.fromString = number2;
      this.barCode.toString = number3;
    }
  }

  calculateNumbers(number1: number): { number2: string, number3: string } {
    const num1Str = number1.toString();
    const lengthOfNum1 = num1Str.length;

    // Calculate the missing digits to make it 11 digits long
    const missingLength = 12 - lengthOfNum1;

    // Create number2: Add missing zeros before '0001'
    const number2 = '0'.repeat(missingLength - 1) + '1';

    // Create number3: Add missing nines before '9999'
    const number3 = '9'.repeat(missingLength - 1) + '9';

    return { number2, number3 };
}

  validateBarCode(): boolean {
    if (this.barCode.from > this.barCode.to) {
      return false;
    } else {
      return true;
    }
  }

  setActions() {
    this.actions = { CanView: true, CanAdd: true, CanUpdate: true, CanDelete: true, 
      CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
  }

  public refreshDataGrid() {
    this.getCountrysStandards();

    this.dataGrid.instance.refresh();
  }

  public async onInitNewRow(data: any) {
    if (data && data.data) {
      data.data.isActive = true;
    }
  } 

  public addBarCodeStandard() {
    this.dataGrid.instance.addRow();
  }

  public async onRowPrepared(event: any) {
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, true);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public deleteRecords(data: any) {
    this.countryBarCodeStandardService.deleteCountryBarCodeStandardsAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (!response) {
        this.notificationService.alert('top', 'center', 'Standard Cod Bare Tari - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Standard Cod Bare Tari - Datele nu au fost sterse!',
          NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  public async onRowUpdated(event: any): Promise<void> {
    let item = new CountryBarCodeStandard();
    item = this.countryBarCodeStandard.find(g => g.id === event.key.id);
    if (item) {
      await this.countryBarCodeStandardService.updateCountryBarCodeStandardsAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Standard Cod Bare Tari - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Standard Cod Bare Tari - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };

        this.refreshDataGrid();
      });
    }
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onRowInserting(event: any): Promise<void> {
    let item = new CountryBarCodeStandard();
    item = event.data;
    if (item) {
      await this.countryBarCodeStandardService.createCountryBarCodeStandardsAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Standard Cod Bare Tari - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Standard Cod Bare Tari - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshDataGrid();
      });
    }
  }

  getDisplayExprPartners(item) {
    if (!item) {
        return '';
      }
      return item.code + ' - ' + item.name;
  }

  onPartnerChanged(e) {
    if (e && e.target && e.target.value && e.target.value.length > 4) {
      let searchFilter = new PartnerSearchFilter();
      searchFilter.name = e.target.value;
      searchFilter.isActive = true;
      this.partnerService.getPartnersByFilter(searchFilter).then(items => {
        this.partners.store.push(...items.filter(x => !this.partners.store.map(y => y.id).includes(x.id)));
      })
    }
  }

  partnerChange(e) {
    if (e) {
      this.barCode.partnerId = e.value;
    }
  }


}
