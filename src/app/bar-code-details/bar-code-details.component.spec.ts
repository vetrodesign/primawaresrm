import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarCodeDetailsComponent } from './bar-code-details.component';

describe('BarCodeDetailsComponent', () => {
  let component: BarCodeDetailsComponent;
  let fixture: ComponentFixture<BarCodeDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarCodeDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarCodeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
