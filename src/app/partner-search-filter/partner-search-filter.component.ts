import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { ProductConventionEnum } from 'app/enums/productConventionEnum';
import { City } from 'app/models/city.model';
import { Country } from 'app/models/country.model';
import { County } from 'app/models/county.model';
import { Language } from 'app/models/language.model';
import { PartnerActivity } from 'app/models/partneractivity.model';
import { PartnerActivityAllocation } from 'app/models/partneractivityallocation.model';
import { PartnerSearchFilter, SuppliersCountData } from 'app/models/partnerSearchFilter.model';
import { Post } from 'app/models/post.model';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { UsersService } from 'app/services/user.service';
import { DxValidatorComponent } from 'devextreme-angular';
import { Site } from 'app/models/site.model';
import { Tag } from 'app/models/tag.model';
import { ItemGroupCategory } from 'app/models/itemgroupcategory.model';
import { ItemGroupCode } from 'app/models/itemgroupcode.model';
import { Department } from 'app/models/department.model';
import { Office } from 'app/models/office.model';
import { PersonTypeEnum } from 'app/enums/personTypeEnum';
import { PartnerTypeEnum } from 'app/enums/partnerTypeEnum';
import { User } from 'app/models/user.model';
import { PartnerService } from 'app/services/partner.service';
import { ClientSmallSearchFilter } from 'app/models/client-small-search-filter.model';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';
import { SupplierSmallSearchFilter } from 'app/models/suppliersmallsearchfilter.model';

@Component({
  selector: 'app-partner-search-filter',
  templateUrl: './partner-search-filter.component.html',
  styleUrls: ['./partner-search-filter.component.css']
})
export class PartnerSearchFilterComponent implements OnInit, OnChanges {
  @Input() tags: Tag[];
  @Input() sites: Site[];
  @Input() posts: Post[];
  @Input() departments: any;
  @Input() partnersTypes: any;
  @Input() personTypes: any;
  @Input() users: User[];

  @Input() clientTypes: any;
  @Input() supplierTypes: any;

  @Input() cities: City[];
  @Input() citiesDS: any;

  @Input() counties: County[];
  @Input() countiesDS: any;

  @Input() countries: Country[];
  @Input() countriesDS: any;

  @Input() language: Language[];

  @Input() partnerActivity: PartnerActivity[];
  @Input() partnerActivityAllocation: PartnerActivityAllocation[];

  @Input() caenSpecializationsDS: any;
  @Input() itemGroupCategories: ItemGroupCategory[];
  @Input() itemGroupCodes: ItemGroupCode[];
  @Input() itemGroupCodesDS: any;
  @Input() caenGridDataSource: any;
  @Output() searchEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() refreshEvent: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('validationSearchGroup') validationSearchGroup: DxValidatorComponent;
  partnerSearchFilter: PartnerSearchFilter;
  productConvention: { id: number; name: string }[] = [];

  partnerActivityAllocationDS: PartnerActivityAllocation[] = [];
  postsDS: Post[] = [];
  userPost: Post;
  isFromMngDepartment: boolean = false;
  isOwner: boolean;
  nameOrCodeFilterPartners: any;
  fiscalCodeFilterPartners: any;
  emailFilterPartners: any;
  nameOrCodeSelectedPartner: any;
  fiscalCodeSelectedPartner: any;
  emailSelectedPartner: any;
  suppliersCountData: SuppliersCountData;

  constructor(private translationService: TranslateService,
    private userService: UsersService, private authService: AuthService, private partnerService: PartnerService) {
    this.getDisplayExprPartnerActivity = this.getDisplayExprPartnerActivity.bind(this);
    this.authService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
    for (let n in ProductConventionEnum) {
      if (typeof ProductConventionEnum[n] === 'number') {
        this.productConvention.push({
          id: <any>ProductConventionEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    this.partnerSearchFilter = new PartnerSearchFilter();
    this.partnerSearchFilter.isActive = true;
    this.partnerSearchFilter.isAssociatedItemsSelected = false;
    this.partnerSearchFilter.isActiveItemsSelected = false;
    this.partnerSearchFilter.isInactiveItemsSelected = false;
    this.partnerSearchFilter.isAllTurnoverSelected = true;
    this.partnerSearchFilter.isWithTurnoverSelected = false;
    this.partnerSearchFilter.isWithoutTurnoverSelected = false;
    this.partnerSearchFilter.isActiveSuppliersSelected = false;
    this.partnerSearchFilter.isInactiveSuppliersSelected = false;
    this.partnerSearchFilter.isPossibleSuppliersSelected = false;
    this.partnerSearchFilter.personTypes = PersonTypeEnum.Legal;
    //this.partnerSearchFilter.partnerType = [PartnerTypeEnum.Provider];
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.posts || changes.departments) {
      this.userService.getByUsernameOrEmail(this.authService.getUserUserName(), null).then(async user => {
        if (user && user.length > 0 && this.departments && this.departments.length > 0) {
          if (user[0] && user[0].postId) {
            this.userPost = this.posts.find(x => Number(x.id) === user[0].postId);
            let dep = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Management);
            const officesFromMngDep = (dep && dep.length > 0 ) ? dep.map(x => x.offices).reduce((x, y) => x.concat(y)) : null; 
            if (officesFromMngDep && officesFromMngDep.length && (officesFromMngDep.find(x => x.id === this.userPost.officeId) !== null)) {
              this.isFromMngDepartment = true;
            }
            await this.getPosts();
          }
        }
      });
    }
    if (changes.partnerActivityAllocation) {
      this.partnerActivityAllocationDS = this.partnerActivityAllocation;
    }
  }

  async getSuppliersCountData(postId?: number) {
    await this.partnerService.getSuppliersCountData(postId).then(result => {
      this.suppliersCountData = result;
    });
  }

  public isUserAdminOrHasSuperiorPost(): boolean {
    if (this.authService.isUserAdmin()) {
      return true;
    } else {
      if (this.posts && this.posts.length > 0 && this.users && this.users.length > 0) {
        let post = this.posts?.find(x => x?.id === this.users.find(x => x.id === Number(this.authService.getUserId()))?.postId);
        if (post && (post.isDepartmentSuperior || post.isGeneralManager || post.isLeadershipPost || post.isOfficeSuperior)) {
          return true;
        } else {
          return false;
        }
      }
      return false;
    }
  }

  async getPosts() {
    this.postsDS = [];
    if (this.userPost) {
      if (this.userPost.isOfficeSuperior) {
        this.postsDS = this.departments.map(x => x.offices).reduce((x, y) => x.concat(y)).filter(z => z.id === this.userPost.officeId).map(p => p.posts).reduce((g, h) => g.concat());
      }
      if (this.userPost.isDepartmentSuperior) {
        this.postsDS = this.departments.find(x => x.offices.map(z => z.id).includes(this.userPost.officeId)).offices.map(p => p.posts).reduce((x, y) => x.concat(y));
      }
      if (this.userPost.isLocationSuperior || this.userPost.isGeneralManager || this.isFromMngDepartment || this.isOwner) {
        this.postsDS = this.posts;
      } else {
        this.postsDS.push(this.userPost);
      }
    }

    await this.userService.getUsersAsync().then(async (users) => {
      if (users && users.length > 0) {
        const supplyDepartments: Department[] = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Supply); 
        const supplyOffices: Office[] = supplyDepartments.reduce((acc: Office[], dept: Department) => acc.concat(dept.offices), []);
        const supplyPosts: Post[] = supplyOffices.reduce((acc: Post[], office: Office) => acc.concat(office.posts), []);
        const activeSupplyPosts = supplyPosts.filter((post) => post.isActive === true);

        this.postsDS = activeSupplyPosts.filter(f => users.map(m => m.postId).includes(f.id));

        if (!this.isUserAdminOrHasSuperiorPost()) {
          this.postsDS = this.postsDS.filter(f => f.id === +this.authService.getUserPostId());

          if (this.postsDS && this.postsDS.length > 0) {
            this.partnerSearchFilter.postIds = [this.postsDS[0].id];
            await this.getSuppliersCountData(this.postsDS[0].id);
          }
        } else {
          await this.getSuppliersCountData(null);
        }
      }
    });
  }

  searchPartners() {
    if (this.validationSearchGroup.instance.validate().isValid) {
      if (this.nameOrCodeSelectedPartner) {
        this.partnerSearchFilter.nameOrCode = this.nameOrCodeSelectedPartner.code;
      } else {
        this.partnerSearchFilter.nameOrCode = this.partnerSearchFilter.customNameOrCode;
      }

      this.searchEvent.emit(this.partnerSearchFilter);
    }
  }

  resetFilters() {
    this.partnerSearchFilter = new PartnerSearchFilter();
    this.partnerSearchFilter.isActive = true;
    this.partnerSearchFilter.isAssociatedItemsSelected = false;
    this.partnerSearchFilter.isActiveItemsSelected = false;
    this.partnerSearchFilter.isInactiveItemsSelected = false;
    this.partnerSearchFilter.isAllTurnoverSelected = true;
    this.partnerSearchFilter.isWithTurnoverSelected = false;
    this.partnerSearchFilter.isWithoutTurnoverSelected = false;
    this.partnerSearchFilter.isActiveSuppliersSelected = false;
    this.partnerSearchFilter.isInactiveSuppliersSelected = false;
    this.partnerSearchFilter.isPossibleSuppliersSelected = false;
    this.partnerSearchFilter.personTypes = PersonTypeEnum.Legal;
    this.partnerSearchFilter.partnerType = [PartnerTypeEnum.Provider];

    this.refreshEvent.emit(true);
  }

  onCitiesChanged(e: any) {
    if (e.value && e.value.length > 0) {
        const selectedCountries = this.partnerSearchFilter.countriesIds || [];
        const selectedCounties = this.partnerSearchFilter.countiesIds || [];
        
        const countryIds = this.cities.filter(x => e.value.includes(x.id)).map(y => y.countryId);
        const countiesIds = this.cities.filter(x => e.value.includes(x.id)).map(y => y.countyId);

        this.countriesDS = {
            paginate: true,
            pageSize: 15,
            store: this.countries.filter(i => countryIds.includes(i.id) || selectedCountries.includes(i.id))
        };

        this.countiesDS = {
            paginate: true,
            pageSize: 15,
            store: this.counties.filter(i => countiesIds.includes(i.id) || selectedCounties.includes(i.id))
        };
    } else {
        this.restoreDS();
    }
  }

  onCountiesChanged(e: any) {
    if (e.value && e.value.length) {
        const selectedCities = this.partnerSearchFilter.citiesIds || [];
        const selectedCountries = this.partnerSearchFilter.countriesIds || [];

        this.citiesDS = {
            paginate: true,
            pageSize: 15,
            store: this.cities.filter(i => e.value.includes(i.countyId) || selectedCities.includes(i.id))
        };

        const countryIds = this.counties.filter(x => e.value.includes(x.id)).map(y => y.countryId);
        this.countriesDS = {
            paginate: true,
            pageSize: 15,
            store: this.countries.filter(i => countryIds.includes(i.id) || selectedCountries.includes(i.id))
        };
    } else {
        this.restoreDS();
    }
  }

  onCountriesChanged(e: any) {
    if (e.value && e.value.length) {
        const selectedCities = this.partnerSearchFilter.citiesIds || [];
        const selectedCounties = this.partnerSearchFilter.countiesIds || [];

        this.countiesDS = {
            paginate: true,
            pageSize: 15,
            store: this.counties.filter(i => e.value.includes(i.countryId) || selectedCounties.includes(i.id))
        };

        this.citiesDS = {
            paginate: true,
            pageSize: 15,
            store: this.cities.filter(i => e.value.includes(i.countryId) || selectedCities.includes(i.id))
        };
    } else {
        this.restoreDS();
    }
  }

  restoreDS() {
    this.citiesDS = {
      paginate: true,
      pageSize: 15,
      store: this.cities
    };
    this.countiesDS = {
      paginate: true,
      pageSize: 15,
      store: this.counties
    };
    this.countriesDS = {
      paginate: true,
      pageSize: 15,
      store: this.countries
    };
  }

  getDisplayExprCities(item) {
    if (!item) {
      return '';
    }
    return item.name + ' - ' + item.countyName + ' - ' + item.countryName;
  }

  getSpecializationDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.description;
  }

  getDisplayExprPartnerActivity(value: any) {
    if (value && this.partnerActivity) {
      const item = this.partnerActivity.find(x => x.id === value.partnerActivityId);
      if (item) {
        return value.saleZoneCode + ' - ' + item.name + ' - ' + value.partnerActivityClassification;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  itemGroupCategoryDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.catCode + ' - ' + item.firstName;
  }

  itemGroupCodeDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.grpCode + ' - ' + item.firstName;
  }

  onMultiTagPostChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagCitiesChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagCountieChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagLanguageChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 25) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagPartnerActivityChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagSpecializationChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagSitesChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }


  onProductConventionChanged(e) {
    if (e && e.value && e.value.length > 0) {
      this.partnerActivityAllocationDS = this.partnerActivityAllocation.filter(x => e.value.includes(x.productConventionId));
    } else {
      this.partnerActivityAllocationDS = this.partnerActivityAllocation;
    }
  }

  toggleTurnoverCheckbox(selected: string, event: any): void {
    const isChecked = event.value; 

    if (isChecked) {
      if (selected === 'all') {
          this.partnerSearchFilter.isWithTurnoverSelected = false;
          this.partnerSearchFilter.isWithoutTurnoverSelected = false;
      } else if (selected === 'with') {
          this.partnerSearchFilter.isAllTurnoverSelected = false;
          this.partnerSearchFilter.isWithoutTurnoverSelected = false;
      } else if (selected === 'without') {
          this.partnerSearchFilter.isAllTurnoverSelected = false;
          this.partnerSearchFilter.isWithTurnoverSelected = false;
      }
    }
  }

  onNameOrCodeFilterSearch(searchTerm) {
    if (searchTerm && searchTerm.length >= 6) {
      let searchFilter = new ClientSmallSearchFilter();
      searchFilter.nameOrCode = searchTerm;

      this.partnerService.getPartnersSmallAsync(searchFilter).then(items => {
        this.nameOrCodeFilterPartners = (items && items.length > 0) ? items : [];
        this.nameOrCodeFilterPartners = this.nameOrCodeFilterPartners.map(m => ({ ...m, codeAndName: `${m.code} - ${m.name}` }));
      });
    } else {
      this.nameOrCodeFilterPartners = [];
      this.nameOrCodeSelectedPartner = null;
    }
  }

  onNameOrCodeFilterItemSelected(event) {
    if (event && event.selectedItem) {
      this.nameOrCodeSelectedPartner = event.selectedItem;

      var url = environment.SRMPrimaware + '/' + Constants.partner + '?id=' + this.nameOrCodeSelectedPartner.id;
      window.open(url, '_blank');
    }
  }

  onFiscalCodeFilterSearch(searchTerm) {
    if (searchTerm && searchTerm.length >= 4) {
      let searchFilter = new SupplierSmallSearchFilter();
      searchFilter.fiscalCode = searchTerm;

      this.partnerService.getSuppliersSmallAsync(searchFilter).then(items => {
        this.fiscalCodeFilterPartners = (items && items.length > 0) ? items : [];
      });
    } else {
      this.fiscalCodeFilterPartners = [];
      this.fiscalCodeSelectedPartner = null;
    }
  }

  onFiscalCodeFilterItemSelected(event) {
    if (event && event.selectedItem) {
      this.fiscalCodeSelectedPartner = event.selectedItem;
    }
  }

  onEmailFilterSearch(searchTerm) {
    if (searchTerm && searchTerm.length >= 6) {
      let searchFilter = new ClientSmallSearchFilter();
      searchFilter.email = searchTerm;

      this.partnerService.getSuppliersSmallByEmailFilterAsync(searchFilter).then(items => {
        this.emailFilterPartners = (items && items.length > 0) ? items : [];
      });
    } else {
      this.emailFilterPartners = [];
      this.emailSelectedPartner = null;
    }
  }

  onEmailFilterItemSelected(event) {
    if (event && event.selectedItem) {
      this.emailSelectedPartner = event.selectedItem;
    }
  }

  openDetails() {
    if (this.nameOrCodeSelectedPartner) {
      var url = environment.SRMPrimaware + '/' + Constants.partner + '?id=' + this.nameOrCodeSelectedPartner.id;
      window.open(url, '_blank');
    }

    if (this.fiscalCodeSelectedPartner) {
      var url = environment.SRMPrimaware + '/' + Constants.partner + '?id=' + this.fiscalCodeSelectedPartner.id;
      window.open(url, '_blank');
    }

    if (this.emailSelectedPartner) {
      var url = environment.SRMPrimaware + '/' + Constants.partner + '?id=' + this.emailSelectedPartner.id;
      window.open(url, '_blank');
    }
  }

  caenGridBoxDisplayExpr(item) {
    return item && `${item.code} - ${item.description}`;
  }
}