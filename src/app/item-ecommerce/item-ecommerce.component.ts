import { Component, Input, OnInit } from '@angular/core';
import { Item } from 'app/models/item.model';
import { ItemEcommerce } from 'app/models/itemecommerce.model';
import { AuthService } from 'app/services/auth.service';
import { ItemEcommerceService } from 'app/services/item-ecommerce.service';


@Component({
  selector: 'app-item-ecommerce',
  templateUrl: './item-ecommerce.component.html',
  styleUrls: ['./item-ecommerce.component.css']
})
export class ItemEcommerceComponent implements OnInit {
@Input() item: Item;

selectedItemEcommerce: ItemEcommerce;
isOnSave: boolean;
isOwner: boolean;

  constructor(private itemEcommerceService: ItemEcommerceService,
    private authenticationService: AuthService
    ) {
      this.authenticationService.currentUserSubject.subscribe(token => {
        if (token) {
          if (this.authenticationService.isUserOwner()) {
            this.isOwner = true;
          }
        }
      });
     }

  ngOnInit(): void {
  }

  async loadData() {
    Promise.all([this.getItemEcommerce()]).then(x => {
   });
  }

  async getItemEcommerce() {
    await this.itemEcommerceService.getItemEcommerceAsync(this.item.id).then(item => {
      if (item) {
        this.selectedItemEcommerce = item;
        this.isOnSave = false;
      } else {
        this.selectedItemEcommerce = new ItemEcommerce();
        this.isOnSave = true;
      }
    })
  }

  async saveItemEcommerce(itemId: number) {
    if (this.isOnSave) {
      if (this.selectedItemEcommerce && Object.keys(this.selectedItemEcommerce).filter(x => this.selectedItemEcommerce[x]).length > 0) {
        this.selectedItemEcommerce.itemId = itemId;
        await this.itemEcommerceService.createItemEcommerceAsync(this.selectedItemEcommerce).then(item => { if (item) {
          this.isOnSave = false;
        } })
      }
    } else {
      if (this.selectedItemEcommerce && Object.keys(this.selectedItemEcommerce).filter(x => this.selectedItemEcommerce[x]).length > 0) {
      await this.itemEcommerceService.updateItemEcommerceAsync(this.selectedItemEcommerce).then(item => {
      })
    }
    }
  }
}
