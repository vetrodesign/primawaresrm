import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemEcommerceComponent } from './item-ecommerce.component';

describe('ItemEcommerceComponent', () => {
  let component: ItemEcommerceComponent;
  let fixture: ComponentFixture<ItemEcommerceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemEcommerceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemEcommerceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
