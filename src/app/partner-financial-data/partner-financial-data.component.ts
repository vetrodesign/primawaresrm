import { Component, OnInit } from '@angular/core';
import { Constants } from 'app/constants';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-partner-financial-data',
  templateUrl: './partner-financial-data.component.html',
  styleUrls: ['./partner-financial-data.component.css']
})
export class PartnerFinancialDataComponent implements OnInit {
  
  constructor() { 
  }

  ngOnInit(): void {
  }

  onDailyReceiptsClick() {
    var url = environment.SRMPrimaware + '/' + Constants.partnerDailyReceipts;
    window.open(url, '_blank');
  }
}
