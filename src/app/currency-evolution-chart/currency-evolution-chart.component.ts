import { Component, Input, OnInit } from '@angular/core';
import { NotificationService } from 'app/services/notification.service';
import { TranslateService } from 'app/services/translate';
import { ClientModuleService } from 'app/services/client-module.service';
import { AuthService } from 'app/services/auth.service';
import { CurrencyService } from 'app/services/currency.service';
import { CurrencyTypeEnum, RateTypeEnum } from 'app/enums/currencyRatesEnum';
import { GenericItem } from 'app/models/generic-item.model';


@Component({
  selector: 'app-currency-evolution-chart',
  templateUrl: './currency-evolution-chart.component.html',
  styleUrls: ['./currency-evolution-chart.component.css']
})
export class CurrencyEvolutionChartComponent implements OnInit {
  @Input() baseCurrencyId: number;
  @Input() rateType: number;
  @Input() currencyId: number;

  bceRateSeries = [
    { valueField: 'bceRateRON', name: 'BCR RON', currencyId: CurrencyTypeEnum.RON },
    { valueField: 'bceRateEUR', name: 'BCR EUR', currencyId: CurrencyTypeEnum.EUR },
    { valueField: 'bceRateUSD', name: 'BCR USD', currencyId: CurrencyTypeEnum.USD },
    { valueField: 'bceRateGBP', name: 'BCR GBP', currencyId: CurrencyTypeEnum.GBP },
    { valueField: 'bceRateCNY', name: 'BCR CNY', currencyId: CurrencyTypeEnum.CNY },
    { valueField: 'bceRateBGN', name: 'BCR BGN', currencyId: CurrencyTypeEnum.BGN },
    { valueField: 'bceRateHUF', name: 'BCR HUF', currencyId: CurrencyTypeEnum.HUF }
  ];

  bnrRateSeries = [
    { valueField: 'bnrRateRON', name: 'BNR RON', currencyId: CurrencyTypeEnum.RON },
    { valueField: 'bnrRateEUR', name: 'BNR EUR', currencyId: CurrencyTypeEnum.EUR },
    { valueField: 'bnrRateUSD', name: 'BNR USD', currencyId: CurrencyTypeEnum.USD },
    { valueField: 'bnrRateGBP', name: 'BNR GBP', currencyId: CurrencyTypeEnum.GBP },
    { valueField: 'bnrRateCNY', name: 'BNR CNY', currencyId: CurrencyTypeEnum.CNY },
    { valueField: 'bnrRateBGN', name: 'BNR BGN', currencyId: CurrencyTypeEnum.BGN },
    { valueField: 'bnrRateHUF', name: 'BNR HUF', currencyId: CurrencyTypeEnum.HUF }
  ];

  loaded: boolean;
  baseCurrencies: GenericItem[] = [];
  rateTypes: GenericItem[] = [];
  currencyRates: any;
  selectedBaseCurrency: number;
  selectedRateTypes: number[] = [];
  chartDataSource: any = [];
  includeBceRate: boolean;
  includeBnrRate: boolean;
  chartStartDate: Date;
  chartEndDate: Date;
  selectedCurrencies: number[] = [];
  isAdmin: boolean;

  constructor(
    private notificationService: NotificationService,
    private translationService: TranslateService,
    private clientModuleService: ClientModuleService,
    private authService: AuthService,
    private currencyRateService: CurrencyService,
    private authenticationService: AuthService
  ) {

    this.isAdmin = this.authenticationService.isUserAdmin();
    for (let n in CurrencyTypeEnum) {
      if (typeof CurrencyTypeEnum[n] === 'number') {
        this.baseCurrencies.push({ id: <any>CurrencyTypeEnum[n],
        name: this.translationService.instant(n) });
      }
    }
    for (let n in RateTypeEnum) {
      if (typeof RateTypeEnum[n] === 'number') {
        this.rateTypes.push({ id: <any>RateTypeEnum[n],
        name: this.translationService.instant(n) });
      }
    }

    this.getData();
  }

  ngOnInit(): void {
  }

  getData() {
    this.loaded = true;
    Promise.all([this.GetLast3YearsCurrencyRates()]).then(x => { 
      this.selectedBaseCurrency = this.baseCurrencyId ? this.baseCurrencyId : CurrencyTypeEnum.RON; 
      this.selectedRateTypes = this.rateType ? [this.rateType] : [RateTypeEnum.BCR, RateTypeEnum.BNR];
      this.selectedCurrencies = this.currencyId ? [this.currencyId] : [CurrencyTypeEnum.USD, CurrencyTypeEnum.GBP, CurrencyTypeEnum.CNY];
      
      const sortedData = (this.currencyRates && this.currencyRates.length > 0) ? this.currencyRates.sort((a, b) => b.date - a.date) : [];
      this.chartEndDate = (sortedData && sortedData.length > 0) ? new Date(sortedData[0].date) : new Date();

      const startDateCopy = new Date(this.chartEndDate);
      startDateCopy.setMonth(startDateCopy.getMonth() - 12);
      this.chartStartDate = startDateCopy;

      this.loaded = false
    });
  }

  async GetLast3YearsCurrencyRates(): Promise<any> {
    await this.currencyRateService.GetLast3YearsCurrencyRates().then(items => {
      this.currencyRates = (items && items.length > 0) ? items.map(item => {
        const date = new Date(item.currencyDate);
        const baseCurrency = item.baseCurrencyId;
        const currency = item.currencyId;
        const bceRateKey = `bceRate${item.currencyName}`;
        const bnrRateKey = `bnrRate${item.currencyName}`;
      
        return {
          date,
          baseCurrency,
          currency,
          [bceRateKey]: item.bceRate,
          [bnrRateKey]: item.bnrRate
        };
      }) : [];
    });
  }

  getRateTypeDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.name;
  }

  onChartFiltersChange(event: any) {
    if (event && event.value) {
      this.applyFilters();
    }
  }
  
  applyFilters() {
    let filteredData = this.currencyRates;

    if (this.selectedBaseCurrency) {
      filteredData = filteredData.filter(f => f.baseCurrency === this.selectedBaseCurrency);
    }

    if (this.selectedRateTypes.length === 1) {
      if (this.selectedRateTypes.includes(1)) {
        filteredData = filteredData.map(({ bnrRate, ...rest }) => rest);
        this.includeBceRate = true;
        this.includeBnrRate = false;
      } else if (this.selectedRateTypes.includes(2)) {
        filteredData = filteredData.map(({ bceRate, ...rest }) => rest);
        this.includeBnrRate = true;
        this.includeBceRate = false;
      }
    } else if (this.selectedRateTypes.length === 2) {
      this.includeBceRate = true;
      this.includeBnrRate = true;
    } else {
      filteredData = [];
      this.includeBceRate = false;
      this.includeBnrRate = false;
    }
    
    if (this.selectedCurrencies) {
      filteredData = filteredData.filter(f => this.selectedCurrencies.includes(f.currency));
    }

    if (this.chartStartDate && this.chartEndDate) {
      filteredData = filteredData.filter(item => {
        const itemDate = new Date(item.date);
        return itemDate >= this.chartStartDate && itemDate <= this.chartEndDate;
      });
    }

    this.chartDataSource = filteredData;
  }

  filterChartDataByDateInterval() {
    if (this.chartStartDate && this.chartEndDate) {
      this.chartDataSource = this.currencyRates.filter(item => {
        const itemDate = new Date(item.date);
        return itemDate >= this.chartStartDate && itemDate <= this.chartEndDate;
      });
    }
  }
}
