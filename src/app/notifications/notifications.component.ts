import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { MenuService } from 'app/services/menu/menu.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
declare var $: any;
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit, OnDestroy {
  @Input() id = 'default-alert';
  notifyArray: string[];
  notificationSubscription: Subscription;

  constructor(private notificationService: NotificationService, private menuService: MenuService) {
    this.notifyArray = [];
    this.notificationSubscription = this.notificationService.subject.subscribe(alert => {
      if (alert) {
        this.showNotification(alert.from, alert.align, alert.text, alert.color ? alert.color : null);
        if (alert.addToNotificationArray) {
          this.notifyPush(alert.text);
        }
      }
     });
   }

   ngOnInit() {
   }

   notifyPush(text: string) {
     if (this.notifyArray.length < 10) {
      this.notifyArray.push(text);
     } else {
      this.notifyArray.shift();
      this.notifyArray.push(text);
     }

    this.menuService.subject.next(this.notifyArray);
   }

   ngOnDestroy() {
    this.notificationSubscription.unsubscribe();
   }

   showNotification(from, align, text: string, color?: NotificationTypeEnum) {
    const type = ['', 'info', 'success', 'warning', 'danger', 'primary'];
  
    const typeColor = color ? color : Math.floor((Math.random() * 5) + 1);
  
    let notification = $.notify({
      icon: "notifications",
      message: text
    },{
      type: type[typeColor],
      placement: {
        from: from,
        align: align
      },
      template: `
        <div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">
          <button mat-button type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  
            <i class="material-icons">close</i>
          </button>
          <i class="material-icons" data-notify="icon">notifications</i> 
          <span data-notify="title">{1}</span> 
          <span data-notify="message">{2}</span>
          <div class="progress" data-notify="progressbar">
            <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
          </div>
          <a href="{3}" target="{4}" data-notify="url"></a>
        </div>`
    });

    setTimeout(() => {
      notification.close();
    }, 2000);
}
}