import { Component, OnInit, ViewChild, AfterViewInit, Input, OnChanges } from '@angular/core';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { WholesaleStoreManagement, WholesaleStoreManagementFilter } from 'app/models/wholesale-store-management.model';
import { WholesaleStoreManagementService } from 'app/services/wholesale-store-management.service';
import { HelperService } from 'app/services/helper.service';
import * as _ from 'lodash';
import { ActivatedRoute } from '@angular/router';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';
import { ItemService } from 'app/services/item.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';

@Component({
  selector: 'app-wholesale-store-management',
  templateUrl: './wholesale-store-management.component.html',
  styleUrls: ['./wholesale-store-management.component.css']
})
export class WholesaleStoreManagementComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;

  displayData: boolean;
  customerId: number;
  actions: IPageActions;
  selectedRows: any[];

  exportTypes: any[] = [];
  orderType: any[] = [];
  orderByColumn: any[] = [];

  selectedExportType: any;
  selectedOrderType: any;
  selectedOrderColumn: any;

  selectedRowIndex = -1;
  rowIndex: any;

  isOwner: boolean = false;
  popupVisible: boolean = false;

  wholesaleStoreManagements: WholesaleStoreManagement[];
  wholesaleStoreManagementsDS: WholesaleStoreManagement[];
  wholesaleStoreManagementFilter: WholesaleStoreManagementFilter;

  loaded: boolean;
  groupedText: string;
  showJustTransfers: boolean = true;
  infoButton: string;

  constructor(private authenticationService: AuthService,
    private translationService: TranslateService,
    private helperService: HelperService,
    private notificationService: NotificationService,
    private wholesaleStoreManagementService: WholesaleStoreManagementService,
    private route: ActivatedRoute,
    private itemService: ItemService,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService) {
    this.setActions();
    this.customerId = Number(this.authenticationService.getUserCustomerId());
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
    this.groupedText = this.translationService.instant('groupedText');
    this.wholesaleStoreManagements = [];
    this.wholesaleStoreManagementsDS = [];
    this.wholesaleStoreManagementFilter = new WholesaleStoreManagementFilter(); 
  }
  ngOnInit(): void {
    this.exportTypes = [
      { id: 1, text: 'Excel' },
      { id: 2, text: 'Pdf' }
    ];
    this.orderType = [
      { id: 1, prop: 'asc', text: 'Crescator' },
      { id: 2, prop: 'desc', text: 'Descrescator' }
    ];
    this.orderByColumn = [
      { id: 1, prop: 'mainManagementCurrentStock', text: 'Stoc Dragan' },
      { id: 2, prop: 'itemVolume', text: 'Volum' },
      { id: 3, prop: 'itemGrossWeight', text: 'Greutate' },
      { id: 4, prop: 'mainManagementPalletsNumber', text: 'Nr Paleti -D' },
      { id: 5, prop: 'mainManagementStockSufficiencyAtTurnoverRate', text: 'Suficient R-D' },
      { id: 6, prop: 'secondaryManagementCurrentStock', text: 'Stoc Basarabiei 256' },
      { id: 7, prop: 'secondaryManagementPalletsNumber', text: 'Nr paleti - B' },
      { id: 8, prop: 'secondaryManagementStockSufficiencyAtTurnoverRate', text: 'Suficient R-B' },
      { id: 9, prop: 'itemTurnover', text: 'Rulaj' },
      { id: 10, prop: 'itemTurnover3Months', text: 'R3' },
      { id: 11, prop: 'itemTurnoversPercentage', text: 'R???' },
      { id: 12, prop: 'destorageEnGrosPallets', text: 'Destocaj ENGROS paleti' },
      { id: 13, prop: 'storagePickingPallets', text: '"Stocaj PICKING paleti' },
      { id: 14, prop: 'itemAcquisitionPeriodDays', text: 'PA (zile)' },
      { id: 15, prop: 'itemPalletGrossWeight', text: 'Greutate palet' }
    ];

    this.getData();
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }


  async getData() {

  }

  async getDataByFilter() {
    if (this.wholesaleStoreManagementFilter.mainManagementDestorageMonths < this.wholesaleStoreManagementFilter.mainManagementStorageMonths) {
           this.notificationService.alert('top', 'center', 'Nu puteti Destoca mai putin decat Stocati!',
             NotificationTypeEnum.Yellow, true)
             return;
    }
    if (this.wholesaleStoreManagementFilter.palletHeight == null) {
      this.notificationService.alert('top', 'center', 'Introduceti inaltime palet(m)!',
        NotificationTypeEnum.Yellow, true)
        return;
}

    if (this.validationGroup.instance.validate().isValid) {
      this.loaded = true;
      await this.wholesaleStoreManagementService.getDataByFilterAsync(this.wholesaleStoreManagementFilter).then(items => {
        this.wholesaleStoreManagements = (items && items.length > 0) ? items : []
        this.wholesaleStoreManagements.forEach(item => {
          const maxValue = Math.max(item.itemTurnover3Months, item.itemTurnover6Months, item.itemTurnover9Months, item.itemTurnover12Months);
          if (item.itemTurnover < maxValue || item.itemTurnover > maxValue *1.2) {
            item.isTurnoverToBeFixed = true;
          }

          item.shouldHideDB = false;
          item.shouldHideBD = false;
          if (!item) {
            item.shouldHideDB = true;
            item.shouldHideBD = true;
          }
          //Cantitate pe palet
          const cartonQuantity = item.cartonQuantity;
          const cartonLayers = item.cartonLayers;
          const palletHeight = this.wholesaleStoreManagementFilter?.palletHeight;
          const cartonHeight = item.cartonHeight;
          if (palletHeight == null || cartonHeight == null || cartonHeight === 0) {
            item.shouldHideDB = true;
          }
          // Perform the calculation and round down
          const nrOfLayersvalue = Math.floor(((palletHeight - 0.144) * 100) / cartonHeight);
          if (cartonQuantity == null || cartonLayers == null || nrOfLayersvalue == null) {
            item.shouldHideDB = true;
          }
      
          const secondaryManagementCurrentStock = item.secondaryManagementCurrentStock;
          const itemTurnover = item.itemTurnover;
          if (secondaryManagementCurrentStock == null || itemTurnover == null) {
            item.shouldHideDB = true;
            item.shouldHideBD = true;
          }
          const mainManagementStockSufficiencyAtTurnoverRate = item.mainManagementCurrentStock / itemTurnover;
      
          const smc = item.secondaryManagementCurrentStock; // Replace with the actual property corresponding to O11
          const mmd = this.wholesaleStoreManagementFilter.mainManagementDestorageMonths
        
          if (mainManagementStockSufficiencyAtTurnoverRate == null || item.itemTurnover == null || smc == null || mmd == null) {
            item.shouldHideDB = true;
          }
        
          if (mainManagementStockSufficiencyAtTurnoverRate < mmd) {
            item.shouldHideDB = true;
          }
        
          const calculatedValue = item.itemTurnover * (mmd - mainManagementStockSufficiencyAtTurnoverRate);
          var v = calculatedValue > smc ? calculatedValue : smc;
          if (v == null || v == 0){
            item.shouldHideDB = true;
          }


         //Second condition
          const stockB = item.secondaryManagementCurrentStock; // Replace with the actual property corresponding to O10
          const mmd2 = this.wholesaleStoreManagementFilter.mainManagementStorageMonths;
          if (mainManagementStockSufficiencyAtTurnoverRate == null || itemTurnover == null || stockB == null || mmd2 == null) {
            item.shouldHideBD = true;
          }

          if (mainManagementStockSufficiencyAtTurnoverRate > mmd2) {
            item.shouldHideBD = true;
          }
          const calculatedValue2 = itemTurnover * (mmd2 - mainManagementStockSufficiencyAtTurnoverRate);
          var v2 = calculatedValue2 < stockB ? calculatedValue2 : stockB;
          if (v2 == null || v2 == 0) {
            item.shouldHideBD = true;
          }


        })
        if (this.showJustTransfers) {
          this.wholesaleStoreManagementsDS =  this.wholesaleStoreManagements.filter(x => !x.shouldHideDB || !x.shouldHideBD);
        } else {
          this.wholesaleStoreManagementsDS =  this.wholesaleStoreManagements;
        }
       
        this.loaded = false;
      }).catch(err => { this.loaded = false; });
    }
  }

  itemAcquisitionPeriodDaysExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.itemAcquisitionPeriodDays) {
        let value = (Number(item.itemAcquisitionPeriodDays)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        return value;
      }
      else {
        return '';
      }
    }
  }

  storagePickingPalletsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.storagePickingPallets) {
        let value = (Number(item.storagePickingPallets)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        return value;
      }
      else {
        return '';
      }
    }
  }
  destorageEnGrosPalletsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.destorageEnGrosPallets) {
        let value = (Number(item.destorageEnGrosPallets)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        return value;
      }
      else {
        return '';
      }
    }
  }
  itemTurnoversPercentageExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.itemTurnoversPercentage) {
        let value = (Number(item.itemTurnoversPercentage)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        return value;
      }
      else {
        return '';
      }
    }
  }
  itemTurnover3MonthsExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.itemTurnover3Months) {
        // let value = (Number(item.itemTurnover3Months)).toLocaleString(undefined, {
        //   minimumFractionDigits: 0,
        //   maximumFractionDigits: 0
        // });
        return item.itemTurnover3Months + '/' + item.itemTurnover6Months + '/' + item.itemTurnover9Months + '/' + item.itemTurnover12Months;
      }
      else {
        return '';
      }
    }
  }
  itemTurnoverExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.itemTurnover) {
        let value = (Number(item.itemTurnover)).toLocaleString(undefined, {
          minimumFractionDigits: 0,
          maximumFractionDigits: 0
        });
        return value;
      }
      else {
        return '';
      }
    }
  }

  cartonQuantityTemplateExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.cartonQuantity) {
        let value = (Number(item.cartonQuantity)).toLocaleString(undefined, {
          minimumFractionDigits: 0,
          maximumFractionDigits: 0
        });
        return value;
      }
      else {
        return '';
      }
    }
  }
  cartonLengthTemplateExpr(item) {
    if (!item) {
      return '';
    } else {
      let length = '-';
      if (item.cartonLength) {
        length = (Number(item.cartonLength)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
      }
      let width = '-';
      if (item.cartonWidth) {
        width = (Number(item.cartonWidth)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
      }
      let height = '-';
      if (item.cartonWidth) {
        height = (Number(item.cartonWidth)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
      }
      return length + '/' + width + '/' + height;
    }
  }

  cartonLayersTemplateExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.cartonLayers) {
        let value = (Number(item.cartonLayers)).toLocaleString(undefined, {
          minimumFractionDigits: 0,
          maximumFractionDigits: 0
        });
        return value;
      }
      else {
        return '';
      }
    }
  }

  nrOfLayersTemplateExpr(item: any): string {
    if (!item) {
      return '';
    }
  
    const palletHeight = this.wholesaleStoreManagementFilter?.palletHeight;
    const cartonHeight = item.cartonHeight;
  
    if (palletHeight == null || cartonHeight == null || cartonHeight === 0) {
      return '';
    }
  
    // Perform the calculation and round down
    const value = Math.floor(((palletHeight - 0.144) * 100) / cartonHeight);
  
    return value.toLocaleString(undefined, {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    });
  }

  layerOptimizedTemplateExpr(item: any): string {
    if (!item) {
      return '';
    }
    const cartonLength = item.cartonLength;
    const cartonWidth = item.cartonWidth;
    const cartonLayers = item.cartonLayers;
    if (cartonLength == null || cartonWidth == null || cartonLayers == null) {
      return '-';
    }
    // Perform the calculation
    const value = (0.96 - ((cartonLength * cartonWidth * cartonLayers) / 10000)) / 0.96;

    return value.toLocaleString(undefined, {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    }) + ' %';
  }
  
  quantityPerPalletTemplateExpr(item: any): string {
    if (!item) {
      return '';
    }
    const cartonQuantity = item.cartonQuantity;
    const cartonLayers = item.cartonLayers;
    const palletHeight = this.wholesaleStoreManagementFilter?.palletHeight;
    const cartonHeight = item.cartonHeight;
    if (palletHeight == null || cartonHeight == null || cartonHeight === 0) {
      return '';
    }
    // Perform the calculation and round down
    const nrOfLayersvalue = Math.floor(((palletHeight - 0.144) * 100) / cartonHeight);
    if (cartonQuantity == null || cartonLayers == null || nrOfLayersvalue == null) {
      return '-';
    }
    // Perform the calculation
    const value = (cartonQuantity * cartonLayers * nrOfLayersvalue);

    return value.toLocaleString(undefined, {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    });
  }

  
  itemPalletGrossWeightExpr(item) {
    if (!item) {
      return '';
    }
    const palletVolume = this.wholesaleStoreManagementFilter.palletVolume;
    const itemVolume = item.itemVolume;
    const itemGrossWeight = item.itemGrossWeight;

    if (palletVolume == null || itemVolume == null || itemGrossWeight == null) {
      return '';
    }
    // Perform the calculation
    const value = palletVolume / itemVolume * itemGrossWeight;

    return value.toLocaleString(undefined, {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

  mainManagementCurrentStockExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.mainManagementCurrentStock) {
        let value = (Number(item.mainManagementCurrentStock)).toLocaleString(undefined, {
          minimumFractionDigits: 0,
          maximumFractionDigits: 0
        });
        return value;
      }
      else {
        return '0';
      }
    }
  }

  secondaryManagementCurrentStockExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.secondaryManagementCurrentStock) {
        let value = (Number(item.secondaryManagementCurrentStock)).toLocaleString(undefined, {
          minimumFractionDigits: 0,
          maximumFractionDigits: 0
        });
        return value;
      }
      else {
        return '0';
      }
    }
  }

  mainManagementStockSufficiencyAtTurnoverRateExpr(item) {
    if (!item || !item.mainManagementCurrentStock || !item.itemTurnover) {
      return '';
    }
    const mainManagementCurrentStock = item.mainManagementCurrentStock;
    const itemTurnover = item.itemTurnover;
    if (mainManagementCurrentStock == null || itemTurnover == null) {
      return '';
    }
    // Perform the calculation
    const value = mainManagementCurrentStock / itemTurnover;
    return value.toLocaleString(undefined, {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    }) + ' luni';
  }

  secondaryManagementStockSufficiencyAtTurnoverRateExpr(item) {
    if (!item || !item.itemTurnover) {
      return '';
    }
    const secondaryManagementCurrentStock = item.secondaryManagementCurrentStock;
    const itemTurnover = item.itemTurnover;
    if (secondaryManagementCurrentStock == null || itemTurnover == null) {
      return '';
    }
    // Perform the calculation
    const value = secondaryManagementCurrentStock / itemTurnover;
    return value.toLocaleString(undefined, {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    });
  }

  proposedDBExpr(item) {
    if (!item) {
      return '';
    }
    //Cantitate pe palet
    const cartonQuantity = item.cartonQuantity;
    const cartonLayers = item.cartonLayers;
    const palletHeight = this.wholesaleStoreManagementFilter?.palletHeight;
    const cartonHeight = item.cartonHeight;
    if (palletHeight == null || cartonHeight == null || cartonHeight === 0) {
      return '';
    }
    // Perform the calculation and round down
    const nrOfLayersvalue = Math.floor(((palletHeight - 0.144) * 100) / cartonHeight);
    if (cartonQuantity == null || cartonLayers == null || nrOfLayersvalue == null) {
      return '-';
    }
    // Perform the calculation
    const paletQuantity = (cartonQuantity * cartonLayers * nrOfLayersvalue);

    const secondaryManagementCurrentStock = item.secondaryManagementCurrentStock;
    const itemTurnover = item.itemTurnover;
    if (secondaryManagementCurrentStock == null || itemTurnover == null) {
      return '';
    }
    const mainManagementStockSufficiencyAtTurnoverRate = item.mainManagementCurrentStock / itemTurnover;

    const smc = item.secondaryManagementCurrentStock; // Replace with the actual property corresponding to O11
    const mmd = this.wholesaleStoreManagementFilter.mainManagementDestorageMonths
  
    if (mainManagementStockSufficiencyAtTurnoverRate == null || item.itemTurnover == null || smc == null || mmd == null) {
      return '';
    }
  
    if (mainManagementStockSufficiencyAtTurnoverRate < mmd) {
      return 0;
    }
  
    const calculatedValue = item.itemTurnover * (mmd - mainManagementStockSufficiencyAtTurnoverRate);
    var v = calculatedValue > smc ? calculatedValue : smc;
    var formula = (v / paletQuantity).toLocaleString(undefined, {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    });

    if (paletQuantity == 0) {
      formula = '0';
    }

    return v.toLocaleString(undefined, {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    }) + '(' + formula + ')';
  }

  proposedBDExpr(item: any): number | string {
    if (!item) {
      return '';
    }
  
    const secondaryManagementCurrentStock = item.secondaryManagementCurrentStock;
    const itemTurnover = item.itemTurnover;
    if (secondaryManagementCurrentStock == null || itemTurnover == null) {
      return '';
    }
    const mainManagementStockSufficiencyAtTurnoverRate = item.mainManagementCurrentStock / itemTurnover;

    const stockB = item.secondaryManagementCurrentStock; // Replace with the actual property corresponding to O10
    const mmd = this.wholesaleStoreManagementFilter.mainManagementStorageMonths;

    if (mainManagementStockSufficiencyAtTurnoverRate == null || itemTurnover == null || stockB == null || mmd == null) {
      return '';
    }
  
    if (mainManagementStockSufficiencyAtTurnoverRate > mmd) {
      return 0;
    }
  
    const calculatedValue = itemTurnover * (mmd - mainManagementStockSufficiencyAtTurnoverRate);
  
    return calculatedValue < stockB ? calculatedValue.toLocaleString(undefined, {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    }) : stockB.toLocaleString(undefined, {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    });
  }


  itemGrossWeightExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.itemGrossWeight) {
        let value = (Number(item.itemGrossWeight)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        return value;
      }
      else {
        return '';
      }
    }
  }
  itemVolumeExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.itemVolume) {
        let value = (Number(item.itemVolume)).toLocaleString(undefined, {
          minimumFractionDigits: 6,
          maximumFractionDigits: 6
        });
        return value;
      }
      else {
        return '';
      }
    }
  }

  resetFilters() {
    this.wholesaleStoreManagementFilter = new WholesaleStoreManagementFilter();
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: false,
      CanUpdate: true,
      CanDelete: false,
      CanPrint: true,
      CanExport: true,
      CanImport: false,
      CanDuplicate: true
    };
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  public add() {
    this.dataGrid.instance.addRow();
  }

  public refreshDataGrid() {
    this.getData();
    this.dataGrid.instance.refresh();
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
    this.rowIndex = row.rowIndex;

  }

  exportDataEvent() {
    this.selectedExportType = this.exportTypes[0].id;
    this.popupVisible = true;
  }

  cancelExport() {
    this.popupVisible = false;
  }

  exportData() {
    if (this.selectedRows && this.selectedRows.length > 0) {
      let filteredData = this.selectedRows;
      if (this.selectedOrderType && this.selectedOrderColumn) {
        filteredData = [];
        filteredData = _.orderBy(this.selectedRows, [this.orderByColumn.find(x => x.id == this.selectedOrderColumn)?.prop], [this.orderType.find(x => x.id == this.selectedOrderType)?.prop]);
      }
      let headers = ["Cod", "Nume", "Stoc Dragan", "Volum", "Greutate", "Nr paleti - D", "Suficient R-D", "Stoc Basarabiei 256", "Nr paleti - B", "Suficient R-B",
        "Rulaj", "R3", "R???", "Rulaj Calcul", "Destocaj ENGROS paleti", "Destocaj Comanda", "Stocaj PICKING paleti", "Stocaj Comanda", "PA (zile)", "Greutate Palet", "Observatii instructiuni"];
      let data = [];
      filteredData.forEach((i, index) => {
        let x = [i.itemCode, i.itemName];

        x.push(i.mainManagementCurrentStock ? (Number(i.mainManagementCurrentStock)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);
        x.push(i.itemVolume ? (Number(i.itemVolume)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.itemGrossWeight ? (Number(i.itemGrossWeight)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.mainManagementPalletsNumber ? (Number(i.mainManagementPalletsNumber)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.mainManagementStockSufficiencyAtTurnoverRate ? (Number(i.mainManagementStockSufficiencyAtTurnoverRate)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.secondaryManagementCurrentStock ? (Number(i.secondaryManagementCurrentStock)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.secondaryManagementPalletsNumber ? (Number(i.secondaryManagementPalletsNumber)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.secondaryManagementStockSufficiencyAtTurnoverRate ? (Number(i.secondaryManagementStockSufficiencyAtTurnoverRate)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.itemTurnover ? (Number(i.itemTurnover)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.itemTurnover3Months ? (Number(i.itemTurnover3Months)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);


        x.push(i.itemTurnoversPercentage ? (Number(i.itemTurnoversPercentage)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.calculatedTurnover);

        x.push(i.destorageEnGrosPallets ? (Number(i.destorageEnGrosPallets)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.orderUnstorage);

        x.push(i.storagePickingPallets ? (Number(i.storagePickingPallets)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.orderStorage);

        x.push(i.itemAcquisitionPeriodDays ? (Number(i.itemAcquisitionPeriodDays)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);
        x.push(i.itemPalletGrossWeight ? (Number(i.itemPalletGrossWeight)).toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) : 0);

        x.push(i.description);
        data.push(x);
      });

      if (this.selectedExportType == 1) {
        this.helperService.exportTemplateAsExcel('raport-magazie', headers, data);
        this.popupVisible = false;
      } else {
        //pdf
        this.helperService.exportTemplateAsPDF('raport-magazie', headers, data);
        this.popupVisible = false;
      }
    }
  }

  deleteRecords(e) {

  }

  public async onRowUpdated(event: any): Promise<void> {
    // let item = new WholesaleStoreManagement();
    // item = this.wholesaleStoreManagements.find(g => g.id === event.key.id);
    // if (item) {
    //   await this.wholesaleStoreManagementService.updateAsync(item).then(r => {
    //     if (r) {
    //       this.notificationService.alert('top', 'center', 'Magazie ENGROS - Datele au fost modificate cu succes!',
    //         NotificationTypeEnum.Green, true)
    //     } else {
    //       this.notificationService.alert('top', 'center', 'Magazie ENGROS - Datele nu au fost modificate! A aparut o eroare!',
    //         NotificationTypeEnum.Red, true)
    //     }
    //     this.refreshDataGrid();
    //   });
    // }
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  displayCustomExpr(item) {
    if (!item) {
      return '';
    } else {
      return item.code + ' - ' + item.name;
    }
  }

  onCellPrepared(e) {
    if (e && e.rowType === 'header') {
      if (e.column.dataField === 'itemCode' || e.column.dataField === 'itemName' || e.column.dataField === 'itemGrossWeight' || e.column.dataField === 'cartonQuantity'
       || e.column.dataField === 'cartonLength' || e.column.dataField === 'cartonWidth' || e.column.dataField === 'cartonHeight' || e.column.dataField === 'cartonLayers'
       || e.column.dataField === 'quantityPerPallet' || e.column.dataField === 'itemPalletGrossWeight'
      ) {
        e.cellElement.style.backgroundColor = "#9d00c4"; 
        e.cellElement.style.color = "#ffffff";
      }

      if (e.column.dataField === 'nrOfLayers' || e.column.dataField === 'layerOptimized') {
         e.cellElement.style.backgroundColor = "#ffe100";  
         e.cellElement.style.color = "#ff0011";
       }
       if (e.column.dataField === 'mainManagementCurrentStock' || e.column.dataField === 'secondaryManagementCurrentStock' || e.column.dataField === 'mainManagementPalletsNumber' || e.column.dataField === 'secondaryManagementPalletsNumber') {
        e.cellElement.style.backgroundColor = "#79c979";  
        e.cellElement.style.color = "#ffffff";
      }
      if (e.column.dataField === 'itemTurnover3Months' || e.column.dataField === 'itemTurnover') {
        e.cellElement.style.backgroundColor = "#59afff";  
        e.cellElement.style.color = "#ffffff";
      }
      if (e.column.dataField === 'mainManagementStockSufficiencyAtTurnoverRate' || e.column.dataField === 'proposedDB' || e.column.dataField === 'btndb' || e.column.dataField === 'decidedDB'  || e.column.dataField === 'decidedBD' || e.column.dataField === 'proposedBD'
        || e.column.dataField === "secondaryManagementStockSufficiencyAtTurnoverRate"
      ) {
        e.cellElement.style.backgroundColor = "#b55b00";  
        e.cellElement.style.color = "#ffffff";
      }


      if (e.column.dataField === 'itemVolume') {
        
        e.cellElement.style.backgroundColor = "#9d00c4"; // Gold background for Volume column header
        e.cellElement.style.color = "#ffffff"; // Optional: Change text color for contrast

        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Volum Produs (mc)");
        ($(cellElement) as any).tooltip({
          placement: 'top'
        });
      }

      if (e.column.dataField === 'layerOptimized') { 
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Optimizare Suprafata Strat<br><br>0.96 este suprafața paletului tip EUR.<br>Valoare pozitivă = trebuie optimizat prin mărire.<br>Valoare negativă = trebuie optimizat prin micșorare (atenție: se depășește paletul). Calcul: (0.96 - ((L carton * W carton * Straturi carton) / 10000)) / 0.96"); 
        ($(cellElement) as any).tooltip({
          placement: 'top',
          html: true // Enable HTML formatting inside the tooltip
        });
      }
    
     if (e.column.dataField === 'quantityPerPallet') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Cantitate pe palet<br><br>Calcul: Cant Carton * Cartoane pe Strat Palet * Nr.Straturi");
      ($(cellElement) as any).tooltip({
        placement: 'top',
        html: true 
      });
     }

     if (e.column.dataField === 'itemPalletGrossWeight') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Cantitate pe palet<br><br>Calcul: Volum Palet / Volum Produs * Greutate Produs");
      ($(cellElement) as any).tooltip({
        placement: 'top',
        html: true 
      });
     }

     if (e.column.dataField === 'mainManagementStockSufficiencyAtTurnoverRate') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Suficient la R Dragan(luni)<br><br>Calcul: Stoc Dragan / R Rulaj");
      ($(cellElement) as any).tooltip({
        placement: 'top',
        html: true 
      });
     }

     if (e.column.dataField === 'secondaryManagementStockSufficiencyAtTurnoverRate') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Suficient la R Basarabia(luni)<br><br>Calcul: Stoc Basarabia / R Rulaj");
      ($(cellElement) as any).tooltip({
        placement: 'top',
        html: true 
      });
     }

     if (e.column.dataField === 'mainManagementPalletsNumber') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "D > B (paleti)<br><br>Calcul: Decis B&rarr;D Transfer / Cantitate pe palet");
      ($(cellElement) as any).tooltip({
        placement: 'top',
        html: true 
      });
     }
     
     if (e.column.dataField === 'secondaryManagementPalletsNumber') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "D < B (paleti)<br><br>Calcul: Decis D&rarr;B Transfer / Cantitate pe palet");
      ($(cellElement) as any).tooltip({
        placement: 'top',
        html: true 
      });
     }
     
      if (e.column.dataField === 'nrOfLayers') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Straturi<br><br>Calcul: Integer((Inaltime Palet - 0.144) * 100) / Inaltime Carton");
        ($(cellElement) as any).tooltip({
          placement: 'top',
          html: true 
        });
      }

      if (e.column.dataField === 'proposedDB') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Propus D&rarr;B Bucati(paleti)<br><br> In paranteza arat nr paleti = Cantitatea Propusa / Cantitatea pe Palet) Calcul: Dacă Suficient La R Dragan(luni) este mai mic decât Destocaj D->B, atunci rezultatul este 0. În caz contrar, se verifică dacă R-Rulaj înmulțit cu (Destocaj D->B minus Suficient La R Dragan(luni)) este mai mare decât Stoc Basarabia. Dacă da, atunci se returnează Stoc Basarabia, altfel se returnează R-Rulaj înmulțit cu (Destocaj D->B minus Suficient La R Dragan(luni))");
        ($(cellElement) as any).tooltip({
          placement: 'top',
          html: true 
        });
      }

      if (e.column.dataField === 'proposedBD') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Propus B&rarr;D Bucati(paleti)<br><br> Calcul: Dacă Suficient La R Dragan(luni) este mai mare decât Stocaj B->D, atunci rezultatul este 0. În caz contrar, se verifică dacă R-Rulaj înmulțit cu (Stocaj B->D minus Suficient La R Dragan(luni)) este mai mic decât Stoc Basarabia. Dacă da, atunci se returnează R-Rulaj înmulțit cu (Stocaj B->D minus Suficient La R Dragan(luni)), altfel se returnează Stoc Basarabia.");
        ($(cellElement) as any).tooltip({
          placement: 'top',
          html: true 
        });
      }

      if (e.column.dataField === 'decidedDB' || e.column.dataField === 'decidedBD') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Introduce operator");
        ($(cellElement) as any).tooltip({
          placement: 'top'
        });
      }
    }
  }

  onPalletHeightChanged(e) {
    if (e && e.value != null) {
      this.wholesaleStoreManagementFilter.palletVolume = (e.value * 1.2 * 0.8);
      this.getDataByFilter();
    }
  }

  async onTurnoverFixClick(e) {
    if (e && e.itemId) {
      this.loaded = true;
      await this.itemService.getItemByItemId(e.itemId).then(result => {
        if (result) {
         const maxValue = Math.max(e.itemTurnover3Months, e.itemTurnover6Months, e.itemTurnover9Months, e.itemTurnover12Months);
         result.turnOver = Math.round(maxValue * 1.1);
         this.itemService.updateItemAsync(result).then(x => {
          if (x) {
            this.notificationService.alert('top', 'center', 'Produse - Rulajul au fost modificate cu succes!',
              NotificationTypeEnum.Green, true);
          } else {
            this.notificationService.alert('top', 'center', 'Produse - Rulajul nu a fost modificat! A aparut o eroare!',
              NotificationTypeEnum.Red, true);
          }
          this.loaded = false;
          this.getDataByFilter();
         });
        }
      })
    }
    }


    async updateTurnover() {
      if (this.wholesaleStoreManagements && this.wholesaleStoreManagements.length > 0) {
        this.loaded = true;
        await this.itemService.updateItemsTurnoverAsync(this.wholesaleStoreManagements.filter(y => y.isTurnoverToBeFixed).map(x => x.itemId)).then(result => { 
          if (result) {
            this.notificationService.alert('top', 'center', 'Produse - Rulajul au fost modificate cu succes!',
              NotificationTypeEnum.Green, true);
              this.loaded = false;
              this.getDataByFilter();
          } else {
            this.notificationService.alert('top', 'center', 'Produse - Rulajul nu a fost modificat! A aparut o eroare!',
              NotificationTypeEnum.Red, true);
          }
        });
      } 
    }

    generateNT() {

    }

    sendDBData(item) {
      if (!item) {
        return '';
      }
      const secondaryManagementCurrentStock = item.secondaryManagementCurrentStock;
      const itemTurnover = item.itemTurnover;
      if (secondaryManagementCurrentStock == null || itemTurnover == null) {
        return '';
      }
      const mainManagementStockSufficiencyAtTurnoverRate = item.mainManagementCurrentStock / itemTurnover;
  
      const smc = item.secondaryManagementCurrentStock; // Replace with the actual property corresponding to O11
      const mmd = this.wholesaleStoreManagementFilter.mainManagementDestorageMonths
      if (mainManagementStockSufficiencyAtTurnoverRate == null || item.itemTurnover == null || smc == null || mmd == null) {
        return '';
      }
      if (mainManagementStockSufficiencyAtTurnoverRate < mmd) {
        return 0;
      }
      const calculatedValue = item.itemTurnover * (mmd - mainManagementStockSufficiencyAtTurnoverRate);
      item.decidedDB = calculatedValue > smc ? calculatedValue : smc;
    }

    sendBDData(item) {
      if (!item) {
        return '';
      }
    
      const secondaryManagementCurrentStock = item.secondaryManagementCurrentStock;
      const itemTurnover = item.itemTurnover;
      if (secondaryManagementCurrentStock == null || itemTurnover == null) {
        return '';
      }
      const mainManagementStockSufficiencyAtTurnoverRate = item.mainManagementCurrentStock / itemTurnover;
  
      const stockB = item.secondaryManagementCurrentStock; // Replace with the actual property corresponding to O10
      const mmd = this.wholesaleStoreManagementFilter.mainManagementStorageMonths;
  
      if (mainManagementStockSufficiencyAtTurnoverRate == null || itemTurnover == null || stockB == null || mmd == null) {
        return '';
      }
    
      if (mainManagementStockSufficiencyAtTurnoverRate > mmd) {
        return 0;
      }
    
      const calculatedValue = itemTurnover * (mmd - mainManagementStockSufficiencyAtTurnoverRate);
    
      item.decidedBD = calculatedValue < stockB ? calculatedValue : stockB;
    }


    mainManagementPalletsNumberExpr(item) {
      if (!item || !item.decidedBD) {
        return '';
      }
      const cartonQuantity = item.cartonQuantity;
      const cartonLayers = item.cartonLayers;
  
      const palletHeight = this.wholesaleStoreManagementFilter?.palletHeight;
      const cartonHeight = item.cartonHeight;
      if (palletHeight == null || cartonHeight == null || cartonHeight === 0) {
        return '';
      }
      // Perform the calculation and round down
      const nrOfLayersvalue = Math.floor(((palletHeight - 0.144) * 100) / cartonHeight);
  
  
      if (cartonQuantity == null || cartonLayers == null || nrOfLayersvalue == null) {
        return '-';
      }
      // Perform the calculation
      const value = (cartonQuantity * cartonLayers * nrOfLayersvalue);
      const f = value / item.decidedBD;
      return f.toLocaleString(undefined, {
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
      });  
    }


    secondaryManagementPalletsNumberExpr(item) {
      if (!item || !item.decidedDB) {
        return '';
      }
      const cartonQuantity = item.cartonQuantity;
      const cartonLayers = item.cartonLayers;
  
      const palletHeight = this.wholesaleStoreManagementFilter?.palletHeight;
      const cartonHeight = item.cartonHeight;
      if (palletHeight == null || cartonHeight == null || cartonHeight === 0) {
        return '';
      }
      // Perform the calculation and round down
      const nrOfLayersvalue = Math.floor(((palletHeight - 0.144) * 100) / cartonHeight);
  
  
      if (cartonQuantity == null || cartonLayers == null || nrOfLayersvalue == null) {
        return '-';
      }
      // Perform the calculation
      const value = (cartonQuantity * cartonLayers * nrOfLayersvalue);
      const f = value / item.decidedDB;
      return f.toLocaleString(undefined, {
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
      });  
    }

    

    // secondaryManagementPalletsNumberExpr(item) {
    //  if (!item) {
    //   return '';
    // }

    // if (!item || !item.decidedBD) {
    //   return '';
    // }
    // const cartonQuantity = item.cartonQuantity;
    // const cartonLayers = item.cartonLayers;

    // const palletHeight = this.wholesaleStoreManagementFilter?.palletHeight;
    // const cartonHeight = item.cartonHeight;
    // if (palletHeight == null || cartonHeight == null || cartonHeight === 0) {
    //   return '';
    // }
    // // Perform the calculation and round down
    // const nrOfLayersvalue = Math.floor(((palletHeight - 0.144) * 100) / cartonHeight);


    // if (cartonQuantity == null || cartonLayers == null || nrOfLayersvalue == null) {
    //   return '';
    // }
    // const value = (cartonQuantity * cartonLayers * nrOfLayersvalue);
  
    // const secondaryManagementCurrentStock = item.secondaryManagementCurrentStock;
    // const itemTurnover = item.itemTurnover;
    // if (secondaryManagementCurrentStock == null || itemTurnover == null) {
    //   return '';
    // }
    // const mainManagementStockSufficiencyAtTurnoverRate = item.mainManagementCurrentStock / itemTurnover;

    // const stockB = item.secondaryManagementCurrentStock; // Replace with the actual property corresponding to O10
    // const mmd = this.wholesaleStoreManagementFilter.mainManagementStorageMonths;

    // if (mainManagementStockSufficiencyAtTurnoverRate == null || itemTurnover == null || stockB == null || mmd == null) {
    //   return '';
    // }
  
    // if (mainManagementStockSufficiencyAtTurnoverRate > mmd) {
    //   return 0;
    // }
  
    // const calculatedValue = itemTurnover * (mmd - mainManagementStockSufficiencyAtTurnoverRate);
    // const v = calculatedValue < stockB ? calculatedValue : stockB;
    // return (v / value).toLocaleString(undefined, {
    //   minimumFractionDigits: 0,
    //   maximumFractionDigits: 0
    // }); 
    // }
}
