import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WholesaleStoreManagementComponent } from './wholesale-store-management.component';

describe('WholesaleStoreManagementComponent', () => {
  let component: WholesaleStoreManagementComponent;
  let fixture: ComponentFixture<WholesaleStoreManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WholesaleStoreManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WholesaleStoreManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
