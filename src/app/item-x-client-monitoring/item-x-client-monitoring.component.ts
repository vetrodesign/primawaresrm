import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { ClientSmallSearchFilter } from 'app/models/client-small-search-filter.model';
import { GetPartnerNamesAndIdsByPartnerIdsResponse } from 'app/models/getPartnerNamesAndIdsByPartnerIdsResponse.model';
import { IPageActions } from 'app/models/ipageactions';
import { ItemMonitoringPartnerModel } from 'app/models/item-monitoring-partner.model';
import { ItemXClientMonitoringModel } from 'app/models/item-x-client-monitoring.model';
import { Item } from 'app/models/item.model';
import { ItemDCRInfo } from 'app/models/itemDCRInfo.model';
import { Partner } from 'app/models/partner.model';
import { AuthService } from 'app/services/auth.service';
import { ItemMonitoringPartnerService } from 'app/services/item-monitoring-partner.service';
import { ItemXClientMonitoringService } from 'app/services/item-x-client-monitoring.service';
import { NotificationService } from 'app/services/notification.service';
import { PageActionService } from 'app/services/page-action.service';
import { PartnerService } from 'app/services/partner.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-item-x-client-monitoring',
  templateUrl: './item-x-client-monitoring.component.html',
  styleUrls: ['./item-x-client-monitoring.component.css']
})
export class ItemXClientMonitoringComponent implements OnInit {
  @Input() item: Item;
  @Input() itemDCR: ItemDCRInfo;

  itemXClientMonitoringList: ItemXClientMonitoringModel[] = [];

  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;

  itemMonitoringPartners: ItemMonitoringPartnerModel[] = [];

  actions: IPageActions;

  selectedItemXClientMonitoring: ItemXClientMonitoringModel = new ItemXClientMonitoringModel();
  isOnSaveItemXClientMonitoring: boolean;

  partners: any;
  partnersForGridDisplay: GetPartnerNamesAndIdsByPartnerIdsResponse[] = [];

  popupVisible: boolean;
  popupRendered: boolean;

  loaded: boolean = false;

  groupedText: string;

  selectedRows: any[];
  selectedRowIndex = -1;

  constructor(
    private itemXClientMonitoringService: ItemXClientMonitoringService,
    private translationService: TranslateService,
    private pageActionService: PageActionService,
    private authenticationService: AuthService,
    private notificationService: NotificationService,
    private partnerService: PartnerService,
    private itemMonitoringService: ItemMonitoringPartnerService
    ) {

      this.setActions();
      this.groupedText = this.translationService.instant('groupedText');

    }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  private async getItemMonitoringPartners() {
    await this.itemMonitoringService.getAllActiveAsync().then(async (itemMonitoringPartners: ItemMonitoringPartnerModel[]) => {
      if (itemMonitoringPartners && itemMonitoringPartners.length > 0) {
        await this.partnerService.getPartnerNamesAndIdsByPartnerIdsAsync(itemMonitoringPartners.map(x => x.partnerId)).then(partnerInfos => {
          itemMonitoringPartners.forEach(imp => {
            const partnerInfo = partnerInfos.find(pi => pi.id == imp.partnerId);
  
            if (partnerInfo) {
              imp.partnerName = partnerInfo.name;
            }
          });
  
          this.itemMonitoringPartners = itemMonitoringPartners;
        });
      }
    });
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }
  
  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.item).then(result => {
      this.actions = result;
    });
  }

  canExport() {
    return this.actions.CanExport;
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  canDelete(rowSiteId) {
    if (rowSiteId && rowSiteId !== Number(this.authenticationService.getUserSiteId())) {
      return false;
    }
    return this.actions.CanDelete;
  }

  async loadData() {
    this.loaded = true;

    await this.getItemMonitoringPartners();

    await this.setItemXClientMonitoringList();

    this.loaded = false;
  }

  async setItemXClientMonitoringList() {

    if (!this.itemDCR || (!this.itemDCR.d0 || !this.itemDCR.c0 || !this.itemDCR.r0)) {
      this.notificationService.alert('top', 'center', 'Monitorizare Produs - D0/C0/R0 nu au fost gasite pentru acest produs',
        NotificationTypeEnum.Red, true);
      
      return;
    }

    const itemDCR = { [this.item.id]: this.itemDCR };

    await this.itemXClientMonitoringService.getByItemIdsAsyncCalculateDCR(Array.of(this.item.id), itemDCR).then(async r => {
      if (r && r.length > 0) {
        this.itemXClientMonitoringList = r;
      }
    })
  }

  private isAddOrUpdateFormValid(): boolean {
    return (this.selectedItemXClientMonitoring.partnerLink !== null && this.selectedItemXClientMonitoring.partnerLink !== undefined && this.selectedItemXClientMonitoring.partnerLink !== '') &&
           (this.selectedItemXClientMonitoring.conversionFactor !== null && this.selectedItemXClientMonitoring.conversionFactor !== undefined);
  }

  public async deleteRecords(data: any) {
    this.loaded = true;
    await this.itemXClientMonitoringService.deleteItemXClientMonitoringListAsync(this.selectedRows).then(r => {
      if (r) {
        this.notificationService.alert('top', 'center', 'Monitorizare Produs - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true);
      } else {
        this.notificationService.alert('top', 'center', 'Monitorizare Produs - Datele nu au fost sterse! A aparut o eroare!',
          NotificationTypeEnum.Red, true);
      }

      this.refreshClientMonitoringDataGrid();
      this.loaded = false;
    });
  }

  openDetails(row: any) {
    this.selectedItemXClientMonitoring = row;
    this.isOnSaveItemXClientMonitoring = false;

    this.togglePopup(true);
  }

  public deleteItemXClientMonitoringRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  addSingleClientMonitoringDataEvent() {
    this.togglePopup(true);

    this.isOnSaveItemXClientMonitoring = true;

    this.selectedItemXClientMonitoring = new ItemXClientMonitoringModel();
    this.selectedItemXClientMonitoring.inStock = false;
    this.selectedItemXClientMonitoring.conversionFactor = 1;
  }

  private removeWhiteSpaceFromString(inputString: string): string {
    return inputString.replace(/\s/g, '');
  }

  async saveData() {
    if (this.selectedItemXClientMonitoring) {

      this.loaded = true;

      let itemXClientMonitoringToAdd = new ItemXClientMonitoringModel();
      itemXClientMonitoringToAdd = this.selectedItemXClientMonitoring;

      itemXClientMonitoringToAdd.itemId = this.item.id;

      itemXClientMonitoringToAdd.partnerLink = this.removeWhiteSpaceFromString(itemXClientMonitoringToAdd.partnerLink);

      const existingItemXClientMonitoring = this.itemXClientMonitoringList.find(x => x.partnerLink == itemXClientMonitoringToAdd.partnerLink);
      if (existingItemXClientMonitoring) {
        this.notificationService.alert('top', 'center', 'Monitorizare Produs - Link-ul furnizat exista deja pentru acest produs.',
          NotificationTypeEnum.Red, true);
        this.togglePopup(false);
        this.loaded = false;

        return;
      }

      const itemMonitoringPartner = this.itemMonitoringPartners.find(imp => itemXClientMonitoringToAdd.partnerLink.startsWith(imp.baseURL));
      if (itemMonitoringPartner) {
        itemXClientMonitoringToAdd.partnerId = itemMonitoringPartner.partnerId;        
      } else {
        this.notificationService.alert('top', 'center', 'Monitorizare Produs - Link-ul furnizat nu exista in lista de parteneri implementati.',
          NotificationTypeEnum.Red, true);
        this.togglePopup(false);
        this.loaded = false;

        return;
      }

      if (itemXClientMonitoringToAdd) {
        await this.itemXClientMonitoringService.createItemXClientMonitoringAsync(itemXClientMonitoringToAdd).then(r => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Monitorizare Produs - Datele au fost inserate cu succes!',
              NotificationTypeEnum.Green, true)

            this.togglePopup(false);
            this.loaded = false;

          } else {
            this.notificationService.alert('top', 'center', 'Monitorizare Produs - Datele nu au fost inserate! A aparut o eroare!',
              NotificationTypeEnum.Red, true);
            this.togglePopup(false);
            this.loaded = false;
          }
        });
      } else {
        this.notificationService.alert('top', 'center', 'A aparut o eroare! Datele nu au fost setate!',
          NotificationTypeEnum.Red, true);
        this.loaded = false;
      }
    }
  }

  async updateData() {
    if (this.selectedItemXClientMonitoring) {

    this.loaded = true;

    let itemXClientMonitoringToUpdate = new ItemXClientMonitoringModel();
    itemXClientMonitoringToUpdate = this.selectedItemXClientMonitoring;

    itemXClientMonitoringToUpdate.partnerLink = this.removeWhiteSpaceFromString(itemXClientMonitoringToUpdate.partnerLink);

    const existingItemXClientMonitoring = this.itemXClientMonitoringList.find(x => x.partnerLink == itemXClientMonitoringToUpdate.partnerLink && x.id !== itemXClientMonitoringToUpdate.id);
    if (existingItemXClientMonitoring) {
      this.notificationService.alert('top', 'center', 'Monitorizare Produs - Link-ul furnizat exista deja pentru acest produs.',
        NotificationTypeEnum.Red, true);
      this.togglePopup(false);
      this.loaded = false;

      return;
    }

    const itemMonitoringPartner = this.itemMonitoringPartners.find(imp => itemXClientMonitoringToUpdate.partnerLink.startsWith(imp.baseURL));
    if (itemMonitoringPartner) {
      itemXClientMonitoringToUpdate.partnerId = itemMonitoringPartner.partnerId;        
    } else {
      this.notificationService.alert('top', 'center', 'Monitorizare Produs - Link-ul furnizat nu exista in lista de parteneri implementati.',
        NotificationTypeEnum.Red, true);
      this.togglePopup(false);
      this.loaded = false;

      return;
    }

    await this.itemXClientMonitoringService.updateItemXClientMonitoringAsync(itemXClientMonitoringToUpdate).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Monitorizarea de produs a fost updatata cu succes!',
            NotificationTypeEnum.Green, true);

          this.togglePopup(false);
          this.loaded = false;
        } else {
          this.notificationService.alert('top', 'center', 'Monitorizarea de produs nu a fost updatata cu succes. A aparut o eroare!',
            NotificationTypeEnum.Red, true);

          this.togglePopup(false);
          this.loaded = false;
        }
      });
    }
  }

  refreshClientMonitoringDataGrid() {
    this.loaded = true;

    this.setItemXClientMonitoringList().then(r => {
      this.loaded = false;
      this.dataGrid.instance.refresh();
    })
  }

  private togglePopup(open: boolean) {
    this.popupVisible = open;
    this.popupRendered = open;

    if (open === false) {
      this.onPopupHidden();
    }
  }

  onPopupHidden() {
    this.isOnSaveItemXClientMonitoring = undefined;
    this.popupRendered = false;
    this.refreshClientMonitoringDataGrid();
  }

  onInsertedPartnerLinkInput(event: any) {
    if (event) {
      if (event.target && (event.target.value !== undefined && event.target.value !== null)) {
        event.target.value = event.target.value.replace(/\s/g, '');

        this.selectedItemXClientMonitoring.partnerLink = event.target.value;
        this.selectedItemXClientMonitoring.partnerLink = this.selectedItemXClientMonitoring.partnerLink.trim();
      }
    }
  }

  onInsertedConversionFactorInput(event: any) {
    if (event) {
      if (event.target && (event.target.value !== undefined && event.target.value !== null)) {
        event.target.value = event.target.value.replace('e', '');

        if (!event.target.value || event.target.value === '') {
          this.selectedItemXClientMonitoring.conversionFactor = undefined;
        } else {
          this.selectedItemXClientMonitoring.conversionFactor = (Number)(event.target.value);
        }
      }
    }
  }
}