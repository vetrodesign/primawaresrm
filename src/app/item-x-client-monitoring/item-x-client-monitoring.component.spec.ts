import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemXClientMonitoringComponent } from './item-x-client-monitoring.component';

describe('ItemXClientMonitoringComponent', () => {
  let component: ItemXClientMonitoringComponent;
  let fixture: ComponentFixture<ItemXClientMonitoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemXClientMonitoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemXClientMonitoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
