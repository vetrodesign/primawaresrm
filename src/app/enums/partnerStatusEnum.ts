export enum PartnerStatusEnum {
    Functioning = 1,
    Radiated = 2,
    Inactive = 3
}