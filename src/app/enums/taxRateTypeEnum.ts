export enum TaxRateTypeEnum {
    NoType = 0,
    StandardRate = 1,
    Reduced1Rate = 2,
    Reduced2Rate = 3,
    VeryReducedRate = 4,
    ParkingTypeRate = 5
  }