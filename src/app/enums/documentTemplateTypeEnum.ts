export enum DocumentTemplateTypeEnum {
    Content = 1,
    Header = 2,
    Footer = 3
}