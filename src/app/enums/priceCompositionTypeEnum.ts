export enum PriceCompositionTypeEnum {
    SinglePrice = 1,
    DiscountGridPrice = 2
  }

export enum SalePriceListCompositionTypeEnum {
    SinglePrice = 1,
    DiscountGridPrice = 2,
    Special = 3
}