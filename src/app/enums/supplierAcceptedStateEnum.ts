export enum SupplierAcceptedStateEnum {
    Approved = 1,
    Unverified = 2,
    Banned = 3
}