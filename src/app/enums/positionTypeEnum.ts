export enum PositionTypeEnum {
    SupplyManager = 1,
    Supply = 2,
    SalesManager = 3,
    Sales = 4,
    EconomicManager = 5,
    Economist = 6,
    GeneralManager = 7,
  }