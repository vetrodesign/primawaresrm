export enum PartnerSubscriptionTableColumnsTypeEnum {
    CustomerStatusAndSubscription = 'customerStatusAndSubscription',
    PartnerStatus = 'partnerStatus',
    Description = 'subscriptionDescription',
}