export enum BarCodeAllocationTypeEnum {
    Existing = 1,
    Allocated = 2,
    AllocatedAtReception = 3
  }