export enum TurnoverSectionTypeEnum {
    ClientTurnover = 1,
    PartnerActivityTurnover = 2,
    NewItemsTurnover = 3,
    MandatoryPromotionTurnover = 4,
    OfficeItemsTurnover = 5,
    LiquidationOfferTurnover = 6,
    AgentOtherItemTurnover = 7
  }