export enum TaxTypeEnum {
    VAT = 1,
    CustomsTax = 2,
    Excise = 3
  }