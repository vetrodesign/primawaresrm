export enum ProductConventionEnum {
    Consumer = 1,
    Distributor = 2,
    Retail = 3
  }