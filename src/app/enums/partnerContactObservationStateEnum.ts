export enum PartnerContactObservationStateEnum {
  Invoiced = 1,
  NoDesire = 2,
  Uncontacted = 3
}