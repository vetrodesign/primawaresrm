export enum ContractSupportTypeEnum {
    Electronic = 1,
    OriginalPaper = 2,
    CopyPaper = 3
  }