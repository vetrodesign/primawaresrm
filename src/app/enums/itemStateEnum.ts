export enum ItemStateEnum {
    AtOrder = 1,
    LiquidationEndOfUse = 2,
    WithTurnover = 3,
    Promotion = 4
}