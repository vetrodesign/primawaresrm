export enum DepartmentTypeEnum {
  Sales = 1,
  Supply = 2,
  Marketing = 3,
  Production = 4,
  Programming = 5,
  Accountancy = 6,
  HumanResource = 7,
  Operations = 8,
  Tehnic = 9,
  Extern = 10,
  Management = 11,
  Personal = 12
}
