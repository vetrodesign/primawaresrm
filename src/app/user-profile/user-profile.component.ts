import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'app/models/user.model';
import { AuthService } from 'app/services/auth.service';
import { UsersService } from 'app/services/user.service';
import { PostService } from 'app/services/post.service';
import { PostDetail } from 'app/models/postDetail.model';
import { Office } from 'app/models/office.model';
import { OfficeService } from 'app/services/office.service';
import { Post } from 'app/models/post.model';
import { PartnerService } from 'app/services/partner.service';
import { PartnerObservationService } from 'app/services/partner-observation.service';
import { TurnoverSectionTypeEnum } from 'app/enums/turnoverSectionTypeEnum';
import { TranslateService } from 'app/services/translate';
import { DepartmentsService } from 'app/services/departments.service';
import { Department } from 'app/models/department.model';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  turnoverSections: {}[] = []
  user: User;
  post: Post;
  office: Office;
  department: Department;
  postDetail: PostDetail;
  timeRange: any;
  isUserSpecial: boolean;
  selectedPosts: Post[];
  selectedUsers: User[];
  searchPosts: number[];
  searchUsers: number[];
  partners: any;
  partnersObservations: any;
  partnersXItemsComments: any;
  loaded: boolean = true;
  groupedText: string;


  constructor(
    private userService: UsersService,
    private authenticationService: AuthService,
    private postService: PostService,
    private officeService: OfficeService,
    private partnerService: PartnerService,
    private partnerObservationService: PartnerObservationService,
    private translationService: TranslateService,
    private departmentService: DepartmentsService) {
    this.getUser().then(() => {
      this.getPost().then(() => {
        this.getIsUserSpecial();
        this.setSearchPostIfNotSpecial();
        this.setSearchUsersIfNotSpecial();
        this.getOffice().then(() => {
          this.getDepartment().then(() => {
            this.getPostDetail().then(() => {
              this.getPosts().then(() => {
                if (!this.isUserSpecial) {
                  this.searchItems();
                }
              });
            });
          });
        });
      });
      this.getPostDetail();
      this.getPosts();
    });
    this.getInitialTimeRange();
    this.groupedText = this.translationService.instant('groupedText');

  }

  ngOnInit() {
    this.loaded = true;
    for (let n in TurnoverSectionTypeEnum) {
      if (typeof TurnoverSectionTypeEnum[n] === 'number') {
        this.turnoverSections.push({
          id: <any>TurnoverSectionTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
    this.loaded = false;
  }

  private async getUser() {
    this.loaded = true;
    let userName = this.authenticationService.getUserUserName();
    await this.userService.getByUsernameOrEmail(userName, null).then(userList => {
      if (userList && userList.length > 0) {
        this.user = userList[0];
      }
    });
    this.loaded = false;
  }

  private async getPost() {
    this.loaded = true;
    await this.postService.getPostsByCustomerId().then(posts => {
      let post = posts.find(post => post.id === this.user.postId);
      if (post) {
        this.post = post;
      }
    });
    this.loaded = false;
  }

  private async getPostDetail() {
    this.loaded = true;
    this.postService.getPostDetailAsync(this.user.postId).then(postDetail => {
      if (postDetail) {
        this.postDetail = postDetail;
      }
    });
    this.loaded = false;
  }

  private getInitialTimeRange() {
    this.loaded = true;
    this.timeRange = {
      startDate: new Date(),
      endDate: new Date()
    };
    this.loaded = false;
  }

  getFirstName() {
    if (this.user) {
      return this.user.firstName;
    }
  }

  getLastName() {
    if (this.user) {
      return this.user.lastName;
    }
  }

  getMobile() {
    if (this.postDetail) {
      return this.postDetail.mobilePhone;
    }
  }

  getPostName() {
    if (this.post) {
      return this.post.companyPost;
    }
  }

  getOfficeName() {
    if (this.office) {
      return this.office.name;
    }
  }

  private async getOffice() {
    this.loaded = true;
    await this.officeService.getOfficeByOfficeIdAsync(this.post.officeId).then(office => this.office = office);
    await this.officeService.getOfficeByOfficeIdAsync(this.post.officeId).then(office => this.office = office);
    this.loaded = false;
  }

  getDepartmentName() {
    if (this.department) {
      return this.department.name;
    }
  }

  private async getDepartment() {
    this.loaded = true;
    await this.departmentService.getByDepartmentIdAsync(this.office.departmentId).then(department => this.department = department);
    this.loaded = false;
  }

  private getIsUserSpecial() {
    this.loaded = true;
    if (this.post) {
      if (this.post.isLeadershipPost || this.post.isOfficeSuperior || this.post.isLocationSuperior || this.post.isGeneralManager) {
        this.isUserSpecial = true;
        this.loaded = false;
        return;
      }
    }

    this.isUserSpecial = false;
    this.loaded = false;
  }

  private async getPosts() {
    this.loaded = true;
    this.postService.getPostsByCustomerId().then(posts => {
      this.selectedPosts = posts;
    });
    this.loaded = false;
  }

  private setSearchPostIfNotSpecial() {
    this.loaded = true;
    if (!this.isUserSpecial) {
      this.searchPosts = [this.post.id];
    }
    this.loaded = false;
  }

  private setSearchUsersIfNotSpecial() {
    this.loaded = true;
    if (!this.isUserSpecial) {
      this.searchUsers = [this.user.id];
    }
    this.loaded = false;
  }

  async searchItems() {
    this.loaded = true;
    await this.getSelectedUsers().then(() => {this.loaded = false;});
    if (this.searchPosts && this.searchUsers) {
      this.loaded = true;
      await this.getPartners().then(() => {
        this.getPartnerObservations().then(() => {
          this.getPartnersXItemsComment().then(() => {
            this.loaded = false;
          });
        })
      });
    }
  }

  private async getPartners() {
    await this.partnerService.getPartnersByCreatedByAndDate(this.searchUsers, this.timeRange.startDate, this.timeRange.endDate).then(partners => this.partners = partners);
  }

  private async getPartnerObservations() {
    await this.partnerObservationService.getByCreatedByAndDate(this.searchPosts, this.timeRange.startDate, this.timeRange.endDate).then(partnersObservations => this.partnersObservations = partnersObservations);
  }

  private async getPartnersXItemsComment() {
    await this.partnerService.getPartnersXItemsCommentByCreatedByAndDate(this.searchPosts, this.timeRange.startDate, this.timeRange.endDate).then(partnersXItemsComments => this.partnersXItemsComments = partnersXItemsComments);
  }

  private async getSearchPosts(item) {
    this.searchPosts = item.value;
  }

  getImage() {
    if (this.user) {
      return this.user.profileImageBase64;
    }
  }

  openPartnerName(event) {
    if (event.rowType === "data" && event.column.dataField == "partnerName") {
      var url = environment.SRMPrimaware + '/' + Constants.partner + '?id=' + event.data.partnerId;
      window.open(url, '_blank');
    }
  }

  private async getSelectedUsers() {
    this.loaded = true;
    if (this.searchPosts && this.isUserSpecial) {
      await this.userService.getByPostIds(this.searchPosts).then(u => this.selectedUsers = u);
      if (this.selectedUsers) {
        this.getSearchUsers();
      }
    }
    this.loaded = false;
  }

  private getSearchUsers() {
    this.searchUsers = [];
    this.selectedUsers.forEach(su => this.searchUsers.push(su.id));
  }

  onCellHoverChanged(event) {
    if (event.rowType === "data" && event.column.dataField == "partnerName" && event.eventType === "mouseover") {
      event.cellElement.style.textDecorationLine = "underline";
      event.cellElement.style.fontWeight = "900";
      event.cellElement.style.cursor = "pointer";
    }

    if (event.rowType === "data" && event.column.dataField == "partnerName" && event.eventType === "mouseout") {
      event.cellElement.style.textDecorationLine = "";
      event.cellElement.style.fontWeight = "400";
    }


  }

  getNumberOfClients() {
    if (this.partners) {
      return this.getNumberOfDistinctPartners(this.partners);
    }
  }

  getNumberOfContactedPersons() {
    if (this.partnersObservations) {
      return this.getNumberOfDistinctPartners(this.partnersObservations);
    }
  }

  getNumberOfCommentsOnTurnover() {
    if (this.partnersXItemsComments) {
      return this.getNumberOfDistinctPartners(this.partnersXItemsComments);
    }
  }

  getNumberOfDistinctPartners(partners) {
    let distinctPartners = [];
    partners.forEach(partner => {
      if (!distinctPartners.includes(partner.partnerId)) {
        distinctPartners.push(partner.partnerId);
      }
    });
    return distinctPartners.length;
  }

}
