import { Component, OnInit, AfterViewInit, ViewChild, Input } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { DepartmentTypeEnum } from "app/enums/departmentTypeEnum";
import { NotificationTypeEnum } from "app/enums/notificationTypeEnum";
import { GridToolbarComponent } from "app/helpers/grid-toolbar/grid-toolbar.component";
import { DailyReport, DailyReportFullDetails } from "app/models/daily-report.model";
import { IPageActions } from "app/models/ipageactions";
import { SalesTarget } from "app/models/salestarget.model";
import { AuthService } from "app/services/auth.service";
import { ClientModuleMenuItemsService } from "app/services/client-module-menu-items.service";
import { CurrencyRateService } from "app/services/currency-rate.service";
import { DailyReportDetailsService } from "app/services/daily-report-details.service";
import { DailyReportService } from "app/services/daily-report.service";
import { DepartmentsService } from "app/services/departments.service";
import { NotificationService } from "app/services/notification.service";
import { PageActionService } from "app/services/page-action.service";
import { SalesGroupTargetService } from "app/services/sales-group-target.service";
import { SalesTargetPostService } from "app/services/sales-target-post.service";
import { SalesTargetService } from "app/services/sales-target.service";
import { TranslateService } from "app/services/translate";
import { UsersService } from "app/services/user.service";
import { DxDataGridComponent, DxValidatorComponent } from "devextreme-angular";
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-daily-report',
  templateUrl: './daily-report.component.html',
  styleUrls: ['./daily-report.component.css']
})
export class DailyReportComponent implements OnInit, AfterViewInit {
  @ViewChild('dailyReportDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('dailyReportGridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;

  displayData: boolean;
  popupVisible: boolean;
  loaded: boolean;
  isOnSaveDailyReport: boolean;

  actions: IPageActions;
  dailyReports: DailyReport[] = [];
  selectedDailyReport: DailyReport;
  salesTargets: SalesTarget[] = [];
  salesGroupTargets: any[] = [];
  popupGroups: any[] = [];
  salesTargetAllocations: any[] = [];
  departments: any[];
  posts: any[];
  allPosts: any[];
  users: any[];
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  groupedText: string;
  isTabActive: string;
  isOwner: boolean;

  selectedPostId: number;
  selectedValid: boolean;

  selectedDayDate: Date;
  selectedPost: any;

  isUserAdminOrHasSuperiorPostValue: boolean;

  dataSource: DailyReportFullDetails[];
  currencies: any[] = [];

  infoButton: string;

  constructor(
    private dailyReportService: DailyReportService,
    private dailyReportDetailsService: DailyReportDetailsService,

    private salesGroupTargetService: SalesGroupTargetService,
    private userService: UsersService,
    private departmentsService: DepartmentsService,
    private translationService: TranslateService,
    private currencyService: CurrencyRateService,
    private salesTargetService: SalesTargetService,
    private salesTargetAllocationService: SalesTargetPostService,
    private notificationService: NotificationService,
    private pageActionService: PageActionService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService) {
    this.isOwner = this.authService.isUserOwner();
    this.groupedText = this.translationService.instant('groupedText');
    this.isTabActive = this.authService.getCustomerPageTitleTheme();
    this.valueExpr = this.valueExpr.bind(this);
    this.setActions();

     
  }

 

  ngOnInit(): void {
    this.getData();
    this.isUserAdminOrHasSuperiorPostValue = this.isUserAdminOrHasSuperiorPost();
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getSaleTargets(), this.getSaleGroupTargets(), this.getUsers(), this.getDepartments(), this.getBaseCurrency()]).then(x => {
      this.selectedDayDate = new Date();
      this.getSalesTargetAllocations().then(y => { this.getDailyReports() });
      this.loaded = false;
    });
  }

  async searchData() {
    if (this.validationGroup.instance.validate().isValid) {
      this.getSalesTargetAllocations().then(y => { this.getDailyReports() });
    }
  }

  async getDailyReports() {
    this.loaded = true;
    await this.dailyReportService.getByDateAndPostIdAsync(this.selectedPost, this.selectedDayDate).then(items => {
      this.dailyReports = (items && items.length > 0) ? items : [];
      this.createDataSource();
      this.loaded = false;
    });
  }

  async getBaseCurrency(): Promise<any> {
    await this.currencyService.getBaseCurrency().then(x => {
      this.currencies = x;
    });
  }

  createDataSource() {
    this.dataSource = [];
    this.salesTargetAllocations.forEach(allocation => {
      let d = new DailyReportFullDetails();
      d.saleTargetAllocationId = allocation.id;
      d.date = this.selectedDayDate;
      d.saleTargetId = allocation.salesTargetId;
      d.saleTargetDescription = allocation.description;

      let target = this.salesTargets.find(st => st.id == allocation.salesTargetId);
      if (target && target.orderNumber) {
        d.orderNumber = target.orderNumber;
      }
      if (target && target.targetType == 1) {
        let c = this.currencies.find(cu => cu.id == allocation.currencyId)?.name;
        d.saleTargetValue = allocation.value + ' ' + c;
      }
      if (target && target.targetType == 2 || target.targetType == 3) {
        d.saleTargetValue = allocation.value;
      }
      if (target && target.targetType == 7) {
        d.saleTargetValue = allocation.value + ' ' + target.customTypeText;
      }
      if (target && target.targetType == 4) {
        d.saleTargetValue = moment(allocation.date).format('DD-MM-YYYY HH:mm').toString();
      }
      if (target && target.targetType == 5) {
        d.saleTargetValue = allocation.value + ' %';
      }
      if (target && target.targetType == 6) {
        d.saleTargetValue = allocation.value + ' Min';
      }

      let dailyReport = this.dailyReports.find(dr => dr.salesTargetXPostId == allocation.id);
      if (dailyReport) {
        d.value = dailyReport.value;
        d.description = dailyReport.description;
        d.validated = dailyReport.validated;
      } else {
        d.value = null
        d.description = null;
        d.validated = false;
      }
      d.isUnderTarget = d.value && d.value < allocation.value;
      this.dataSource.push(d);
    });
    this.dataSource = _.sortBy(this.dataSource, (item) => parseInt(item.orderNumber));
  }

  loadData() {
    this.getData();
    this.setGridInstance();
  }

  async getDepartments() {
    this.departments = [];
    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(items => {
        this.departments = items;
        this.setSalesPosts();
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(items => {
        this.departments = items;
        this.setSalesPosts();
      });
    }
  }

  setSalesPosts() {
    if (this.departments && this.departments.length > 0) {
      const off = this.departments.map(x => x.offices).reduce((x, y) => x.concat(y));
      if (off && off.some(x => x.posts.length > 0)) {
        this.allPosts = off.map(x => x.posts)
          .reduce((x, y) => x.concat(y));

        this.selectedPost = this.allPosts.find(x => x.id === this.users.find(x => x.id === Number(this.authService.getUserId()))?.postId)?.id;
      }

      const salesDeps = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Sales);
      if (salesDeps && salesDeps.length > 0 && salesDeps[0].offices) {
        const off = salesDeps.map(x => x.offices).reduce((x, y) => x.concat());
        if (off && off.some(x => x.posts.length > 0)) {
          this.posts = off.map(x => x.posts)
            .reduce((x, y) => x.concat(y));
        }
      }
    }
  }

  async getSaleTargets() {
    if (this.isOwner) {
      await this.salesTargetService.getAllAsync().then(items => {
        this.salesTargets = items;
      });
    } else {
      await this.salesTargetService.getByCustomerIdAsync().then(items => {
        this.salesTargets = items;
      });
    }
  }

  async getSaleGroupTargets() {
    if (this.isOwner) {
      await this.salesGroupTargetService.getAllAsync().then(items => {
        this.salesGroupTargets = items;
      });
    } else {
      await this.salesGroupTargetService.getByCustomerIdAsync().then(items => {
        this.salesGroupTargets = items;
      });
    }
  }

  async getUsers() {
    if (this.isOwner) {
      await this.userService.getUsersAsync().then(items => {
        this.users = items;
      });
    } else {
      await this.userService.getUserAsyncByID().then(items => {
        this.users = items;
      });
    }
  }

  /*User has admin role or superior post*/
  public isUserAdminOrHasSuperiorPost(): boolean {
    if (this.authService.isUserAdmin()) {
      return true;
    } else {
      if (this.allPosts && this.allPosts.length > 0 && this.users && this.users.length > 0) {
        let post = this.allPosts?.find(x => x.id === this.users.find(x => x.id === Number(this.authService.getUserId()))?.postId);
        if (post && (post.isDepartmentSuperior || post.isGeneralManager || post.isLeadershipPost)) {
          return true;
        } else {
          return false;
        }
      }
      return false;
    }
  }

  /*Is user post from sales department*/
  public isUserFromSalesDepartment(): boolean {
    return this.posts.filter(x => x.id === this.users.find(x => x.id === Number(this.authService.getUserId()))?.postId).length !== 0;
  }

  async getSalesTargetAllocations() {
    await this.salesTargetAllocationService.getAllAsync().then(items => {
      this.salesTargetAllocations = (items && items.length > 0) ? items.filter(x => x.postId == this.selectedPost && x.isActive) : [];
    });
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  ngAfterViewInit() {
    this.setGridInstance();
  }

  setGridInstance() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: false,
      CanUpdate: true,
      CanDelete: false,
      CanPrint: true,
      CanExport: true,
      CanImport: false,
      CanDuplicate: false
    };
  }

  async validateReport() {
    this.loaded = true;
    this.dataSource.forEach(item => {
      if (item && item.validated == false) {
        let dailyReport = this.dailyReports.find(dr => dr.salesTargetXPostId == item.saleTargetAllocationId);
        if (dailyReport) {
          dailyReport.validated = true;
          this.dailyReportService.updateDailyReportAsync(dailyReport).then(response => {
          });
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost modificate! Targetul nu a fost completat de agent pentru a se valida!', NotificationTypeEnum.Red, true)
        }
      }
    });

    setTimeout(() => {
      this.getDailyReports();
      this.loaded = false;
    }, 2000);
  }

  public refreshDataGrid() {
    this.loadData();
    this.dataGrid.instance.refresh();
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
    this.rowIndex = row.rowIndex;
  }

  public async validateRow(row: any) {
    if (row.value && row.description) {
      let dailyReport = this.dailyReports.find(dr => dr.salesTargetXPostId == row.saleTargetAllocationId);
      dailyReport.validated = true;

      await this.dailyReportService.updateDailyReportAsync(dailyReport).then(response => {
        if (response) {
          this.notificationService.alert('top', 'center', 'Datele au fost create cu succes!', NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost create!', NotificationTypeEnum.Red, true)
        }
        this.getDailyReports();
      });
    } else {
      this.notificationService.alert('top', 'center', 'Datele nu au fost modificate! Targetul nu a fost completat de agent pentru a se valida!', NotificationTypeEnum.Red, true)
    }
  }

  public async onRowUpdated(event: any): Promise<void> {
    let dailyReport = this.dailyReports.find(dr => dr.salesTargetXPostId == event.data.saleTargetAllocationId);

    if (dailyReport) {
      //update
      if (dailyReport.validated) {
        this.notificationService.alert('top', 'center', 'Targetul a fost deja validat! Nu se mai poate modifica!', NotificationTypeEnum.Red, true)
        this.getDailyReports();
        return;
      }

      dailyReport.value = event.data.value;
      dailyReport.description = event.data.description ? event.data.description : 'Fara observatii';
      await this.dailyReportService.updateDailyReportAsync(dailyReport).then(response => {
        if (response) {
          this.notificationService.alert('top', 'center', 'Datele au fost modificate cu succes!', NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost modificate!', NotificationTypeEnum.Red, true)
        }
        this.getDailyReports();
      });
    } else {
      //create
      let dr = new DailyReport();
      dr.customerId = Number(this.authService.getUserCustomerId());
      dr.salesTargetXPostId = event.data.saleTargetAllocationId;
      dr.value = event.data.value;
      dr.description = event.data.description ? event.data.description : 'Fara observatii';
      dr.validated = event.data.validated;
      dr.fromDate = this.selectedDayDate;

      await this.dailyReportService.createDailyReportAsync(dr).then(response => {
        if (response) {
          this.notificationService.alert('top', 'center', 'Datele au fost create cu succes!', NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost create!', NotificationTypeEnum.Red, true)
        }
        this.getDailyReports();
      });
    }
  }

  isToday() {
    if (moment(this.selectedDayDate).format('DD-MM-YYYY').toString() === moment(new Date()).format('DD-MM-YYYY').toString())
      return true;
    else return false;
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  getPostDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  getTargetDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  valueExpr(item) {
    if (!item && !item.value) {
      return '';
    } else {
      let result = item.value;
      let target = this.salesTargets.find(st => st.id == item.saleTargetId);
      let allocation = this.salesTargetAllocations.find(al => al.id == item.saleTargetAllocationId);

      if (target != undefined && item.value && allocation && allocation.currencyId && target.targetType == 1) {
        let c = this.currencies.find(cu => cu.id == allocation.currencyId)?.name;
        result += ' ' + c;
      }
      if (target != undefined && target.targetType == 2 || target.targetType == 3) {
        result = result;
      }
      if (target != undefined && item.value && target.targetType == 7) {
        result += ' ' + target.customTypeText;
      }
      if (target != undefined && item.value && target.targetType == 5) {
        result += ' %';
      }
      if (target != undefined && item.value && target.targetType == 6) {
        result += ' Min';
      }
      return result
    }
  }

  deleteRecords(e) {

  }
}


