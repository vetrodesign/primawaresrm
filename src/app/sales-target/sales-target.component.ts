import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { Constants } from 'app/constants';
import { PageActionService } from 'app/services/page-action.service';
import { SalesTarget } from 'app/models/salestarget.model';
import { SalesTargetService } from 'app/services/sales-target.service';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { DepartmentsService } from 'app/services/departments.service';
import { SalesTargetAllocationComponent } from 'app/sales-target-allocation/sales-target-allocation.component';
import { DailyReportComponent } from 'app/daily-report/daily-report.component';
import { UsersService } from 'app/services/user.service';
import { SalesGroupTargetComponent } from 'app/sales-group-target/sales-group-target.component';
import { SalesGroupTarget } from 'app/models/salegrouptarget.model';
import { SalesGroupTargetService } from 'app/services/sales-group-target.service';
import { TargetTypeEnum } from 'app/enums/saleTargetTypeEnum';
import { SalesTargetReportComponent } from 'app/sales-target-report/sales-target-report.component';
import { ActivatedRoute } from '@angular/router';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';

@Component({
  selector: 'app-sales-target',
  templateUrl: './sales-target.component.html',
  styleUrls: ['./sales-target.component.css']
})
export class SalesTargetComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;
  @ViewChild('saleTargetValidationGroup') saleTargetValidationGroup: DxValidatorComponent;

  @ViewChild('salesGroupTarget') salesGroupTarget: SalesGroupTargetComponent;
  @ViewChild('salesTargetAllocation') salesTargetAllocation: SalesTargetAllocationComponent;
  @ViewChild('dailyReports') dailyReports: DailyReportComponent;
  @ViewChild('salesTargetReports') salesTargetReports: SalesTargetReportComponent;


  actions: IPageActions;
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  departments: any[];
  posts: any[];
  allPosts: any[];
  users: any[];
  isOwner: boolean = false;
  salesTargetPopup: boolean = false;
  salesTargets: SalesTarget[];
  selectedSaleTarget: SalesTarget;
  salesGroupTargets: SalesGroupTarget[] = [];
  saleTargetTypes: { id: number; name: string }[] = [];
  popupDataGrid: any[] = [];
  loaded: boolean;
  customerId: string;
  groupedText: string;
  isTabActive: string;
  tabs: any[];

  infoButton: string;

  constructor(private authenticationService: AuthService,
    private translationService: TranslateService,
    private userService: UsersService,
    private notificationService: NotificationService,
    private salesTargetService: SalesTargetService,
    private salesGroupTargetService: SalesGroupTargetService,
    private departmentsService: DepartmentsService,
    private pageActionService: PageActionService,
    private route: ActivatedRoute,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService) {
    this.setActions();
    this.customerId = this.authenticationService.getUserCustomerId();
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
    this.salesTargets = [];
    this.groupedText = this.translationService.instant('groupedText');

    this.isTabActive = this.authenticationService.getCustomerPageTitleTheme();

    this.tabs = [{ 'id': 0, 'text': 'Grupe Target', 'isActive': false },
    { 'id': 1, 'text': 'Administrare Target', 'isActive': true },
    { 'id': 2, 'text': 'Alocare Target', 'isActive': false },
    { 'id': 3, 'text': 'Centralizator Rapoarte', 'isActive': false }
    ];

    this.getUsers();
    this.getDepartments();
    this.getSalesGroupTargets();
    this.getData();

     
  }

 

  ngOnInit(): void {
    for (let n in TargetTypeEnum) {
      if (typeof TargetTypeEnum[n] === 'number') {
        this.saleTargetTypes.push({
          id: <any>TargetTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
  }

  async getSalesGroupTargets() {
    if (this.isOwner) {
      await this.salesGroupTargetService.getAllAsync().then(items => {
        this.salesGroupTargets = (items && items.length > 0) ? items : [];
      });
    } else {
      await this.salesGroupTargetService.getByCustomerIdAsync().then(items => {
        this.salesGroupTargets = (items && items.length > 0) ? items : [];
      });
    }
  }

  /*User has admin role or superior post*/
  public isUserAdminOrHasSuperiorPost(): boolean {
    if (this.authenticationService.isUserAdmin()) {
      return true;
    } else {
      if (this.allPosts && this.allPosts.length > 0 && this.users && this.users.length > 0) {
        let post = this.allPosts?.find(x => x.id === this.users.find(x => x.id === Number(this.authenticationService.getUserId()))?.postId);
        if (post && (post.isDepartmentSuperior || post.isGeneralManager || post.isLeadershipPost)) {
          return true;
        } else {
          return false;
        }
      }
      return false;
    }
  }

  /*Is user post from sales department*/
  public isUserFromSalesDepartment(): boolean {
    return this.posts.filter(x => x.id === this.users.find(x => x.id === Number(this.authenticationService.getUserId()))?.postId).length !== 0;
  }

  async getDepartments() {
    this.departments = [];
    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(items => {
        this.departments = items;
        this.setSupplyPosts();
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(items => {
        this.departments = items;
        this.setSupplyPosts();
      });
    }
  }


  //This is SRM module so we need t oget posts from Supply Department
  setSupplyPosts() {
    if (this.departments && this.departments.length > 0) {
      const off = this.departments.map(x => x.offices).reduce((x, y) => x.concat(y));
      if (off && off.some(x => x.posts.length > 0)) {
        this.allPosts = off.map(x => x.posts)
          .reduce((x, y) => x.concat(y));
      }

      const supplyDeps = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Supply);
      if (supplyDeps && supplyDeps.length > 0 && supplyDeps[0].offices) {
        const off = supplyDeps.map(x => x.offices).reduce((x, y) => x.concat());
        if (off && off.some(x => x.posts.length > 0)) {
          this.posts = off.map(x => x.posts)
            .reduce((x, y) => x.concat(y));
        }
      }
    }
  }

  async getUsers() {
    if (this.isOwner) {
      await this.userService.getUsersAsync().then(items => {
        this.users = items;
      });
    } else {
      await this.userService.getUserAsyncByID().then(items => {
        this.users = items;
      });
    }
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getSaleTargets()]).then(x => {
      this.loaded = false;
    });
  }

  async getSaleTargets() {
    if (this.isOwner) {
      await this.salesTargetService.getAllAsync().then(items => {
        this.salesTargets = items;
      });
    } else {
      await this.salesTargetService.getByCustomerIdAsync().then(items => {
        this.salesTargets = items;
      });
    }
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.salesTarget).then(result => {
      this.actions = result;
    });
  }
  canUpdate() {
    return this.actions.CanUpdate;
  }

  canExport() {
    return this.actions.CanExport;
  }

  canDelete() {
    return this.actions.CanDelete;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  toggleMode(tab: any) {
    this.tabs.find(x => x.isActive).isActive = false;
    tab.isActive = true;

    if (tab.id == 0) {
      this.salesGroupTarget.getData();
    }

    if (tab.id == 1) {
      this.getSalesGroupTargets();
    }

    if (tab.id == 2) {
      this.salesTargetAllocation.loadData();
    }

    if (tab.id == 3) {
      this.salesTargetReports.loadData();
    }
  }

  public async refreshDataGrid() {
    this.loaded = true;
    await this.getSaleTargets().then(x => {
      this.loaded = false;
      this.dataGrid.instance.refresh();
    });
  }

  public deleteRecords(data: any) {
    this.salesTargetService.deleteAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Datele au fost inactivate cu succes!', NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Datele nu au inactivate!', NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  public async openDetails(data: any) {
    this.selectedSaleTarget = data;
    this.salesTargetPopup = true;
    this.setDataGridInPopup(this.selectedSaleTarget.saleGroupTargetId);
  }

  public add() {
    this.selectedSaleTarget = new SalesTarget();
    this.selectedSaleTarget.isActive = true;
    this.salesTargetPopup = true;
  }

  public closePopup() {
    this.salesTargetPopup = false;
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async saveData(): Promise<void> {
    if (this.saleTargetValidationGroup.instance.validate().isValid) {
      let item = new SalesTarget();
      item = this.selectedSaleTarget;
      item.customerId = Number(this.authenticationService.getUserCustomerId());

      if (item) {
        await this.salesTargetService.createAsync(item).then(r => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Target Vanzari - Datele au fost inserate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Target Vanzari - Datele nu au fost inserate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          }
          this.salesTargetPopup = false;
          this.refreshDataGrid();
        });
      }
    }
  }

  public async updateData(): Promise<void> {
    if (this.saleTargetValidationGroup.instance.validate().isValid) {
      let item = new SalesTarget();
      item = this.selectedSaleTarget;
      if (item) {
        await this.salesTargetService.updateAsync(item).then(r => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Target Vanzari - Datele au fost modificate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Target Vanzari - Datele nu au fost modificate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          };
          this.salesTargetPopup = false;
          this.refreshDataGrid();
        });
      }
    }
  }

  getSaleGroupDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  popupSalesGroupChanged(e) {
    if (e && e.value && e.event) {
      this.setDataGridInPopup(e.value);
    }
  }

  setDataGridInPopup(saleGroupId: number) {
    this.popupDataGrid = this.salesTargets.filter(x => x.saleGroupTargetId === saleGroupId);
  }
}
