import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { DailyReport } from 'app/models/daily-report.model';
import { SalesTargetXPost } from 'app/models/saletargetxpost.model';
import { DailyReportService } from 'app/services/daily-report.service';
import { SalesTargetPostService } from 'app/services/sales-target-post.service';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';
import * as _ from 'lodash';

@Component({
  selector: 'app-sales-target-report',
  templateUrl: './sales-target-report.component.html',
  styleUrls: ['./sales-target-report.component.css']
})
export class SalesTargetReportComponent implements OnInit {
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;
  @ViewChild('salesTargetAllocationDataGrid') dataGrid: DxDataGridComponent;
  
  @Input() salesTargets: any;
  @Input() salesGroupTargets: any;
  @Input() posts: any;
  @Input() saleTargetTypes: any;
  @Input() departments: any;
  
  selectedDayDate: Date = new Date();
  selectedDepartment: any;
  selectedGroupTarget: any;
  loaded: boolean;

  gridColumns: any[];
  gridDataSource: any[];
  selectedRows: any[];

  salesTargetAllocations: SalesTargetXPost[] = [];
  dailyReports: DailyReport[] = [];
  
  constructor(private salesTargetAllocationService: SalesTargetPostService, private dailyReportService: DailyReportService,) { }

  ngOnInit(): void {
    this.selectedDayDate = new Date();
  }

  getDepartmentDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.name;
  }

  getGroupTargetDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' +item.name;
  }

    onSelectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
 }

  onRowPrepared(e) {
  }

  onCellPrepared(e) {
    if (e.rowType === 'data' && e.data) {
        //  console.log(e);
     
        //  for (const property in e.data) {
        //   if (e.data[property] == 'Not completed/ found') {
        //     e.cellElement.style.backgroundColor = "#ffaf82";
        //    }
        // }


    }
  }

  async loadData(): Promise<any> {
    await this.salesTargetAllocationService.getAllAsync().then(items => {
      this.salesTargetAllocations = (items && items.length >  0) ? items : [];
    });

    await this.dailyReportService.getByDateAsync(this.selectedDayDate).then(items => {
      this.dailyReports = (items && items.length >  0) ? items : [];
    });
  }



  addStyle(data) {
    let styles = {};
    if (data.text == "Not completed/ found") {
      styles['background-color'] = '#ffaf82';
    } else {
      let col = data.column.dataField.replace("Realizat", "Reached"); 
      if (data.data[col] == false) { 
        styles['background-color'] = '#ffaf82';
      }
    }
    return styles;
  }


  async getData() {
    if (this.validationGroup.instance.validate().isValid) {
      this.loaded = true;
      await this.loadData().then(r => {
        this.loaded = false;
        if (this.selectedGroupTarget && this.salesTargetAllocations && this.salesTargetAllocations.length > 0) {
          this.gridDataSource = [];
          this.gridColumns = [];
  
          this.gridColumns.push({ dataField: 'Obiectiv', caption: 'Obiectiv', columnAutoWidth: true, width: 300 });
  
          let postItems = _.uniq(this.salesTargetAllocations.filter(x => x.salesGroupId == this.selectedGroupTarget).map(y => y.postId));
  
          postItems.forEach(p => {
            let post = this.posts.find(h => h.id == p);
            this.gridColumns.push({ dataField: post.companyPost + 'TM', caption: post.companyPost + ' - ' + 'TM', columnAutoWidth: true, alignment: 'right',  post: p, isTm: true });
            this.gridColumns.push({ dataField: post.companyPost + 'Realizat', caption: post.companyPost + ' - ' + 'Realizat', columnAutoWidth: true, alignment: 'right', cellTemplate: 'cellTemplate', post: p, isReal: true });
            this.gridColumns.push({ dataField: post.companyPost + 'Reached', caption: post.companyPost + ' - ' + 'Atins', visible: 0, columnAutoWidth: true, alignment: 'right', post: p, isReached: true});
          });
  
          this.salesTargets?.filter(st => st.saleGroupTargetId == this.selectedGroupTarget).forEach(sti => {
            let dataSourceItem = {};
            this.gridColumns.forEach(gc => {
              if (gc.dataField == 'Obiectiv') {
                dataSourceItem[gc.dataField] = sti.code + ' - ' + sti.name;
              }
              if (gc.isTm == true) {
                let sta = this.salesTargetAllocations.find(x => x.salesTargetId == sti.id && x.postId == gc.post) ? this.salesTargetAllocations.find(x => x.salesTargetId == sti.id && x.postId == gc.post)?.value : 0;
                let value = (Number(sta)).toLocaleString(undefined, {
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0
                });         
                dataSourceItem[gc.dataField] = value ? value : null;
              }
              if (gc.isReal == true) {
                let d = this.dailyReports?.find(dr => dr.salesTargetXPostId == this.salesTargetAllocations.find(x => x.salesTargetId == sti.id && x.postId == gc.post)?.id);
                let value = '0';
                if (d && d.value) {
                  value = (Number(d.value)).toLocaleString(undefined, {
                    minimumFractionDigits: 0,
                    maximumFractionDigits: 0
                  });
                } 
                dataSourceItem[gc.dataField] = value;
              }
              if (gc.isReached == true) {
                let postName = this.posts.find(h => h.id == gc.post)?.companyPost;
                let tm  = postName + 'TM';
                let real  = postName + 'Realizat';

                if (dataSourceItem[real] == '0') {
                  dataSourceItem[gc.dataField] = false;
                } else {

                  if (Number(dataSourceItem[real].replace(/,/g, "")) < Number(dataSourceItem[tm].replace(/,/g, ""))) {
                    dataSourceItem[gc.dataField] = false;
                  } else {
                    dataSourceItem[gc.dataField] = true;
                  }
                }
              }
            });
            this.gridDataSource.push(dataSourceItem);
          });
        }
      });
    }
  }
}
