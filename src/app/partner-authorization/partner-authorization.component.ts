import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { Department } from 'app/models/department.model';
import { IPageActions } from 'app/models/ipageactions';
import { Office } from 'app/models/office.model';
import { Partner } from 'app/models/partner.model';
import { Post } from 'app/models/post.model';
import { AuthService } from 'app/services/auth.service';
import { NotificationService } from 'app/services/notification.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';
import { AuthorizationType } from 'app/models/authorization-type.model';
import { PartnerAuthorization } from 'app/models/partner-authorization.model';
import { PartnerAuthorizationService } from 'app/services/partner-authorization.service';
import { AuthorizationTypeService } from 'app/services/authorization-type.service';
import { PageActionService } from 'app/services/page-action.service';

@Component({
  selector: 'app-partner-authorization',
  templateUrl: './partner-authorization.component.html',
  styleUrls: ['./partner-authorization.component.css']
})
export class PartnerAuthorizationComponent implements OnInit {
  authorizationTypes: AuthorizationType[] = [];
  partnerAuthorizations: PartnerAuthorization[] = [];

  isOwner: boolean;
  actions: IPageActions;
  groupedText: string;

  selectedRows: any[];
  selectedRowIndex = -1;

  loaded: boolean = false;

  partnerAuthorizationInfo: string;
  isPopupVisible: boolean = false;
  displayValue: string = '';
  
  @Input() partner: Partner;

  @ViewChild('partnerAuthorizationGridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('partnerAuthorizationDataGrid') dataGrid: DxDataGridComponent;

  constructor(
    private authenticationService: AuthService,
    private translationService: TranslateService,
    private pageActionService: PageActionService,
    private notificationService: NotificationService, 
    private partnerAuthroizationService: PartnerAuthorizationService, 
    private authorizationTypeService: AuthorizationTypeService
  ) {
    this.isOwner = this.authenticationService.isUserOwner();
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();
    this.partnerAuthorizationInfo = this.translationService.instant("partnerAuthorizationInfo");
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  loadData() {
    this.getData();
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getAuthroizationType(), this.getPartnerAuthroization()]).then(x => {
      this.loaded = false;
    });
  }

  async getAuthroizationType(): Promise<any> {
    await this.authorizationTypeService.getByCustomerIdAsync().then(items => {
      this.authorizationTypes = items;
    });
  }

  async getPartnerAuthroization(): Promise<any> {
    if (this.partner) {
      await this.partnerAuthroizationService.getByCustomerIdAndPartnerIdAsync(this.partner.id).then(items => {
        this.partnerAuthorizations = (items && items.length > 0) ? items : [];
      });
    }
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.partner).then(result => {
      this.actions = result;
      this.actions.CanAdd = true;
      this.actions.CanDelete = true;
    });
  }

  canExport() {
    return this.actions.CanExport;
  }

  canDelete(rowSiteId) {
    if (rowSiteId && rowSiteId !== Number(this.authenticationService.getUserSiteId())) {
      return false;
    }
    return this.actions.CanDelete;
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  addPartnerObservation() {
    this.dataGrid.instance.addRow();
  }

  refreshPartnerAuthorizationDataGrid() {
    if (this.partner && this.partner.id) {
      this.getData();
    }
  }

  public deleteRecords(data: any) {
    this.loaded = true;
    this.partnerAuthroizationService.deleteAsync(this.selectedRows).then(r => {
      if (r) {
        this.notificationService.alert('top', 'center', 'Autorizatii - Datele au fost inactivate cu succes!',
        NotificationTypeEnum.Green, true);
      } else {
        this.notificationService.alert('top', 'center', 'Autorizatii - Datele nu au fost inactivate! A aparut o eroare!',
        NotificationTypeEnum.Red, true);
      }
      this.refreshPartnerAuthorizationDataGrid();
      this.loaded = false;
    });
  }

  public async onRowInserting(event: any): Promise<void> {
    this.loaded = true;

    let pa = new PartnerAuthorization();
    pa = event.data;
    pa.isActive = true;
    pa.customerId = Number(this.authenticationService.getUserCustomerId());
    pa.partnerId = this.partner.id;

    if (pa) {
      await this.partnerAuthroizationService.createAsync(pa).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Autorizatii Partener - Datele au fost inserate cu succes!',
          NotificationTypeEnum.Green, true);
          this.refreshPartnerAuthorizationDataGrid();
          this.loaded = false;
        } else {
          this.notificationService.alert('top', 'center', 'Autorizatii Partener - Datele nu au fost inserate! A aparut o eroare!',
          NotificationTypeEnum.Red, true);
          this.refreshPartnerAuthorizationDataGrid();
          this.loaded = false;
        }
      })
    } else {
      this.loaded = false;
    }
  }

  public async onRowUpdating(event: any): Promise<void> {
    this.loaded = true;

    const itemUpdating = { ...event.oldData, ...event.newData };

    if (itemUpdating) {
      await this.partnerAuthroizationService.updateAsync(itemUpdating).then (r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Autorizatii Partener - Datele au fost updatate cu succes!',
          NotificationTypeEnum.Green, true);
          this.refreshPartnerAuthorizationDataGrid();
          this.loaded = false;
        } else {
          this.notificationService.alert('top', 'center', 'Autorizatii Partener - Datele nu au fost updatate! A aparut o eroare!',
          NotificationTypeEnum.Red, true);
          this.refreshPartnerAuthorizationDataGrid();
          this.loaded = false;
        }
      });
    } else {
      this.loaded = false;
    }
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onInitNewRow(data: any) {
    if (data && data.data) {
      data.data.isActive = true;
    }
  }
  public deletePartnerObservationRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  getDisplayExprAuthorization(item) {
    if (!item) {
      return '';
    }
  
    return item.code + ' - ' + item.name;
  }



  openDescriptionPopup(data) {
    this.isPopupVisible = true;
    this.displayValue = data.data.description;
  }

  onRowPrepared(event) {
    if (event.rowType == "data" && event.data.description == "Nu are Site Online") {
      event.rowElement.style.color = 'green';
    }
  }

  public addPartnerAuthorization() {
    this.dataGrid.instance.addRow();
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
  }
}
