import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerAuthorizationComponent } from './partner-authorization.component';

describe('PartnerAuthorizationComponent', () => {
  let component: PartnerAuthorizationComponent;
  let fixture: ComponentFixture<PartnerAuthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerAuthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
