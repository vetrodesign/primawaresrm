import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { AdminComponent } from 'app/admin/admin.component';
import { CustomerComponent } from 'app/customer/customer.component';
import { NotificationsComponent } from 'app/notifications/notifications.component';
import { AuthGuard } from 'app/helpers/auth.guard';
import { Constants } from 'app/constants';
import { SetDataComponent } from 'app/set-data/set-data.component';


import { PartnerComponent } from 'app/partner/partner.component';
import { ExceptionLogComponent } from 'app/exception-log/exception-log.component';
import { ItemComponent } from 'app/item/item.component';
import { TurnoverComponent } from 'app/turnover/turnover.component';
import { WholesaleStoreManagementComponent } from 'app/wholesale-store-management/wholesale-store-management.component';
import { DailyReportComponent } from 'app/daily-report/daily-report.component';
import { SalesTargetComponent } from 'app/sales-target/sales-target.component';
import { PartnerObservationsDetailsComponent } from 'app/partner-observations-details/partner-observations-details.component';
import { PartnerDailyReceiptsComponent } from 'app/partner-daily-receipts/partner-daily-receipts.component';
import { PartnerNameEquivalenceComponent } from 'app/partner-name-equivalence/partner-name-equivalence.component';
import { EshopVisibleItemsComponent } from 'app/eshop-visible-items/eshop-visible-items.component';
import { BarCodeComponent } from 'app/bar-code/bar-code.component';
import { ItemTurnoverAnalysisComponent } from 'app/item-turnover-analysis/item-turnover-analysis.component';
import { ContactCampaignPartnersComponent } from 'app/contact-campaign-partners/contact-campaign-partners.component';
import { SupplierOriginComponent } from 'app/supplier-origin/supplier-origin.component';


export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'upgrade',        component: UpgradeComponent },
    { path: 'notifications',        component: NotificationsComponent },


    { path: Constants.admin,        component: AdminComponent, canActivate: [AuthGuard] },
    { path: Constants.partner,        component: PartnerComponent, canActivate: [AuthGuard] },
    { path: Constants.partnerObservationsDetails,        component: PartnerObservationsDetailsComponent, canActivate: [AuthGuard] },
    { path: Constants.partnerNameEquivalence,        component: PartnerNameEquivalenceComponent, canActivate: [AuthGuard] },
    { path: Constants.item,        component: ItemComponent, canActivate: [AuthGuard] },
    { path: Constants.customer,        component: CustomerComponent, canActivate: [AuthGuard] },
    { path: Constants.userProfile,   component: UserProfileComponent, canActivate: [AuthGuard]},
    { path: Constants.turnover,   component: TurnoverComponent, canActivate: [AuthGuard]},
    { path: Constants.setData,   component: SetDataComponent},
    { path: Constants.logs,        component: ExceptionLogComponent, canActivate: [AuthGuard] },
    { path: Constants.wholesaleStoreManagement,        component: WholesaleStoreManagementComponent, canActivate: [AuthGuard] },
    { path: Constants.dailyReport,   component: DailyReportComponent, canActivate: [AuthGuard]},
    { path: Constants.salesTarget,   component: SalesTargetComponent, canActivate: [AuthGuard]},
    { path: Constants.partnerDailyReceipts, component: PartnerDailyReceiptsComponent, canActivate: [AuthGuard]},
    { path: Constants.eshopVisibleItems, component: EshopVisibleItemsComponent, canActivate: [AuthGuard]},
    { path: Constants.barcode,        component: BarCodeComponent, canActivate: [AuthGuard] },
    { path: Constants.itemTurnoverAnalysis,        component: ItemTurnoverAnalysisComponent, canActivate: [AuthGuard] },
    { path: Constants.contactCampaignPartners,        component: ContactCampaignPartnersComponent, canActivate: [AuthGuard] },
    { path: Constants.supplierOrigin,        component: SupplierOriginComponent, canActivate: [AuthGuard] },
    
    { path: '**', redirectTo: 'customer' }
];
