import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import { DxButtonModule, DxDataGridModule, DxButtonGroupModule, DxDropDownBoxModule, DxTagBoxModule,
 DxSelectBoxModule, DxPieChartModule, DxChartModule, DxSpeedDialActionModule, DxListModule, DxValidationSummaryModule,
 DxValidationGroupModule, DxTextBoxModule, DxValidatorModule, DxFormModule, DxTreeListModule, DxTextAreaModule,
 DxDateBoxModule, DxFileUploaderModule, DxNumberBoxModule, DxPopupModule, DxSwitchModule, DxCheckBoxModule, DxMapModule, DxBulletModule, DxScrollViewModule, DxLoadPanelModule, DxTooltipModule, 
 DxAccordionModule,
 DxColorBoxModule,
 DxRadioGroupModule,
 DxHtmlEditorModule,
 DxSchedulerModule,
 DxPopoverModule,
 DxTabPanelModule,
 DxTabsModule,
 DxAutocompleteModule} from 'devextreme-angular';
import { AdminComponent } from '../../admin/admin.component';
import { CustomerComponent } from '../../customer/customer.component';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { SetDataComponent } from 'app/set-data/set-data.component';
import { SpinnerComponent } from 'app/spinner/spinner.component';
import { ImportComponent } from 'app/import/import.component';
import { PartnerComponent } from 'app/partner/partner.component';
import { PartnerDetailsComponent } from 'app/partner-details/partner-details.component';
import { MapComponent } from 'app/helpers/map/map.component';
import { ExceptionLogComponent } from 'app/exception-log/exception-log.component';
import { TranslateModule } from 'app/services/translate/translate.module';
import { TitleComponent } from 'app/components/title/title.component';
import { PartnerSearchFilterComponent } from 'app/partner-search-filter/partner-search-filter.component';
import { PartnerLegalFormComponent } from 'app/partner-legal-form/partner-legal-form.component';
import { PartnerLocationContactComponent } from 'app/partner-location-contact/partner-location-contact.component';
import { PartnerReviewComponent } from 'app/partner-review/partner-review.component';
import { PartnerDropdownSearchComponent } from 'app/partner-dropdown-search/partner-dropdown-search.component';
import { ItemComponent } from 'app/item/item.component';
import { ItemDetailsComponent } from 'app/item-details/item-details.component';
import { ItemSearchFilterComponent } from 'app/item-search-filter/item-search-filter.component';
import { PartnerFinancialInfoComponent } from 'app/partner-financial-info/partner-financial-info.component';
import { TurnoverComponent } from 'app/turnover/turnover.component';
import { TurnoverSearchFilterComponent } from 'app/turnover-search-filter/turnover-search-filter.component';
import { ItemEcommerceComponent } from 'app/item-ecommerce/item-ecommerce.component';
import { ItemPropertiesComponent } from 'app/item-properties/item-properties.component';
import { ItemRelatedComponent } from 'app/item-related/item-related.component';
import { PartnerNameEquivalenceComponent } from 'app/partner-name-equivalence/partner-name-equivalence.component';
import { WholesaleStoreManagementComponent } from 'app/wholesale-store-management/wholesale-store-management.component';
import { DailyReportComponent } from '../../daily-report/daily-report.component';
import { SalesTargetComponent } from 'app/sales-target/sales-target.component';
import { SalesTargetAllocationComponent } from 'app/sales-target-allocation/sales-target-allocation.component';
import { SalesGroupTargetComponent } from 'app/sales-group-target/sales-group-target.component';
import { SalesTargetReportComponent } from 'app/sales-target-report/sales-target-report.component';
import { PartnerLocationScheduleComponent } from 'app/partner-location-schedule/partner-location-schedule.component';
import { PartnerContractDetailsComponent } from 'app/components/partner-contract-details/partner-contract-details.component';
import { PartnerObservationsComponent } from 'app/partner-observations/partner-observations.component';
import { ItemLogisticsComponent } from 'app/item-logistics/item-logistics.component';
import { ItemXClientMonitoringComponent } from 'app/item-x-client-monitoring/item-x-client-monitoring.component';
import { PartnerWebsiteComponent } from 'app/partner-website/partner-website.component';
import { PartnerObservationsDetailsComponent } from 'app/partner-observations-details/partner-observations-details.component';
import { ItemAutomationComponent } from 'app/item-automation/item-automation.component';
import { ItemSpecificationComponent } from 'app/item-specification/item-specification.component';
import { PartnerFinancialDataComponent } from 'app/partner-financial-data/partner-financial-data.component';
import { PartnerDailyReceiptsComponent } from 'app/partner-daily-receipts/partner-daily-receipts.component';
import { PartnerItemsComponent } from 'app/partner-items/partner-items.component';
import { PartnerSpecializationsComponent } from 'app/partner-specializations/partner-specializations.component';
import { PartnerSalesPriceListComponent } from 'app/partner-sales-price-list/partner-sales-price-list.component';
import { EshopVisibleItemsComponent } from 'app/eshop-visible-items/eshop-visible-items.component';
import { ItemTurnoverHistoryComponent } from 'app/item-turnover-history/item-turnover-history.component';
import { YearlyTurnoverComponent } from 'app/yearly-turnover/yearly-turnover.component';
import { BarCodeComponent } from 'app/bar-code/bar-code.component';
import { BarCodeDetailsComponent } from 'app/bar-code-details/bar-code-details.component';
import { PartnerSubscriptionComponent } from 'app/partner-subscription/partner-subscription.component';
import { PartnerAuthorizationComponent } from 'app/partner-authorization/partner-authorization.component';
import { ItemTurnoverAnalysisComponent } from 'app/item-turnover-analysis/item-turnover-analysis.component';
import { ContactCampaignPartnersComponent } from 'app/contact-campaign-partners/contact-campaign-partners.component';
import { SrmPartnerSearchFilterComponent } from 'app/srm-partner-search-filter/srm-partner-search-filter.component';
import { SupplierOriginComponent } from 'app/supplier-origin/supplier-origin.component';
import { BnrRoborComponent } from 'app/bnr-robor/bnr-robor.component';
import { CurrencyEvolutionChartComponent } from 'app/currency-evolution-chart/currency-evolution-chart.component';
import { EuriborDataComponent } from 'app/euribor-data/euribor-data.component';
import { WeatherForecastComponent } from 'app/weather-forecast/weather-forecast.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    DxButtonModule,
    DxDataGridModule,
    DxDataGridModule,
    DxDropDownBoxModule,
    DxDataGridModule,
    DxTagBoxModule,
    DxSelectBoxModule,
    DxPieChartModule,
    DxChartModule,
    DxSpeedDialActionModule,
    DxListModule,
    DxValidationGroupModule,
    DxValidationSummaryModule,
    DxTextBoxModule,
    DxValidatorModule,
    DxFormModule,
    DxSelectBoxModule,
    DxTreeListModule,
    DxTextAreaModule,
    DxDateBoxModule,
    DxBulletModule,
    DxMapModule,
    DxSwitchModule,
    DxFileUploaderModule,
    DxNumberBoxModule,
    DxPopupModule,
    DxCheckBoxModule,
    TranslateModule,
    DxScrollViewModule,
    DxLoadPanelModule,
    DxTooltipModule,
    DxAccordionModule,
    DxColorBoxModule,
    DxRadioGroupModule,
    DxHtmlEditorModule,
    DxSchedulerModule,
    DxPopoverModule,
    DxTabPanelModule,
    DxTabsModule,
    DxAutocompleteModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    UpgradeComponent,
    AdminComponent,
    CustomerComponent,
    GridToolbarComponent,
    SetDataComponent,
    SpinnerComponent,
    ImportComponent,
    PartnerComponent,
    PartnerObservationsDetailsComponent,
    PartnerDetailsComponent,
    PartnerSearchFilterComponent,
    PartnerLegalFormComponent,
    PartnerLocationContactComponent,
    PartnerReviewComponent,
    PartnerDropdownSearchComponent,
    MapComponent,
    ExceptionLogComponent,
    TitleComponent,
    ItemComponent,
    ItemDetailsComponent,
    ItemSearchFilterComponent,
    PartnerFinancialInfoComponent,
    TurnoverComponent,
    TurnoverSearchFilterComponent,
    ItemEcommerceComponent,
    ItemPropertiesComponent,
    ItemRelatedComponent,
    PartnerNameEquivalenceComponent,
    WholesaleStoreManagementComponent,
    DailyReportComponent,
    SalesTargetComponent,
    SalesTargetAllocationComponent,
    SalesGroupTargetComponent,
    SalesTargetReportComponent,
    PartnerLocationScheduleComponent,
    PartnerContractDetailsComponent,
    PartnerObservationsComponent,
    ItemLogisticsComponent,
    ItemXClientMonitoringComponent,
    PartnerWebsiteComponent,
    ItemAutomationComponent,
    ItemSpecificationComponent,
    PartnerFinancialDataComponent,
    PartnerDailyReceiptsComponent,
    PartnerItemsComponent,
    PartnerSpecializationsComponent,
    PartnerSalesPriceListComponent,
    EshopVisibleItemsComponent,
    ItemTurnoverHistoryComponent,
    YearlyTurnoverComponent,
    BarCodeComponent,
    BarCodeDetailsComponent,
    PartnerSubscriptionComponent,
    PartnerAuthorizationComponent,
    ItemTurnoverAnalysisComponent,
    ContactCampaignPartnersComponent,
    SupplierOriginComponent,
    SrmPartnerSearchFilterComponent,
    BnrRoborComponent,
    CurrencyEvolutionChartComponent,
    EuriborDataComponent,
    WeatherForecastComponent
  ],
  providers: [
    DecimalPipe
  ],
  exports: []
})

export class AdminLayoutModule {}
