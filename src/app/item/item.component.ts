import { Component, OnInit, ViewChild, AfterViewInit, Input, OnChanges, ViewChildren } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { Post } from 'app/models/post.model';
import { ItemService } from 'app/services/item.service';
import { Item } from 'app/models/item.model';
import { MeasurementUnitService } from 'app/services/measurement-unit.service';
import { MeasurementUnit } from 'app/models/measurementunit.model';
import { PageActionService } from 'app/services/page-action.service';
import { Constants } from 'app/constants';
import { ItemType } from 'app/models/itemtype.model';
import { ItemTypeService } from 'app/services/item-type.service';
import { ItemDetailsComponent } from 'app/item-details/item-details.component';
import { BarCodeIntervalService } from 'app/services/bar-code-interval.service';
import { BarCodeItemAllocationService } from 'app/services/bar-code-item-allocation.service';
import { BarCodeItemAllocation } from 'app/models/barcodeitemallocation.model';
import { ItemCaenCodeSpecializationService } from 'app/services/item-caen-code-specialization.service';
import { CaenCodeSpecializationService } from 'app/services/caen-code-specialization.service';
import { TaxService } from 'app/services/tax.service';
import { TaxTypeEnum } from 'app/enums/taxTypeEnum';
import { ItemListFilter } from 'app/models/itemlistfilter.model';
import { ItemGroup } from 'app/models/itemgroup.model';
import { ItemGroupService } from 'app/services/item-group.service';
import { User } from 'app/models/user.model';
import { UsersService } from 'app/services/user.service';
import { PostService } from 'app/services/post.service';
import { CustomCodeService } from 'app/services/custom-code.service';
import { CustomCode } from 'app/models/customcode.model';
import { TaxRateTypeEnum } from 'app/enums/taxRateTypeEnum';
import { Site } from 'app/models/site.model';
import { SiteService } from 'app/services/site.service';
import { Tag } from 'app/models/tag.model';
import { TagService } from 'app/services/tag.service';
import { ItemStateEnum } from 'app/enums/itemStateEnum';
import { PriceCompositionTypeEnum } from 'app/enums/priceCompositionTypeEnum';
import { DepartmentsService } from 'app/services/departments.service';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { Department } from 'app/models/department.model';
import { Office } from 'app/models/office.model';
import { ActivatedRoute } from '@angular/router';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('itemDetails') itemDetails: ItemDetailsComponent;

  actions: IPageActions;
  selectedRows: any[];
  selectedRowsLenght: number;
  selectedRowIndex = -1;
  rowIndex: any;
  customerId: string;
  posts: Post[];
  caenSpecializationsDS: any;
  measurementUnits: MeasurementUnit[];
  supplyAndSalesPosts: Post[];
  barCodeItemAllocations: BarCodeItemAllocation[];
  itemTypes: ItemType[];
  customCodes: CustomCode[];
  customCodesDS: any;
  taxRates: any[];

  taxRateTypes: { id: number; name: string }[] = [];
  itemStates: { id: number; name: string }[] = [];
  priceCompositionType: { id: number; name: string }[] = [];

  items: Item[];
  users: User[] = [];
  supplyAgents: Post[] = [];
  sites: Site[] = [];
  tags: Tag[] = [];
  itemGroups: ItemGroup[];
  selectedItem: Item;
  itemSearchFilter: ItemListFilter;
  barCodeIntervals: any;
  detailsVisibility: boolean;
  multipleUpdateModel: Item;
  isOwner: boolean = false;
  loaded: boolean;
  multipleUpdateItemsPopup: boolean;
  partnersDS: any;
  groupedText: string;
  infoButton: string;

  constructor(private authenticationService: AuthService,
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private itemService: ItemService,
    private siteService: SiteService,
    private measurementUnitService: MeasurementUnitService,
    private departmentsService: DepartmentsService,
    private itemTypeService: ItemTypeService,
    private pageActionService: PageActionService,
    private barCodeIntervalService: BarCodeIntervalService,
    private taxService: TaxService,
    private tagService: TagService,
    private customCodeService: CustomCodeService,
    private userService: UsersService,
    private itemGroupService: ItemGroupService,
    private postService: PostService,
    private barCodeItemAllocationService: BarCodeItemAllocationService,
    private itemCaenCodeSpecializationService: ItemCaenCodeSpecializationService,
    private caenCodeSpecializationService: CaenCodeSpecializationService,
    private route: ActivatedRoute,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService) {
    this.setActions();
    this.customerId = this.authenticationService.getUserCustomerId();
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });

    for (let n in TaxRateTypeEnum) {
      if (typeof TaxRateTypeEnum[n] === 'number') {
        this.taxRateTypes.push({
          id: <any>TaxRateTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    for (let n in ItemStateEnum) {
      if (typeof ItemStateEnum[n] === 'number') {
        this.itemStates.push({
          id: <any>ItemStateEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    for (let n in PriceCompositionTypeEnum) {
      if (typeof PriceCompositionTypeEnum[n] === 'number') {
        this.priceCompositionType.push({
          id: <any>PriceCompositionTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    this.groupedText = this.translationService.instant('groupedText');
    this.measurementUnits = [];
    this.supplyAndSalesPosts = [];
    this.itemGroups = [];
    this.items = [];
    this.posts = [];
    this.users = [];
    this.tags = [];
    this.itemTypes = [];
    this.barCodeItemAllocations = [];
    this.loaded = true;
    this.getDictionary().then(y => { this.loadData(); this.loaded = false; });

     

  }

 

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.setGridInstances();
  }

  setGridInstances() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  async getDictionary() {
    Promise.all([this.getSpecializations(), this.getTaxes(), this.getPosts(), this.getSites(), this.getCustomCodes(), this.getTags(),
    this.getMU(), this.getSupplyAndSalesPosts(), this.getItemTypes()]).then(x => {
    });
  }

  async loadData() {
    Promise.all([
      this.getBarCodes(), this.getBarCodeAllocations()]).then(x => {
        this.getItemsGroup(),
          this.getUsers(),
          this.getSupplyAgents(),
          this.setGridInstances();
      });
  }

  canExport() {
    return this.actions.CanExport;
  }

  async searchEvent(e: any) {
    this.itemSearchFilter = e
    this.loaded = true;
    await this.getItems().then(x => this.loaded = false);
  }

  refreshEvent(e: any) {
    this.loaded = true;
    this.getDictionary().then(x => this.loaded = false);
  }

  async getSites() {
    if (this.isOwner) {
      await this.siteService.getAllCustomerLocationsAsync().then(items => {
        this.sites = items;
      });
    } else {
      await this.siteService.getCustomerLocationsAsyncByID().then(items => {
        this.sites = items;
      });
    }
  }

  async getPosts() {
    if (this.isOwner) {
      await this.postService.getAllPostsAsync().then(items => {
        this.posts = items;
      });
    } else {
      await this.postService.getPostsByCustomerId().then(items => {
        this.posts = items;
      });
    }
  }

  async getTags() {
    if (this.isOwner) {
      await this.tagService.getAllTagsAsync().then(items => {
        this.tags = (items && items.length > 0) ? items : [];
      });
    } else {
      await this.tagService.getAllTagsByCustomerIdAsync().then(items => {
        this.tags = (items && items.length > 0) ? items : [];
      });
    }
  }

  async getItemsGroup(): Promise<any> {
    if (this.isOwner) {
      await this.itemGroupService.getAllItemGroupsAsync().then(items => {
        this.itemGroups = items;
      });
    } else {
      await this.itemGroupService.getItemGroupsAsyncByID().then(items => {
        this.itemGroups = items;
      });
    }
  }


  async getUsers(): Promise<any> {
    await this.userService.getUserAsyncByID().then(x => {
      this.users = x;
    });
  }

  async getSupplyAgents() {
    if (this.isOwner) {
      await this.postService.getAllPostsAsync().then(items => {
        this.supplyAgents = items;
      });
    } else {
      await this.postService.getPostsByCustomerId().then(items => {
        this.supplyAgents = items;
      });
    }
  }

  async getCustomCodes(): Promise<any> {
    await this.customCodeService.getAllCustomCodesAsync().then(items => {
      this.customCodes = items;
      this.customCodesDS = {
        paginate: true,
        pageSize: 15,
        store: this.customCodes
      };
    });
  }

  async getTaxes() {
    this.taxRates = [];
    await this.taxService.getAllTaxesAsync().then(i => {
      if (i && i.length > 0) {
        if (this.authenticationService.getUserCustomerCountryId() !== undefined) {
          this.taxRates = i.filter(type => type.taxType === TaxTypeEnum.VAT && type.countryId === Number(this.authenticationService.getUserCustomerCountryId())).map(y => y.taxRates).reduce((z, y) => z.concat(y));
        }
      }
    });
  }

  async getBarCodes() {
    if (this.isOwner) {
      await this.barCodeIntervalService.getAllBarCodeIntervalsAsync().then(items => {
        this.barCodeIntervals = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    } else {
      await this.barCodeIntervalService.getBarCodeIntervalsAsyncByID().then(items => {
        this.barCodeIntervals = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    }
  }

  async getBarCodeAllocations() {
    if (this.isOwner) {
      await this.barCodeItemAllocationService.getAllBarCodesAllocationsAsync().then(items => {
        this.barCodeItemAllocations = items;
      });
    } else {
      await this.barCodeItemAllocationService.getBarCodesAllocationsAsyncByID().then(items => {
        this.barCodeItemAllocations = items;
      });
    }
  }

  async getSpecializations() {
    await this.caenCodeSpecializationService.getAllCaenCodesSpecializationsAsync().then(items => {
      this.caenSpecializationsDS = {
        paginate: true,
        pageSize: 15,
        store: items
      };
    });
  }

  async getMU() {
    if (this.isOwner) {
      await this.measurementUnitService.getAllMUAsync().then(items => {
        if (items && items.length > 0) {
          this.measurementUnits = items.filter(x => x.syncERPExternalId);;
        }
      });
    }
    else {
      await this.measurementUnitService.getAllMUByCustomerIdAsync().then(items => {
        if (items && items.length > 0) {
          this.measurementUnits = items.filter(x => x.syncERPExternalId);
        }
      });
    }
  }

  async getSupplyAndSalesPosts() {
    let departments = [];

    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(items => {
        departments = items;
        this.supplyAndSalesPosts = this.getAllSupplyAndSalesPosts(departments);
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(items => {
        departments = items;
        this.supplyAndSalesPosts = this.getAllSupplyAndSalesPosts(departments);
      });
    }
  }

  private getAllSupplyAndSalesPosts(departments: Department[]): Post[] {
    if (!departments || departments.length == 0)
      return [];

    const supplyAndSalesDepartments: Department[] = departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Sales || dep.departmentType === DepartmentTypeEnum.Supply);
    const supplyAndSalesOffices: Office[] = supplyAndSalesDepartments.reduce((acc: Office[], dept: Department) => acc.concat(dept.offices), []);
    const supplyAndSalesPosts: Post[] = supplyAndSalesOffices.reduce((acc: Post[], office: Office) => acc.concat(office.posts), []);

    return supplyAndSalesPosts;
  }

  async getItemTypes() {
    if (this.isOwner) {
      await this.itemTypeService.getAllItemTypesAsync().then(items => {
        this.itemTypes = items;
      });
    }
    else {
      await this.itemTypeService.getItemTypesByCustomerId().then(items => {
        this.itemTypes = items;
      });
    }
  }

  getSecondDisplayExpr(item) {
    if (!item) {
      return '';
    }

    return item.code + ' - ' + item.description;
  }

  cellTemplate(container, options) {
    const noBreakSpace = '\u00A0',
      text = (options.value || []).map(element => {
        return options.column.lookup.calculateCellValue(element);
      }).join(', ');
    container.textContent = text || noBreakSpace;
    container.title = text;
  }

  calculateSecondFilterExpression(filterValue, selectedFilterOperation, target) {
    if (target === 'search' && typeof (filterValue) === 'string') {
      return [(this as any).dataField, 'contains', filterValue];
    }
    return function (data) {
      return (data.caenCodeSpecializationIds || []).indexOf(filterValue) !== -1
    }
  }

  async getItems(): Promise<any> {
    this.loaded = true;
    await this.itemService.getItemsByFilter(this.itemSearchFilter).then(async items => {
      if (items) {
        let itemIds = items.map(i => i.id);

        await this.itemService.getItemStocks(itemIds).then(r => {
          if (r) {
            items.forEach(i => {
              if (r.hasOwnProperty(i.id)) {
                i.totalStock = r[i.id];
              } else {
                i.totalStock = 0;
              }
            });
          }
        });

        await this.itemService.getItemTurnovers(itemIds).then(r => {
          if (r) {
            items.forEach(i => {
              if (r.hasOwnProperty(i.id)) {
                let value = r[i.id];

                const digitsRule = {
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0
                };
                const locale: string = 'en-US';
                i.turnOversRs =
                  Math.round(value.rulajReal3M).toLocaleString(locale, digitsRule) + ' / ' +
                  Math.round(value.rulajReal6M).toLocaleString(locale, digitsRule) + ' / ' +
                  Math.round(value.rulajReal9M).toLocaleString(locale, digitsRule) + ' / ' +
                  Math.round(value.rulajReal12M).toLocaleString(locale, digitsRule);
              }
            });
          }
        });

        this.items = items;

        this.loaded = false;
      }
    });
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.item).then(result => {
      this.actions = result;
    });
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  canDelete(rowSiteId) {
    if (rowSiteId && rowSiteId !== Number(this.authenticationService.getUserSiteId())) {
      return false;
    }
    return this.actions.CanDelete;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public addItem() {
    this.dataGrid.instance.addRow();
  }

  public refreshDataGrid() {
    this.loadData();
    if (Object.keys(this.itemSearchFilter).length > 2) {
      this.getItems();
    }
    if (this.itemSearchFilter && Object.keys(this.itemSearchFilter).length > 2) {
      this.dataGrid.instance.refresh();
    }
  }

  getDisplayExprMU(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  public deleteRecords(data: any) {
    this.selectedRows.forEach(item => {
      this.itemService.deleteItemAsync(this.selectedRows.map(x => x.id)).then(response => {
        if (response) {
          this.notificationService.alert('top', 'center', 'Produse - Datele au fost sterse cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'produse - Datele nu au fost sterse!',
            NotificationTypeEnum.Red, true)
        }
      }).then(x => { this.refreshDataGrid(); });
    });
  }

  changeVisible() {
    this.detailsVisibility = false;
    this.refreshDataGrid();
  }

  public async openDetails(row: any) {
    this.loaded = true;
    this.selectedItem = row;
    setTimeout(async () => {
      await this.itemDetails.loadItemData().then(function () {
        this.detailsVisibility = true;
        this.loaded = false;
      }.bind(this));
    }, 0);
  }

  customCallbackSort(e): number {
    if (e && e.turnOversRs) {
      const inputString = e.turnOversRs;
      const match = inputString.match(/^([^\/\s]+)\s*\/.*$/);
      if (match && match[1]) {
        const result = parseInt(match[1].replace(/,/g, ''));
        return result;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowsLenght = data.selectedRowsData.length;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType === 'filterRow' && event.editorName === 'dxSelectBox') {
      event.editorName = 'dxTextBox';
    }
    if (event.parentType === 'dataRow' && (event.dataField === 'shortProductNameRO' || event.dataField === 'shortProductNameENG')) {
      event.editorOptions.maxLength = 50;
    }
  }

  getDisplayExprPartners(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }
}
