import { Component, Input, Output, EventEmitter, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { TranslateService } from '../../services/translate';
import { HelperService } from 'app/services/helper.service';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';

@Component({
  selector: 'app-grid-toolbar',
  templateUrl: './grid-toolbar.component.html',
  styleUrls: ['./grid-toolbar.component.css']
})
export class GridToolbarComponent implements OnInit, AfterViewInit {
  @Input() public actions: IPageActions = {} as any;
  @Input() public dataGrid: DxDataGridComponent;
  @Input() public fileName = 'exportedFile';
  @Input() public infoText = 'No Info';
  @Input() public printTitleName = 'Tabel title';

  @Input() public showColumnSettings: boolean;
  @Input() public showEditMultiple: boolean;
  @Input() public showExcel: boolean;
  @Input() public showInfo: boolean;
  @Input() public showPrint: boolean;
  @Input() rowsSelected: boolean;
  @Input() showExtraButtons: boolean = false;

  @Input() public showSpecialButton: boolean;
  @Input() public specialButtonDescription: string;
  @Input() public specialButtonIcon: string;
  @Input() public specialButtonText: string;

  @Input() public showSecondSpecialButton: boolean;
  @Input() public secondSpecialButtonDescription: string;
  @Input() public secondSpecialButtonIcon: string;
  @Input() public secondSpecialButtonText: string;

  @Output() public addEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() public displayEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() public editMultipleEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() public updateMultipleEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() public deleteEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() public excelEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() public printEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() public refreshEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() public specialEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() public secondSpecialEvent: EventEmitter<any> = new EventEmitter<any>();

  deleteConfirmationPopup: boolean;
  displayData: boolean;
  hideActions: boolean;
  templateVisible: boolean = false;
  selectedRows: number;
  selectMode: any;
  selectAllModes = [];
  themeColor: string = 'btn-success';
  isPopupVisible: boolean = false;

  constructor(private helperService: HelperService, private translateService: TranslateService, private authService: AuthService) {
    this.selectAllModes = [{ 'key': 'allPages', 'value': this.translateService.instant('grid.allPages') },
    { 'key': 'page', 'value': this.translateService.instant('grid.currentPage') }];

    if (this.authService.getCustomerGridToolbarTheme()) {
      this.themeColor = 'btn-' + this.authService.getCustomerGridToolbarTheme();
    }
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.setGridInstance();
  }

  toggleTemplate() {
    this.templateVisible = !this.templateVisible;
    setTimeout(async () => {
      if (document.getElementById("tooltipInfo")) {
        document.getElementById("tooltipInfo").innerHTML = this.infoText;
      }
    }, 0);

  }

  setGridInstance() {
    if (this.dataGrid) {
      if (this.dataGrid.columnChooser) {
        this.dataGrid.columnChooser.mode = 'select';
        this.dataGrid.columnChooser.allowSearch = true;
        this.dataGrid.columnChooser.title = 'Coloane tabel';
        this.dataGrid.columnChooser.enabled = false;
      }
      this.dataGrid.onSelectionChanged.subscribe(event => this.onSelectionChangedDataGridRows(event));
    }
  }

  onSelectionChangedDataGridRows(event) {
    this.rowsSelected = event.selectedRowsData.length > 0;
    this.selectedRows = event.selectedRowsData.length;

    if (event.selectedRowsData.map(x => x.siteId).filter(y => y !== undefined && y !== Number(this.authService.getUserSiteId())).length > 0) {
      this.rowsSelected = false;
    }
  }

  onSelectionModeChanged(event) {
    this.dataGrid.selection.selectAllMode = this.selectMode.key;
  }

  public add(): void {
    this.addEvent.emit();
  }

  public display(): void {
    this.displayEvent.emit(this.displayData);
  }

  public editMultiple(): void {
    this.editMultipleEvent.emit();
  }

  public updateMultiple(): void {
    this.updateMultipleEvent.emit();
  }

  public refresh(): void {
    this.refreshEvent.emit();
  }

  public specialEventClick(): void {
    this.specialEvent.emit();
  }

  public secondSpecialEventClick(): void {
    this.secondSpecialEvent.emit();
  }

  public hideColumn(e) {
    if (this.dataGrid) {
      this.dataGrid.instance.columnOption('Actiuni', 'visible', !e.value);
    }
  }

  public displayDeleteConfirmation(): void {
    this.deleteConfirmationPopup = true;
  }

  public delete(option: boolean) {
    if (option) {
      this.deleteEvent.emit();
    }
    this.deleteConfirmationPopup = false;
  }

  infoClick() {
    this.isPopupVisible = !this.isPopupVisible;
  }

  onShowing() {
    document.getElementById('popupId').innerHTML = this.infoText;
  }
}
