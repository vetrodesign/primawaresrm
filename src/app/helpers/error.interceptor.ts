import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthService } from '../services/auth.service';
import { Constants } from '../constants';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { NotificationService } from 'app/services/notification.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    coreRoutesNames = Constants;
    constructor(private authenticationService: AuthService, private router: Router, private notificationService: NotificationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err && err.error && err.error.errorMessage) {
                this.notificationService.alert('top', 'center', err.error.errorMessage,
                NotificationTypeEnum.Red, true);
            } else {
                if (err && err.error && (err.status == 400 || err.status == 412)) {
                    this.notificationService.alert('top', 'center', err.error,
                    NotificationTypeEnum.Red, true);
                }
            }  
            if (err.status === 401 || !this.authenticationService.currentUserValue) {
                 window.location.href = environment.SSOPrimaware;
            }
            const error = (err && err.error) ? (err.error.message || err.statusText) : err;
            if (err?.error?.message != undefined) {
                return throwError(err.error.message);
            }
            if (err?.error?.text != undefined) {
                return throwError(err.error.text);
            }
            if (err?.message != undefined) {
                return throwError(err.message);
            }
            if (err?.error != undefined) {
                return throwError(err.error);
            }
        
            return throwError(error);
        }))
    }
}