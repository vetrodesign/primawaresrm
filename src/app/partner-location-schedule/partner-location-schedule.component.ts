import { Component, Input, OnInit } from '@angular/core';
import { DaysOfWeek } from 'app/enums/dayOfTheWeekEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { PartnerLocationSchedule } from 'app/models/partnerlocationschedule.model';
import { NotificationService } from 'app/services/notification.service';
import { PartnerLocationScheduleService } from 'app/services/partner-location-schedule.service';
import { TranslateService } from 'app/services/translate';
import * as moment from 'moment';

@Component({
  selector: 'app-partner-location-schedule',
  templateUrl: './partner-location-schedule.component.html',
  styleUrls: ['./partner-location-schedule.component.css']
})
export class PartnerLocationScheduleComponent implements OnInit {
  @Input() customerId: string;
  items: PartnerLocationSchedule[] = [];
  schedules: PartnerLocationSchedule[] = [];
  defaultSchedule: PartnerLocationSchedule[] = [];

  loaded: Boolean;
  constructor(private partnerLocationScheduleService: PartnerLocationScheduleService, private translationService: TranslateService,
    private notificationService: NotificationService,) { }

  ngOnInit(): void { 
  }

  getDefaultSchedule() {
    this.defaultSchedule = [];
    for (let n in DaysOfWeek) {
      if (typeof DaysOfWeek[n] === 'number') {
        let day = new PartnerLocationSchedule();
        day.customerId = Number(this.customerId);
        day.dayOfTheWeek = <any>DaysOfWeek[n];
        day.dayOfTheWeekName = this.translationService.instant(n);
        day.isClose = false;
        day.startTime = new Date(); day.startTime.setHours(8, 0, 0, 0);
        day.endTime = new Date(); day.endTime.setHours(18, 30, 0, 0);
        if (day.dayOfTheWeek == 6 || day.dayOfTheWeek == 7) {
          day.isClose = true;
        }
        this.defaultSchedule.push(day);
      }
    }
  }

  async getPartnerLocationSchedule(partnerLocationId: number) {
    this.getDefaultSchedule();
    if (partnerLocationId) {
      this.items = [];
      await this.partnerLocationScheduleService.getItemByPartnerLocationIdAsync(partnerLocationId).then(partnerLocationSchedules => {
        this.schedules = (partnerLocationSchedules && partnerLocationSchedules.length > 0) ? partnerLocationSchedules : [];
        this.defaultSchedule.forEach(defaultItem => {
          defaultItem.partnerLocationId = partnerLocationId;
          let findItem = this.schedules.find(x => x.dayOfTheWeek == defaultItem.dayOfTheWeek);
          if (findItem) {
            findItem.dayOfTheWeekName = this.translationService.instant(DaysOfWeek[defaultItem.dayOfTheWeek]);
            this.items.push(findItem);
          } else {
            this.items.push(defaultItem);
          }
        });
      });
    }
  }

  async saveData() {
    this.loaded = true;
    this.items.forEach(async j => {
      let i = JSON.parse(JSON.stringify(j));
      if (i.id) {
        //update
        i.startTime = moment(i.startTime).add(3, 'hours').toDate();
        i.endTime = moment(i.endTime).add(3, 'hours').toDate();
        await this.partnerLocationScheduleService.update(i).then(response => {
          if (response) {
            this.notificationService.alert('top', 'center', 'Orar Locatie Partener - Datele au fost actualizate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Orar Locatie Partener - Datele nu au fost actualizate!', NotificationTypeEnum.Red, true)
          }
          this.loaded = false;
        });
      } else {
        //create
        i.startTime = moment(i.startTime).add(3, 'hours').toDate();
        i.endTime = moment(i.endTime).add(3, 'hours').toDate();
        await this.partnerLocationScheduleService.create(i).then(response => {
          if (response) {
            this.notificationService.alert('top', 'center', 'Orar Locatie Partener - Datele au fost actualizate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Orar Locatie Partener - Datele nu au fost actualizate!', NotificationTypeEnum.Red, true)
          };
          this.loaded = false;
        });
      }
    })
  }
}
