import { Injectable } from '@angular/core';
import { ItemType } from 'app/models/itemtype.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemTypeService {
  constructor(private apiService: ApiService) { }

  async getAllItemTypesAsync(): Promise<ItemType[]> {
    const apiURL = `/itemtype/getall`;
    return await this.apiService.GetAsync<ItemType[]>(apiURL, Constants.mrkApi);
  }

  async getItemTypesByCustomerId(): Promise<ItemType[]> {
    const apiURL = `/itemtype/getbycustomerid/`;
    return await this.apiService.GetAsync<ItemType[]>(apiURL, Constants.mrkApi);
  }

  async createItemTypeAsync(itemType: ItemType): Promise<any> {
    const apiURL = `/itemtype/create`;
    return await this.apiService.PostAsync<any>(apiURL, itemType, Constants.mrkApi);
  }

  async updateItemTypeAsync(itemType: ItemType): Promise<any> {
    const apiURL = `/itemtype/update`;
    return await this.apiService.PutAsync<any>(apiURL, itemType, Constants.mrkApi);
  }

  async deleteItemTypesAsync(ids: number[]): Promise<any> {
    const apiURL = `/itemtype/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
