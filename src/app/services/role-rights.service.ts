import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Role } from '../models/role.model';
import { RoleRights } from 'app/models/rolemenuitemactions.model';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class RoleRightsService {

  constructor(private apiService: ApiService) { }

  async getAllRolesRights(): Promise<any[]> {
    const apiURL = `/RoleRights/GetRoleRights`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.ssoApi);
  }

  async getAllRolesRightsByRoleAndModuleAsync(roleId, clientModuleId): Promise<any[]> {
    const apiURL = `/RoleRights/GetByRoleAndClientModule/` + roleId + `/` + clientModuleId;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.ssoApi);
  }

  async createRoleRightAsync(roleRights: RoleRights): Promise<any> {
    const apiURL = `/RoleRights`;
    return await this.apiService.PostAsync<any>(apiURL, roleRights, Constants.ssoApi);
  }

  async updateRoleRightAsync(roleRights: RoleRights): Promise<any> {
    const apiURL = `/RoleRights/` + roleRights.id;
    return await this.apiService.PutAsync<any>(apiURL, roleRights, Constants.ssoApi);
  }

  async deleteRoleRightAsync(id: number): Promise<any> {
    const apiURL = `/RoleRights/` + id;
    return await this.apiService.DeleteSingleAsync<any>(apiURL, Constants.ssoApi);
  }
}