import { TestBed } from '@angular/core/testing';

import { PartnerLocationScheduleService } from './partner-location-schedule.service';

describe('PartnerLocationScheduleService', () => {
  let service: PartnerLocationScheduleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerLocationScheduleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
