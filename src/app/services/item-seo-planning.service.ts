import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Constants } from "app/constants";

@Injectable({
    providedIn: 'root'
  })
  export class ItemSeoPlanningService {
  
    constructor(private apiService: ApiService) { }
  
    async getByIdsAsync(itemIds: number[]): Promise<any[]> {
      const apiURL = `/itemseoplanning/GetByIds`;
      return await this.apiService.PutAsync<any[]>(apiURL, itemIds, Constants.mrkApi);
    }
  }
  