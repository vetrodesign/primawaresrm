import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { ItemMonitoringPartnerModel } from 'app/models/item-monitoring-partner.model';

@Injectable({
  providedIn: 'root'
})
export class ItemMonitoringPartnerService {
  constructor(private apiService: ApiService) { }

  async getAllActiveAsync(): Promise<ItemMonitoringPartnerModel[]> {
    const apiURL = `/itemmonitoringpartner/getallactive`;
    return await this.apiService.GetAsync<ItemMonitoringPartnerModel[]>(apiURL, Constants.mrkApi);
  }

  async getByIdAsync(id: number): Promise<ItemMonitoringPartnerModel> {
    const apiURL = `/itemmonitoringpartner/getById/` + id;
    return await this.apiService.GetAsync<ItemMonitoringPartnerModel>(apiURL, Constants.mrkApi);
  }

  async createAsync(model: ItemMonitoringPartnerModel): Promise<any> {
    const apiURL = `/itemmonitoringpartner/create`;
    return await this.apiService.PostAsync<any>(apiURL, model, Constants.mrkApi);
  }
}
