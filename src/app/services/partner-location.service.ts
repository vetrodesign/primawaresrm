import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { environment } from 'environments/environment';
import { HelperService } from './helper.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerLocationService {
  constructor(private apiService: ApiService, private helperService: HelperService) { }

  async getAllPartnerLocationsAsync(): Promise<PartnerLocation[]> {
    const apiURL = `/partnerlocation`;
    return await this.apiService.GetAsync<PartnerLocation[]>(apiURL, Constants.mrkApi);
  }

  async getPartnerLocationsByID(id): Promise<PartnerLocation> {
    const apiURL = `/partnerlocation/getbyid/` + id;
    return await this.apiService.GetAsync<PartnerLocation>(apiURL, Constants.mrkApi);
  }

  async getPartnerLocationsByPartnerID(partnerId): Promise<PartnerLocation[]> {
    const apiURL = `/partnerlocation/getbypartnerid/` + partnerId;
    return await this.apiService.GetAsync<PartnerLocation[]>(apiURL, Constants.mrkApi);
  }

  async getPartnerLocationsIncludingInactiveByPartnerID(partnerId): Promise<PartnerLocation[]> {
    const apiURL = `/partnerlocation/GetByPartnerIdIncludingInactive/` + partnerId;
    return await this.apiService.GetAsync<PartnerLocation[]>(apiURL, Constants.mrkApi);
  }

  async getPartnerLocatonsByPartnerIds(ids: number[]): Promise<PartnerLocation[]> {
    const apiURL = `/partnerlocation/getbypartnerids`;
    return await this.apiService.PostAsync<PartnerLocation[]>(apiURL, ids, Constants.mrkApi);
  }


  async createPartnerLocationAsync(partnerLocation: PartnerLocation): Promise<any> {
    partnerLocation = await this.helperService.trimObject(partnerLocation);
    const apiURL = `/partnerlocation/create`;
    return await this.apiService.PostAsync<any>(apiURL, partnerLocation, Constants.mrkApi);
  }

  async activatePartnerLocationAsync(locationId: number): Promise<any> {
    const apiURL = `/partnerlocation/activate`;
    return await this.apiService.PutAsync<any>(apiURL, locationId, Constants.mrkApi);
  }

  async updatePartnerLocationAsync(partnerLocation: PartnerLocation): Promise<any> {
    partnerLocation = await this.helperService.trimObject(partnerLocation);
    const apiURL = `/partnerlocation/update`;
    return await this.apiService.PutAsync<any>(apiURL, partnerLocation, Constants.mrkApi);
  }

  async updatePartnerLocationPartnerActivityAllocationAsync(partnerLocation: PartnerLocation): Promise<any> {
    partnerLocation = await this.helperService.trimObject(partnerLocation);
    const apiURL = `/partnerlocation/UpdatePartnerActivityAllocation`;
    return await this.apiService.PutAsync<any>(apiURL, partnerLocation, Constants.mrkApi);
  }

  async deletePartnerLocationAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnerlocation/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
