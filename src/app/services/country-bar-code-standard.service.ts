import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CountryBarCodeStandardService {

  constructor(private apiService: ApiService) { }

  async getAllCountryBarCodeStandardsAsync(): Promise<any[]> {
    const apiURL = `/countrybarcodestandard/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.commonApi);
  }

  async createCountryBarCodeStandardsAsync(item): Promise<any[]> {
    const apiURL = `/countrybarcodestandard/create`;
    return await this.apiService.PostAsync<any[]>(apiURL, item, Constants.commonApi);
  }

  async updateCountryBarCodeStandardsAsync(item): Promise<any[]> {
    const apiURL = `/countrybarcodestandard/update`;
    return await this.apiService.PutAsync<any[]>(apiURL, item, Constants.commonApi);
  }

  async deleteCountryBarCodeStandardsAsync(m: Number[]): Promise<any> {
    const apiURL = `/countrybarcodestandard/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, m, Constants.commonApi);
  }
}