import { Injectable } from '@angular/core';
import { PartnerLocationXItemGroupCategory } from 'app/models/partnerlocationxitemgroupcategory.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerLocationItemGroupCategoryService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/partnerLocationXItemGroupCategory/GetAll`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async getAllPartnerLocationItemGroupCategoryByPartnerLocationAsync(partnerLocationId: number): Promise<any[]> {
    const apiURL = `/partnerLocationXItemGroupCategory/GetAllByPartnerLocationId`;
    return await this.apiService.PostAsync<any>(apiURL, partnerLocationId, Constants.mrkApi);
  }

  async getAllPartnerLocationItemGroupCategoryByPartnerAsync(partnerId: number): Promise<any[]> {
    const apiURL = `/partnerLocationXItemGroupCategory/GetAllByPartnerId`;
    return await this.apiService.PostAsync<any>(apiURL, partnerId, Constants.mrkApi);
  }

  async createMultiplePartnerLocationItemGroupCategoryAsync(i: PartnerLocationXItemGroupCategory[]): Promise<any> {
    const apiURL = `/partnerLocationXItemGroupCategory/CreateMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async deletePartnerLocationItemGroupCategoryAsync(i: PartnerLocationXItemGroupCategory[]): Promise<any> {
    const apiURL = `/partnerLocationXItemGroupCategory/DeleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }
}
