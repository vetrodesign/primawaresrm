import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { GeneralOffer} from 'app/models/generaloffer.model';
import { ItemGroup } from 'app/models/itemgroup.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class GeneralOfferService {
  constructor(private apiService: ApiService) { }

  async UpdateByItemGroupIdAsync(itemGroupId: ItemGroup): Promise<any[]> {
    const apiURL = `/generalOffer/UpdateByItemGroupId/`;
    return await this.apiService.PostAsync<any[]>(apiURL, itemGroupId, Constants.mrkApi);
  }
}
