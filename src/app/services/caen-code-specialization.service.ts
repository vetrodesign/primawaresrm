import { Injectable } from '@angular/core';
import { CaenCodeSpecialization } from 'app/models/caencodespecialization.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CaenCodeSpecializationService {
  constructor(private apiService: ApiService) { }

  async getAllCaenCodesSpecializationsAsync(): Promise<CaenCodeSpecialization[]> {
    const apiURL = `/CaenCodeSpecialization/getall`;
    return await this.apiService.GetAsync<CaenCodeSpecialization[]>(apiURL, Constants.commonApi);
  }

  async getByItemIdAsync(itemId: number): Promise<any[]> {
    const apiURL = `/CaenCodeSpecialization/getByItemId`;
    return await this.apiService.PostAsync<any[]>(apiURL, itemId, Constants.commonApi);
  }

  async createCaenCodeSpecializationAsync(caenCode: CaenCodeSpecialization): Promise<any> {
    const apiURL = `/CaenCodeSpecialization/create`;
    return await this.apiService.PostAsync<any>(apiURL, caenCode, Constants.commonApi);
  }

  async updateCaenCodeSpecializationAsync(caenCode: CaenCodeSpecialization): Promise<any> {
    const apiURL = `/CaenCodeSpecialization/update`;
    return await this.apiService.PutAsync<any>(apiURL, caenCode, Constants.commonApi);
  }

  async deleteCaenCodeSpecializationAsync(ids: number[]): Promise<any> {
    const apiURL = `/CaenCodeSpecialization/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
