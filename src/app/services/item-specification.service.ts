import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { ItemSpecification } from "app/models/item-specification.model";
import { Constants } from "app/constants";

@Injectable({
    providedIn: 'root'
})

export class ItemSpecificationService {
    constructor(private apiService: ApiService) { }

    async getByIdAsync(id: number): Promise<any[]> {
        const apiURL = `/itemspecification/getbyid?id=${id}`;
        return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
    }

    async createAsync(itemSpecification: ItemSpecification): Promise<any> {
        const apiURL = `/itemspecification/create`;
        return await this.apiService.PostAsync<any>(apiURL, itemSpecification, Constants.mrkApi);
    }

    async deleteAsync(ids: number[]): Promise<any> {
        const apiURL = `/itemspecification/delete`;
        return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
    }
}

