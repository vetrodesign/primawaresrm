import { Injectable } from '@angular/core';
import { PartnerActivity } from 'app/models/partneractivity.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerActivityService {
  constructor(private apiService: ApiService) { }

  async getAllPartnerActivityAsync(): Promise<PartnerActivity[]> {
    const apiURL = `/partneractivity/getall`;
    return await this.apiService.GetAsync<PartnerActivity[]>(apiURL, Constants.mrkApi);
  }

  async createPartnerActivityAsync(p: PartnerActivity): Promise<any> {
    const apiURL = `/partneractivity/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updatePartnerActivityAsync(p: PartnerActivity): Promise<any> {
    const apiURL = `/partneractivity/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deletePartnerActivityAsync(ids: number[]): Promise<any> {
    const apiURL = `/partneractivity/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
