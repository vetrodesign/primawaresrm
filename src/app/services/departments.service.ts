import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Customer } from '../models/customer.model';
import { Office } from 'app/models/office.model';
import { Department } from 'app/models/department.model';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class DepartmentsService {

  constructor(private apiService: ApiService) { }

  async getAllDepartmentsAsync(): Promise<Department[]> {
    const apiURL = `/department/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.ssoApi);
  }

  async getDepartmentsAsyncByID(): Promise<Department[]> {
    const apiURL = `/department/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.ssoApi);
  }

  async createDepartmentAsync(department: Department): Promise<any> {
    const apiURL = `/department/create`;
    return await this.apiService.PostAsync<any>(apiURL, department, Constants.ssoApi);
  }

  async updateDepartmentAsync(department: Department): Promise<any> {
    const apiURL = `/department/update` + department.id;
    return await this.apiService.PutAsync<any>(apiURL, department, Constants.ssoApi);
  }

  async deleteDepartmentAsync(id: number): Promise<any> {
    const apiURL = `/department/delete` + id;
    return await this.apiService.DeleteSingleAsync<any>(apiURL, Constants.ssoApi);
  }

  async getByDepartmentIdAsync(departmentId: number): Promise<Department> {
    const apiURL = '/department/getbydepartmentid';
    return await this.apiService.PostAsync<any>(apiURL, departmentId, Constants.ssoApi);
  }
}
