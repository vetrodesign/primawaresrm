import { Injectable } from '@angular/core';
import { Material } from 'app/models/material.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  constructor(private apiService: ApiService) { }

  async getAllMaterialsAsync(): Promise<Material[]> {
    const apiURL = `/material/getAll`;
    return await this.apiService.GetAsync<Material[]>(apiURL, Constants.commonApi);
  }

  async getAllMaterialsByCustomerIdAsync(): Promise<Material[]> {
    const apiURL = `/material/getAllByCustomer`;
    return await this.apiService.GetAsync<Material[]>(apiURL, Constants.commonApi);
  }

  async createMaterialAsync(material: Material): Promise<any> {
    const apiURL = `/material/create`;
    return await this.apiService.PostAsync<any>(apiURL, material, Constants.commonApi);
  }

  async updateMaterialAsync(material: Material): Promise<any> {
    const apiURL = `/material/update`;
    return await this.apiService.PutAsync<any>(apiURL, material, Constants.commonApi);
  }

  async deleteMaterialsAsync(ids: number[]): Promise<any> {
    const apiURL = `/material/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
