import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Post } from 'app/models/post.model';
import { environment } from 'environments/environment';
import { PostDetail } from 'app/models/postDetail.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private apiService: ApiService) { }

  async getAllPostsAsync(): Promise<Post[]> {
    const apiURL = `/post/getAll`;
    return await this.apiService.GetAsync<Post[]>(apiURL, Constants.ssoApi);
  }

  async getPostsByCustomerId(): Promise<Post[]> {
    const apiURL = `/post/getByCustomerId`;
    return await this.apiService.GetAsync<Post[]>(apiURL, Constants.ssoApi);
  }

  async createPostAsync(post: Post): Promise<any> {
    const apiURL = `/post/create`;
    return await this.apiService.PostAsync<any>(apiURL, post, Constants.ssoApi);
  }

  async updatePostAsync(post: Post): Promise<any> {
    const apiURL = `/post/update`;
    return await this.apiService.PutAsync<any>(apiURL, post, Constants.ssoApi);
  }

  async deletePostAsync(ids: number[]): Promise<any> {
    const apiURL = `/post/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.ssoApi);
  }

  async getPostDetailAsync(postId: number): Promise<PostDetail> {
    const apiURL = `/post/GetPostDetail`;
    return await this.apiService.PostAsync<any>(apiURL, postId, Constants.ssoApi);
  }
}
