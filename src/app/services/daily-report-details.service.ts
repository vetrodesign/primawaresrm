import { Injectable } from '@angular/core';
import { DailyReportDetails } from 'app/models/daily-report-details.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class DailyReportDetailsService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/dailyReportDetails/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async GetByDailyReportAsync(dailyReportId: number): Promise<any> {
    const apiURL = `/dailyReportDetails/GetByDailyReport`;
    return await this.apiService.PostAsync<any>(apiURL, dailyReportId, Constants.mrkApi);
  }

  async createAsync(dr: DailyReportDetails): Promise<any> {
    const apiURL = `/dailyReportDetails/create`;
    return await this.apiService.PostAsync<any>(apiURL, dr, Constants.mrkApi);
  }

  async updateAsync(dr: DailyReportDetails): Promise<any> {
    const apiURL = `/dailyReportDetails/update`;
    return await this.apiService.PutAsync<any>(apiURL, dr, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/dailyReportDetails/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
