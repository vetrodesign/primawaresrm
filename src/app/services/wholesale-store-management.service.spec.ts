import { TestBed } from '@angular/core/testing';

import { WholesaleStoreManagementService } from './wholesale-store-management.service';

describe('WholesaleStoreManagementService', () => {
  let service: WholesaleStoreManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WholesaleStoreManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
