import { Injectable } from '@angular/core';
import { PartnerActivityAllocation } from 'app/models/partneractivityallocation.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerActivityAllocationService {
  constructor(private apiService: ApiService) { }

  async getAllPartnerActivityAllocationAsync(): Promise<PartnerActivityAllocation[]> {
    const apiURL = `/partneractivityallocation/getall`;
    return await this.apiService.GetAsync<PartnerActivityAllocation[]>(apiURL, Constants.mrkApi);
  }

  async createPartnerActivityAllocationAsync(p: PartnerActivityAllocation): Promise<any> {
    const apiURL = `/partneractivityallocation/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updatePartnerActivityAllocationAsync(p: PartnerActivityAllocation): Promise<any> {
    const apiURL = `/partneractivityallocation/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deletePartnerActivityAllocationAsync(ids: number[]): Promise<any> {
    const apiURL = `/partneractivityallocation/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
