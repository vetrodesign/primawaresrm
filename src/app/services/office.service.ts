import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Office } from 'app/models/office.model';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class OfficeService {

  constructor(private apiService: ApiService) { }

  async getAllOfficesAsync(): Promise<Office[]> {
    const apiURL = `/office`;
    return await this.apiService.GetAsync<Office[]>(apiURL, Constants.ssoApi);
  }

  async getOfficeAsyncByID(customerId): Promise<Office[]> {
    const apiURL = `/office/GetByCustomer/` + customerId;
    return await this.apiService.GetAsync<Office[]>(apiURL, Constants.ssoApi);
  }

  async createOfficeAsync(office: Office): Promise<any> {
    const apiURL = `/office`;
    return await this.apiService.PostAsync<any>(apiURL, office, Constants.ssoApi);
  }

  async updateOfficeAsync(office: Office): Promise<any> {
    const apiURL = `/office/` + office.id;
    return await this.apiService.PutAsync<any>(apiURL, office, Constants.ssoApi);
  }

  async deleteOfficerAsync(id: number): Promise<any> {
    const apiURL = `/office/` + id;
    return await this.apiService.DeleteSingleAsync<any>(apiURL, Constants.ssoApi);
  }

  async getOfficeByOfficeIdAsync(officeId: number): Promise<Office> {
    const apiUrl = '/office/getbyofficeid';
    return await this.apiService.PostAsync<Office>(apiUrl, officeId, Constants.ssoApi);
  }
}
