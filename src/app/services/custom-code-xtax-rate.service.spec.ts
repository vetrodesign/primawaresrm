import { TestBed } from '@angular/core/testing';

import { CustomCodeXtaxRateService } from './custom-code-xtax-rate.service';

describe('CustomCodeXtaxRateService', () => {
  let service: CustomCodeXtaxRateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomCodeXtaxRateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
