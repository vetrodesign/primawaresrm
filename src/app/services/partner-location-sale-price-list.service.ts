import { Injectable } from '@angular/core';
import { PartnerLocationXSalePriceList, PartnerLocationXSalePriceListUpdateMultipleVM } from 'app/models/partnerLocationXSalePriceList.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerLocationSalePriceListService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/partnerlocationxsalepricelist/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getAllByPartnerAsync(partnerId: number): Promise<any[]> {
    const apiURL = `/partnerlocationxsalepricelist/getallbypartner`;
    return await this.apiService.PostAsync<any>(apiURL, partnerId, Constants.mrkApi);
  }

  async getAllByPartnerLocationAsync(partnerLocationId: number): Promise<any[]> {
    const apiURL = `/partnerlocationxsalepricelist/getallbypartnerlocation`;
    return await this.apiService.PostAsync<any>(apiURL, partnerLocationId, Constants.mrkApi);
  }

  async createAsync(p: PartnerLocationXSalePriceList): Promise<any> {
    const apiURL = `/partnerlocationxsalepricelist/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async createMultipleAsync(p: PartnerLocationXSalePriceList[]): Promise<any> {
    const apiURL = `/partnerlocationxsalepricelist/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updateAsync(p: PartnerLocationXSalePriceList): Promise<any> {
    const apiURL = `/partnerlocationxsalepricelist/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updateMultipleAsync(partnerLocationId: number, createdSalePriceListIds: number[], deletedSalePriceListIds: number[]): Promise<any> {
    let vm = new PartnerLocationXSalePriceListUpdateMultipleVM();
    vm.partnerLocationId = partnerLocationId;
    vm.createdSalePriceListIds = createdSalePriceListIds;
    vm.deletedSalePriceListIds = deletedSalePriceListIds;
    const apiURL = `/partnerlocationxsalepricelist/updateMultiple`;
    return await this.apiService.PutAsync<any>(apiURL, vm, Constants.mrkApi);
  }

  async deleteAsync(p: PartnerLocationXSalePriceList[]): Promise<any> {
    const apiURL = `/partnerlocationxsalepricelist/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }
}
