import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { BarCodeItemAllocation } from 'app/models/barcodeitemallocation.model';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class BarCodeItemAllocationService {

  constructor(private apiService: ApiService) { }

  async getAllBarCodesAllocationsAsync(): Promise<any[]> {
    const apiURL = `/barcodeitemallocation/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getBarCodesAllocationsAsyncByID(): Promise<any[]> {
    const apiURL = `/barcodeitemallocation/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getByItemIdAsync(itemId: number): Promise<any[]> {
    const apiURL = `/barcodeitemallocation/GetAllByItemId`;
    return await this.apiService.PostAsync<any[]>(apiURL, itemId, Constants.mrkApi);
  }

  async createBarCodeAllocationsAsync(barCode: BarCodeItemAllocation): Promise<any> {
    const apiURL = `/barcodeitemallocation/create`;
    return await this.apiService.PostAsync<any>(apiURL, barCode, Constants.mrkApi);
  }

  async deleteBarCodeAllocationAsync(ids: number[]): Promise<any> {
    const apiURL = `/barcodeitemallocation/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
