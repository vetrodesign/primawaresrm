import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HelperService } from './helper.service';
import { PartnerContractXSalePriceList } from 'app/models/partnerContractXSalePriceList.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerContractXSalePriceListService {

  constructor(private apiService: ApiService, private helperService: HelperService) { }

  async getAllPartnerContractXSalePriceListAsync(): Promise<PartnerContractXSalePriceList[]> {
    const apiURL = `/partnerContractXSalePriceList`;
    return await this.apiService.GetAsync<PartnerContractXSalePriceList[]>(apiURL, Constants.mrkApi);
  }

  async getPartnerContractXSalePriceListByID(id): Promise<PartnerContractXSalePriceList[]> {
    const apiURL = `/partnerContractXSalePriceList/getbyid/` + id;
    return await this.apiService.GetAsync<PartnerContractXSalePriceList[]>(apiURL, Constants.mrkApi);
  }

  async getAllPartnerContractXSalePriceListByPartnerId(partnerId: number): Promise<PartnerContractXSalePriceList[]> {
    const apiURL = `/partnerContractXSalePriceList/getAllPartnerContractXSalePriceListByPartnerId/`;
    return await this.apiService.PostAsync<PartnerContractXSalePriceList[]>(apiURL, partnerId, Constants.mrkApi);
  }

  async getPartnerContractXSalePriceListByPartnerContractID(partnerContractId: number): Promise<PartnerContractXSalePriceList[]> {
    const apiURL = `/partnerContractXSalePriceList/getbypartnercontractid/`;
    return await this.apiService.PostAsync<PartnerContractXSalePriceList[]>(apiURL, partnerContractId, Constants.mrkApi);
  }

  async createMultiplePartnerContractXSalePriceListAsync(p: PartnerContractXSalePriceList[]): Promise<string> {
    p = await this.helperService.trimObject(p);
    const apiURL = `/partnerContractXSalePriceList/createmultiple`;
    return await this.apiService.PostAsync<string>(apiURL, p, Constants.mrkApi);
  }

  async deletePartnerContractXSalePriceListAsync(p: PartnerContractXSalePriceList[]): Promise<void> {
    const apiURL = `/partnerContractXSalePriceList/deletemultiple`;
    return await this.apiService.DeleteMultipleAsync<void>(apiURL, p, Constants.mrkApi);
  }}
