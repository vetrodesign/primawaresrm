import { Injectable } from '@angular/core';
import { HelperService } from './helper.service';
import { ApiService } from './api.service';
import { DocumentTemplate } from 'app/models/document-template.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class DocumentTemplateService {

  constructor(private apiService: ApiService, private helperService: HelperService) { }

  public async getByDocumentTypeAsync(documentType: number): Promise<DocumentTemplate[]> {
    const apiURL = `/documenttemplate/getbydocumenttype/` + documentType;
    return await this.apiService.GetAsync<DocumentTemplate[]>(apiURL, Constants.mrkApi);
  }
}
