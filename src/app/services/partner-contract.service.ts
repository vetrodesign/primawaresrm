import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerContract } from 'app/models/partnerContract.model';
import { HelperService } from './helper.service';
import { PartnerContractDownloadPDFRequestModel } from 'app/models/partnercontractdownloadpdfrequestmodel.model';
import { PartnerContractGetDataForPDFRequestModel } from 'app/models/partnercontractgetdataforpdfrequestmodel.model';
import { DownloadPDFResponseModel } from 'app/models/downloadpdfresponsemodel.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerContractService {

  constructor(private apiService: ApiService, private helperService: HelperService) { }

  async getAllPartnerContractsAsync(): Promise<PartnerContract[]> {
    const apiURL = `/partnercontract`;
    return await this.apiService.GetAsync<PartnerContract[]>(apiURL, Constants.mrkApi);
  }

  async getLastOwnerContractNumberAsync(): Promise<number> {
    const apiURL = `/partnercontract/getlastownercontractnumber`;
    return await this.apiService.GetAsync<number>(apiURL, Constants.mrkApi);
  }
  
  async getPartnerContractByID(id): Promise<PartnerContract> {
    const apiURL = `/partnercontract/getbyid/` + id;
    return await this.apiService.GetAsync<PartnerContract>(apiURL, Constants.mrkApi);
  }

  async getPartnerContractsByPartnerID(partnerId: number): Promise<PartnerContract[]> {
    const apiURL = `/partnercontract/getbypartnerid/` + partnerId;
    return await this.apiService.GetAsync<PartnerContract[]>(apiURL, Constants.mrkApi);
  }

  async getPartnerContractsWithSalePriceListNamesByPartnerIdAsync(partnerId: number): Promise<PartnerContract[]> {
    const apiURL = `/partnercontract/getpartnercontractswithsalepricelistnamesbypartnerid/` + partnerId;
    return await this.apiService.GetAsync<PartnerContract[]>(apiURL, Constants.mrkApi);
  }

  async createPartnerContractAsync(partnerContract: PartnerContract): Promise<PartnerContract> {
    partnerContract = await this.helperService.trimObject(partnerContract);
    const apiURL = `/partnercontract/create`;
    return await this.apiService.PostAsync<PartnerContract>(apiURL, partnerContract, Constants.mrkApi);
  }

  async updatePartnerContractAsync(partnerContract: PartnerContract): Promise<PartnerContract> {
    partnerContract = await this.helperService.trimObject(partnerContract);
    const apiURL = `/partnercontract/update`;
    return await this.apiService.PutAsync<PartnerContract>(apiURL, partnerContract, Constants.mrkApi);
  }

  async deletePartnerContractAsync(ids: PartnerContract[]): Promise<any> {
    const apiURL = `/partnercontract/deletepartnercontractandpartnercontractsalepricelists`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
  
  public async downloadPdfAsync(requestModel: PartnerContractDownloadPDFRequestModel): Promise<any> {
    const apiURL = `/PartnerContract/DownloadPdf`;
    return await this.apiService.PostAsync<any>(apiURL, requestModel, Constants.mrkApi);
  }

  public async downloadWordAsync(requestModel: PartnerContractDownloadPDFRequestModel): Promise<any> {
    const apiURL = `/PartnerContract/DownloadWordFile`;
    return await this.apiService.PostAsync<any>(apiURL, requestModel,  Constants.mrkApi);
  }

  public async getDataForPdfAsync(requestModel: PartnerContractGetDataForPDFRequestModel): Promise<DownloadPDFResponseModel> {
    const apiURL = `/PartnerContract/GetDataForPDF`;
    return await this.apiService.PostAsync<DownloadPDFResponseModel>(apiURL, requestModel, Constants.mrkApi);
  }
}
