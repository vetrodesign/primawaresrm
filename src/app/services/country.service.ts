import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Country, SmallCountry } from 'app/models/country.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private apiService: ApiService) { }

  async getAllCountrysAsync(): Promise<Country[]> {
    const apiURL = `/country/getall`;
    return await this.apiService.GetAsync<Country[]>(apiURL, Constants.commonApi);
  }

  async getAllSmallCountriesAsync(): Promise<SmallCountry[]> {
    const apiURL = `/country/getallsmall`;
    return await this.apiService.GetAsync<SmallCountry[]>(apiURL, Constants.commonApi);
  }

  async createCountryAsync(country: Country): Promise<any> {
    const apiURL = `/country/create`;
    return await this.apiService.PostAsync<any>(apiURL, country, Constants.commonApi);
  }

  async updateCountryAsync(country: Country): Promise<any> {
    const apiURL = `/country/update`;
    return await this.apiService.PutAsync<any>(apiURL, country, Constants.commonApi);
  }

  async deleteCountryAsync(ids: number[]): Promise<any> {
    const apiURL = `/country/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
