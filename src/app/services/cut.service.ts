import { Injectable } from '@angular/core';
import { Cut } from 'app/models/cut.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CutService {

  constructor(private apiService: ApiService) { }

  async getAllCutsAsync(): Promise<Cut[]> {
    const apiURL = `/cut/getAll`;
    return await this.apiService.GetAsync<Cut[]>(apiURL, Constants.commonApi);
  }

  async getAllCutsByCustomerIdAsync(): Promise<Cut[]> {
    const apiURL = `/cut/getAllByCustomer`;
    return await this.apiService.GetAsync<Cut[]>(apiURL, Constants.commonApi);
  }

  async createCutAsync(cut: Cut): Promise<any> {
    const apiURL = `/cut/create`;
    return await this.apiService.PostAsync<any>(apiURL, cut, Constants.commonApi);
  }

  async updateCutAsync(cut: Cut): Promise<any> {
    const apiURL = `/cut/update`;
    return await this.apiService.PutAsync<any>(apiURL, cut, Constants.commonApi);
  }

  async deleteCutsAsync(ids: number[]): Promise<any> {
    const apiURL = `/cut/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
