import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { SupplierAccepted } from 'app/models/supplier-accepted.model';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class SupplierAcceptedService {

  constructor(private apiService: ApiService) { }


  async getByPartnerIdAsync(partnerId: number): Promise<SupplierAccepted> {
    const apiURL = `/SupplierAccepted/getByPartnerId`;
    return await this.apiService.PostAsync<SupplierAccepted>(apiURL, partnerId, Constants.mrkApi);
  }

  async createAsync(sa: SupplierAccepted): Promise<any> {
    const apiURL = `/SupplierAccepted/create`;
    return await this.apiService.PostAsync<any>(apiURL, sa, Constants.mrkApi);
  }

  async updateAsync(sa: SupplierAccepted): Promise<any> {
    const apiURL = `/SupplierAccepted/update`;
    return await this.apiService.PutAsync<any>(apiURL, sa, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/SupplierAccepted/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
