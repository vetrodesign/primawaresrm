import { TestBed } from '@angular/core/testing';

import { PartnerClientCreditControlService } from './partner-client-credit-control.service';

describe('PartnerClientCreditControlService', () => {
  let service: PartnerClientCreditControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerClientCreditControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
