import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Continent } from 'app/models/continent.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ContinentService {

  constructor(private apiService: ApiService) { }

  async getAllContinentAsync(): Promise<Continent[]> {
    const apiURL = `/continent/getall`;
    return await this.apiService.GetAsync<Continent[]>(apiURL, Constants.commonApi);
  }

 
}
