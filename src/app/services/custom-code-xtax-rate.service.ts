import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { CustomCodeXTaxRateAntiDumping, CustomCodeXTaxRateCustoms, CustomCodeXTaxRateExcise, CustomCodeXTaxRateVAT } from 'app/models/customcodextaxrate.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CustomCodeXtaxRateService {

  constructor(private apiService: ApiService) { }

  // Custom Taxes
  async getAllCustomCodeXTaxRateCustomsAsync(): Promise<CustomCodeXTaxRateCustoms[]> {
    const apiURL = `/CustomCodeXTaxRateCustoms/getall`;
    return await this.apiService.GetAsync<CustomCodeXTaxRateCustoms[]>(apiURL, Constants.commonApi);
  }

  async createCustomCodeXTaxRateCustomsAsync(c: CustomCodeXTaxRateCustoms): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateCustoms/create`;
    return await this.apiService.PostAsync<any>(apiURL, c, Constants.commonApi);
  }

  async updateCustomCodeXTaxRateCustomsAsync(c: CustomCodeXTaxRateCustoms): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateCustoms/update`;
    return await this.apiService.PutAsync<any>(apiURL, c, Constants.commonApi);
  }

  async deleteCustomCodeXTaxRateCustomsAsync(ids: number[]): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateCustoms/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }

  // VAT Taxes
  async getAllCustomCodeXTaxRateVATAsync(): Promise<CustomCodeXTaxRateVAT[]> {
    const apiURL = `/CustomCodeXTaxRateVAT/getall`;
    return await this.apiService.GetAsync<CustomCodeXTaxRateVAT[]>(apiURL, Constants.commonApi);
  }

  async getCustomCodeXTaxRateByIdVATAsync(id: number): Promise<CustomCodeXTaxRateVAT[]> {
    const apiURL = `/CustomCodeXTaxRateVAT/GetByCustomId`;
    return await this.apiService.PostAsync<CustomCodeXTaxRateVAT[]>(apiURL, id, Constants.commonApi);
  }

  async createCustomCodeXTaxRateVATAsync(c: CustomCodeXTaxRateVAT): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateVAT/create`;
    return await this.apiService.PostAsync<any>(apiURL, c, Constants.commonApi);
  }

  async updateCustomCodeXTaxRateVATAsync(c: CustomCodeXTaxRateVAT): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateVAT/update`;
    return await this.apiService.PutAsync<any>(apiURL, c, Constants.commonApi);
  }

  async deleteCustomCodeXTaxRateVATAsync(ids: number[]): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateVAT/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }

  // Excise Taxes
  async getAllCustomCodeXTaxRateExciseAsync(): Promise<CustomCodeXTaxRateExcise[]> {
    const apiURL = `/CustomCodeXTaxRateExcise/getall`;
    return await this.apiService.GetAsync<CustomCodeXTaxRateExcise[]>(apiURL, Constants.commonApi);
  }

  async createCustomCodeXTaxRateExciseAsync(c: CustomCodeXTaxRateExcise): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateExcise/create`;
    return await this.apiService.PostAsync<any>(apiURL, c, Constants.commonApi);
  }

  async updateCustomCodeXTaxRateExciseAsync(c: CustomCodeXTaxRateExcise): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateExcise/update`;
    return await this.apiService.PutAsync<any>(apiURL, c, Constants.commonApi);
  }

  async deleteCustomCodeXTaxRateExciseAsync(ids: number[]): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateExcise/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }

  // AntiDumping Taxes
  async getAllCustomCodeXTaxRateAntiDumpingAsync(): Promise<CustomCodeXTaxRateAntiDumping[]> {
    const apiURL = `/CustomCodeXTaxRateAntiDumping/getall`;
    return await this.apiService.GetAsync<CustomCodeXTaxRateAntiDumping[]>(apiURL, Constants.commonApi);
  }

  async createCustomCodeXTaxRateAntiDumpingAsync(c: CustomCodeXTaxRateAntiDumping): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateAntiDumping/create`;
    return await this.apiService.PostAsync<any>(apiURL, c, Constants.commonApi);
  }

  async updateCustomCodeXTaxRateAntiDumpingAsync(c: CustomCodeXTaxRateExcise): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateAntiDumping/update`;
    return await this.apiService.PutAsync<any>(apiURL, c, Constants.commonApi);
  }

  async deleteCustomCodeXTaxRateAntiDumpingAsync(ids: number[]): Promise<any> {
    const apiURL = `/CustomCodeXTaxRateAntiDumping/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
