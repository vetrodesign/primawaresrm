import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { County } from 'app/models/county.model';
import { Country } from 'app/models/country.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CountyService {

  constructor(private apiService: ApiService) { }

  async getAllCountysAsync(): Promise<County[]> {
    const apiURL = `/county/getall`;
    return await this.apiService.GetAsync<County[]>(apiURL, Constants.commonApi);
  }

  async getByCountryIdAsync(countryId: number): Promise<County[]> {
    const apiURL = `/county/GetByCountry/`;
    const county = new County();
    county.countryId = countryId;
    return await this.apiService.PostAsync<County[]>(apiURL, county, Constants.commonApi);
  }

  async createCountyAsync(county: County): Promise<any> {
    const apiURL = `/county/create`;
    return await this.apiService.PostAsync<any>(apiURL, county, Constants.commonApi);
  }

  async updateCountyAsync(county: County): Promise<any> {
    const apiURL = `/county/update`;
    return await this.apiService.PutAsync<any>(apiURL, county, Constants.commonApi);
  }

  async deleteCountyAsync(ids: number[]): Promise<any> {
    const apiURL = `/county/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
