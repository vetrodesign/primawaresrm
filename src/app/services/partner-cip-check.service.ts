import { Injectable } from '@angular/core';
import { PartnerCIPCheckVM } from 'app/models/partnercipcheck.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerCipCheckService {
  constructor(private apiService: ApiService) { }

  async getByIdAsync(id: number): Promise<PartnerCIPCheckVM[]> {
    const apiURL = `/partnerCIPcheck/GetById`;
    return await this.apiService.PostAsync<PartnerCIPCheckVM[]>(apiURL, id, Constants.mrkApi);
  }

  async getByPartnerIdAsync(partnerId: number): Promise<any> {
    const apiURL = `/partnerCIPcheck/GetByPartnerId`;
    return await this.apiService.PostAsync<any>(apiURL, partnerId, Constants.mrkApi);
  }

  async create(p: PartnerCIPCheckVM): Promise<any> {
    const apiURL = `/partnerCIPcheck/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async update(p: PartnerCIPCheckVM): Promise<any> {
    const apiURL = `/partnerCIPcheck/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnerCIPcheck/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
