
import { Injectable } from '@angular/core';
import { PartnerERPSyncHistory } from 'app/models/partner-erp-sync-history.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerERPSyncHistoryService {
  constructor(private apiService: ApiService) { }

  async getByPartnerId(partnerId: number): Promise<PartnerERPSyncHistory[]> {
    const apiURL = `/partnerERPSyncHistory/getByPartnerId`;
    return await this.apiService.PostAsync<PartnerERPSyncHistory[]>(apiURL, partnerId, Constants.mrkApi);
  }
}

