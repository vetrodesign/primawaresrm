import { TestBed } from '@angular/core/testing';

import { DeliverytimeService } from './deliverytime.service';

describe('DeliverytimeService', () => {
  let service: DeliverytimeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeliverytimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
