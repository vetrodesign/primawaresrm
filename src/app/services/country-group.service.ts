import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { CountryGroup } from 'app/models/countrygroup.model';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class CountryGroupService {

  constructor(private apiService: ApiService) { }

  async getAllCountryGroupsAsync(): Promise<any[]> {
    const apiURL = `/CountryGroup/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.commonApi);
  }

  async createCountryGroupAsync(country: CountryGroup): Promise<any> {
    const apiURL = `/CountryGroup/create`;
    return await this.apiService.PostAsync<any>(apiURL, country, Constants.commonApi);
  }

  async updateCountryGroupAsync(country: CountryGroup): Promise<any> {
    const apiURL = `/CountryGroup/update`;
    return await this.apiService.PutAsync<any>(apiURL, country, Constants.commonApi);
  }

  async deleteCountryGroupAsync(ids: number[]): Promise<any> {
    const apiURL = `/CountryGroup/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}