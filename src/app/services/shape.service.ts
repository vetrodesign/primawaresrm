import { Injectable } from '@angular/core';
import { Shape } from 'app/models/shape.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ShapeService {

  constructor(private apiService: ApiService) { }

  async getAllShapesAsync(): Promise<Shape[]> {
    const apiURL = `/shape/getAll`;
    return await this.apiService.GetAsync<Shape[]>(apiURL, Constants.commonApi);
  }

  async getAllShapesByCustomerIdAsync(): Promise<Shape[]> {
    const apiURL = `/shape/getAllByCustomer`;
    return await this.apiService.GetAsync<Shape[]>(apiURL, Constants.commonApi);
  }

  async createShapeAsync(shape: Shape): Promise<any> {
    const apiURL = `/shape/create`;
    return await this.apiService.PostAsync<any>(apiURL, shape, Constants.commonApi);
  }

  async updateShapeAsync(shape: Shape): Promise<any> {
    const apiURL = `/shape/update`;
    return await this.apiService.PutAsync<any>(apiURL, shape, Constants.commonApi);
  }

  async deleteShapesAsync(ids: number[]): Promise<any> {
    const apiURL = `/shape/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
