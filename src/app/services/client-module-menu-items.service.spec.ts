import { TestBed } from '@angular/core/testing';

import { ClientModuleMenuItemsService } from './client-module-menu-items.service';

describe('ClientModuleMenuItemsService', () => {
  let service: ClientModuleMenuItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClientModuleMenuItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
