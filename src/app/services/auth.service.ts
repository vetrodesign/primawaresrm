import { Injectable } from '@angular/core';
import { JwtHelper } from 'angular2-jwt';
import * as _ from 'lodash';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ActivatedRoute } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthService {
    public currentUserSubject: BehaviorSubject<any>;
    public snapshot: any;
    public userEmail: any;
    private readonly _localStorageName: string = 'AuthToken';
    private readonly _clientModuleId: string = 'ClientModule';
    private readonly _claimsKey: string = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role';

    clientId: any;
    returnUrl: any;

    constructor(private http: HttpClient, private activatedRoute: ActivatedRoute,
        private jwtHelper: JwtHelper) {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem(this._localStorageName)));
        this.activatedRoute.queryParams.subscribe(params => {
            this.clientId = params['clientId'];
            this.returnUrl = params['returnUrl'];
        });
    }

    public get currentUserValue() {
        return this.getToken();
    }

    public getToken(): string {
        return JSON.parse(localStorage.getItem(this._localStorageName));
    }

    setToken(token: string) {
        localStorage.removeItem(this._localStorageName);
        localStorage.setItem(this._localStorageName, JSON.stringify(token));
        this.currentUserSubject.next(token);
    }

    setClientModuleId(id) {
        if (localStorage.getItem(this._clientModuleId)) {
            localStorage.removeItem(this._clientModuleId);
        }
        localStorage.setItem(this._clientModuleId, JSON.stringify(id));
    }

    public getClientModuleId() {
        return JSON.parse(localStorage.getItem(this._clientModuleId));
    }

    checkQuerryStringParams() {
        console.log(this.clientId, this.returnUrl);
        // if (route.queryParams['clientId'])) {
        //    const href = Constants.crm + (route.queryParams['returnUrl'] ? route.queryParams['returnUrl'] : '');
        //    window.location.href = href;
        // }
    }

    public getUserId(): number {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['sid'];
            if (claims != null) {
                return claims;
            }
            else {
                return null;
            }
        }
        return null;
    }

    public getUserRole(): string[] {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken[this._claimsKey];
            if (claims != null) {
                if (!(claims instanceof Array)) {
                    return [claims];
                }
                return claims;
            } else {
                return null;
            }
        }
        return null;
    }

    public getUserCustomerCountryId(): string {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['customercountryid'];
            if (claims != null) {
                return claims;
            }
            else {
                return null;
            }
        }
        return null;
    }

    public getCustomerPageTitleTheme(): string {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['customerPageTitleTheme'];
            if (claims != null) {
                return 'success';
            }
            else {
                return null;
            }
        }
        return null;
    }

    public getCustomerGridToolbarTheme(): string {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['customerGridToolbarTheme'];
            if (claims != null) {
                return 'success';
            }
            else {
                return null;
            }
        }
        return null;
    }

    public getUserEmail(): string {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['email'];
            if (claims != null) {
                return claims;
            }
            else {
                return null;
            }
        }
        return null;
    }

    public getUserFirstNameAndLastName(): string {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['sub'];
            if (claims != null) {
                return claims;
            }
            else {
                return null;
            }
        }
        return null;
    }

    public getUserCustomerId(): string {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['customerid'];
            if (claims != null) {
                return claims;
            }
            else {
                return null;
            }
        }
        return null;
    }

    public getUserSiteId(): number {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['siteid'];
            if (claims != null) {
                return claims;
            }
            else {
                return null;
            }
        }
        return null;
    }

    public getCustomerERPActive(): boolean {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['customerERPactive'];
            if (claims != null) {
                return claims == 'True';
            }
            else {
                return null;
            }
        }
        return null;
    }

    public getCustomerReferenceCurrencyId(): number {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['referenceCurrencyid'];
            if (claims != null) {
                return claims;
            }
            else {
                return null;
            }
        }
        return null;
    }

    public getUserUserName(): string {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['unique_name'];
            if (claims != null) {
                return claims;
            }
            else {
                return null;
            }
        }
        return null;
    }

    public isUserOwner(): boolean {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['isowner'];
            if (claims != null) {
                return claims == 'True';
            }
            else {
                return null;
            }
        }
        return null;
    }

    public isUserAdmin(): boolean {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['isadmin'];
            if (claims != null) {
                return claims == 'True';
            }
            else {
                return null;
            }
        }
        return null;
    }

    public getUserPostId(): string {
        const authToken = this.getToken();
        if (authToken) {
            const decodedToken = this.jwtHelper.decodeToken(authToken);
            const claims = decodedToken['postId'];
            if (claims != null) {
                return claims;
            }
            else {
                return null;
            }
        }
        return null;
    }

    logout() {
        // remove user from local storage to log user out
        this.userEmail = null;
        localStorage.removeItem(this._localStorageName);
        this.currentUserSubject.next(null);
        window.location.href = environment.SSOPrimaware + '/logout';
    }

    public removeToken(): void {
        localStorage.removeItem(this._localStorageName);
        this.currentUserSubject.next(null);
    }
}
