import { TestBed } from '@angular/core/testing';

import { CaenCodeSectionService } from './caen-code-section.service';

describe('CaenCodeSectionService', () => {
  let service: CaenCodeSectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaenCodeSectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
