import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ItemXClientMonitoringModel } from 'app/models/item-x-client-monitoring.model';
import { HelperService } from './helper.service';
import { ItemMonitoringPartnerModel } from 'app/models/item-monitoring-partner.model';
import { ItemDCRInfo } from 'app/models/itemDCRInfo.model';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { NotificationService } from './notification.service';
import { ItemService } from './item.service';
import { PartnerService } from './partner.service';
import { ItemMonitoringPartnerService } from './item-monitoring-partner.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemXClientMonitoringService {

  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private notificationService: NotificationService,
    private itemService: ItemService,
    private itemMonitoringPartnerService: ItemMonitoringPartnerService,
    private partnerService: PartnerService
    ) { }

  async getAllAsync(): Promise<ItemXClientMonitoringModel[]> {
    const apiURL = `/itemxclientmonitoring/getall`;
    return await this.apiService.GetAsync<ItemXClientMonitoringModel[]>(apiURL, Constants.mrkApi);
  }

  async getByItemIdAsync(itemId: number): Promise<ItemXClientMonitoringModel[]> {
    const apiURL = `/itemxclientmonitoring/getbyitemid`;
    return await this.apiService.PostAsync<ItemXClientMonitoringModel[]>(apiURL, itemId, Constants.mrkApi);
  }

  async getByItemIdsAsyncCalculateDCR(itemIds: number[], itemDCRs: any): Promise<ItemXClientMonitoringModel[]> {
    const apiURL = `/itemxclientmonitoring/getbyitemids`;
    let items = await this.apiService.PostAsync<ItemXClientMonitoringModel[]>(apiURL, itemIds, Constants.mrkApi);

    if (items.length > 0) {

      const roCountryId: number = 180;

      const taxRatesDictionary: { [itemId: number]: number } = {};

      await this.itemService.getTaxRateByItemIdsAsync(itemIds, roCountryId).then(r => {
        if (r && r.length > 0) {
          r.forEach(item => {
            taxRatesDictionary[item.itemId] = item.taxRate;
          });
        }
      });

      if (Object.keys(taxRatesDictionary).length === 0) {
        this.notificationService.alert('top', 'center', 'Monitorizare Produs - Nu exista date despre ratele TVA',
          NotificationTypeEnum.Red, true);

        return;
      }

      let itemMonitoringPartners: ItemMonitoringPartnerModel[] = [];

      await this.itemMonitoringPartnerService.getAllActiveAsync().then(async (imps: ItemMonitoringPartnerModel[]) => {
        if (imps && imps.length > 0) {
          await this.partnerService.getPartnerNamesAndIdsByPartnerIdsAsync(imps.map(x => x.partnerId)).then(partnerInfos => {
            imps.forEach(imp => {
              const partnerInfo = partnerInfos.find(pi => pi.id == imp.partnerId);
    
              if (partnerInfo) {
                imp.partnerName = partnerInfo.name;
              }
            });
    
            itemMonitoringPartners = imps;
          });
        }
      });

      const itemCodeDictionary: { [itemId: number]: string } = {};

      await this.itemService.getCodesForItemIds(itemIds).then(r => {
        if (r && r.length > 0) {
          r.forEach(elem => {
            itemCodeDictionary[elem.id] = elem.code;
          });
        }
      });

      if (Object.keys(itemCodeDictionary).length === 0) {
        this.notificationService.alert('top', 'center', 'Monitorizare Produs - Nu s-au gasit codurile VETRO ale produselor',
          NotificationTypeEnum.Red, true);

        return;
      }

      for(const e of items) {
        let priceWithTVA: number = e.priceWithTVA;
        const priceWithoutTVA: number = e.priceWithoutTVA;
        const conversionFactor: number = e.conversionFactor;
        const itemId: number = e.itemId;
        const itemVATRate: number = taxRatesDictionary[itemId];

        const itemDCR: ItemDCRInfo = itemDCRs[itemId];
        if (!itemDCR || (!itemDCR.d0 || !itemDCR.c0 || !itemDCR.r0)) {
          continue;
        }

        const d0: number = itemDCR.d0;
        const c0: number = itemDCR.c0;
        const r0: number = itemDCR.r0;
        const hasPriceWithTVA: boolean = e.priceWithTVA !== null && e.priceWithTVA !== undefined && e.priceWithTVA !== 0;
        const hasPriceWithoutTVA: boolean = e.priceWithoutTVA !== null && e.priceWithoutTVA !== undefined && e.priceWithoutTVA !== 0;
        
        let partner: ItemMonitoringPartnerModel = itemMonitoringPartners.find(x => x.partnerId == e.partnerId);
        if (partner != undefined) {
          e.partnerName = (partner.partnerName !== '' && partner.partnerName !== null && partner.partnerName !== undefined) ? partner.partnerName : '(lipsa nume)';
        }

        let codeVetro: string = itemCodeDictionary[itemId];
        e.codeVetro = (codeVetro !== '' && codeVetro !== null && codeVetro !== undefined) ? codeVetro : '(lipsa cod)';

        if (hasPriceWithTVA || hasPriceWithoutTVA) {
          if (!hasPriceWithTVA && hasPriceWithoutTVA) {
            // Non-VAT Price only

            // Deducted, based on "TVA Dedus" formula
            priceWithTVA = priceWithoutTVA + (itemVATRate * priceWithoutTVA);
          }

          e.totalPrice = priceWithTVA * conversionFactor;

          e.d0 = this.getDCRPercentFromPriceWithTVA(priceWithTVA, conversionFactor, itemVATRate, d0);
          e.c0 = this.getDCRPercentFromPriceWithTVA(priceWithTVA, conversionFactor, itemVATRate, c0);
          e.r0 = this.getDCRPercentFromPriceWithTVA(priceWithTVA, conversionFactor, itemVATRate, r0);
        }
      }
    }

    return items;
  }

  async createItemXClientMonitoringAsync(itemXClientMonitoring: ItemXClientMonitoringModel): Promise<ItemXClientMonitoringModel> {
    itemXClientMonitoring = await this.helperService.trimObject(itemXClientMonitoring);
    const apiURL = `/itemxclientmonitoring/create`;
    return await this.apiService.PostAsync<ItemXClientMonitoringModel>(apiURL, itemXClientMonitoring, Constants.mrkApi);
  }

  async updateItemXClientMonitoringAsync(itemXClientMonitoring: ItemXClientMonitoringModel): Promise<ItemXClientMonitoringModel> {
    itemXClientMonitoring = await this.helperService.trimObject(itemXClientMonitoring);
    const apiURL = `/itemxclientmonitoring/update`;
    return await this.apiService.PutAsync<ItemXClientMonitoringModel>(apiURL, itemXClientMonitoring, Constants.mrkApi);
  }

  async deleteItemXClientMonitoringListAsync(ids: ItemXClientMonitoringService[]): Promise<any> {
    const apiURL = `/itemxclientmonitoring/DeleteItemXClientMonitoringList`;
    return await this.apiService.PostAsync<any>(apiURL, ids, Constants.mrkApi);
  }

    // Private methods

    private getDCRPercentFromPriceWithTVA(priceWithTVA: number, conversionFactor: number, itemVATRate: number, dcr: number): number {
      // Convertit fara TVA (convertedWithoutTVA):
      //  > (D2*E2)/H2
      //  - (conversionFactor * priceWithTVA)/itemVATRate
      let convertedWithoutTVA: number = (conversionFactor * priceWithTVA) / (itemVATRate / 100);
  
      // DCR% (dcrPercent):
      //  > (T2-J2)/J2
      //  - (dcr - convertedWithoutTVA) / convertedWithoutTVA
      let dcrPercent: number = (dcr - convertedWithoutTVA) / convertedWithoutTVA;
  
      return dcrPercent;
    }
}