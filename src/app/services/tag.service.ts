import { Injectable } from '@angular/core';
import { Tag } from 'app/models/tag.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private apiService: ApiService) { }

  async getAllTagsAsync(): Promise<Tag[]> {
    const apiURL = `/tag/GetAll`;
    return await this.apiService.GetAsync<Tag[]>(apiURL, Constants.commonApi);
  }

  async getAllTagsByCustomerIdAsync(): Promise<Tag[]> {
    const apiURL = `/tag/GetAllByCustomer`;
    return await this.apiService.GetAsync<Tag[]>(apiURL, Constants.commonApi);
  }

  async createTagAsync(tag: Tag): Promise<any> {
    const apiURL = `/tag/create`;
    return await this.apiService.PostAsync<any>(apiURL, tag, Constants.commonApi);
  }

  async updateTagAsync(tag: Tag): Promise<any> {
    const apiURL = `/tag/update`;
    return await this.apiService.PutAsync<any>(apiURL, tag, Constants.commonApi);
  }

  async deleteTagsAsync(ids: number[]): Promise<any> {
    const apiURL = `/tag/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
