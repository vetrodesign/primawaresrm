import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { AuthorizationType } from 'app/models/authorization-type.model';

@Injectable({
  providedIn: 'root'
})

export class AuthorizationTypeService {
  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<AuthorizationType[]> {
    const apiURL = `/authorizationtype/getAll`;
    return await this.apiService.GetAsync<AuthorizationType[]>(apiURL, Constants.mrkApi);
  }

  async getByCustomerIdAsync(): Promise<AuthorizationType[]> {
    const apiURL = `/authorizationtype/getByCustomerId`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async createAsync(ec: AuthorizationType): Promise<any> {
    const apiURL = `/authorizationtype/create`;
    return await this.apiService.PostAsync<any>(apiURL, ec, Constants.mrkApi);
  }

  async updateAsync(ec: AuthorizationType): Promise<any> {
    const apiURL = `/authorizationtype/update`;
    return await this.apiService.PutAsync<any>(apiURL, ec, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/authorizationtype/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
