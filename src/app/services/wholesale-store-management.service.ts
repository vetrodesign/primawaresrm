import { Injectable } from '@angular/core';
import { WholesaleStoreManagement, WholesaleStoreManagementFilter } from 'app/models/wholesale-store-management.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class WholesaleStoreManagementService {

  constructor(private apiService: ApiService) { }

  // async getByIDAsync(id): Promise<any[]> {
  //   const apiURL = `/marketingApi/salepricelistitem/getbysalepricelistid/` + id;
  //   return await this.apiService.GetAsync<any[]>(apiURL);
  // }

  // async getAllAsync(): Promise<WholesaleStoreManagement[]> {
  //   const apiURL = `/marketingApi/salepricelistitem/getall`;
  //   return await this.apiService.GetAsync<WholesaleStoreManagement[]>(apiURL);
  // }

  async getDataByFilterAsync(filter: WholesaleStoreManagementFilter): Promise<any> {
    const apiURL = `/buffer/GetWholesaleStoreManagements`;
    return await this.apiService.PostAsync<any>(apiURL, filter, Constants.mrkApi);
  }

  // async createAsync(w: WholesaleStoreManagement): Promise<any> {
  //   const apiURL = `/marketingApi/salepricelistitem/create`;
  //   return await this.apiService.PostAsync<any>(apiURL, w);
  // }

  // async updateAsync(w: WholesaleStoreManagement): Promise<any> {
  //   const apiURL = `/marketingApi/salepricelistitem/update`;
  //   return await this.apiService.PutAsync<any>(apiURL, w);
  // }

  // async deleteAsync(ids: number[]): Promise<any> {
  //   const apiURL = `/marketingApi/salepricelistitem/delete`;
  //   return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids);
  // }
}
