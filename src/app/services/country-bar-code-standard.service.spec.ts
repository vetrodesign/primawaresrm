import { TestBed } from '@angular/core/testing';

import { CountryBarCodeStandardService } from './country-bar-code-standard.service';

describe('CountryBarCodeStandardService', () => {
  let service: CountryBarCodeStandardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CountryBarCodeStandardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
