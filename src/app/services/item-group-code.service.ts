import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ItemGroupCode } from 'app/models/itemgroupcode.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemGroupCodeService {

  constructor(private apiService: ApiService) { }

  async getAllItemGroupCodesAsync(): Promise<any[]> {
    const apiURL = `/itemgroupcode/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getItemGroupCodesAsyncByID(): Promise<any[]> {
    const apiURL = `/itemgroupcode/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async createItemGroupCodesAsync(itemGroupCode: ItemGroupCode): Promise<any> {
    const apiURL = `/itemgroupcode/create`;
    return await this.apiService.PostAsync<any>(apiURL, itemGroupCode, Constants.mrkApi);
  }

  async updateItemGroupCodesAsync(itemGroupCode: ItemGroupCode): Promise<any> {
    const apiURL = `/itemgroupcode/update`;
    return await this.apiService.PutAsync<any>(apiURL, itemGroupCode, Constants.mrkApi);
  }

  async deleteItemGroupCodesAsync(ids: number[]): Promise<any> {
    const apiURL = `/itemgroupcode/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}