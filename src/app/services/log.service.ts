import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { LogException } from 'app/models/logexception.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private apiService: ApiService) { }

  async getAllLogsAsync(): Promise<LogException[]> {
    const apiURL = `/logexception/getall`;
    return await this.apiService.GetAsync<LogException[]>(apiURL, Constants.ssoApi);
  }
}
