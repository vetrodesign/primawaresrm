import { TestBed } from '@angular/core/testing';

import { SalePriceListItemService } from './sale-price-list-item.service';

describe('SalePriceListItemService', () => {
  let service: SalePriceListItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalePriceListItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
