import { Injectable } from '@angular/core';
import { CharismaPriceList } from 'app/models/charismaPriceList.model';
import { SalePriceList } from 'app/models/salepricelist.model';
import { ApiService } from './api.service';
import { SalePriceListFilterVM } from 'app/models/salepricelistfilter.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SalePriceListService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<SalePriceList[]> {
    const apiURL = `/salepricelist/GetAll`;
    return await this.apiService.GetAsync<SalePriceList[]>(apiURL, Constants.mrkApi);
  }

  async getAllSalePriceListsAsync(): Promise<any[]> {
    const apiURL = `/salepricelist/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getAllByIds(ids: number[]): Promise<SalePriceList[]> {
    const apiURL = `/salepricelist/GetAllByIds`;
    return await this.apiService.PostAsync<SalePriceList[]>(apiURL, ids, Constants.mrkApi)
  }
  
  async getSalePriceListsAsyncByID(): Promise<any[]> {
    const apiURL = `/salepricelist/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getSalePriceListByFilter(filter: SalePriceListFilterVM): Promise<any> {
    const apiURL = `/salepricelist/GetFiltered`;
    return await this.apiService.PostAsync<any>(apiURL, filter, Constants.mrkApi);
  }

  async getAllSalePriceListsWithProductConventionsAsync(): Promise<any[]> {
    const apiURL = `/salepricelist/GetAllWithProductConventions`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getSalePriceListsAsyncByIDWithProductConventions(): Promise<any[]> {
    const apiURL = `/salepricelist/GetByCustomerIdWithProductConventions`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getCharismaPriceListsAsync(): Promise<CharismaPriceList[]> {
    const apiURL = `/salepricelist/GetCharismaPriceLists`;
    return await this.apiService.GetAsync<CharismaPriceList[]>(apiURL, Constants.mrkApi);
  }
  
  async createSalePriceListAsync(saleList: SalePriceList): Promise<any> {
    const apiURL = `/salepricelist/create`;
    return await this.apiService.PostAsync<any>(apiURL, saleList, Constants.mrkApi);
  }

  async updateSalePriceListAsync(saleList: SalePriceList): Promise<any> {
    const apiURL = `/salepricelist/update`;
    return await this.apiService.PutAsync<any>(apiURL, saleList, Constants.mrkApi);
  }

  async deleteSalePriceListsAsync(ids: number[]): Promise<any> {
    const apiURL = `/salepricelist/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }


  
  async getAllByPartnerLocationAsync(partnerLocationId: number): Promise<any[]> {
    const apiURL = `/supplierpricelistxpartnerlocation/getallbypartnerlocation`;
    return await this.apiService.PostAsync<any[]>(apiURL, partnerLocationId, Constants.mrkApi);
  }
}
