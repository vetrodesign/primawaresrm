import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Site } from 'app/models/site.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SiteService {

  constructor(private apiService: ApiService) { }

  async getAllCustomerLocationsAsync(): Promise<Site[]> {
    const apiURL = `/site/getall`;
    return await this.apiService.GetAsync<Site[]>(apiURL, Constants.ssoApi);
  }

  async getCustomerLocationsAsyncByID(): Promise<Site[]> {
    const apiURL = `/site/getbycustomerid`;
    return await this.apiService.GetAsync<Site[]>(apiURL, Constants.ssoApi);
  }
}
