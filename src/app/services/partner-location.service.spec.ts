import { TestBed } from '@angular/core/testing';

import { PartnerLocationService } from './partner-location.service';

describe('PartnerLocationService', () => {
  let service: PartnerLocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerLocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
