import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ItemGroupCpvCode } from 'app/models/itemgroupcpvcode.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemGroupCpvCodeService {

  constructor(private apiService: ApiService) { }

  async getAllItemGroupsCpvCodeAsync(): Promise<any[]> {
    const apiURL = `/itemgroupcpvcode/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async createItemGroupCpvCodeAsync(item: ItemGroupCpvCode): Promise<any> {
    const apiURL = `/itemgroupcpvcode/create`;
    return await this.apiService.PostAsync<any>(apiURL, item, Constants.mrkApi);
  }

  async createMultipleItemGroupCpvCodesAsync(items: ItemGroupCpvCode[]): Promise<any> {
    const apiURL = `/ItemGroupCpvCode/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, items, Constants.mrkApi);
  }

  async updateItemGroupCpvCodeAsync(item: ItemGroupCpvCode): Promise<any> {
    const apiURL = `/itemgroupcpvcode/update`;
    return await this.apiService.PutAsync<any>(apiURL, item, Constants.mrkApi);
  }

  async deleteItemGroupCpvCodeAsync(itemGroupId: number, cpvCodeId: number): Promise<any> {
    const apiURL = `/itemgroupcpvcode/delete/` + itemGroupId + `/` + cpvCodeId;
    return await this.apiService.DeleteSingleAsync<any>(apiURL, Constants.mrkApi);
  }

  async deleteMultipleItemGroupCpvCodesAsync(items: ItemGroupCpvCode[]): Promise<any> {
    const apiURL = `/ItemGroupCpvCode/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, items, Constants.mrkApi);
  }
}
