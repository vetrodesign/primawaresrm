import { Injectable } from '@angular/core';
import { DiscountGridDetails } from 'app/models/discount-grid-details.model';
import { ApiService } from './api.service';
import { IntervalOverlapValidation } from 'app/models/interval-overlap-validation.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class DiscountGridDetailsService {

  constructor(private apiService: ApiService) { }

  async GetAllAsync(): Promise<any[]> {
    const apiURL = `/discountGridDetails/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }
  async GetByDiscountGridAsync(dgId: number): Promise<any[]> {
    const apiURL = `/discountGridDetails/GetByDiscountGrid`;
    return await this.apiService.PostAsync<any[]>(apiURL, dgId, Constants.mrkApi);
  }

  async GetByDiscountGridsAsync(discountGridIds: number[]): Promise<DiscountGridDetails[]> {
    const apiURL = `/discountGridDetails/GetByDiscountGrids`;
    return await this.apiService.PostAsync<DiscountGridDetails[]>(apiURL, discountGridIds, Constants.mrkApi);
  }

  async CreateAsync(dgd: DiscountGridDetails): Promise<any> {
    const apiURL = `/discountGridDetails/create`;
    return await this.apiService.PostAsync<any>(apiURL, dgd, Constants.mrkApi);
  }

  async UpdateAsync(dgd: DiscountGridDetails): Promise<any> {
    const apiURL = `/discountGridDetails/update`;
    return await this.apiService.PutAsync<any>(apiURL, dgd, Constants.mrkApi);
  }

  async DeleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/discountGridDetails/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  async CheckOverlapIntervalDiscountGridAsync(dgd: DiscountGridDetails): Promise<IntervalOverlapValidation> {
    const apiURL = `/discountGridDetails/CheckOverlapIntervalDiscountGrid`;
    return await this.apiService.PostAsync<IntervalOverlapValidation>(apiURL, dgd, Constants.mrkApi);
  }
}