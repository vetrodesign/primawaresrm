import { Injectable } from '@angular/core';
import { PartnerFinancialInfoVM } from 'app/models/partnerfinancialinfo.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerFinancialInfoService {
  constructor(private apiService: ApiService) { }

  async getByIdAsync(id: number): Promise<PartnerFinancialInfoVM[]> {
    const apiURL = `/partnerfinancialinfo/GetById`;
    return await this.apiService.PostAsync<PartnerFinancialInfoVM[]>(apiURL, id, Constants.mrkApi);
  }

  async getDetailsByPartnerIdAsync(partnerId: number): Promise<any[]> {
    const apiURL = `/partnerfinancialinfo/GetDetailsByPartnerId`;
    return await this.apiService.PostAsync<any[]>(apiURL, partnerId, Constants.mrkApi);
  }

  async getNumericDetailsByPartnerIdAsync(partnerId: number): Promise<any[]> {
    const apiURL = `/partnerfinancialinfo/GetNumericDetailsByPartnerId`;
    return await this.apiService.PostAsync<any[]>(apiURL, partnerId, Constants.mrkApi);
  }

  async getSupplierDetailsByPartnerIdAsync(partnerId: number): Promise<any[]> {
    const apiURL = `/partnerfinancialinfo/GetSupplierDetailsByPartnerId`;
    return await this.apiService.PostAsync<any[]>(apiURL, partnerId,  Constants.mrkApi);
  }

  async getSupplierNumericDetailsByPartnerIdAsync(partnerId: number): Promise<any[]> {
    const apiURL = `/partnerfinancialinfo/GetSupplierNumericDetailsByPartnerId`;
    return await this.apiService.PostAsync<any[]>(apiURL, partnerId, Constants.mrkApi);
  }

  async getByPartnerIdAsync(partnerId: number): Promise<any> {
    const apiURL = `/partnerfinancialinfo/GetByPartnerId`;
    return await this.apiService.PostAsync<any>(apiURL, partnerId, Constants.mrkApi);
  }

  async create(pfi: PartnerFinancialInfoVM): Promise<any> {
    const apiURL = `/partnerfinancialinfo/create`;
    return await this.apiService.PostAsync<any>(apiURL, pfi, Constants.mrkApi);
  }

  async createMultipleForLastYearsAsync(partnerId: number): Promise<number> {
    const apiURL = `/partnerfinancialinfo/CreateMultipleForLastYears`;
    return await this.apiService.PostAsync<number>(apiURL, partnerId, Constants.mrkApi);
  }

  async update(pfi: PartnerFinancialInfoVM): Promise<any> {
    const apiURL = `/partnerfinancialinfo/update`;
    return await this.apiService.PutAsync<any>(apiURL, pfi, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnerfinancialinfo/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
