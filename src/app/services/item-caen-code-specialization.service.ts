import { Injectable } from '@angular/core';
import { CaenCodeSpecializationXItem } from 'app/models/caencodespecializationxitem.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemCaenCodeSpecializationService {

  constructor(private apiService: ApiService) { }

  async getAllItemCaenCodeSpecializationAsync(): Promise<any[]> {
    const apiURL = `/CaenCodeSpecializationXItem/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.commonApi);
  }

  async getAllItemCaenCodeSpecializationByCustomerAsync(): Promise<any[]> {
    const apiURL = `/CaenCodeSpecializationXItem/getallbycustomer`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.commonApi);
  }

  async getAllByCaenCodeSpecializationId(caenCodeSpecializationId: number): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXItem/GetAllByCaenCodeSpecializationId`;
    return await this.apiService.PostAsync<any>(apiURL, caenCodeSpecializationId, Constants.commonApi);
  }

  async getAllByItemId(itemId: number): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXItem/GetAllByItemId`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.commonApi);
  }

  async createItemCaenCodeSpecializationAsync(item: CaenCodeSpecializationXItem): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXItem/create`;
    return await this.apiService.PostAsync<any>(apiURL, item, Constants.commonApi);
  }

  async syncItemCaenCodeSpecializationAsync(items: CaenCodeSpecializationXItem[]): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXItem/SyncMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, items, Constants.commonApi);
  }

  async createMultipleItemCaenCodeSpecializationAsync(items: CaenCodeSpecializationXItem[]): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXItem/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, items, Constants.commonApi);
  }

  async updateItemCaenCodeSpecializationAsync(item: CaenCodeSpecializationXItem): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXItem/update`;
    return await this.apiService.PutAsync<any>(apiURL, item, Constants.commonApi);
  }

  async deleteMultipleItemCaenCodeSpecializationsAsync(items: CaenCodeSpecializationXItem[]): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXItem/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, items, Constants.commonApi);
  }
}
