import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { Notification } from 'app/models/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  public subject: BehaviorSubject<Notification>;
  constructor() {
    this.subject = new BehaviorSubject<Notification>(null);
   }

  alert(from: string, align: string, text: string, color?: NotificationTypeEnum , addToNotificationArray?: boolean, ) {
      const notification = new Notification();
      notification.from = from;
      notification.text = text;
      notification.align = align;
      notification.color = color;
      notification.addToNotificationArray = addToNotificationArray ? addToNotificationArray : false;
      this.subject.next(notification);
  }
}
