import { Injectable } from '@angular/core';
import { DeliveryTime } from 'app/models/deliverytime.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class DeliverytimeService {

  constructor(private apiService: ApiService) { }

  async getAllDeliveryTimesAsync(): Promise<DeliveryTime[]> {
    const apiURL = `/deliverytime/getAll`;
    return await this.apiService.GetAsync<DeliveryTime[]>(apiURL, Constants.commonApi);
  }

  async getAllDeliveryTimesByCustomerIdAsync(): Promise<DeliveryTime[]> {
    const apiURL = `/deliverytime/getAllByCustomer`;
    return await this.apiService.GetAsync<DeliveryTime[]>(apiURL, Constants.commonApi);
  }

  async createDeliveryTimeAsync(deliverytime: DeliveryTime): Promise<any> {
    const apiURL = `/deliverytime/create`;
    return await this.apiService.PostAsync<any>(apiURL, deliverytime, Constants.commonApi);
  }

  async updateDeliveryTimeAsync(deliverytime: DeliveryTime): Promise<any> {
    const apiURL = `/deliverytime/update`;
    return await this.apiService.PutAsync<any>(apiURL, deliverytime, Constants.commonApi);
  }

  async deleteDeliveryTimesAsync(ids: number[]): Promise<any> {
    const apiURL = `/deliverytime/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
