import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
// import { ContactCampaignPartners, ContactCampaignPartnersModel } from 'app/models/contact-campaign-partners.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ContactCampaignPartnersService {

  constructor(private apiService: ApiService) { }

  // async getAutomaticContactCampaignPartnersAsync(postId: number, personTypes?: any): Promise<any> {   
  //   const apiURL = `/partner/GetAutomaticContactCampaignPartners/`;
  //   return await this.apiService.PostAsync<ContactCampaignPartnersModel>(apiURL, { postId: postId, personType: personTypes }, Constants.mrkApi);
  // }

  async getSeniorNIRDocumentsToGenerateAsync(): Promise<any> {   
    const apiURL = `/contactcampaign/GetSeniorNIRDocumentsToGenerate/`;
    return await this.apiService.PostAsync<any>(apiURL, { }, Constants.mrkApi);
  }
}
