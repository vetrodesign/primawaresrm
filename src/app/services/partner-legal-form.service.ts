import { Injectable } from '@angular/core';
import { PartnerLegalForm } from 'app/models/partnerlegalform.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerLegalFormService {
  constructor(private apiService: ApiService) { }

  async getAllPartnerLegalFormAsync(): Promise<PartnerLegalForm[]> {
    const apiURL = `/partnerlegalform/getall`;
    return await this.apiService.GetAsync<PartnerLegalForm[]>(apiURL, Constants.mrkApi);
  }

  async createPartnerLegalFormAsync(partnerLegalForm: PartnerLegalForm): Promise<any> {
    const apiURL = `/partnerlegalform/create`;
    return await this.apiService.PostAsync<any>(apiURL, partnerLegalForm, Constants.mrkApi);
  }

  async updatePartnerLegalFormAsync(partnerLegalForm: PartnerLegalForm): Promise<any> {
    const apiURL = `/partnerlegalform/update`;
    return await this.apiService.PutAsync<any>(apiURL, partnerLegalForm, Constants.mrkApi);
  }

  async deletePartnerLegalFormAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnerlegalform/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
