import { Injectable } from '@angular/core';
import { PartnerClientCreditControl } from 'app/models/partnerclientcreditcontrol.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerClientCreditControlService {
  constructor(private apiService: ApiService) { }

  async getPartnerClientCreditControlAsync(partnerId: number): Promise<PartnerClientCreditControl> {
    const apiURL = `/partnerclientcreditcontrol/GetByPartnerId`;
    return await this.apiService.PostAsync<PartnerClientCreditControl>(apiURL, partnerId, Constants.mrkApi);
  }

  async createPartnerClientCreditControlAsync(p: PartnerClientCreditControl): Promise<any> {
    const apiURL = `/partnerclientcreditcontrol/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updatePartnerClientCreditControlAsync(p: PartnerClientCreditControl): Promise<any> {
    const apiURL = `/partnerclientcreditcontrol/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deletePartnerClientCreditControlAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnerclientcreditcontrol/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
