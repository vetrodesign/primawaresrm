import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Language } from 'app/models/language.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(private apiService: ApiService) { }

  async getAllLanguagesAsync(): Promise<Language[]> {
    const apiURL = `/language/getall`;
    return await this.apiService.GetAsync<Language[]>(apiURL, Constants.commonApi);
  }

  async createLanguageAsync(language: Language): Promise<any> {
    const apiURL = `/language/create`;
    return await this.apiService.PostAsync<any>(apiURL, language, Constants.commonApi);
  }

  async updateLanguageAsync(language: Language): Promise<any> {
    const apiURL = `/language/update`;
    return await this.apiService.PutAsync<any>(apiURL, language, Constants.commonApi);
  }

  async deleteLanguageAsync(ids: number[]): Promise<any> {
    const apiURL = `/language/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
