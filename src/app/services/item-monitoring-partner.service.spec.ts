import { TestBed } from '@angular/core/testing';

import { ItemMonitoringPartnerService } from './item-monitoring-partner.service';

describe('ItemMonitoringPartnerService', () => {
  let service: ItemMonitoringPartnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemMonitoringPartnerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
