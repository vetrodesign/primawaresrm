import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ClientModuleMenuItem } from 'app/models/clientmodulemenuitem.model';
import { AuthService } from './auth.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ClientModuleMenuItemsService {

  constructor(private apiService: ApiService,
    private authenticationService: AuthService
  ) { }

  async getAllClientModulesMenuItemsAsync(): Promise<ClientModuleMenuItem[]> {
    const apiURL = `/clientModuleMenuItems/getall`;
    return await this.apiService.GetAsync<ClientModuleMenuItem[]>(apiURL, Constants.ssoApi);
  }

  async getClientModuleMenuItemsByModuleIDAsync(moduleId, isSystem: boolean): Promise<any[]> {
    const apiURL = `/clientModuleMenuItems/getallbyid/` + moduleId + `/` + isSystem;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.ssoApi);
  }

  async getMenuItemsByTextAsync(input: string): Promise<any> {
    const apiURL = `/clientModuleMenuItems/GetMenuItemByText`;
    let b = JSON.stringify(input);
    return await this.apiService.PostAsync<any>(apiURL, b , Constants.ssoApi);
  }

  async createClientModuleMenuItemAsync(menuItem: ClientModuleMenuItem): Promise<any> {
    const apiURL = `/clientModuleMenuItems`;
    return await this.apiService.PostAsync<any>(apiURL, menuItem, Constants.ssoApi);
  }

  async updateClientModuleMenuItemAsync(menuItem: ClientModuleMenuItem): Promise<any> {
    const apiURL = `/clientModuleMenuItems/` + menuItem.id;
    return await this.apiService.PutAsync<any>(apiURL, menuItem, Constants.ssoApi);
  }

  async deleteClientModuleMenuItemAsync(id: number): Promise<any> {
    const apiURL = `/clientModuleMenuItems/` + id;
    return await this.apiService.DeleteSingleAsync<any>(apiURL, Constants.ssoApi);
  }

  async getInfoButtonText(path: string) {
    const apiURL = `/clientModuleMenuItems/getInfoButtonText?path=/${path}&&clientModuleId=${this.authenticationService.getClientModuleId()}`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.ssoApi);
  }
}
