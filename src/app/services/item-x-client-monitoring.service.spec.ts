import { TestBed } from '@angular/core/testing';

import { ItemXClientMonitoringService } from './item-x-client-monitoring.service';

describe('ItemXClientMonitoringService', () => {
  let service: ItemXClientMonitoringService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemXClientMonitoringService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
