import { TestBed } from '@angular/core/testing';

import { ItemCaenCodeSpecializationService } from './item-caen-code-specialization.service';

describe('ItemCaenCodeSpecializationService', () => {
  let service: ItemCaenCodeSpecializationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemCaenCodeSpecializationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
