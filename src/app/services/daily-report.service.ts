import { Injectable } from '@angular/core';
import { DailyReport } from 'app/models/daily-report.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class DailyReportService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/dailyreport/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getByCustomerIdAsync(): Promise<any[]> {
    const apiURL = `/dailyreport/GetByCustomerId`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getByPostIdAsync(postId: number): Promise<any> {
    const apiURL = `/dailyreport/GetByPostId`;
    return await this.apiService.PostAsync<any>(apiURL, postId, Constants.mrkApi);
  }

  async getByDateAndPostIdAsync(postId: number, date: any): Promise<any> {
    let filter = {'postId': postId, 'date': date};
    const apiURL = `/dailyreport/GetByDateAndPostId`;
    return await this.apiService.PostAsync<any>(apiURL, filter, Constants.mrkApi);
  }

  async getByDateAsync(date: any): Promise<any[]> {
    let filter = {'postId': 1, 'date': date};
    const apiURL = `/dailyreport/GetByDate`;
    return await this.apiService.PostAsync<any[]>(apiURL, filter, Constants.mrkApi);
  }

  async createDailyReportAsync(dr: DailyReport): Promise<any> {
    const apiURL = `/dailyreport/create`;
    return await this.apiService.PostAsync<any>(apiURL, dr, Constants.mrkApi);
  }

  async updateDailyReportAsync(dr: DailyReport): Promise<any> {
    const apiURL = `/dailyreport/update`;
    return await this.apiService.PutAsync<any>(apiURL, dr, Constants.mrkApi);
  }

  async deleteDailyReportAsync(ids: number[]): Promise<any> {
    const apiURL = `/dailyreport/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
