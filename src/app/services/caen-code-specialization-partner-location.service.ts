import { Injectable } from '@angular/core';
import { CaenCodeSpecializationXPartnerLocation } from 'app/models/caencodespecializationxpartner.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CaenCodeSpecializationPartnerLocationService {

  constructor(private apiService: ApiService) { }

  async getAllCaenCodeSpecializationPartnerLocationByPartnerAsync(partnerId: number): Promise<any[]> {
    const apiURL = `/CaenCodeSpecializationXPartnerLocation/getallbypartner`;
    return await this.apiService.PostAsync<any>(apiURL, partnerId, Constants.commonApi);
  }

  async getAllCaenCodeSpecializationPartnerLocationByPartnerLocationAsync(locationId: number): Promise<any[]> {
    const apiURL = `/CaenCodeSpecializationXPartnerLocation/getallbypartnerlocation`;
    return await this.apiService.PostAsync<any>(apiURL, locationId, Constants.commonApi);
  }

  async createPartnerCaenCodeSpecializationPartnerLocationAsync(partner: CaenCodeSpecializationXPartnerLocation): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXPartnerLocation/create`;
    return await this.apiService.PostAsync<any>(apiURL, partner, Constants.commonApi);
  }

  async createMultipleCaenCodeSpecializationPartnerLocationAsync(partners: CaenCodeSpecializationXPartnerLocation[]): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXPartnerLocation/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, partners, Constants.commonApi);
  }

  async updatePartnerCaenCodeSpecializationPartnerLocationAsync(partner: CaenCodeSpecializationXPartnerLocation): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXPartnerLocation/update`;
    return await this.apiService.PutAsync<any>(apiURL, partner, Constants.commonApi);
  }

  async deleteMultipleCaenCodeSpecializationPartnerLocationAsync(partners: CaenCodeSpecializationXPartnerLocation[]): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXPartnerLocation/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, partners, Constants.commonApi);
  }
}
