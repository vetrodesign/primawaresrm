import { Injectable } from '@angular/core';
import { ItemEcommerce } from 'app/models/itemecommerce.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemEcommerceService {
  constructor(private apiService: ApiService) { }

  async getItemEcommerceAsync(itemId: number): Promise<ItemEcommerce> {
    const apiURL = `/itemecommerce/GetByItemId`;
    return await this.apiService.PostAsync<ItemEcommerce>(apiURL, itemId, Constants.mrkApi);
  }

  async createItemEcommerceAsync(i: ItemEcommerce): Promise<any> {
    const apiURL = `/itemecommerce/create`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async updateItemEcommerceAsync(i: ItemEcommerce): Promise<any> {
    const apiURL = `/itemecommerce/update`;
    return await this.apiService.PutAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async deleteItemEcommerceAsync(ids: number[]): Promise<any> {
    const apiURL = `/itemecommerce/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
