import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class CurrencyRateService {

  constructor(private apiService: ApiService) { }

  async getBaseCurrency(): Promise<any> {
    const apiURL = `/currency/getallbasecurrencies`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.commonApi);

  }

  async getAllCurrencyRatesAsync(): Promise<any[]> {
    const apiURL = `/currency/GetAllCurrencyRatesByCustomer`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.commonApi);
  }

  async createCurrencyRateAsync(rate: any): Promise<any> {
    const apiURL = `/currency/CreateCurrencyRatesByCustomer`;
    return await this.apiService.PostAsync<any>(apiURL, rate, Constants.commonApi);
  }

  async updateCustomCodeAsync(rate: any): Promise<any> {
    const apiURL = `/currency/UpdateCurrencyRatesByCustomer`;
    return await this.apiService.PutAsync<any>(apiURL, rate, Constants.commonApi);
  }
}
