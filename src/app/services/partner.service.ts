import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { DuplicatePartner, Partner } from 'app/models/partner.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PartnerSearchFilter, SuppliersCountData } from 'app/models/partnerSearchFilter.model';
import { ERPSyncOptions } from 'app/models/erp-sync-options.model';
import { environment } from 'environments/environment';
import { SupplierSmallSearchFilter } from 'app/models/suppliersmallsearchfilter.model';
import { HelperService } from './helper.service';
import { PartnerLocationConventionAndCurrencyResult } from 'app/models/partnerlocationconventionandcurrencyresponse.model';
import { ClientSmallSearchFilter } from 'app/models/client-small-search-filter.model';
import { ClientSmall } from 'app/models/client-small.model';
import { GetPartnerNamesAndIdsByPartnerIdsResponse } from 'app/models/getPartnerNamesAndIdsByPartnerIdsResponse.model';
import { Constants } from 'app/constants';
import { PartnerSmall } from 'app/models/partner-small.model';
import { PartnerLocationDTO } from 'app/models/partnerlocationdto.model';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  constructor(private apiService: ApiService, private http: HttpClient, private helperService: HelperService) { }

  async getPartnersByCreatedByAndDate(ids:number[], startDate:Date, endDate: Date) {
    let filter = {ids:ids, startDate:startDate, endDate:endDate};
    const apiURL = `/partner/getpartnersbycreatedbyanddate`;
    return await this.apiService.PostAsync(apiURL, filter, Constants.mrkApi);
  }

  async getPartnersXItemsCommentByCreatedByAndDate(ids:number[], startDate:Date, endDate: Date) {
    let filter = {ids:ids, startDate:startDate, endDate:endDate};
    const apiURL = `/partner/getpartnersxitemscomment`;
    return await this.apiService.PostAsync(apiURL, filter, Constants.mrkApi);
  }

  async getAllPartnersAsync(): Promise<Partner[]> {
    const apiURL = `/partner/getall`;
    return await this.apiService.GetAsync<Partner[]>(apiURL, Constants.mrkApi);
  }

  async getPartnerByPartnerIdsAsync(partnerIds: number[]): Promise<Partner[]> {
    const apiURL = `/partner/GetPartnerByPartnerIds`;
    return await this.apiService.PostAsync<Partner[]>(apiURL, partnerIds, Constants.mrkApi);
  }

  async getPartnerAsyncByID(): Promise<Partner[]> {
    const apiURL = `/partner/getbycustomerid`;
    return await this.apiService.GetAsync<Partner[]>(apiURL, Constants.mrkApi);
  }

  async getAllDuplicates(): Promise<DuplicatePartner[]> {
    const apiURL = `/partner/getAllDuplicates`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  } 

  async getAllSuppliersAsync(): Promise<Partner[]> {
    const apiURL = `/partner/GetSuppliers`;
    return await this.apiService.GetAsync<Partner[]>(apiURL, Constants.mrkApi);
  }

  async getSuppliersAsyncByID(): Promise<Partner[]> {
    const apiURL = `/partner/GetByCustomerIdSuppliers`;
    return await this.apiService.GetAsync<Partner[]>(apiURL, Constants.mrkApi);
  }

  async getAllPartnersSmallAsync(): Promise<PartnerSmall[]> {
    const apiURL = `/partner/getallsmall`;
    return await this.apiService.GetAsync<PartnerSmall[]>(apiURL, Constants.mrkApi);
  }

  async getPartnerActiveItems(partnerId: number): Promise<any> {
    const apiURL = `/partner/GetPartnerActiveItems`;
    return await this.apiService.PostAsync<any>(apiURL, partnerId, Constants.mrkApi);
  }

  async getPartnerInactiveItems(partnerId: number): Promise<any> {
    const apiURL = `/partner/GetPartnerInactiveItems`;
    return await this.apiService.PostAsync<any>(apiURL, partnerId, Constants.mrkApi);
  } 

  async getPartnerItemGroupCategory(partnerId: number, allocationType: number): Promise<any> {
    const apiURL = `/partner/GetPartnerItemGroupCategory`;
    return await this.apiService.PostAsync<any>(apiURL, { partnerId: partnerId, allocationType: allocationType }, Constants.mrkApi);
  }

  async getPartnerItemGroupCode(partnerId: number, allocationType: number): Promise<any> {
    const apiURL = `/partner/GetPartnerItemGroupCode`;
    return await this.apiService.PostAsync<any>(apiURL, { partnerId: partnerId, allocationType: allocationType }, Constants.mrkApi);
  }

  async getPartnerNamesAndIdsByPartnerIdsAsync(partnerIds: number[]): Promise<GetPartnerNamesAndIdsByPartnerIdsResponse[]> {
    const apiURL = `/partner/GetPartnerNamesAndIdsByPartnerIds`;
    return await this.apiService.PostAsync<GetPartnerNamesAndIdsByPartnerIdsResponse[]>(apiURL, partnerIds, Constants.mrkApi);
  }

  async getPartnerProductConventionIdsByLocationsAsync(partnerLocationIds: number[]): Promise<PartnerLocationDTO[]> {
    const apiURL = `/partner/GetPartnerProductConventionIdsByLocations/`;
    return await this.apiService.PostAsync<PartnerLocationDTO[]>(apiURL, partnerLocationIds, Constants.mrkApi);
  }

  async getPartnersByFilter(partnerSearchFilter: PartnerSearchFilter): Promise<any> {
    const apiURL = `/partner/getPartnersByFilter`;
    return await this.apiService.PostAsync<any>(apiURL, partnerSearchFilter, Constants.mrkApi);
  }

  async getSuppliersCountData(postId: number): Promise<SuppliersCountData> {
    let apiURL = `/partner/GetSuppliersCountData`;
    
    if (postId !== null && postId !== undefined) {
        apiURL += `?postId=${postId}`;
    }

    return await this.apiService.GetAsync<SuppliersCountData>(apiURL, Constants.mrkApi);
  }

  async getERPInvoiceSmallByPartnerIdsAsync(partnerIds: number[]): Promise<any[]> {
    const apiURL = `/order/GetERPInvoiceSmallByPartnerIds`;
    return await this.apiService.PostAsync<any[]>(apiURL, partnerIds, Constants.mrkApi);
  }

  async getPartnersWithUnresolvedSubscriptions(): Promise<any[]> {
    const apiURL = `/partner/getPartnersWithUnresolvedSubscriptions`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  } 

  async createPartnerAsync(partner: Partner): Promise<any> {
    partner = await this.helperService.trimObject(partner);
    partner.initialPartnerLocation = await this.helperService.trimObject(partner.initialPartnerLocation);

    const apiURL = `/partner/create`;
    return await this.apiService.PostAsync<any>(apiURL, partner, Constants.mrkApi);
  }

  async getByPartnerId(partnerId: number): Promise<any> {
    const apiURL = `/partner/GetByPartnerId`;
    return await this.apiService.PostAsync<any>(apiURL, partnerId, Constants.mrkApi);
  }

  async getPartnerProductConventionIdByImplicitLocationAsync(partnerId: number): Promise<PartnerLocationConventionAndCurrencyResult> {
    const apiUrl = `/partner/GetPartnerProductConventionIdByImplicitLocation/` + partnerId;
    return await this.apiService.GetAsync<PartnerLocationConventionAndCurrencyResult>(apiUrl, Constants.mrkApi);
  }
  
  async getPartnerProductConventionIdByLocationAsync(partnerLocationId: number): Promise<number> {
    const apiUrl = `/partner/GetPartnerProductConventionIdByLocation/`;
    return await this.apiService.PostAsync<number>(apiUrl, partnerLocationId, Constants.mrkApi);
  }

  async upsertToCharismaAsync(options: ERPSyncOptions): Promise<any> {
    const apiURL = `/partner/upsertToCharisma`;
    return await this.apiService.PostAsync<any>(apiURL, options, Constants.mrkApi);
  }

  async getSuppliersSmallAsync(searchFilter: SupplierSmallSearchFilter): Promise<Partner[]> {
    const apiURL = `/partner/getSuppliersSmall`;
    return await this.apiService.PostAsync<Partner[]>(apiURL, searchFilter, Constants.mrkApi);
  }

  async getPartnersSmallAsync(filter: ClientSmallSearchFilter): Promise<ClientSmall[]> {
    const apiURL = `/partner/getPartnersSmall`;
    return await this.apiService.PostAsync<ClientSmall[]>(apiURL, filter, Constants.mrkApi);
  }

  async updatePartnerAsync(partner: Partner): Promise<any> {
    partner = await this.helperService.trimObject(partner);
    partner.initialPartnerLocation = await this.helperService.trimObject(partner.initialPartnerLocation);

    const apiURL = `/partner/update`;
    return await this.apiService.PutAsync<any>(apiURL, partner, Constants.mrkApi);
  }

  async deletePartnerAsync(ids: number[]): Promise<any> {
    const apiURL = `/partner/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  async getClientsSmallAsync(filter: ClientSmallSearchFilter): Promise<ClientSmall[]> {
    const apiURL = `/partner/GetClientsSmall`;
    return await this.apiService.PostAsync<ClientSmall[]>(apiURL, filter, Constants.mrkApi);
  }

  async getSuppliersSmallByEmailFilterAsync(searchFilter: ClientSmallSearchFilter): Promise<ClientSmall[]> {
    const apiURL = `/partner/getSuppliersSmallByEmailFilter`;
    return await this.apiService.PostAsync<ClientSmall[]>(apiURL, searchFilter, Constants.mrkApi);
  }

  async updatePartnerIsPrincipalActivityUnique(partnerId: number, isPrincipalActivityUniqueForAllLocations: boolean, rowVersion: any): Promise<boolean> {
    const apiURL = `/partner/UpdatePartnerIsPrincipalActivityUnique`;
    return await this.apiService.PostAsync(apiURL, { partnerId: partnerId, isPrincipalActivityUniqueForAllLocations: isPrincipalActivityUniqueForAllLocations, rowVersion: rowVersion }, Constants.mrkApi);
  }

  getName(): Promise<any> {
    return this.http.post<any>(`https://webservicesp.anaf.ro/RegCult/api/v1/ws/cult`,
      [{ cui: 8409931, data: '2020-07-20'}], { headers: this.getHeaders() }).toPromise().then(t => {
      });
  }

  private getHeaders(): HttpHeaders {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Cache-Control', 'no-cache');
    headers = headers.append('Pragma', 'no-cache');
    headers = headers.append('Access-Control-Allow-Origin', 'https://localhost:4207');
    headers = headers.append('Access-Control-Allow-Methods', 'POST');
    headers = headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers = headers.append('If-Modified-Since', '0');
    headers = headers.append('Content-Type', 'application/json');
    let copyHeader = headers;
    return copyHeader;
  }
}
