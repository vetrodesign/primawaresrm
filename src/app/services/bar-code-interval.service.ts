import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { BarCodeInterval } from 'app/models/barcodeinterval.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class BarCodeIntervalService {

  constructor(private apiService: ApiService) { }

  async getAllBarCodeIntervalsAsync(): Promise<any[]> {
    const apiURL = `/barcodeinterval/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getBarCodeIntervalsAsyncByID(): Promise<any[]> {
    const apiURL = `/barcodeinterval/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getFirstAvailableBarCode(): Promise<any> {
    const apiURL = `/barcodeinterval/GetFirstAvailableBarCode`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async getByPartnerIdAsync(partnerId: number): Promise<any[]> {
    const apiURL = `/barcodeinterval/GetAllByPartnerId`;
    return await this.apiService.PostAsync<any[]>(apiURL, partnerId, Constants.mrkApi);
  }

  async createBarCodeIntervalAsync(barCode: BarCodeInterval): Promise<any> {
    const apiURL = `/barcodeinterval/create`;
    return await this.apiService.PostAsync<any>(apiURL, barCode, Constants.mrkApi);
  }

  async updateBarCodeIntervalAsync(barCode: BarCodeInterval): Promise<any> {
    const apiURL = `/barcodeinterval/update`;
    return await this.apiService.PutAsync<any>(apiURL, barCode, Constants.mrkApi);
  }

  async deleteBarCodeIntervalsAsync(ids: number[]): Promise<any> {
    const apiURL = `/barcodeinterval/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}