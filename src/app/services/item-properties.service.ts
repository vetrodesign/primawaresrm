import { Injectable } from '@angular/core';
import { ItemProperties } from 'app/models/itemproperties.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemPropertiesService {
  constructor(private apiService: ApiService) { }

  async getItemPropertiesAsync(itemId: number): Promise<ItemProperties> {
    const apiURL = `/itemproperties/GetByItemId`;
    return await this.apiService.PostAsync<ItemProperties>(apiURL, itemId, Constants.mrkApi);
  }

  async createItemPropertiesAsync(i: ItemProperties): Promise<any> {
    const apiURL = `/itemproperties/create`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async updateItemPropertiesAsync(i: ItemProperties): Promise<any> {
    const apiURL = `/itemproperties/update`;
    return await this.apiService.PutAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async deleteItemPropertiesAsync(ids: number[]): Promise<any> {
    const apiURL = `/itemproperties/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
