import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { EshopVisibleItem, Item, ItemsTurnoversCountData } from 'app/models/item.model';
import { ItemListFilter, ItemERPSyncOptionsVM } from 'app/models/itemlistfilter.model';
import { environment } from 'environments/environment';
import { ItemSmall } from 'app/models/item-small.model';
import { ItemDCRInfo } from 'app/models/itemDCRInfo.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private apiService: ApiService) { }

  async UpdateByItemGroupIdAsync(itemGroupId: number): Promise<any[]> {
    const apiURL = `/generalOffer/UpdateByItemGroupId/`;
    return await this.apiService.PostAsync<any[]>(apiURL, itemGroupId, Constants.mrkApi);
  }

  async getAllItemsAsync(): Promise<any[]> {
    const apiURL = `/item/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getCodesForItemIds(itemIds: number[]): Promise<ItemSmall[]> {
    const apiURL = `/item/getCodesForItemIds`;
    return await this.apiService.PostAsync<ItemSmall[]>(apiURL, itemIds, Constants.mrkApi);
  }

  async getTaxRateByItemIdsAsync(ids: number[], countryId: number): Promise<any[]> {
    let body = {
      'itemIds': ids,
      'countryId': countryId
    };
    const apiURL = `/item/GetTaxRateByItemIds`;
    return await this.apiService.PostAsync<any[]>(apiURL, body, Constants.mrkApi);
  }

  async getItemsByFilter(itemListFilter: ItemListFilter): Promise<any> {
    const apiURL = `/item/GetItems`;
    return await this.apiService.PostAsync<any>(apiURL, itemListFilter, Constants.mrkApi);
  }

  async getItemDCRAsync(itemId: number): Promise<ItemDCRInfo> {
    const apiURL = `/item/getitemdcr`;
    return await this.apiService.PostAsync<ItemDCRInfo>(apiURL, itemId, Constants.mrkApi);
  }

  async getFilteredByCodeSmall(code: string): Promise<any[]> {
    let c = JSON.stringify(code);
    const apiURL = `/item/getfilteredbycodesmall`;
    return await this.apiService.PostAsync<any[]>(apiURL, c, Constants.mrkApi);
  }

  async CheckIfItemIsOnInvoiceAsync(itemId: number): Promise<any> {
    const apiURL = `/item/checkIfItemIsOnInvoice`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }

  async getItemStock(itemId: number): Promise<any> {
    const apiURL = `/item/GetItemStock`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }

  async getItemssAsyncByID(): Promise<any[]> {
    const apiURL = `/item/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getItemTurnover(itemId: number): Promise<any[]> {
    const apiURL = `/item/GetItemTurnOver`;
    return await this.apiService.PostAsync<any[]>(apiURL, itemId, Constants.mrkApi);
  }

  async getAllItemSmallAsync(): Promise<ItemSmall[]> {
    const apiURL = `/item/getallitemsmall`;
    return await this.apiService.GetAsync<ItemSmall[]>(apiURL, Constants.mrkApi);
  }

  async getItemByItemId(itemId: number): Promise<any> {
    const apiURL = `/item/GetItemByItemId`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }

  async getItemMonthTurnover(itemId: number): Promise<any[]> {
    const apiURL = `/item/GetItemMonthTurnOver`;
    return await this.apiService.PostAsync<any[]>(apiURL, itemId, Constants.mrkApi);
  }

  async getItemLogisticsAsync(itemId: number): Promise<any> {
    const apiURL = `/item/getitemlogistics`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }

  async getAllItemMonthTurnover(): Promise<any[]> {
    const apiURL = `/item/GetAllItemMonthTurnOver`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getItemTurnoversCountData(): Promise<ItemsTurnoversCountData[]> {
    const apiURL = `/item/GetItemTurnoversCountData`;
    return await this.apiService.PostAsync<ItemsTurnoversCountData[]>(apiURL, {}, Constants.mrkApi);
  }

  async createItemAsync(item: Item): Promise<any> {
    const apiURL = `/item/create`;
    return await this.apiService.PostAsync<any>(apiURL, item, Constants.mrkApi);
  }

  async updateItemAsync(item: Item): Promise<any> {
    const apiURL = `/item/update`;
    return await this.apiService.PutAsync<any>(apiURL, item, Constants.mrkApi);
  }

  async updateItemsTurnoverAsync(ids: number[]): Promise<any> {
    const apiURL = `/item/updateitemsTurnover`;
    return await this.apiService.PostAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  async deleteItemAsync(ids: number[]): Promise<any> {
    const apiURL = `/item/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  async insertToErpAsync(itemId: number, userName: string, postName: string, isSeniorErp?: boolean): Promise<any> {
    let o = new ItemERPSyncOptionsVM();
    o.itemId = itemId;
    o.historyUserName = userName;
    o.historyPostName = postName;
    o.isSeniorErp = isSeniorErp;
    const apiURL = `/item/InsertItemToERP`;
    return await this.apiService.PostAsync<any>(apiURL, o, Constants.mrkApi);
  }

  async updateToErpAsync(itemId: number, userName: string, postName: string): Promise<any> {
    let o = new ItemERPSyncOptionsVM();
    o.itemId = itemId;
    o.historyUserName = userName;
    o.historyPostName = postName;
    const apiURL = `/item/UpdateItemToERP`;
    return await this.apiService.PutAsync<any>(apiURL, o, Constants.mrkApi);
  }

  async getItemTurnovers(itemIds: number[]): Promise<any[]> {
    const apiURL = `/item/GetItemTurnOvers`;
    return await this.apiService.PostAsync<any[]>(apiURL, itemIds, Constants.mrkApi);
  }

  async getItemStocks(itemId: number[]): Promise<any> {
    const apiURL = `/item/GetItemStocks`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }

  async getAllManagementsItemStocks(itemId: number[]): Promise<any> {
    const apiURL = `/item/GetAllManagementsItemStocks`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }

  async getByItemId(itemId: number): Promise<any> {
    const apiURL = `/ItemERPSyncHistory/GetLastHistoryEntryByItemId`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }

  async getEshopVisibleItems(eshopLanguageIds: number[]): Promise<EshopVisibleItem[]> {
    const apiURL = `/item/GetEshopVisibleItems`;
    return await this.apiService.PostAsync<EshopVisibleItem[]>(apiURL, eshopLanguageIds, Constants.mrkApi);
  }
}