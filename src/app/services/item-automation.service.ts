import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { ItemAutomation } from "app/models/item-automation.model";
import { Constants } from "app/constants";

@Injectable({
    providedIn: 'root'
})

export class ItemAutomationService {
    constructor(private apiService: ApiService) { }

    async getByItemId(itemId: number): Promise<ItemAutomation> {
        const apiURL = `/itemautomation/getbyitemid?itemid=${itemId}`;
        return await this.apiService.GetAsync<ItemAutomation>(apiURL, Constants.mrkApi);
    }

    async createAsync(itemAutomation: ItemAutomation): Promise<any> {
        const apiURL = `/itemautomation/create`;
        return await this.apiService.PostAsync<any>(apiURL, itemAutomation, Constants.mrkApi);
    }

    async updateAsync(itemAutomation: ItemAutomation): Promise<any> {
        const apiURL = `/itemautomation/update`;
        return await this.apiService.PutAsync<any>(apiURL, itemAutomation, Constants.mrkApi);
    }
}