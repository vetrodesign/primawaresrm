import { TestBed } from '@angular/core/testing';

import { PartnerAuthorizationService } from './partner-authorization.service';

describe('PartnerAuthorizationService', () => {
  let service: PartnerAuthorizationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerAuthorizationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
