import { TestBed } from '@angular/core/testing';

import { PartnerActivityAllocationService } from './partner-activity-allocation.service';

describe('PartnerActivityAllocationService', () => {
  let service: PartnerActivityAllocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerActivityAllocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
