import { Injectable } from '@angular/core';
import { ItemXTag } from 'app/models/itemxtag.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})

export class ItemTagService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/itemxtag/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getByItemIdAsync(itemId: number): Promise<any[]> {
    const apiURL = `/itemxtag/getByItemId`;
    return await this.apiService.PostAsync<any[]>(apiURL, itemId, Constants.mrkApi);
  }

  async createMultipleAsync(i: ItemXTag[]): Promise<any> {
    const apiURL = `/itemxtag/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async deleteMultipleAsync(i: ItemXTag[]): Promise<any> {
    const apiURL = `/itemxtag/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }
}
