import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Brand } from 'app/models/brand.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class BrandService {
  constructor(private apiService: ApiService) { }

  async getBrandsByCustomerIdAsync(): Promise<any[]> {
    const apiURL = `/brand/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getAllBrandsAsync(): Promise<Brand[]> {
    const apiURL = `/brand/getall`;
    return await this.apiService.GetAsync<Brand[]>(apiURL, Constants.mrkApi);
  }

  async createBrandAsync(brand: Brand): Promise<any> {
    const apiURL = `/brand/create`;
    return await this.apiService.PostAsync<any>(apiURL, brand, Constants.mrkApi);
  }

  async updateBrandAsync(brand: Brand): Promise<any> {
    const apiURL = `/brand/update`;
    return await this.apiService.PutAsync<any>(apiURL, brand, Constants.mrkApi);
  }

  async deleteBrandsAsync(ids: number[]): Promise<any> {
    const apiURL = `/brand/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
