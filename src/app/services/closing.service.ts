import { Injectable } from '@angular/core';
import { Closing } from 'app/models/closing.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class ClosingService {

  constructor(private apiService: ApiService) { }

  async getAllClosingsAsync(): Promise<Closing[]> {
    const apiURL = `/closing/getAll`;
    return await this.apiService.GetAsync<Closing[]>(apiURL, Constants.commonApi);
  }

  async getAllClosingsByCustomerIdAsync(): Promise<Closing[]> {
    const apiURL = `/closing/getAllByCustomer`;
    return await this.apiService.GetAsync<Closing[]>(apiURL, Constants.commonApi);
  }

  async createClosingAsync(closing: Closing): Promise<any> {
    const apiURL = `/closing/create`;
    return await this.apiService.PostAsync<any>(apiURL, closing, Constants.commonApi);
  }

  async updateClosingAsync(closing: Closing): Promise<any> {
    const apiURL = `/closing/update`;
    return await this.apiService.PutAsync<any>(apiURL, closing, Constants.commonApi);
  }

  async deleteClosingsAsync(ids: number[]): Promise<any> {
    const apiURL = `/closing/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
