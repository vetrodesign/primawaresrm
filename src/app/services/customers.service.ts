import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Customer } from '../models/customer.model';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(private apiService: ApiService) { }

  async getAllCustomersAsync(): Promise<Customer[]> {
    const apiURL = `/customer`;
    return await this.apiService.GetAsync<Customer[]>(apiURL, Constants.ssoApi);
  }

  async getCustomerAsyncByID(customerId): Promise<Customer> {
    const apiURL = `/customer/GetById/` + customerId;
    return await this.apiService.GetAsync<Customer>(apiURL, Constants.ssoApi);
  }

  async createCustomerAsync(customer: Customer): Promise<any> {
    const apiURL = `/customer`;
    return await this.apiService.PostAsync<any>(apiURL, customer, Constants.ssoApi);
  }

  async updateCustomerAsync(customer: Customer): Promise<any> {
    const apiURL = `/customer/` + customer.id;
    return await this.apiService.PutAsync<any>(apiURL, customer, Constants.ssoApi);
  }

  async deleteCustomerAsync(id: number): Promise<any> {
    const apiURL = `/customer/` + id;
    return await this.apiService.DeleteSingleAsync<any>(apiURL, Constants.ssoApi);
  }
}
