import { Injectable } from '@angular/core';
import { PartnerNameEquivalence } from 'app/models/partnernameequivalence.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerNameEquivalenceService {

  constructor(private apiService: ApiService) { }

  //PartnerNameEquivalence
  async getAllPartnerNameEquivalenceAsync(): Promise<PartnerNameEquivalence[]> {
    const apiURL = `/PartnerNameEquivalence/getAll`;
    return await this.apiService.GetAsync<PartnerNameEquivalence[]>(apiURL, Constants.mrkApi);
  }

  async getByPartnerIdNameEquivalenceAsync(id: number): Promise<PartnerNameEquivalence[]> {
    const apiURL = `/PartnerNameEquivalence/getByPartnerId`;
    return await this.apiService.PostAsync<PartnerNameEquivalence[]>(apiURL, id, Constants.mrkApi);
  }

  async createPartnerNameEquivalenceAsync(countryNameEquivalence: PartnerNameEquivalence): Promise<any> {
    const apiURL = `/PartnerNameEquivalence/create`;
    return await this.apiService.PostAsync<any>(apiURL, countryNameEquivalence, Constants.mrkApi);
  }

  async deletePartnerNameEquivalenceAsync(ids: number[]): Promise<any> {
    const apiURL = `/PartnerNameEquivalence/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
