import { Injectable } from '@angular/core';
import { PartnerReview } from 'app/models/partnerreview.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class PartnerReviewService {
  constructor(private apiService: ApiService) { }

  async getAllPartnerReviewsAsync(): Promise<PartnerReview[]> {
    const apiURL = `/partnerreview`;
    return await this.apiService.GetAsync<PartnerReview[]>(apiURL, Constants.mrkApi);
  }

  async getPartnerReviewsByID(id): Promise<PartnerReview> {
    const apiURL = `/partnerreview/getbyid/` + id;
    return await this.apiService.GetAsync<PartnerReview>(apiURL, Constants.mrkApi);
  }

  async getPartnerReviewsByPartnerID(partnerId): Promise<PartnerReview[]> {
    const apiURL = `/partnerreview/getbypartnerid/` + partnerId;
    return await this.apiService.GetAsync<PartnerReview[]>(apiURL, Constants.mrkApi);
  }

  async createPartnerReviewAsync(p: PartnerReview): Promise<any> {
    const apiURL = `/partnerreview/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updatePartnerReviewAsync(p: PartnerReview): Promise<any> {
    const apiURL = `/partnerreview/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deletePartnerReviewAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnerreview/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
