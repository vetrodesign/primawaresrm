import { TestBed } from '@angular/core/testing';

import { BarCodeItemAllocationService } from './bar-code-item-allocation.service';

describe('BarCodeProductAllocationService', () => {
  let service: BarCodeItemAllocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BarCodeItemAllocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
