import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerContactObservation } from 'app/models/partner-contact-observation.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerContactObservationService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<PartnerContactObservation[]> {
    const apiURL = `/PartnerContactObservation/GetAll`;
    return await this.apiService.GetAsync<PartnerContactObservation[]>(apiURL, Constants.mrkApi);
  }

  async getAllByCustomerIdAsync(customerId): Promise<PartnerContactObservation[]> {
    const apiURL = `/PartnerContactObservation/GetByCustomerId/` + customerId;
    return await this.apiService.GetAsync<PartnerContactObservation[]>(apiURL, Constants.mrkApi);
  }

  async getAllByPartnerContactIdAsync(customerId): Promise<PartnerContactObservation[]> {
    const apiURL = `/PartnerContactObservation/GetByPartnerContactId/` + customerId;
    return await this.apiService.GetAsync<PartnerContactObservation[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(m: PartnerContactObservation): Promise<any> {
    const apiURL = `/PartnerContactObservation/create`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async updateAsync(m: PartnerContactObservation): Promise<any> {
    const apiURL = `/PartnerContactObservation/update`;
    return await this.apiService.PutAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async deleteMultipleAsync(m: Number[]): Promise<any> {
    const apiURL = `/PartnerContactObservation/deleteMultiple`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, m, Constants.mrkApi);
  }
}
