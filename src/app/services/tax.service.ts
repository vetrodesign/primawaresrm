import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Tax } from 'app/models/tax.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class TaxService {

  constructor(private apiService: ApiService) { }

  async getAllTaxesAsync(): Promise<Tax[]> {
    const apiURL = `/tax/getall`;
    return await this.apiService.GetAsync<Tax[]>(apiURL, Constants.commonApi);
  }

  async createTaxAsync(tax: Tax): Promise<any> {
    const apiURL = `/tax/create`;
    return await this.apiService.PostAsync<any>(apiURL, tax, Constants.commonApi);
  }

  async updateTaxAsync(tax: Tax): Promise<any> {
    const apiURL = `/tax/update`;
    return await this.apiService.PutAsync<any>(apiURL, tax, Constants.commonApi);
  }

  async deleteTaxAsync(ids: number[]): Promise<any> {
    const apiURL = `/tax/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
