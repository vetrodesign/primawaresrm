import { TestBed } from '@angular/core/testing';

import { PartnerObservationService } from './partner-observation.service';

describe('PartnerObservationService', () => {
  let service: PartnerObservationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerObservationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
