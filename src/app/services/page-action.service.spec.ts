import { TestBed } from '@angular/core/testing';

import { PageActionService } from './page-action.service';

describe('PageActionService', () => {
  let service: PageActionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PageActionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
