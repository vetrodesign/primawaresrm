import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ItemGroup } from 'app/models/itemgroup.model';
import { ItemGroupFilter } from 'app/models/itemgroupfilter.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemGroupService {

  constructor(private apiService: ApiService) { }

  async getAllItemGroupsAsync(): Promise<any[]> {
    const apiURL = `/itemgroup/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getItemGroupsAsyncByID(): Promise<any[]> {
    const apiURL = `/itemgroup/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getItemGroupsFilterAsync(itemGroupFilter: ItemGroupFilter): Promise<ItemGroup[]> {
    const apiURL = `/itemgroup/getItemGroups`;
    return await this.apiService.PostAsync<ItemGroup[]>(apiURL, itemGroupFilter, Constants.mrkApi);
  }

  async createItemGroupAsync(item: ItemGroup): Promise<any> {
    const apiURL = `/itemgroup/create`;
    return await this.apiService.PostAsync<any>(apiURL, item, Constants.mrkApi);
  }

  async updateItemGroupAsync(item: ItemGroup): Promise<any> {
    const apiURL = `/itemgroup/update`;
    return await this.apiService.PutAsync<any>(apiURL, item, Constants.mrkApi);
  }

  async deleteItemGroupAsync(ids: number[]): Promise<any> {
    const apiURL = `/itemgroup/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
  
  async getNextAvailableCodeAsync(item: string): Promise<any> {
    const apiURL = `/itemgroup/GetNextAvailableCode`;
    return await this.apiService.PostAsync<any>(apiURL, item, Constants.mrkApi);
  }

}