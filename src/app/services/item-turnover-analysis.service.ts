import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { ItemTurnoverAnalysis } from 'app/models/item-turnover-analysis.model';


@Injectable({
  providedIn: 'root'
})
export class ItemTurnoverAnalysisService {
  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<ItemTurnoverAnalysis[]> {
    const apiURL = `/itemturnoveranalysis/getAll`;
    return await this.apiService.GetAsync<ItemTurnoverAnalysis[]>(apiURL, Constants.mrkApi);
  }

  async getItemTurnoverAnalysisData(): Promise<any> {
    const apiURL = `/itemturnoveranalysis/getItemTurnoverAnalysisData`;
    return await this.apiService.PostAsync<any>(apiURL, {}, Constants.mrkApi);
  }

  async createAsync(p: ItemTurnoverAnalysis): Promise<any> {
    const apiURL = `/itemturnoveranalysis/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updateAsync(p: ItemTurnoverAnalysis): Promise<any> {
    const apiURL = `/itemturnoveranalysis/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }
}
