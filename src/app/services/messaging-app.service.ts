import { Injectable } from '@angular/core';
import { MessagingApp } from 'app/models/messaging-app.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})

export class MessagingAppService {
  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<MessagingApp[]> {
    const apiURL = `/messagingApp/getall`;
    return await this.apiService.GetAsync<MessagingApp[]>(apiURL, Constants.mrkApi);
  }
}
