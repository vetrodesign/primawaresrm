import { Injectable } from '@angular/core';
import { PartnerSupplierCreditControl } from 'app/models/partnersuppliercreditcontrol.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerSupplierCreditControlService {
  constructor(private apiService: ApiService) { }

  async getPartnerSupplierCreditControlAsync(partnerId: number): Promise<PartnerSupplierCreditControl> {
    const apiURL = `/partnersuppliercreditcontrol/GetByPartnerId`;
    return await this.apiService.PostAsync<PartnerSupplierCreditControl>(apiURL, partnerId, Constants.mrkApi);
  }

  async createPartnerSupplierCreditControlAsync(p: PartnerSupplierCreditControl): Promise<any> {
    const apiURL = `/partnersuppliercreditcontrol/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updatePartnerSupplierCreditControlAsync(p: PartnerSupplierCreditControl): Promise<any> {
    const apiURL = `/partnersuppliercreditcontrol/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deletePartnerSupplierCreditControlAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnersuppliercreditcontrol/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
