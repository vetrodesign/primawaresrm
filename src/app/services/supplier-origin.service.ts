import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { SupplierOrigin } from 'app/models/supplier-origin.model';

@Injectable({
  providedIn: 'root'
})
export class SupplierOriginService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<SupplierOrigin[]> {
    const apiURL = `/SupplierOrigin/GetAll`;
    return await this.apiService.GetAsync<SupplierOrigin[]>(apiURL, Constants.srmApi);
  }

  async getAllByCustomerIdAsync(): Promise<SupplierOrigin[]> {
    const apiURL = `/SupplierOrigin/GetByCustomerId/`;
    return await this.apiService.GetAsync<SupplierOrigin[]>(apiURL, Constants.srmApi);
  }

  async createAsync(m: SupplierOrigin): Promise<any> {
    const apiURL = `/SupplierOrigin/create`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.srmApi);
  }

  async updateAsync(m: SupplierOrigin): Promise<any> {
    const apiURL = `/SupplierOrigin/update`;
    return await this.apiService.PutAsync<any>(apiURL, m, Constants.srmApi);
  }

  async deleteMultipleAsync(m: Number[]): Promise<any> {
    const apiURL = `/SupplierOrigin/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, m, Constants.srmApi);
  }
}

