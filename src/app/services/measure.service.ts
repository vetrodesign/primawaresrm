import { Injectable } from '@angular/core';
import { Measure } from 'app/models/measure.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class MeasureService {

  constructor(private apiService: ApiService) { }

  async getAllMeasuresAsync(): Promise<Measure[]> {
    const apiURL = `/measure/getAll`;
    return await this.apiService.GetAsync<Measure[]>(apiURL, Constants.commonApi);
  }

  async getAllMeasuresByCustomerIdAsync(): Promise<Measure[]> {
    const apiURL = `/measure/getAllByCustomer`;
    return await this.apiService.GetAsync<Measure[]>(apiURL, Constants.commonApi);
  }

  async createMeasureAsync(measure: Measure): Promise<any> {
    const apiURL = `/measure/create`;
    return await this.apiService.PostAsync<any>(apiURL, measure, Constants.commonApi);
  }

  async updateMeasureAsync(measure: Measure): Promise<any> {
    const apiURL = `/measure/update`;
    return await this.apiService.PutAsync<any>(apiURL, measure, Constants.commonApi);
  }

  async deleteMeasuresAsync(ids: number[]): Promise<any> {
    const apiURL = `/measure/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
