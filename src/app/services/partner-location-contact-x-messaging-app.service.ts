import { Injectable } from '@angular/core';
import { PartnerLocationContactXMessagingApp } from 'app/models/partnerlocationcontactxmessagingapp.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})

export class PartnerLocationContactXMessagingAppService {

  constructor(private apiService: ApiService) { }

  async getAllPartnerLocationContactXMessagingAppAsync(): Promise<any[]> {
    const apiURL = `/partnerLocationContactXMessagingApp/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getByPartnerLocationContactIdAsync(partnerLocationContactId: number): Promise<any[]> {
    const apiURL = `/PartnerLocationContactXMessagingApp/getByPartnerLocationContactId`;
    return await this.apiService.PostAsync<any[]>(apiURL, partnerLocationContactId, Constants.mrkApi);
  }

  async createPartnerLocationContactXMessagingAppAsync(p: PartnerLocationContactXMessagingApp): Promise<any> {
    const apiURL = `/PartnerLocationContactXMessagingApp/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async createMultiplePartnerLocationContactXMessagingAppAsync(p: PartnerLocationContactXMessagingApp[]): Promise<any> {
    const apiURL = `/PartnerLocationContactXMessagingApp/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updatePartnerLocationContactXMessagingAppAsync(p: PartnerLocationContactXMessagingApp): Promise<any> {
    const apiURL = `/PartnerLocationContactXMessagingApp/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deleteMultiplePartnerLocationContactXMessagingAppsAsync(p: PartnerLocationContactXMessagingApp[]): Promise<any> {
    const apiURL = `/PartnerLocationContactXMessagingApp/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }
}
