import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerDailyReceipts } from 'app/models/partner-daily-receipts.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerDailyReceiptsService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<PartnerDailyReceipts[]> {
    const apiURL = `/partnerdailyreceipts/getall`;
    return await this.apiService.GetAsync<PartnerDailyReceipts[]>(apiURL, Constants.mrkApi);
  }
}
