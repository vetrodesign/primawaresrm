import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ItemGroupCategory } from 'app/models/itemgroupcategory.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemGroupCategoryService {

  constructor(private apiService: ApiService) { }

  async getAllItemGroupCategorysAsync(): Promise<any[]> {
    const apiURL = `/itemgroupcategory/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getItemGroupCategoryAsyncByID(): Promise<any[]> {
    const apiURL = `/itemgroupcategory/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async createItemGroupCategoryAsync(itemgroupcategory: ItemGroupCategory): Promise<any> {
    const apiURL = `/itemgroupcategory/create`;
    return await this.apiService.PostAsync<any>(apiURL, itemgroupcategory, Constants.mrkApi);
  }

  async updateItemGroupCategoryAsync(itemgroupcategory: ItemGroupCategory): Promise<any> {
    const apiURL = `/itemgroupcategory/update`;
    return await this.apiService.PutAsync<any>(apiURL, itemgroupcategory, Constants.mrkApi);
  }

  async deleteItemGroupCategoryAsync(ids: number[]): Promise<any> {
    const apiURL = `/itemgroupcategory/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}