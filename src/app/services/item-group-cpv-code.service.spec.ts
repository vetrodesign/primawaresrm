import { TestBed } from '@angular/core/testing';

import { ItemGroupCpvCodeService } from './item-group-cpv-code.service';

describe('ItemGroupCpvCodeService', () => {
  let service: ItemGroupCpvCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemGroupCpvCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
