import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { CaenCode } from 'app/models/caencode.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CaenCodeService {
  constructor(private apiService: ApiService) { }

  async getAllCaenCodesAsync(): Promise<CaenCode[]> {
    const apiURL = `/caencode/getall`;
    return await this.apiService.GetAsync<CaenCode[]>(apiURL, Constants.commonApi);
  }

  async createCaenCodeAsync(caenCode: CaenCode): Promise<any> {
    const apiURL = `/caencode/create`;
    return await this.apiService.PostAsync<any>(apiURL, caenCode, Constants.commonApi);
  }

  async updateCaenAsync(caenCode: CaenCode): Promise<any> {
    const apiURL = `/caencode/update`;
    return await this.apiService.PutAsync<any>(apiURL, caenCode, Constants.commonApi);
  }

  async deleteCaenAsync(ids: number[]): Promise<any> {
    const apiURL = `/caencode/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
