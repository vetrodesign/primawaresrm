import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerObservation } from 'app/models/partner-observation';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerObservationService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<PartnerObservation[]> {
    const apiURL = `/partnerobservation/getall`;
    return await this.apiService.GetAsync<PartnerObservation[]>(apiURL, Constants.mrkApi);
  }

  async getByPartnerID(partnerId: number): Promise<PartnerObservation[]> {
    const apiURL = `/partnerobservation/getbypartnerid/` + partnerId;
    return await this.apiService.GetAsync<PartnerObservation[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(cir: PartnerObservation): Promise<any> {
    const apiURL = `/partnerobservation/create`;
    return await this.apiService.PostAsync<any>(apiURL, cir, Constants.mrkApi);
  }

  async updateAsync(cir: PartnerObservation): Promise<any> {
    const apiURL = `/partnerobservation/update`;
    return await this.apiService.PutAsync<any>(apiURL, cir, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnerobservation/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  async getByCreatedByAndDate(ids:number[], startDate:Date, endDate: Date) {
    let filter = {ids:ids, startDate:startDate, endDate:endDate};
    const apiURL = `/partnerobservation/getbycreatedbyanddate`;
    return await this.apiService.PostAsync(apiURL, filter, Constants.mrkApi);
  }
}
