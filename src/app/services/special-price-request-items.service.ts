import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SpecialPriceRequestItemsService {

  constructor(private apiService: ApiService) { }

  async getItemPrices(itemId: number): Promise<any> {
    const apiURL = `/SpecialPriceRequestItems/getItemPrices`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }
}
