import { TestBed } from '@angular/core/testing';

import { ItemEcommerceService } from './item-ecommerce.service';

describe('ItemEcommerceService', () => {
  let service: ItemEcommerceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemEcommerceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
