import { Injectable } from '@angular/core';
import { EshopLanguage } from 'app/models/eshoplanguage.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})

export class EshopLanguageService {
  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<EshopLanguage> {
    const apiURL = `/eshoplanguage/getAll`;
    return await this.apiService.GetAsync<EshopLanguage>(apiURL, Constants.mrkApi);
  }

  async getByCustomerIdAsync(): Promise<EshopLanguage[]> {
    const apiURL = `/eshoplanguage/getByCustomerId`;
    return await this.apiService.GetAsync<EshopLanguage[]>(apiURL, Constants.mrkApi);
  }

  async createMultipleAsync(ec: EshopLanguage[]): Promise<any> {
    const apiURL = `/eshoplanguage/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, ec, Constants.mrkApi);
  }


  async updateAsync(ec: EshopLanguage[]): Promise<any> {
    const apiURL = `/eshoplanguage/update`;
    return await this.apiService.PutAsync<any>(apiURL, ec, Constants.mrkApi);
  }

  async deleteMultipleAsync(ec: EshopLanguage[]): Promise<any> {
    const apiURL = `/eshoplanguage/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, ec, Constants.mrkApi);
  }
}
