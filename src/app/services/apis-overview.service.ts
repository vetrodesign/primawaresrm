import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { ApiOverview } from 'app/models/api-overview.model';
import { ApiLogs } from 'app/models/api-logs.model';

@Injectable({
  providedIn: 'root'
})
export class ApisOverviewService {
  constructor(private apiService: ApiService) { }

  async getAllApisAsync(): Promise<ApiOverview[]> {
    const apiURL = `/apioverview/getall`;
    return await this.apiService.GetAsync<ApiOverview[]>(apiURL, Constants.ssoApi);
  }

  async getApiByIdAsync(id: number): Promise<ApiOverview> {
    const apiURL = `/apioverview/GetById?id=${id}`;
    return await this.apiService.GetAsync<ApiOverview>(apiURL, Constants.ssoApi);
  }

  async getApiLogs(id: number): Promise<ApiLogs[]> {
    const apiURL = `/apioverview/GetLogsById?id=${id}`;
    return await this.apiService.GetAsync<ApiLogs[]>(apiURL, Constants.ssoApi);
  }

  async getAllApiLogsAsync(): Promise<ApiLogs[]> {
    const apiURL = `/apioverview/getallApiLogs`;
    return await this.apiService.GetAsync<ApiLogs[]>(apiURL, Constants.ssoApi);
  }

  async createLogs(apiOverviewVM: ApiOverview): Promise<ApiLogs[]> {
    const apiURL = `/apioverview/Create`;
    return await this.apiService.PostAsync<ApiLogs[]>(apiURL, apiOverviewVM, Constants.ssoApi);
  }

  async updateLogs(apiOverviewVM: ApiOverview): Promise<ApiLogs[]> {
    const apiURL = `/apioverview/Update`;
    return await this.apiService.PutAsync<ApiLogs[]>(apiURL, apiOverviewVM, Constants.ssoApi);
  }

  async getWeatherForecastAsync(): Promise<any> {
    const apiURL = `/apioverview/getWeatherForecast`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.ssoApi);
  }

  async getEuriborData(): Promise<any> {
    const apiURL = `/apioverview/getEuriborData`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.ssoApi);
  }

  async getLastRunDate(id: number): Promise<Date> {
    const apiURL = `/apioverview/GetLogsById?id=${id}`;
    const logs = await this.apiService.GetAsync<ApiLogs[]>(apiURL, Constants.ssoApi);

    const today = new Date();
    today.setHours(0,0,0,0);

    const recentLogs = logs.filter(log => {
      const logDate = new Date(log.created);
      logDate.setHours(0,0,0,0);
      return logDate.getTime() === today.getTime();
    });

    if (recentLogs.length > 0) {
      return recentLogs[0].created;
    }

    return null;
  }

  constructDataForApiServer(apiServiceVM: ApiOverview) {
    this.getApiLogs(apiServiceVM.id)
      .then(apiLogs => {
        const lastRunDate = apiLogs.sort((a, b) => b.id - a.id).slice(0, 1)[0].created;
        apiServiceVM.lastRunDate = lastRunDate;
        const lastRunLogs = apiLogs.filter(s => {
          const created = new Date(s.created);
          const lastRunDateObj = new Date(lastRunDate);
          return created.getDay() === lastRunDateObj.getDay() && created.getMonth() === lastRunDateObj.getMonth() && created.getFullYear() === lastRunDateObj.getFullYear();
        });
        apiServiceVM.hasError = lastRunLogs.some(s => s.logMessage.toLowerCase().includes("error") || s.logMessage.toLowerCase().includes("fail"));
      });
  }
  

}
