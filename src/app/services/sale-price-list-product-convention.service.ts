import { Injectable } from '@angular/core';
import { SalePriceListXProductConvention } from 'app/models/salepricelistxproductconvention.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SalePriceListProductConventionService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/salepricelistxproductconvention/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(p: SalePriceListXProductConvention): Promise<any> {
    const apiURL = `/salepricelistxproductconvention/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async createMultipleAsync(p: SalePriceListXProductConvention[]): Promise<any> {
    const apiURL = `/salepricelistxproductconvention/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updateAsync(p: SalePriceListXProductConvention): Promise<any> {
    const apiURL = `/salepricelistxproductconvention/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deleteAsync(p: SalePriceListXProductConvention[]): Promise<any> {
    const apiURL = `/salepricelistxproductconvention/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }
}
