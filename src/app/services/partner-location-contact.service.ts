import { Injectable } from '@angular/core';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class PartnerLocationContactService {
  constructor(private apiService: ApiService) { }

  async getAllPartnerLocationContactsAsync(): Promise<PartnerLocationContact[]> {
    const apiURL = `/partnerlocationcontact`;
    return await this.apiService.GetAsync<PartnerLocationContact[]>(apiURL, Constants.mrkApi);
  }

  async getPartnerLocationContactByID(id): Promise<PartnerLocationContact> {
    const apiURL = `/partnerlocationcontact/getbyid/` + id;
    return await this.apiService.GetAsync<PartnerLocationContact>(apiURL, Constants.mrkApi);
  }

  async getPartnerLocationContactByPartnerID(partnerId): Promise<PartnerLocationContact[]> {
    const apiURL = `/partnerlocationcontact/getbypartnerid/` + partnerId;
    return await this.apiService.GetAsync<PartnerLocationContact[]>(apiURL, Constants.mrkApi);
  }

  async getPartnerLocationContactIncludingInactiveByPartnerID(partnerId): Promise<PartnerLocationContact[]> {
    const apiURL = `/partnerlocationcontact/GetByPartnerIdIncludingInactive/` + partnerId;
    return await this.apiService.GetAsync<PartnerLocationContact[]>(apiURL, Constants.mrkApi);
  }

  async createPartnerLocationContactAsync(partnerLocationContact: PartnerLocationContact): Promise<any> {
    const apiURL = `/partnerlocationcontact/create`;
    return await this.apiService.PostAsync<any>(apiURL, partnerLocationContact, Constants.mrkApi);
  }

  async activatePartnerContactAsync(contactId: number): Promise<any> {
    const apiURL = `/partnerlocationcontact/activate`;
    return await this.apiService.PutAsync<any>(apiURL, contactId, Constants.mrkApi);
  }

  async updatePartnerLocationContactAsync(partnerLocationContact: PartnerLocationContact): Promise<any> {
    const apiURL = `/partnerlocationcontact/update`;
    return await this.apiService.PutAsync<any>(apiURL, partnerLocationContact, Constants.mrkApi);
  }

  async deletePartnerLocationContactAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnerlocationcontact/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
