import { TestBed } from '@angular/core/testing';

import { GeneralOfferService } from './general-offer.service';

describe('GeneralOfferService', () => {
  let service: GeneralOfferService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GeneralOfferService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
