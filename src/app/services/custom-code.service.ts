import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { CustomCode } from 'app/models/customcode.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CustomCodeService {

  constructor(private apiService: ApiService) { }

  async getAllCustomCodesAsync(): Promise<any[]> {
    const apiURL = `/customcode/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.commonApi);
  }

  async createCustomCodeAsync(customCode: CustomCode): Promise<any> {
    const apiURL = `/customcode/create`;
    return await this.apiService.PostAsync<any>(apiURL, customCode, Constants.commonApi);
  }

  async updateCustomCodeAsync(customCode: CustomCode): Promise<any> {
    const apiURL = `/customcode/update`;
    return await this.apiService.PutAsync<any>(apiURL, customCode, Constants.commonApi);
  }

  async deleteCustomCodeAsync(ids: number[]): Promise<any> {
    const apiURL = `/customcode/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
