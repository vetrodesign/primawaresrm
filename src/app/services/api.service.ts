import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/toPromise';
import { LogException } from 'app/models/logexception.model';
import { Constants } from 'app/constants';
import { NotificationService } from './notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';

@Injectable()
export class ApiService {
  constructor(private http: HttpClient, private authService: AuthService, private notificationService: NotificationService) {
  }

  private getApiUrl(apiURL: string, serviceType: string): string {
    const basePath = environment.useApiGateway ? environment.apiGatewayUrl + environment.apiPaths[serviceType] : environment[serviceType];
    return `${basePath}${apiURL}`;
  }

  public async GetAsync<T>(apiURL: string, serviceType?: string): Promise<T> {
    const start = performance.now();
    const result = await this.http.get(this.getApiUrl(apiURL, serviceType), { headers: this.getHeaders() })
      .toPromise()
      .catch(e => {
        const end = performance.now();
        const time = (end - start) + ' ms.';
        this.logException('GET', apiURL, e, null, time); });
    return <T>result;
  }

  public async PostAsync<T>(apiURL: string, body: any, serviceType?: string): Promise<T> {
    const start = performance.now();
    const result = await this.http.post(this.getApiUrl(apiURL, serviceType), body, { headers: this.getHeaders() })
      .toPromise()
      .catch(e => {
        this.notificationService.alert('top', 'center', `A aparut o eroare!  ${e}`,
          NotificationTypeEnum.Red, true)
        const end = performance.now();
        const time = (end - start) + ' ms.';
        this.logException('POST', apiURL, e, body, time); });

    return <T>result;
  }

  public async PutAsync<T>(apiURL: string, body: any, serviceType?: string): Promise<T> {
    const start = performance.now();
    const result = await this.http.put(this.getApiUrl(apiURL, serviceType), body, { headers: this.getHeaders() })
      .toPromise()
      .catch(e => {
        const end = performance.now();
        const time = (end - start) + ' ms.';
        this.logException('PUT', apiURL, e, body, time); });

    return <T>result;
  }

  public async DeleteSingleAsync<T>(apiURL: string, serviceType?: string): Promise<T> {
    const start = performance.now();
    const result = await this.http.delete(this.getApiUrl(apiURL, serviceType), { headers: this.getHeaders() })
      .toPromise()
      .catch(e => {
        const end = performance.now();
        const time = (end - start) + ' ms.';
        this.logException('DELETE SINGLE', apiURL, e, null, time); });

    return <T>result;
  }

  public async DeleteMultipleAsync<T>(apiURL: string, body: any, serviceType?: string): Promise<T> {
    const start = performance.now();
    const result = await this.http.post(this.getApiUrl(apiURL, serviceType), body, { headers: this.getHeaders() })
      .toPromise()
      .catch(e => {
        const end = performance.now();
        const time = (end - start) + ' ms.';
        this.logException('DELETE MULTIPLE', apiURL, e, body, time); });

    return <T>result;
  }

  private getHeaders(): HttpHeaders {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Cache-Control', 'no-cache');
    headers = headers.append('Pragma', 'no-cache');
    headers = headers.append('Expires', 'Sat, 01 Jan 2000 00:00:00 GMT');
    headers = headers.append('If-Modified-Since', '0');
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', 'Bearer ' + this.authService.getToken());
    let copyHeader = headers;
    return copyHeader;
  }

  private logException(actionType: string, apiURL: string, e: any, body?: any, performance?: string) {
    let item = new LogException();
    item.ActionType = actionType + ' - ' + apiURL;
    item.Exception = e;
    item.Body = JSON.stringify(body);
    item.User = this.authService.getUserUserName();
    item.Customer = this.authService.getUserCustomerId();
    item.Performance = performance;

    //const basePath = environment.useApiGateway ? environment.apiGatewayUrl + environment.apiPaths[Constants.ssoApi] : environment[Constants.ssoApi];
    //let result = this.http.post<any>(`${basePath}/logexception/create`, item);
  }
}
