import { Injectable } from '@angular/core';
import { Size } from 'app/models/size.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class SizeService {

  constructor(private apiService: ApiService) { }

  async getAllSizesAsync(): Promise<Size[]> {
    const apiURL = `/size/getAll`;
    return await this.apiService.GetAsync<Size[]>(apiURL, Constants.commonApi);
  }

  async getAllSizesByCustomerIdAsync(): Promise<Size[]> {
    const apiURL = `/size/getAllByCustomer`;
    return await this.apiService.GetAsync<Size[]>(apiURL, Constants.commonApi);
  }

  async createSizeAsync(size: Size): Promise<any> {
    const apiURL = `/size/create`;
    return await this.apiService.PostAsync<any>(apiURL, size, Constants.commonApi);
  }

  async updateSizeAsync(size: Size): Promise<any> {
    const apiURL = `/size/update`;
    return await this.apiService.PutAsync<any>(apiURL, size, Constants.commonApi);
  }

  async deleteSizesAsync(ids: number[]): Promise<any> {
    const apiURL = `/size/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
