import { Injectable } from '@angular/core';
import { EshopItem } from 'app/models/eshopitem.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})

export class EshopItemService {
  constructor(private apiService: ApiService) { }

  async getAllByItemIdAsync(itemId: number): Promise<EshopItem[]> {
    const apiURL = `/EshopItem/getAllByItemId?itemId=${itemId}`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }
}
