import { TestBed } from '@angular/core/testing';

import { PartnerWebsiteService } from './partner-website.service';

describe('PartnerWebsiteService', () => {
  let service: PartnerWebsiteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerWebsiteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
