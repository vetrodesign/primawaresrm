import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { SupplierPriceList } from 'app/models/supplierpricelist.model';
import { SupplierPriceListItem } from 'app/models/supplierpricelistitem.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SupplierPriceListItemService {

  constructor(private apiService: ApiService) { }

  async getSupplierPriceListItemsAsyncByID(id): Promise<any[]> {
    const apiURL = `/supplierpricelistitem/getbysupplierpricelistid/` + id;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async createSupplierPriceListItemAsync(supplierListItem: SupplierPriceListItem): Promise<any> {
    const apiURL = `/supplierpricelistitem/create`;
    return await this.apiService.PostAsync<any>(apiURL, supplierListItem, Constants.mrkApi);
  }

  async updateSupplierPriceListItemAsync(supplierListItem: SupplierPriceListItem): Promise<any> {
    const apiURL = `/supplierpricelistitem/update`;
    return await this.apiService.PutAsync<any>(apiURL, supplierListItem, Constants.mrkApi);
  }

  async deleteSupplierPriceListItemsAsync(ids: number[]): Promise<any> {
    const apiURL = `/supplierpricelistitem/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
