import { Injectable } from '@angular/core';
import { ItemXRelated } from 'app/models/itemxrelated.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemRelatedService {

  constructor(private apiService: ApiService) { }

  async getItemXRelatedByItemIdAsync(itemId: number): Promise<any[]> {
    const apiURL = `/itemxrelated/GetByItemId`;
    return await this.apiService.PostAsync<any[]>(apiURL, itemId, Constants.mrkApi);
  }

  async createItemXRelatedAsync(i: ItemXRelated): Promise<any> {
    const apiURL = `/itemxrelated/create`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async createMultipleAsync(m: ItemXRelated[]): Promise<any> {
    const apiURL = `/itemxrelated/CreateMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async updateItemXRelatedAsync(i: ItemXRelated): Promise<any> {
    const apiURL = `/itemxrelated/update`;
    return await this.apiService.PutAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async deleteItemXRelatedAsync(ids: number[]): Promise<any> {
    const apiURL = `/itemxrelated/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
