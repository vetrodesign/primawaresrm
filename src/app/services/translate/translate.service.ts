import { Injectable, Inject } from '@angular/core';
import { TRANSLATIONS } from './translation';
import { langRoName } from './lang-ro';
import * as _ from 'lodash';
import { Language } from '../../models/language.model';
import { langEnName } from './lang-en';
import { Constants } from '../../constants';
import { BehaviorSubject } from 'rxjs';
import { NotificationService } from '../notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { SetLanguageService } from '../set-language.service';

@Injectable()
export class TranslateService {
  public langChangedSubject: BehaviorSubject<boolean>;
  public allLangs: Language[];
  private _currentLang: string;
  private _currentLangID: number;

  public get currentLang() {
    return this._currentLang;
  }

  public get currentLangID() {
    return this._currentLangID;
  }

  constructor(@Inject(TRANSLATIONS) private _translations: any, private notificationService: NotificationService,
  private setLangService: SetLanguageService) {
    this.langChangedSubject = new BehaviorSubject<boolean>(false);
    if (JSON.parse(localStorage.getItem(Constants.clientLang)) != null) {
      this._currentLang = JSON.parse(localStorage.getItem(Constants.clientLang));
      localStorage.setItem(Constants.clientLang, JSON.stringify(this._currentLang));
    } else {
      this._currentLang = langRoName;
    }
  }

 // set user selected language
  public use(lang: Language): void {
    this._currentLang = lang.code;
    this._currentLangID = lang.id;
    if (JSON.parse(localStorage.getItem(Constants.clientLang)) != null) {
      localStorage.removeItem(Constants.clientLang);
      localStorage.setItem(Constants.clientLang, JSON.stringify(this._currentLang));
    } else {
      localStorage.setItem(Constants.clientLang, JSON.stringify(this._currentLang));
    }
    this.setLangService.setLanguage(lang.culture).then(i => {
      this.notificationService.alert('top', 'right',
      this.instant('langChange')  + this.instant(this._currentLang) + '!', NotificationTypeEnum.Blue);
    });
    this.langChangedSubject.next(true);
  }

   //  perform translation with selected lang
  public instant(key: string): string {
    if (this._translations[this.currentLang]) {
      const translationValue = _.get(this._translations[this.currentLang], key);
      if (translationValue) {
        return <string>translationValue;
      }
    }
    return key;
  }

  // maybe we will get values from database
  public getLangs(): Language[] {
    if (this.allLangs) {
      return this.allLangs;
    } else {
      const ro: Language = {
        id: 1,
        code: 'RO',
        name: 'romania',
        culture: 'ro-RO',
        flagClass: 'flag-icon flag-icon-ro'
      };
      const en: Language = {
        id: 2,
        code: 'EN',
        name: 'english',
        culture: 'en-US',
        flagClass: 'flag-icon flag-icon-us'
      };
      this.allLangs = [ro, en];
      return this.allLangs;
    }
  }

  public getCurrentLang(): string {
    return JSON.parse(localStorage.getItem(Constants.clientLang));
 }
}
