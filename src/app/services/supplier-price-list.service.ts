import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { SupplierPriceList } from 'app/models/supplierpricelist.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SupplierPriceListService {

  constructor(private apiService: ApiService) { }

  async getAllSupplierPriceListsAsync(): Promise<any[]> {
    const apiURL = `/supplierpricelist/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getSupplierPriceListsAsyncByID(): Promise<any[]> {
    const apiURL = `/supplierpricelist/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getSupplierPriceListsByPartnerId(id: number): Promise<any> {
    const apiURL = `/supplierpricelist/GetByPartnerId`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, id, Constants.mrkApi);
  }

  async createSupplierPriceListAsync(supplierList: SupplierPriceList): Promise<any> {
    const apiURL = `/supplierpricelist/create`;
    return await this.apiService.PostAsync<any>(apiURL, supplierList, Constants.mrkApi);
  }

  async updateSupplierPriceListAsync(supplierList: SupplierPriceList): Promise<any> {
    const apiURL = `/supplierpricelist/update`;
    return await this.apiService.PutAsync<any>(apiURL, supplierList, Constants.mrkApi);
  }

  async deleteSupplierPriceListsAsync(ids: number[]): Promise<any> {
    const apiURL = `/supplierpricelist/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
