import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Role } from '../models/role.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private apiService: ApiService) { }

  async getAllRolesAsync(): Promise<Role[]> {
    const apiURL = `/role`;
    return await this.apiService.GetAsync<Role[]>(apiURL, Constants.ssoApi);
  }

  async getRolesAsyncByID(customerId): Promise<Role[]> {
    const apiURL = `/role/getbycustomer/` + customerId;
    return await this.apiService.GetAsync<Role[]>(apiURL, Constants.ssoApi);
  }

  async getRolesAsyncByUserIdAndSiteId(userId: number): Promise<number[]> {
    const apiURL = `/role/GetRoles/` + userId;
    return await this.apiService.GetAsync<number[]>(apiURL, Constants.ssoApi);
  }

  async createRoleAsync(role: Role): Promise<any> {
    const apiURL = `/role`;
    return await this.apiService.PostAsync<any>(apiURL, role, Constants.ssoApi);
  }

  async updateRoleAsync(role: Role): Promise<any> {
    const apiURL = `/role/` + role.id;
    return await this.apiService.PutAsync<any>(apiURL, role, Constants.ssoApi);
  }

  async deleteRoleAsync(id: number): Promise<any> {
    const apiURL = `/role/` + id;
    return await this.apiService.DeleteSingleAsync<any>(apiURL, Constants.ssoApi);
  }
}
