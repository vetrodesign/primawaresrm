import { Injectable } from '@angular/core';
import { SalesTargetXPost } from 'app/models/saletargetxpost.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SalesTargetPostService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/salesTargetXPost/GetAll`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async getAllByPostIdAsync(postId: number): Promise<any[]> {
    const apiURL = `/salesTargetXPost/GetAllByPostId`;
    return await this.apiService.PostAsync<any>(apiURL, postId, Constants.mrkApi);
  }

  async createAsync(st: SalesTargetXPost): Promise<any> {
    const apiURL = `/salesTargetXPost/create`;
    return await this.apiService.PostAsync<any>(apiURL, st, Constants.mrkApi);
  }

  async updateAsync(st: SalesTargetXPost): Promise<any> {
    const apiURL = `/salesTargetXPost/update`;
    return await this.apiService.PutAsync<any>(apiURL, st, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/salesTargetXPost/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  async createMultipleAsync(s: SalesTargetXPost[]): Promise<any> {
    const apiURL = `/salesTargetXPost/CreateMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, s, Constants.mrkApi);
  }

  async deleteMultipleAsync(s: SalesTargetXPost[]): Promise<any> {
    const apiURL = `/salesTargetXPost/DeleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, s, Constants.mrkApi);
  }
}
