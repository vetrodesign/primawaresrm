import { Injectable } from '@angular/core';
import { Color } from 'app/models/color.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  constructor(private apiService: ApiService) { }

  async getAllColorsAsync(): Promise<Color[]> {
    const apiURL = `/color/getAll`;
    return await this.apiService.GetAsync<Color[]>(apiURL, Constants.commonApi);
  }

  async getAllColorsByCustomerIdAsync(): Promise<Color[]> {
    const apiURL = `/color/getAllByCustomer`;
    return await this.apiService.GetAsync<Color[]>(apiURL, Constants.commonApi);
  }

  async createColorAsync(color: Color): Promise<any> {
    const apiURL = `/color/create`;
    return await this.apiService.PostAsync<any>(apiURL, color, Constants.commonApi);
  }

  async updateColorAsync(color: Color): Promise<any> {
    const apiURL = `/color/update`;
    return await this.apiService.PutAsync<any>(apiURL, color, Constants.commonApi);
  }

  async deleteColorsAsync(ids: number[]): Promise<any> {
    const apiURL = `/color/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
