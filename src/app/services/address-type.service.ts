import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { AddressType } from 'app/models/address-type.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class AddressTypeService {
  constructor(private apiService: ApiService) { }
  async getAllAddressTypesAsync(): Promise<AddressType[]> {
    const apiURL = `/addresstype/getall`;
    return await this.apiService.GetAsync<AddressType[]>(apiURL, Constants.commonApi);
  }

  async getAllAddressTypesSmallAsync(): Promise<AddressType[]> {
    const apiURL = `/addresstype/getallsmall`;
    return await this.apiService.GetAsync<AddressType[]>(apiURL, Constants.commonApi);
  }

  async createAddressTypeAsync(addressType: AddressType): Promise<any> {
    const apiURL = `/addresstype/create`;
    return await this.apiService.PostAsync<any>(apiURL, addressType, Constants.commonApi);
  }

  async updateAddressTypeAsync(addressType: AddressType): Promise<any> {
    const apiURL = `/addresstype/update`;
    return await this.apiService.PutAsync<any>(apiURL, addressType, Constants.commonApi);
  }

  async deleteAddressTypesAsync(ids: number[]): Promise<any> {
    const apiURL = `/addresstype/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
