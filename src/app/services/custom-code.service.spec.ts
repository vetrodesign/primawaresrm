import { TestBed } from '@angular/core/testing';

import { CustomCodeService } from './custom-code.service';

describe('CustomCodeService', () => {
  let service: CustomCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
