import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { MeasurementUnit } from 'app/models/measurementunit.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class MeasurementUnitService {

  constructor(private apiService: ApiService) { }

  async getAllMUAsync(): Promise<MeasurementUnit[]> {
    const apiURL = `/measurementunit/getall`;
    return await this.apiService.GetAsync<MeasurementUnit[]>(apiURL, Constants.commonApi);
  }

  async getAllMUByCustomerIdAsync(): Promise<MeasurementUnit[]> {
    const apiURL = `/measurementunit/getallbycustomer`;
    return await this.apiService.GetAsync<MeasurementUnit[]>(apiURL, Constants.commonApi);
  }

  async createMUAsync(mu: MeasurementUnit): Promise<any> {
    const apiURL = `/measurementunit/create`;
    return await this.apiService.PostAsync<any>(apiURL, mu, Constants.commonApi);
  }

  async updateMUAsync(mu: MeasurementUnit): Promise<any> {
    const apiURL = `/measurementunit/update`;
    return await this.apiService.PutAsync<any>(apiURL, mu, Constants.commonApi);
  }

  async deleteMeasurementUnitsAsync(ids: number[]): Promise<any> {
    const apiURL = `/measurementunit/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
