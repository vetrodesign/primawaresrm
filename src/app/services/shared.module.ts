import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersService } from './user.service';
import { ApiService } from './api.service';
import { AuthService } from './auth.service';
import { JwtHelper } from 'angular2-jwt';
import { HelperService } from './helper.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptor } from 'app/helpers/error.interceptor';
import { NotificationService } from './notification.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from './translate/translate.module';
import { TranslateService } from './translate/translate.service';
import { TRANSLATION_PROVIDERS } from './translate';
import { RolesService } from './roles.service';
import { CustomersService } from './customers.service';
import { ClientModuleService } from './client-module.service';
import { NotificationsComponent } from 'app/notifications/notifications.component';
import { ClientModuleMenuItemsService } from './client-module-menu-items.service';
import { RoleRightsService } from './role-rights.service';
import { PostService } from './post.service';
import { DepartmentsService } from './departments.service';
import { LanguageService } from './language.service';
import { CountryService } from './country.service';
import { CountyService } from './county.service';
import { PartnerService } from './partner.service';
import { CurrencyRateService } from './currency-rate.service';
import { MeasurementUnitService } from './measurement-unit.service';
import { CountryGroupService } from './country-group.service';
import { CountryGroupXcountryService } from './country-group-xcountry.service';
import { SetLanguageService } from './set-language.service';
import { PaymentInstrumentService } from './payment-instrument.service';
import { SupplierPriceListService } from './supplier-price-list.service';
import { CityService } from './city.service';
import { CaenCodeService } from './caen-code.service';
import { SupplierPriceListItemService } from './supplier-price-list-item.service';
import { PageActionService } from './page-action.service';
import { PartnerLegalFormService } from './partner-legal-form.service';
import { SalesZoneService } from './sales-zone.service';
import { CaenCodeSectionService } from './caen-code-section.service';
import { CaenCodeCategoryService } from './caen-code-category.service';
import { CaenCodeDomainService } from './caen-code-domain.service';
import { CaenCodeSpecializationService } from './caen-code-specialization.service';
import { PartnerLocationContactService } from './partner-location-contact.service';
import { PartnerActivityService } from './partner-activity.service';
import { PartnerActivityAllocationService } from './partner-activity-allocation.service';
import { CaenCodeSpecializationPartnerLocationService } from './caen-code-specialization-partner-location.service';
import { PartnerClientCreditControlService } from './partner-client-credit-control.service';
import { PartnerSupplierCreditControlService } from './partner-supplier-credit-control.service';
import { SalePriceListItemService } from './sale-price-list-item.service';
import { SalePriceListService } from './sale-price-list.service';
import { PartnerLocationSalePriceListService } from './partner-location-sale-price-list.service';
import { PartnerReviewService } from './partner-review.service';
import { SalePriceListProductConventionService } from './sale-price-list-product-convention.service';
import { ItemService } from './item.service';
import { ItemTypeService } from './item-type.service';
import { TaxService } from './tax.service';
import { BarCodeIntervalService } from './bar-code-interval.service';
import { ItemCaenCodeSpecializationService } from './item-caen-code-specialization.service';
import { BarCodeItemAllocationService } from './bar-code-item-allocation.service';
import { ItemGroupCpvCodeService } from './item-group-cpv-code.service';
import { PartnerFinancialInfoService } from './partner-financial-info.service';
import { PartnerCipCheckService } from './partner-cip-check.service';
import { CustomCodeService } from './custom-code.service';
import { CustomCodeXtaxRateService } from './custom-code-xtax-rate.service';
import { SiteService } from './site.service';
import { TagService } from './tag.service';
import { ItemTagService } from './item-tag.service';
import { PartnerTagService } from './partner-tag.service';
import { ItemEcommerceService } from './item-ecommerce.service';
import { ItemPropertiesService } from './item-properties.service';
import { ColorService } from './color.service';
import { SizeService } from './size.service';
import { ShapeService } from './shape.service';
import { CutService } from './cut.service';
import { ClosingService } from './closing.service';
import { MeasureService } from './measure.service';
import { MaterialService } from './material.service';
import { DeliverytimeService } from './deliverytime.service';
import { ItemRelatedService } from './item-related.service';
import { PartnerLocationItemGroupCategoryService } from './partner-location-item-group-category.service';
import { ItemGroupCategoryService } from './item-group-category.service';
import { ItemGroupCodeService } from './item-group-code.service';
import { ItemGroupCategoryCodeService } from './item-group-category-code.service';
import { PartnerLocationItemGroupCodeService } from './partner-location-item-group-code.service';
import { PartnerNameEquivalenceService } from './partner-name-equivalence.service';
import { OccupationService } from './occupation.service';
import { DailyReportService } from './daily-report.service';
import { PartnerContract } from 'app/models/partnerContract.model';
import { PartnerContractXSalePriceList } from 'app/models/partnerContractXSalePriceList.model';
import { ItemXClientMonitoringService } from './item-x-client-monitoring.service';
import { ItemMonitoringPartnerService } from './item-monitoring-partner.service';
import { PartnerWebsiteService } from './partner-website.service';
import { PartnerXItemGroupCodeService } from './partner-x-item-group-code.service';
import { ItemTurnoverService } from './item-turnover.service';
import { AuthorizationTypeService } from './authorization-type.service';
import { PartnerAuthorizationService } from './partner-authorization.service';

@NgModule({
  imports: [ CommonModule, FormsModule, ReactiveFormsModule, TranslateModule],
  declarations: [ NotificationsComponent],
  providers: [UsersService, ApiService, AuthService, DepartmentsService, LanguageService,
     SupplierPriceListItemService, ItemTypeService, BarCodeIntervalService, CustomCodeService,
     JwtHelper, HelperService, TranslateService, TRANSLATION_PROVIDERS, RolesService, CustomersService,
     CurrencyRateService, MeasurementUnitService, PaymentInstrumentService, PartnerFinancialInfoService, PartnerCipCheckService,
     CountryService, ClientModuleService, NotificationService, ClientModuleMenuItemsService, RoleRightsService,
     PostService, CountyService, PartnerService, ItemService, TaxService, ItemCaenCodeSpecializationService,
     CountryGroupService, CountryGroupXcountryService, SetLanguageService, BarCodeItemAllocationService,
     SupplierPriceListService, CityService, CaenCodeService, PageActionService, PartnerLegalFormService,
     SalesZoneService, CaenCodeSectionService, CaenCodeCategoryService, CaenCodeDomainService, CustomCodeXtaxRateService,
     CaenCodeSpecializationService, CaenCodeSpecializationPartnerLocationService, ItemGroupCpvCodeService,
     PartnerLocationContactService, PartnerClientCreditControlService, PartnerSupplierCreditControlService,
     PartnerActivityService, PartnerActivityAllocationService, SalePriceListItemService, SiteService, ItemGroupCategoryCodeService,
     SalePriceListService, PartnerLocationSalePriceListService, PartnerReviewService, ItemEcommerceService, PartnerLocationItemGroupCodeService,
     ItemPropertiesService, ColorService, SizeService, ShapeService, CutService, PartnerLocationItemGroupCategoryService,
     ClosingService, MeasureService, MaterialService, DeliverytimeService, ItemRelatedService, ItemGroupCodeService, OccupationService,
     SalePriceListProductConventionService, TagService, ItemTagService, PartnerTagService, ItemGroupCategoryService, PartnerNameEquivalenceService,
     DailyReportService, PartnerContract, PartnerContractXSalePriceList, ItemXClientMonitoringService, ItemMonitoringPartnerService,
      PartnerWebsiteService, PartnerXItemGroupCodeService, ItemTurnoverService, PartnerAuthorizationService, AuthorizationTypeService,
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
  exports: [NotificationsComponent]
})
export class SharedModule { }
