import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { Partner } from 'app/models/partner.model';
import { NotificationService } from 'app/services/notification.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';
import * as _ from 'lodash';
import { PartnerFinancialInfoVM } from 'app/models/partnerfinancialinfo.model';
import { PartnerFinancialInfoService } from 'app/services/partner-financial-info.service';
import { PartnerCIPCheckVM } from 'app/models/partnercipcheck.model';
import { PartnerCipCheckService } from 'app/services/partner-cip-check.service';
import { HelperService } from 'app/services/helper.service';
import { SupplierAcceptedService } from 'app/services/supplier-accepted.service';
import { SupplierAccepted } from 'app/models/supplier-accepted.model';
import { AuthService } from 'app/services/auth.service';
import { PartnerActivity } from 'app/models/partneractivity.model';
import { PartnerActivityAllocation } from 'app/models/partneractivityallocation.model';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { CaenCode } from 'app/models/caencode.model';
import { PartnerClientCreditControl } from 'app/models/partnerclientcreditcontrol.model';
import { PartnerContract } from 'app/models/partnerContract.model';
import { SupplierAcceptedStateEnum } from 'app/enums/supplierAcceptedStateEnum';

@Component({
  selector: 'app-partner-financial-info',
  templateUrl: './partner-financial-info.component.html',
  styleUrls: ['./partner-financial-info.component.css']
})
export class PartnerFinancialInfoComponent implements OnInit, AfterViewInit {
  @Input() partner: Partner;
  @Input() posts: any;
  @Input() users: any;
  @Input() partnerActivities: PartnerActivity[];
  @Input() partnerActivityAllocations: PartnerActivityAllocation[];
  @Input() partnerLocations: PartnerLocation[];
  @Input() rawCaenCodes: CaenCode[];
  @Input() citiesDS: any;
  @Input() countiesDS: any;
  @Input() countriesDS: any;
  @Input() partnerClientCreditControl: PartnerClientCreditControl;
  @Input() competitorTypes: any;
  @Input() partnerContracts: PartnerContract[];

  groupedText: string;
  partnerFinancialInfo: PartnerFinancialInfoVM[] =[];
  partnerCIPCheck: PartnerCIPCheckVM[] =[];
  lastCIPCheck: PartnerCIPCheckVM;
  selectedRows: any[];
  selectedCIPRows: any[];
  actions: any;
  cipActions: any;
  loaded: boolean;
  cipCheckPopup: boolean;
  selectedRowIndex = -1;
  selectedPartnerFinancialInfo: PartnerFinancialInfoVM;
  supplierAccepted: SupplierAccepted;
  supplierAcceptedBool: boolean;
  supplierAcceptedStateIcon: string;
  supplierAcceptedStateName: string;
  isSupplierAcceptedPopupVisible: boolean;
  supplierAcceptedOtherSupplierAcceptedStateIcons: { stateId: number, stateName: string, icon: string }[] = [];

  @ViewChild('partnerFinancialDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('partnerFinancialGridToolbar') gridToolbar: GridToolbarComponent;

  @ViewChild('cipDataGrid') cipDataGrid: DxDataGridComponent;
  @ViewChild('cipGridToolbar') cipGridToolbar: GridToolbarComponent;

  constructor(private notificationService: NotificationService, private helperService: HelperService,
    private translationService: TranslateService, private financialInfoService: PartnerFinancialInfoService, private supplierAcceptedService: SupplierAcceptedService, private authenticationService: AuthService,
    private cipCheckService: PartnerCipCheckService) {
    this.setActions();
    this.groupedText = this.translationService.instant('groupedText');
   }

   setActions() {
    this.actions = {
      CanView: true,
      CanAdd: false,
      CanUpdate: true,
      CanDelete: false,
      CanPrint: false,
      CanExport: true,
      CanImport: false,
      CanDuplicate: false
    };
    this.cipActions = {
      CanView: true,
      CanAdd: true,
      CanUpdate: true,
      CanDelete: false,
      CanPrint: false,
      CanExport: true,
      CanImport: false,
      CanDuplicate: false
    };
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  canExport() {
    return this.actions.CanExport;
  }

  async ngOnInit(): Promise<void> {
    this.getPartnerFinancialInfo().then(async response => {
      
    });
  }

  ngAfterViewInit() {
    this.setGridInstances();
  }

  async getPartnerFinancialInfo() {
    this.loaded = true;
    this.partnerFinancialInfo = [];
    if (this.partner && this.partner.id) {
      await this.financialInfoService.getByPartnerIdAsync(this.partner.id).then(items => {
        if (items) {
          this.partnerFinancialInfo = items;
        } else {
          this.partnerFinancialInfo = [];
        }
        this.loaded = false;
      });
      await this.cipCheckService.getByPartnerIdAsync(this.partner.id).then(items => {
        if (items) {
          this.partnerCIPCheck = items;
          this.lastCIPCheck = items[0];
        } else {
          this.partnerCIPCheck = [];
          this.lastCIPCheck = null;
        }
        this.loaded = false;
      });
    }
    this.getAcceptedSupplier();
  }

  async getAcceptedSupplier() {
    await this.supplierAcceptedService.getByPartnerIdAsync(this.partner.id).then(item => {
      this.supplierAccepted = item ? item : null;
      this.supplierAcceptedBool = item ? true : false;

      if (item) {
        switch (item.state) {
          case null:
            this.supplierAcceptedStateIcon = 'supplier-accepted-unverified.png';
            this.supplierAcceptedStateName = 'Furnizor Neverificat';

            const safeState = item?.state ?? SupplierAcceptedStateEnum.Unverified;
            this.supplierAcceptedOtherSupplierAcceptedStateIcons = this.getOtherSupplierAcceptedStateIcons(safeState);
            break;
          case SupplierAcceptedStateEnum.Approved:
            this.supplierAcceptedStateIcon = 'supplier-accepted-approved.png';
            this.supplierAcceptedStateName = 'Furnizor Aprobat';

            this.supplierAcceptedOtherSupplierAcceptedStateIcons = this.getOtherSupplierAcceptedStateIcons(SupplierAcceptedStateEnum.Approved);
            break;
          case SupplierAcceptedStateEnum.Unverified:
            this.supplierAcceptedStateIcon = 'supplier-accepted-unverified.png';
            this.supplierAcceptedStateName = 'Furnizor Neverificat';

            this.supplierAcceptedOtherSupplierAcceptedStateIcons = this.getOtherSupplierAcceptedStateIcons(SupplierAcceptedStateEnum.Unverified);
            break;
          case SupplierAcceptedStateEnum.Banned:
            this.supplierAcceptedStateIcon = 'supplier-accepted-banned.png';
            this.supplierAcceptedStateName = 'Furnizor Interzis';

            this.supplierAcceptedOtherSupplierAcceptedStateIcons = this.getOtherSupplierAcceptedStateIcons(SupplierAcceptedStateEnum.Banned);
            break;
          default:
            break;
        }
      } else {
        this.supplierAcceptedStateIcon = 'supplier-accepted-unverified.png';
        this.supplierAcceptedStateName = 'Furnizor Neverificat';

        this.supplierAcceptedOtherSupplierAcceptedStateIcons = this.getOtherSupplierAcceptedStateIcons(SupplierAcceptedStateEnum.Unverified);
      }
    })
  }

  getOtherSupplierAcceptedStateIcons(currentState: SupplierAcceptedStateEnum | null): { stateId: number, stateName: string, icon: string }[] {
    const stateIcons: Record<SupplierAcceptedStateEnum, string> = {
      [SupplierAcceptedStateEnum.Approved]: 'supplier-accepted-approved.png',
      [SupplierAcceptedStateEnum.Unverified]: 'supplier-accepted-unverified.png',
      [SupplierAcceptedStateEnum.Banned]: 'supplier-accepted-banned.png'
    };
  
    return Object.keys(stateIcons)
    .map(key => Number(key) as SupplierAcceptedStateEnum)
    .filter(state => state !== currentState) 
    .map(state => ({
      stateId: state,
      stateName: this.translationService.instant(`supplierAcceptedState.${SupplierAcceptedStateEnum[state]}`),
      icon: stateIcons[state]
    }));
  } 

  async supplierAcceptedEventChange(e) {
    if (e && e.value == true && this.supplierAccepted == null) {
      //create
        let sa = new SupplierAccepted();
        if (this.posts && this.posts.length && this.users && this.users.length) {
          let user = this.users.find(x => x.userName === this.authenticationService.getUserUserName());
          if (user) {
            sa.postId = this.posts.find(x => Number(x.id) === user.postId)?.id;
          }
        }
        sa.partnerId = this.partner.id;
        await this.supplierAcceptedService.createAsync(sa).then(x => {
          if (sa) {
            this.notificationService.alert('top', 'center', 'Furnizor Acceptat - Informatiile au fost create cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Furnizor Acceptat - Informatiile nu au fost create! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          }
          this.getAcceptedSupplier();
        });


    } else {
      if (e && e.value == false && this.supplierAccepted != null) {
        //delete
          let sa = new SupplierAccepted();
          if (this.posts && this.posts.length && this.users && this.users.length) {
            let user = this.users.find(x => x.userName === this.authenticationService.getUserUserName());
            if (user) {
              sa.postId = this.posts.find(x => Number(x.id) === user.postId)?.id;
            }
          }
          sa.partnerId = this.partner.id;
          let ids = [];
          ids.push(this.supplierAccepted.id);
          await this.supplierAcceptedService.deleteAsync(ids).then(x => {
            if (sa) {
              this.notificationService.alert('top', 'center', 'Furnizor Acceptat - Informatiile au fost sterse cu succes!',
                NotificationTypeEnum.Green, true)
            } else {
              this.notificationService.alert('top', 'center', 'Furnizor Acceptat - Informatiile nu au sterse! A aparut o eroare!',
                NotificationTypeEnum.Red, true)
            }
            this.getAcceptedSupplier();
          });
      }
    }
  }

  async updatePartnerFinancialInfoForTheLastYears() {
    if (this.partner && this.partner.id) {
      this.loaded = true;

      await this.financialInfoService.createMultipleForLastYearsAsync(this.partner.id).then(r => {
        this.refresh();

        this.notificationService.alert('top', 'center', 'Info Financiar Partener - Informatiile au fost updatate cu succes!',
        NotificationTypeEnum.Green, true)
        this.loaded = false;        
      })
    }
  }

  setGridInstances() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
    if (this.cipGridToolbar && this.cipDataGrid) {
      this.cipGridToolbar.dataGrid = this.cipDataGrid;
      this.cipGridToolbar.setGridInstance();
    }
  }

  getDisplayExpr(item) {
    if (!item) {
        return '';
      }
      return 'VCIP' + item.id;
  }

  getFormattedText(): string {
    return 'FRZOK-' + this.supplierAccepted.id;
  }

  refresh() {
    this.getPartnerFinancialInfo();
    this.dataGrid.instance.refresh();
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  selectionCIPChanged(data: any) {
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async openDetails(row: any) {
    this.selectedPartnerFinancialInfo = row;
    this.dataGrid.instance.editRow(row.rowIndex);
  }

  public async onRowFinancialInfoUpdated(event: any): Promise<void> {
    let item = new PartnerFinancialInfoVM();
    item = this.partnerFinancialInfo.find(g => g.id === event.key.id);
    if (item) {
      await this.financialInfoService.update(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Info Financiar Partener - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Info Financiar Partener - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refresh();
      });
    }
  }

  openCIPCheck() {
    this.cipCheckPopup = true;
  }

  addCIPCheck() {
    this.cipDataGrid.instance.addRow();
  }

  async refreshCIPGrid() {
    this.loaded = true;
    if (this.partner && this.partner.id) {
      await this.cipCheckService.getByPartnerIdAsync(this.partner.id).then(items => {
        if (items) {
          this.partnerCIPCheck = items;
          this.lastCIPCheck = items[0];
        } else {
          this.partnerCIPCheck = [];
          this.lastCIPCheck = null;
        }
        this.loaded = false;
      });
    }
  }

  public async onRowCIPInserting(event: any): Promise<void> {
    let item = new PartnerCIPCheckVM();
    item = event.data;
    item.partnerId = Number(this.partner.id);

    // if ((item.numberOfIncidentsAbove > 0 || item.numberOfIncidentsBelow > 0) && !item.dateOfLastIncident) {
    //   this.notificationService.alert('top', 'center', 'Verificare CIP Partener - Introduceti data ultimului incident!', NotificationTypeEnum.Red, true);
    //   this.refreshCIPGrid();
    //   return;
    // }

    if (item) {
      await this.cipCheckService.create(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Verificare CIP Partener - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Verificare CIP Partener - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshCIPGrid();
      });
    }
  }

  getCAExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.ca) {
        item.ca = this.helperService.roundUp(item.ca);
        return this.helperService.setNumberWithCommas((item.ca));
      }
      else {
        return '';
      }
    }
  }

  getTurnoverExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.turnover) {
        item.turnover = this.helperService.roundUp(item.turnover);
        return this.helperService.setNumberWithCommas((item.turnover));
      }
      else {
        return '';
      }
    }
  }

  getProfitExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.profitLoss) {
        item.profitLoss = this.helperService.roundUp(item.profitLoss);
        return this.helperService.setNumberWithCommas((item.profitLoss));
      }
      else {
        return '';
      }
    }
  }

  getPostExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  onSupplierAcceptedStateClick() {
    this.isSupplierAcceptedPopupVisible = true;
  }

  async onChangeSupplierAcceptedStateClick(stateId: number) {
    this.loaded = true;

    if (this.supplierAccepted && this.supplierAccepted.id) {
      this.supplierAccepted.state = stateId;

      await this.supplierAcceptedService.updateAsync(this.supplierAccepted).then(async x => {
        if (x) {
          this.notificationService.alert('top', 'center', 'Stare Furnizor - Stare schimbata cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Stare Furnizor - Starea nu a fost schimbata! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }

        await this.getAcceptedSupplier();

        this.isSupplierAcceptedPopupVisible = false;
        this.loaded = false;
      });
    } else {
      let sa = new SupplierAccepted();

      if (this.posts && this.posts.length && this.users && this.users.length) {
        let user = this.users.find(x => x.userName === this.authenticationService.getUserUserName());
        if (user) {
          sa.postId = this.posts.find(x => Number(x.id) === user.postId)?.id;
        }
      }

      sa.partnerId = this.partner.id;
      sa.state = stateId;
  
      await this.supplierAcceptedService.createAsync(sa).then(async x => {
        if (x) {
          this.notificationService.alert('top', 'center', 'Stare Furnizor - Stare schimbata cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Stare Furnizor - Starea nu a fost schimbata! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }

        await this.getAcceptedSupplier();

        this.isSupplierAcceptedPopupVisible = false;
        this.loaded = false;
      });
    }
  }
}
