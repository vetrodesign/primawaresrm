import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { ItemSpecification } from 'app/models/item-specification.model';
import { Item } from 'app/models/item.model';
import { AuthService } from 'app/services/auth.service';
import { HelperService } from 'app/services/helper.service';
import { ItemSpecificationService } from 'app/services/item-specification.service';
import { NotificationService } from 'app/services/notification.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-item-specification',
  templateUrl: './item-specification.component.html',
  styleUrls: ['./item-specification.component.css']
})
export class ItemSpecificationComponent implements OnInit {

  // #region Fields

  @Input() item: Item;

  actions: IPageActions;
  infoText: string;
  itemSpecifications: ItemSpecification[] = [];
  itemAddImagePopupVisible = false;
  loaded: number = 0;
  openPopupImage: boolean = false;
  selectedItemSpecification: string;
  selectedRows: any[];
  shouldDisableSaveUserImageBtn: boolean;
  start: boolean = false;
  uploadedImageData: string;

  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;

  // #endregion

  // #region Hooks

  constructor(
    private authenticationService: AuthService,
    private helperService: HelperService,
    private itemSpecificationService: ItemSpecificationService,
    private notificationService: NotificationService,
    private translationService: TranslateService
  ) { }

  ngOnInit(): void {
    
  }

  // #endregion

  // #region Public Methods

  addItemSpecification() {
    this.itemAddImagePopupVisible = true;
  }

  cancelImageUploadPopup() {
    this.itemAddImagePopupVisible = false;
  }

  async deleteRecords() {
    const idsToDelete = this.selectedRows.map(x => x.id);
    this.loaded++;
    await this.itemSpecificationService.deleteAsync(idsToDelete).then(r => {
      if (r) {
        this.notificationService.alert('top', 'center', 'Specificatii - Datele au fost sterse cu succes!', NotificationTypeEnum.Green, true);
      } else {
        this.notificationService.alert('top', 'center', 'Specificatii - Datele nu au fost sterse!', NotificationTypeEnum.Red, true);
      }
    });
    this.loaded--;
    this.refreshDataGrid();
  }

  deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  async getItemSpecifications() {
    this.loaded++;
    await this.itemSpecificationService.getByIdAsync(this.item.id).then(r => {
      this.itemSpecifications = r;
    })
    this.loaded--;
  }

  async loadData() {
    this.start = true;
    this.setActions();
    this.setInfoButton();
    await this.getItemSpecifications();
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  openItemSpecificationPopup(data) {
    this.selectedItemSpecification = data;
    this.openPopupImage = true;
  }

  async refreshDataGrid() {
    await this.getItemSpecifications();
    this.dataGrid.instance.refresh();
  }

  async saveImageUploadPopup() {
    const itemSpecificationToSave = new ItemSpecification();
    itemSpecificationToSave.itemId = this.item.id;
    itemSpecificationToSave.itemImageBase64 = this.uploadedImageData;

    this.itemAddImagePopupVisible = false;
    this.loaded++;
    await this.itemSpecificationService.createAsync(itemSpecificationToSave).then(r => {
      if (r) {
        this.notificationService.alert('top', 'center', 'Specificatii - Datele au fost introduse cu succes!', NotificationTypeEnum.Green, true);
      } else {
        this.notificationService.alert('top', 'center', 'Specificatii - Datele nu au fost introduse!', NotificationTypeEnum.Red, true);
      }
    });
    this.loaded--;
    this.refreshDataGrid();
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
  }

  setInfoButton() {
    this.infoText = this.translationService.instant("item.itemSpecification.infoText");
  }

  uploadItemSpecificationImageData(event) {
    const files: FileList = event.target.files;

    if (files.length > 0) {
        this.shouldDisableSaveUserImageBtn = this.helperService.isImageValidFileType(files[0]);

        const reader: FileReader = new FileReader();
        reader.onloadend = () => {
            this.uploadedImageData = reader.result as string;        
        };

        reader.readAsDataURL(files[0]);
    }
  }

  // #endregion

  // #region Private Methods

  private setActions() {
    this.actions = {
      CanView: true,
      CanAdd: true,
      CanUpdate: true,
      CanDelete: true,
      CanPrint: true,
      CanExport: true,
      CanImport: true,
      CanDuplicate: true
    };
  }

  // #endregion
}

  

  

  
  

  

  

  

  

  

  

  

  

  
