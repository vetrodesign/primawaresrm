import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { PartnerObservationSourceTypeEnum } from 'app/enums/partnerObservationSourceType';
import { PartnerTypeEnum } from 'app/enums/partnerTypeEnum';
import { PartnerWebsiteRequestsDataEnum } from 'app/enums/partnerWebsiteRequestsDataEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { PartnerObservation } from 'app/models/partner-observation';
import { PartnerWebsite } from 'app/models/partner-website.model';
import { Partner } from 'app/models/partner.model';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { AuthService } from 'app/services/auth.service';
import { FeedApiConfigurationService } from 'app/services/feed-api-configuration.service';
import { FeedLogService } from 'app/services/feed-log.service';
import { ItemMonitoringPartnerService } from 'app/services/item-monitoring-partner.service';
import { NotificationService } from 'app/services/notification.service';
import { PageActionService } from 'app/services/page-action.service';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { PartnerObservationService } from 'app/services/partner-observation.service';
import { PartnerWebsiteService } from 'app/services/partner-website.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent, DxPopupComponent, DxValidatorComponent } from 'devextreme-angular';
import * as moment from 'moment';

@Component({
  selector: 'app-partner-website',
  templateUrl: './partner-website.component.html',
  styleUrls: ['./partner-website.component.css']
})
export class PartnerWebsiteComponent implements OnInit {
  @Input() partner: Partner;

  loaded: boolean;
  actions: IPageActions;
  isOwner: boolean = false;
  groupedText: string;

  selectedRows: any[];
  selectedRowIndex = -1;

  partnerWebsites: PartnerWebsite[] = [];

  selectedPartnerWebsite: PartnerWebsite = new PartnerWebsite();
  isOnSavePartnerWebsite: boolean;
  popupVisible: boolean;
  popupRendered: boolean;

  partnerWebsiteInfo: string;
  requestsDataDataSource = [];
  shouldSave: boolean = false;
  selectedApproveValue: boolean;

  visibleNhwp: boolean = false;
  partnerLocationContacts: PartnerLocationContact[];

  firstTimeShowingNhwp: boolean = true;
  isAdmin: boolean;
  clientWebsites: PartnerWebsite[] = [];
  supplierWebsites: PartnerWebsite[] = [];

  @ViewChild('clientWebsiteDataGrid') clientDataGrid: DxDataGridComponent;
  @ViewChild('clientWebsiteGridToolbar') clientGridToolbar: GridToolbarComponent;
  @ViewChild('supplierWebsiteDataGrid') supplierDataGrid: DxDataGridComponent;
  @ViewChild('supplierWebsiteGridToolbar') supplierGridToolbar: GridToolbarComponent;
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;
  @ViewChild('checkBox') checkBox;
  @ViewChild('popupGrid') popupGrid: DxDataGridComponent;
  @ViewChild('nhwp') nhwp: DxPopupComponent;

  constructor(
    private authenticationService: AuthService,
    private translationService: TranslateService,
    private pageActionService: PageActionService,
    private notificationService: NotificationService,
    private partnerWebsiteService: PartnerWebsiteService,
    private feedApiConfigurationService: FeedApiConfigurationService,
    private feedLogService: FeedLogService,
    private itemMonitoringPartnerService: ItemMonitoringPartnerService,
    private partnerObservationService: PartnerObservationService,
    private partnerLocationContactService: PartnerLocationContactService,
  ) {
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
    this.isAdmin = this.authenticationService.isUserAdmin();
    this.partnerWebsiteInfo = this.translationService.instant("partnerWebsiteInfo");
    this.requestsDataDataSource = this.transformEnumToDataSource(PartnerWebsiteRequestsDataEnum);
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.clientGridToolbar && this.clientDataGrid) {
      this.clientGridToolbar.dataGrid = this.clientDataGrid;
      this.clientGridToolbar.setGridInstance();
    }

    if (this.supplierGridToolbar && this.supplierDataGrid) {
      this.supplierGridToolbar.dataGrid = this.supplierDataGrid;
      this.supplierGridToolbar.setGridInstance();
    }
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.partner).then(result => {
      this.actions = result;
    });
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  canDelete(rowSiteId) {
    if (rowSiteId && rowSiteId !== Number(this.authenticationService.getUserSiteId())) {
      return false;
    }
    return this.actions.CanDelete;
  }

  canAdd() {
    return this.actions.CanAdd;
  }

  canExport() {
    return this.actions.CanExport;
  }

  async loadData() {
    if (this.partner && this.partner.id) {
      this.setActions();
      await this.setPartnerWebsites();
    }
    
    // if (this.hasWebsites) {
    //   this.nhw = false;
    // }

    // if (this.partner.hasSites === false) {
    //   this.nhw = true;
    // }

    this.getPartnerLocationContact();
  }

  public addPartnerWebsite(partnerType: number) {
    this.selectedPartnerWebsite = new PartnerWebsite();
    this.selectedPartnerWebsite.isActive = true;
    this.selectedPartnerWebsite.partnerType = partnerType;

    this.togglePopup(true);

    this.isOnSavePartnerWebsite = true;
  }

  public deleteRecords(data: any) {
    this.loaded = true;
    this.partnerWebsiteService.deletePartnerWebsitesAsync(this.selectedRows.map(x => x.id)).then(r => {
      if (r) {
        this.notificationService.alert('top', 'center', 'Website Partener - Datele au fost inactivate cu succes, iar listele de pret au fost dealocate!',
          NotificationTypeEnum.Green, true);
      } else {
        this.notificationService.alert('top', 'center', 'Website Partener - Datele nu au fost inactivate! A aparut o eroare!',
          NotificationTypeEnum.Red, true);
      }

      this.refreshPartnerWebsitesDataGrid();
      this.loaded = false;
    });
  }

  public async openDetails(row: any) {
    this.selectedPartnerWebsite = row;
    this.isOnSavePartnerWebsite = false;
    this.selectedApproveValue = this.selectedPartnerWebsite.approved;

    this.togglePopup(true);
  }

  private togglePopup(open: boolean) {
    this.popupVisible = open;
    this.popupRendered = open;

    if (open === false) {
      this.onPopupHidden();
      this.isOnSavePartnerWebsite = undefined;
    }
  }

  public deleteClientWebsiteRow(data: any) {
    this.clientDataGrid.instance.selectRows(data.key, false);
    this.clientGridToolbar.displayDeleteConfirmation();
  }

  public deleteSupplierWebsiteRow(data: any) {
    this.supplierDataGrid.instance.selectRows(data.key, false);
    this.supplierGridToolbar.displayDeleteConfirmation();
  }

  public async activateClientWebsiteRow(data: any) {
    this.clientDataGrid.instance.selectRows(data.key, false);

    const partnerWebsiteId: number = data.data.id;

    await this.partnerWebsiteService.activateForIdAsync(partnerWebsiteId).then(() => {
        this.notificationService.alert('top', 'center', 'Website Partener a fost activat!',
          NotificationTypeEnum.Green, true);

        this.togglePopup(false);
        this.loaded = false;
    }).catch(() => {
      this.notificationService.alert('top', 'center', 'Website Partener nu a fost activat. A aparut o eroare!',
      NotificationTypeEnum.Red, true);

      this.togglePopup(false);
      this.loaded = false;
    });
  }

  public async activateSupplierWebsiteRow(data: any) {
    this.supplierDataGrid.instance.selectRows(data.key, false);

    const partnerWebsiteId: number = data.data.id;

    await this.partnerWebsiteService.activateForIdAsync(partnerWebsiteId).then(() => {
        this.notificationService.alert('top', 'center', 'Website Partener a fost activat!',
          NotificationTypeEnum.Green, true);

        this.togglePopup(false);
        this.loaded = false;
    }).catch(() => {
      this.notificationService.alert('top', 'center', 'Website Partener nu a fost activat. A aparut o eroare!',
      NotificationTypeEnum.Red, true);

      this.togglePopup(false);
      this.loaded = false;
    });
  }

  clientSelectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  supplierSelectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  onPopupHidden() {
    this.isOnSavePartnerWebsite = undefined;
    this.popupRendered = false;

    this.refreshPartnerWebsitesDataGrid();
  }

  
  refreshPartnerWebsitesDataGrid() {
    this.loaded = true;

    this.setPartnerWebsites()
      .then(() => {
        this.clientDataGrid.instance.refresh();
        this.supplierDataGrid.instance.refresh();
        this.loaded = false;
      });
  }

  private async setPartnerWebsites() {
    this.loaded = true;
    await this.partnerWebsiteService.getByPartnerIdIncludingInactiveAsync(this.partner.id).then(partnerWebsites => {
      this.partnerWebsites = partnerWebsites;
      if (this.partnerWebsites && this.partnerWebsites.length > 0) {
        this.clientWebsites = this.partnerWebsites.filter(f => !f.partnerType || f.partnerType === PartnerTypeEnum.Client);
        this.supplierWebsites = this.partnerWebsites.filter(f => f.partnerType === PartnerTypeEnum.Provider);

        if (this.partner.hasWebsite == true) {
          if (this.partnerWebsites && this.partnerWebsites .length > 0 && this.partnerWebsites.filter(x => x.isActive).length > 0) {
            this.partner.hasWebsite = false;
          }
        }
        this.addToPartnerWebsites();
      }
      this.loaded = false;
    });
  }

  async saveData() {
    if (this.selectedPartnerWebsite) {
      if (this.validationGroup.instance.validate().isValid) {
        this.loaded = true;

        // Remove white space
        this.selectedPartnerWebsite.website = this.selectedPartnerWebsite.website.replace(/\s/g, "");

        // Validate
        const website: string = this.selectedPartnerWebsite.website;

        let partnerWebsiteToAdd = new PartnerWebsite();
        partnerWebsiteToAdd = this.selectedPartnerWebsite;

        partnerWebsiteToAdd.customerId = Number(this.authenticationService.getUserCustomerId());
        partnerWebsiteToAdd.siteId = Number(this.authenticationService.getUserSiteId());

        partnerWebsiteToAdd.partnerId = this.partner.id;

        if (partnerWebsiteToAdd && partnerWebsiteToAdd.customerId) {
          await this.partnerWebsiteService.createPartnerWebsiteAsync(partnerWebsiteToAdd).then(r => {
            if (r) {
              this.notificationService.alert('top', 'center', 'Website Partener - Datele au fost inserate cu succes!',
                NotificationTypeEnum.Green, true);
              
              this.togglePopup(false);
              this.loaded = false;
            } else {
              this.notificationService.alert('top', 'center', 'Website Partener - Datele nu au fost inserate! A aparut o eroare!',
                NotificationTypeEnum.Red, true);
              this.togglePopup(false);
              this.loaded = false;
            }
          });
        } else {
          this.notificationService.alert('top', 'center', 'A aparut o eroare! Datele nu au fost setate!',
            NotificationTypeEnum.Red, true);
          this.loaded = false;
        }
      } else {
        this.notificationService.alert('top', 'center', 'Form-ul nu este valid!',
          NotificationTypeEnum.Red, true);
        this.loaded = false;
      }
    }
  }

  async updateData() {
    if (this.selectedPartnerWebsite) {
      if (this.validationGroup.instance.validate().isValid) {
        this.loaded = true;

        // Remove white space
        this.selectedPartnerWebsite.website = this.selectedPartnerWebsite.website.replace(/\s/g, "");

        // Validate
        const website: string = this.selectedPartnerWebsite.website;

        let partnerWebsiteToUpdate = new PartnerWebsite();
        partnerWebsiteToUpdate = this.selectedPartnerWebsite;
        partnerWebsiteToUpdate.customerId = Number(this.authenticationService.getUserCustomerId());
        partnerWebsiteToUpdate.siteId = Number(this.authenticationService.getUserSiteId());
        partnerWebsiteToUpdate.partnerId = this.partner.id;

        if (partnerWebsiteToUpdate && partnerWebsiteToUpdate.customerId) {
          await this.partnerWebsiteService.updatePartnerWebsiteAsync(partnerWebsiteToUpdate).then(r => {
            if (r) {
              this.changeFeedAPIConfiguration(partnerWebsiteToUpdate);
              this.notificationService.alert('top', 'center', 'Website Partener - Datele au fost actualizate cu succes!',
                NotificationTypeEnum.Green, true)

              this.togglePopup(false);
              this.loaded = false;
            } else {
              this.notificationService.alert('top', 'center', 'Website Partener - Datele nu au fost actualizate! A aparut o eroare!',
                NotificationTypeEnum.Red, true);
              this.togglePopup(false);
              this.loaded = false;
            }
          });
        } else {
          this.notificationService.alert('top', 'center', 'A aparut o eroare! Datele nu au fost setate!',
            NotificationTypeEnum.Red, true);
          this.loaded = false;
        }
      }
    } else {
      this.notificationService.alert('top', 'center', 'Form-ul nu este valid!',
        NotificationTypeEnum.Red, true);
      this.loaded = false;
    }
  }

  private transformEnumToDataSource(enumPar) {
    return Object.keys(enumPar).filter(value => isNaN(+value)).map(value => (
      {
        id: enumPar[value],
        name: value
      }
    ));
  }

  private async addToPartnerWebsites() {
    this.addConnections();
    this.addMonitoring();
  }

  private addConnections() {
    let connection = '';
    this.loaded = true;
    this.feedApiConfigurationService.getByPartnerIdAsync(this.partner.id).then(value => {
      let isActive = this.isAnyApiActive(value);
      if (value.length > 0 && isActive) {
        connection += 'Are API configurat';

        this.loaded = true;
        this.feedLogService.getByPartnerIdAsync(this.partner.id).then(value => {
          if (value.length > 0) {
            value = value.sort((a, b) => +new Date(b.modified) - +new Date(a.modified));
            let date = new Date(value[0].modified);
            connection += ` | A apelat datele ultima data in data de: ${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`;
          }
          
          this.partnerWebsites.forEach(value => {value.connections = connection});
          this.loaded = false;
        });
      }
      this.loaded = false;
    });
  }

  private addMonitoring() {
    this.loaded = true;
    this.itemMonitoringPartnerService.getAllActiveAsync().then(value => {
      let baseURLs = value.filter(value => value.partnerId == this.partner.id).map(value => value.baseURL.toLowerCase());
      this.partnerWebsites.forEach(value => {
        if (baseURLs.includes(value.website.toLowerCase())) {
          value.monitoring = true;
        } else {
          value.monitoring = false;
        }
        this.loaded = false;
      });
    });
  }

  onWebsiteClick(data, event: MouseEvent) {
    event.stopPropagation();

    if (data) {
      const formattedWebsite = data.data.website.startsWith('http://') || data.data.website.startsWith('https://') ? data.data.website : `https://${data.data.website}`;
      window.open(formattedWebsite, '_blank');
    }
  }

  async changeFeedAPIConfiguration(partnerWebsiteToUpdate: PartnerWebsite) {
    if (this.selectedApproveValue && ! partnerWebsiteToUpdate.approved) {
      this.feedApiConfigurationService.getByPartnerIdAsync(this.partner.id).then(r => {
        this.feedApiConfigurationService.deleteAsync(r.map(x => x.id));
      });
    }
    
    this.selectedApproveValue = null;

  }
  
  isAnyApiActive(value) {
    let result = false;
    value.forEach(item => {
      if (item.isActive) {
        result = true;
      }
    })
    return result;
  }

  hasWebsites() {
    var sites = (this.partnerWebsites && this.partnerWebsites.length > 0)  ? (this.partnerWebsites.filter(x => x.isActive)) : [];
    return sites.length > 0;
  }

  onValueChangedNhw(event) {
    if (event.previousValue == false && event.value == true && !this.hasWebsites()) {
      this.visibleNhwp = true;
    }
  }


  getPartnerLocationContact() {
    this.loaded = true;
    this.partnerLocationContactService.getPartnerLocationContactByPartnerID(this.partner.id).then(partnerLocationContacts => {
      this.partnerLocationContacts = partnerLocationContacts;
      this.loaded = false;
    });
  }

  async onCancelNhwp() {
    await this.popupGrid.instance.deselectAll();
    this.visibleNhwp = false;
  }

  async onSaveNhwp() {
    if (this.partnerWebsites && this.partnerWebsites .length > 0 && this.partnerWebsites.filter(x => x.isActive).length > 0) {
      this.notificationService.alert('top', 'center', 'Website Partener - Partnerul are un website activ!',
        NotificationTypeEnum.Red, true)
        return 0;
    }

    let selectedRowKeys = this.popupGrid.instance.getSelectedRowKeys();
    if (selectedRowKeys.length > 0) {
      let partnerObservation = new PartnerObservation();
      partnerObservation.created = moment().toDate();
      partnerObservation.modified = moment().toDate();
      partnerObservation.salesAgentUserId = +this.authenticationService.getUserId();
      partnerObservation.postId = +this.authenticationService.getUserPostId();
      partnerObservation.partnerContactPersonId = selectedRowKeys[0].id;
      partnerObservation.description = 'Nu are Site Online';
      partnerObservation.partnerId = this.partner.id;
      partnerObservation.customerId = +this.authenticationService.getUserCustomerId();
      partnerObservation.source = PartnerObservationSourceTypeEnum.FromExternalProcedure;

      this.loaded = true;
      await this.partnerObservationService.createAsync(partnerObservation).then(r => {
        this.loaded = false;
      });
    }

    this.popupGrid.instance.deselectAll().then(r => {
      this.visibleNhwp = false;
    });
  }

  async onSelectionChangedNhwp(event) {
    if (event.selectedRowKeys.length > 1) {
      let currentRow = event.currentSelectedRowKeys[0];
      await this.popupGrid.instance.selectRows([currentRow], false);
    }
  }

  onShowingNhwp(event) {
    if (this.firstTimeShowingNhwp) {
      this.firstTimeShowingNhwp = false;
      this.nhwp.instance.hide();
      this.nhwp.instance.show();
    }
  }

  public async onRowUpdated(event: any): Promise<void> {
    if (event) {
      this.loaded = true;

      let item = event.data;
      item.website = item.website.replace(/\s/g, "");
      let partnerWebsiteToUpdate = new PartnerWebsite();

      partnerWebsiteToUpdate = item;
      partnerWebsiteToUpdate.customerId = Number(this.authenticationService.getUserCustomerId());
      partnerWebsiteToUpdate.siteId = Number(this.authenticationService.getUserSiteId());
      partnerWebsiteToUpdate.partnerId = this.partner.id;

      if (partnerWebsiteToUpdate && partnerWebsiteToUpdate.customerId) {
        await this.partnerWebsiteService.updatePartnerWebsiteAsync(partnerWebsiteToUpdate).then(r => {
          if (r) {
            this.changeFeedAPIConfiguration(partnerWebsiteToUpdate);
            this.notificationService.alert('top', 'center', 'Website Partener - Datele au fost actualizate cu succes!',
              NotificationTypeEnum.Green, true)

            this.refreshPartnerWebsitesDataGrid();
            this.loaded = false;
          } else {
            this.notificationService.alert('top', 'center', 'Website Partener - Datele nu au fost actualizate! A aparut o eroare!',
              NotificationTypeEnum.Red, true);

            this.loaded = false;
          }
        });
      } else {
        this.notificationService.alert('top', 'center', 'A aparut o eroare! Datele nu au fost setate!',
          NotificationTypeEnum.Red, true);
        this.loaded = false;
      }
    } else {
      this.notificationService.alert('top', 'center', 'Form-ul nu este valid!',
        NotificationTypeEnum.Red, true);
      this.loaded = false;
    }
  }

  onCellPrepared(e) {
    if (e && e.rowType === 'header') {
      if (e.column.dataField === 'website') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Denumire site web. Toate site-urile adaugate trebuie sa inceapa cu https://");
        ($(cellElement) as any).tooltip({
          placement: 'top'
        });
      }

      if (e.column.dataField === 'isActive') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Starea website-ului partenerului (activ/inactiv)");
        ($(cellElement) as any).tooltip({
          placement: 'top'
        });
      }

      if (e.column.dataField === 'requestsData') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Tipul de metoda dorit de partener pentru a primi date. API = trimitere date in mod automat intre aplicatii. Feed = trimitere date prin fisiere XML/CSV/JSON");
        ($(cellElement) as any).tooltip({
          placement: 'top'
        });
      }

      if (e.column.dataField === 'approved') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Stare care indica daca cererea de date este aprobata de catre posturile de conducere");
        ($(cellElement) as any).tooltip({
          placement: 'top'
        });
      }

      if (e.column.dataField === 'connections') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Indica data si ora ultimei sincronizari prin metoda API");
        ($(cellElement) as any).tooltip({
          placement: 'top'
        });
      }

      if (e.column.dataField === 'observations') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Observatii agent referitoare la website-ul partenerului");
        ($(cellElement) as any).tooltip({
          placement: 'top'
        });
      }

      if (e.column.dataField === 'monitoring') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Stare care indica daca website-ul este introdus in sistemul de monitorizare PMW");
        ($(cellElement) as any).tooltip({
          placement: 'top'
        });
      }
    }
  }
}