import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerWebsiteComponent } from './partner-website.component';

describe('PartnerWebsiteComponent', () => {
  let component: PartnerWebsiteComponent;
  let fixture: ComponentFixture<PartnerWebsiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerWebsiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerWebsiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
