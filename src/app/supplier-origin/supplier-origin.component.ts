import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { PartnerObservationSourceTypeEnum } from 'app/enums/partnerObservationSourceType';
import { PartnerTypeEnum } from 'app/enums/partnerTypeEnum';
import { PartnerWebsiteRequestsDataEnum } from 'app/enums/partnerWebsiteRequestsDataEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { PartnerObservation } from 'app/models/partner-observation';
import { PartnerWebsite } from 'app/models/partner-website.model';
import { Partner } from 'app/models/partner.model';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { SupplierOrigin } from 'app/models/supplier-origin.model';
import { AuthService } from 'app/services/auth.service';
import { FeedApiConfigurationService } from 'app/services/feed-api-configuration.service';
import { FeedLogService } from 'app/services/feed-log.service';
import { ItemMonitoringPartnerService } from 'app/services/item-monitoring-partner.service';
import { NotificationService } from 'app/services/notification.service';
import { PageActionService } from 'app/services/page-action.service';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { PartnerObservationService } from 'app/services/partner-observation.service';
import { PartnerWebsiteService } from 'app/services/partner-website.service';
import { SupplierOriginService } from 'app/services/supplier-origin.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent, DxPopupComponent, DxValidatorComponent } from 'devextreme-angular';
import * as moment from 'moment';

@Component({
  selector: 'app-supplier-origin',
  templateUrl: './supplier-origin.component.html',
  styleUrls: ['./supplier-origin.component.css']
})
export class SupplierOriginComponent implements OnInit {

  loaded: boolean;
  actions: IPageActions;
  isOwner: boolean = false;
  groupedText: string;

  selectedRows: any[];
  selectedRowIndex = -1;
  
  isAdmin: boolean;
  supplierOrigin: SupplierOrigin[];

  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;

  constructor(
    private authenticationService: AuthService,
    private translationService: TranslateService,
    private pageActionService: PageActionService,
    private supplierOriginService: SupplierOriginService,
    private notificationService: NotificationService
  ) {
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
    this.isAdmin = this.authenticationService.isUserAdmin();
    this.getData();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  setActions() {
    this.actions = { CanView: true, CanAdd: true, CanUpdate: true, CanDelete: true, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.supplierOrigin).then(result => {
      this.actions = result;
    });
  }

  async getData() {
    this.loaded = true;
    await Promise.all([this.getSupplierOrigin()]).then(() => {

      this.loaded = false;
    })
  }

  async getSupplierOrigin() {
    if (this.isOwner) {
      await this.supplierOriginService.getAllAsync().then(items => {
        this.supplierOrigin = (items && items.length > 0) ? items : [];
      });
    } else {
      await this.supplierOriginService.getAllByCustomerIdAsync().then(items => {
        this.supplierOrigin = (items && items.length > 0) ? items : [];
      });
    }
  }

  onCellPrepared(event: any) {

  }

  public refreshDataGrid() {
    this.getData();
    this.dataGrid.instance.refresh();
  }

  public async onRowInserting(event: any): Promise<void> {
    let item = new SupplierOrigin();
    item = event.data;
    item.customerId = Number(this.authenticationService.getUserCustomerId());
    item.isActive = true;

    if (item) {
      await this.supplierOriginService.createAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Alertare Stoc - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Alertare Stoc - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshDataGrid();
      });
    }
  }

  public addSupplierOrigin() {
    this.dataGrid.instance.addRow();
  }

  public async onRowUpdated(event: any): Promise<void> {
    let item = new SupplierOrigin();
    item = event.data;

    if (item) {
      await this.supplierOriginService.updateAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Alertare Stoc - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Alertare Stoc - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshDataGrid();
      });
    } else {
      this.notificationService.alert('top', 'center', 'Alertare Stoc - Datele nu au fost modificate! A aparut o eroare!',
        NotificationTypeEnum.Red, true);
    }
  }

  onSelectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public deleteRecords(data: any) {
    this.supplierOriginService.deleteMultipleAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Alertare Stoc - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Alertare Stoc - Datele nu au fost sterse!',
          NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  roundValue(value: number): number {
    return Math.round(value);
  }  
}