import { Component, OnInit } from '@angular/core';
import { ClientModuleService } from 'app/services/client-module.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  constructor(private clientModuleService: ClientModuleService) {
    this.clientModuleService.getClientModuleAsyncByID(1).then(r => {
      if (r) {
      }
    });
  }

  ngOnInit(): void {
  }
}
