import { Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { ProductConventionEnum } from 'app/enums/productConventionEnum';
import { City } from 'app/models/city.model';
import { Country } from 'app/models/country.model';
import { County } from 'app/models/county.model';
import { Language } from 'app/models/language.model';
import { PartnerActivity } from 'app/models/partneractivity.model';
import { PartnerActivityAllocation } from 'app/models/partneractivityallocation.model';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { Post } from 'app/models/post.model';
import { Site } from 'app/models/site.model';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { UsersService } from 'app/services/user.service';
import { DxValidatorComponent } from 'devextreme-angular';
import { Tag } from 'app/models/tag.model';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';

@Component({
  selector: 'app-srm-partner-search-filter',
  templateUrl: './srm-partner-search-filter.component.html',
  styleUrls: ['./srm-partner-search-filter.component.css']
})
export class SrmPartnerSearchFilterComponent implements OnInit, OnChanges {
  @Input() sites: Site[];
  @Input() tags: Tag[];
  @Input() posts: Post[];
  @Input() departments: any;
  @Input() partnersTypes: any;
  @Input() personTypes: any;
  @Input() clientTypes: any;
  @Input() supplierTypes: any;

  @Input() cities: City[];
  @Input() citiesDS: any;

  @Input() counties: County[];
  @Input() countiesDS: any;

  @Input() countries: Country[];
  @Input() countriesDS: any;

  @Input() language: Language[];

  @Input() partnerActivity: PartnerActivity[];
  @Input() partnerActivityAllocation: PartnerActivityAllocation[];
  @Input() itemSmall: any;
  @Input() itemGroupSmall: any;

  @Input() caenSpecializationsDS: any;
  @Input() nirDocumentHIds: string;
  @Output() searchEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() refreshEvent: EventEmitter<any> = new EventEmitter<any>();
  
  @ViewChild('validationSearchGroup') validationSearchGroup: DxValidatorComponent;
  partnerSearchFilter: PartnerSearchFilter;
  productConvention: { id: number; name: string }[] = [];

  partnerActivityAllocationDS: PartnerActivityAllocation[] = [];
  postsDS: Post[] = [];
  userPost: Post;
  isFromMngDepartment: boolean = false;
  isOwner: boolean;
  selectedPersonTypes: number;
  selectedPost: number;

  @Output() onActivateContactCampaignPartnersChangeOutput: EventEmitter<any> = new EventEmitter<any>();
  shouldActivateContactCampaignPartners: boolean = true;
  @Input() isUserAdminOrHasSuperiorPost;
  @Input() getDisplayExprPosts;
  @Output() onPostChangeOutput: EventEmitter<any> = new EventEmitter<any>();
  @Output() onPersonTypeChangeOutput: EventEmitter<any> = new EventEmitter<any>();
  constructor(private translationService: TranslateService,
    private userService: UsersService, private authService: AuthService) {
    this.getDisplayExprPartnerActivity = this.getDisplayExprPartnerActivity.bind(this);

    this.authService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
    for (let n in ProductConventionEnum) {
      if (typeof ProductConventionEnum[n] === 'number') {
        this.productConvention.push({
          id: <any>ProductConventionEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    this.partnerSearchFilter = new PartnerSearchFilter();
    this.partnerSearchFilter.isActive = true;

    this.selectedPersonTypes = 1;
    this.onPersonTypeChangeOutput.emit(1);
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.posts || changes.departments) {
      this.userService.getByUsernameOrEmail(this.authService.getUserUserName(), null).then(user => {
        if (user && user.length > 0 && this.departments && this.departments.length > 0) {
          if (user[0] && user[0].postId) {
            this.userPost = this.posts.find(x => Number(x.id) === user[0].postId);
            let dep = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Management);
            const officesFromMngDep = (dep && dep.lenght > 0 ) ? dep.map(x => x.offices).reduce((x, y) => x.concat(y)) : null; 
            if (officesFromMngDep && officesFromMngDep.length && officesFromMngDep.filter(x => x.id === this.userPost.officeId) > 0) {
              this.isFromMngDepartment = true;
            }
            this.getPosts();
          }
        }
      });

      const userPostId = +this.authService.getUserPostId();
      let postId = this.posts.some(f => f?.id === userPostId) ? userPostId : null;

      if (postId) {
        this.partnerSearchFilter.postIds = [postId];

        let copyPosts = [...this.posts];
        this.posts = copyPosts.filter(f => f.id === postId);
      }
    }
    if (changes.partnerActivityAllocation) {
      this.partnerActivityAllocationDS = this.partnerActivityAllocation;
    }
  }

  @HostListener('document:keydown', ['$event'])
  onKeydown(event: KeyboardEvent) {
    if (!this.shouldActivateContactCampaignPartners) {
      if (event.key === 'F5') {
        event.preventDefault(); 
        this.searchPartners();
      }

      if (event.key === 'Delete') {
        event.preventDefault(); 
        this.resetFilters();
      }
    }
  }

  getPosts() {
    this.postsDS = [];
    if (this.userPost) {
      if (this.userPost.isOfficeSuperior) {
        this.postsDS = this.departments.map(x => x.offices).reduce((x, y) => x.concat(y)).filter(z => z.id === this.userPost.officeId).map(p => p.posts).reduce((g, h) => g.concat());
      }
      if (this.userPost.isDepartmentSuperior) {
        this.postsDS = this.departments.find(x => x.offices.map(z => z.id).includes(this.userPost.officeId)).offices.map(p => p.posts).reduce((x, y) => x.concat(y));
      }
      if (this.userPost.isLocationSuperior || this.userPost.isGeneralManager || this.isFromMngDepartment || this.isOwner) {
        this.postsDS = this.posts;
      } else {
        this.postsDS.push(this.userPost);
      }
    }
  }

  searchPartners() {
    if (this.validationSearchGroup.instance.validate().isValid) {
      this.searchEvent.emit(this.partnerSearchFilter);
    }
  }

  resetFilters() {
    this.partnerSearchFilter = new PartnerSearchFilter();
    this.partnerSearchFilter.isActive = true;

    this.refreshEvent.emit(true);
  }

  onCitiesChanged(e: any) {
    if (e.value && e.value.length > 0) {
      var countryIds = this.cities.filter(x => e.value.includes(x.id)).map(y => y.countryId);
      var countiesIds = this.cities.filter(x => e.value.includes(x.id)).map(y => y.countyId);
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries.filter(i => countryIds.includes(i.id))
      };

      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties.filter(i => countiesIds.includes(i.id))
      };
    } else {
      this.restoreDS();
    }
  }

  onCountiesChanged(e: any) {
    if (e.value && e.value.length) {
      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities.filter(i => e.value.includes(i.countryId))
      };

      var countryIds = this.counties.filter(x => e.value.includes(x.id)).map(y => y.countryId);
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries.filter(i => countryIds.includes(i.id))
      };
    } else {
      this.restoreDS();
    }
  }

  onCountriesChanged(e: any) {
    if (e.value && e.value.length) {
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties.filter(i => e.value.includes(i.countryId))
      };

      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities.filter(i => e.value.includes(i.countryId))
      };
    }
    else {
      this.restoreDS();
    }
  }

  getSpecializationDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.description;
  }

  restoreDS() {
    this.citiesDS = {
      paginate: true,
      pageSize: 15,
      store: this.cities
    };
    this.countiesDS = {
      paginate: true,
      pageSize: 15,
      store: this.counties
    };
    this.countriesDS = {
      paginate: true,
      pageSize: 15,
      store: this.countries
    };
  }

  getDisplayExprCities(item) {
    if (!item) {
      return '';
    }
    return item.name + ' - ' + item.countyName + ' - ' + item.countryName;
  }

  getDisplayExprPartnerActivity(value: any) {
    if (value && this.partnerActivity) {
      const item = this.partnerActivity.find(x => x.id === value.partnerActivityId);
      if (item) {
        return value.saleZoneCode + ' - ' + item.name + ' - ' + value.partnerActivityClassification;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  onMultiTagPostChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagCitiesChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagCountieChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagLanguageChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 25) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagPartnerActivityChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagSpecializationChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagSitesChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onProductConventionChanged(e) {
    if (e && e.value && e.value.length > 0) {
      this.partnerActivityAllocationDS = this.partnerActivityAllocation.filter(x => e.value.includes(x.productConventionId));
    } else {
      this.partnerActivityAllocationDS = this.partnerActivityAllocation;
    }
  }

  getDisplayExprMU(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  onActivateContactCampaignPartnersChange(event) {
    this.onActivateContactCampaignPartnersChangeOutput.emit(event);
    this.shouldActivateContactCampaignPartners = event.value;
  }

  onActivateContactCampaignPartnersClick() {
    this.shouldActivateContactCampaignPartners = !this.shouldActivateContactCampaignPartners;
    this.onActivateContactCampaignPartnersChangeOutput.emit(this.shouldActivateContactCampaignPartners);
  }

  onPostChange(event) {
    this.onPostChangeOutput.emit(event);
  }

  onPersonTypeChange(event) {
    this.onPersonTypeChangeOutput.emit(event);
  }

  onMyClientsClick() {
    const userPostId = +this.authService.getUserPostId();
    let postId = this.posts.some(f => f?.id === userPostId) ? userPostId : null;
   
    if (this.selectedPost) {
      postId = this.selectedPost;
    }

    // var url = `${environment.SRMPrimaware}/${Constants.myClients}?postId=${postId}`;
    // window.open(url, '_blank');
  }
}