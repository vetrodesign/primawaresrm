import { formatDate } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { BarCodeAllocationTypeEnum } from 'app/enums/barcodeAllocationTypeEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { ItemEcommerceComponent } from 'app/item-ecommerce/item-ecommerce.component';
import { ItemLogisticsComponent } from 'app/item-logistics/item-logistics.component';
import { ItemXClientMonitoringComponent } from 'app/item-x-client-monitoring/item-x-client-monitoring.component';
import { ItemPropertiesComponent } from 'app/item-properties/item-properties.component';
import { ItemRelatedComponent } from 'app/item-related/item-related.component';
import { BarCodeItemAllocation } from 'app/models/barcodeitemallocation.model';
import { CaenCodeSpecializationXItem } from 'app/models/caencodespecializationxitem.model';
import { IPageActions } from 'app/models/ipageactions';
import { Item } from 'app/models/item.model';
import { ItemXTag } from 'app/models/itemxtag.model';
import { Post } from 'app/models/post.model';
import { Tag } from 'app/models/tag.model';
import { User } from 'app/models/user.model';
import { PartnerDropdownSearchComponent } from 'app/partner-dropdown-search/partner-dropdown-search.component';
import { AuthService } from 'app/services/auth.service';
import { BarCodeIntervalService } from 'app/services/bar-code-interval.service';
import { BarCodeItemAllocationService } from 'app/services/bar-code-item-allocation.service';
import { CaenCodeSpecializationService } from 'app/services/caen-code-specialization.service';
import { CustomCodeXtaxRateService } from 'app/services/custom-code-xtax-rate.service';
import { GeneralOfferService } from 'app/services/general-offer.service';
import { ItemCaenCodeSpecializationService } from 'app/services/item-caen-code-specialization.service';
import { ItemTagService } from 'app/services/item-tag.service';
import { ItemService } from 'app/services/item.service';
import { NotificationService } from 'app/services/notification.service';
import { SupplierPriceListService } from 'app/services/supplier-price-list.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';
import { ItemDCRInfo } from 'app/models/itemDCRInfo.model';
import { ItemAutomationComponent } from 'app/item-automation/item-automation.component';
import { ItemSpecificationComponent } from 'app/item-specification/item-specification.component';
import { EshopItemService } from 'app/services/eshop-item.service';
import { LanguageService } from 'app/services/language.service';
import { EshopLanguageService } from 'app/services/eshop-language.service';

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit, OnChanges {
  @Input() selectedItem: Item;
  @Input() tags: Tag[];
  @Input() measurementUnits: any;
  @Input() supplyAndSalesPosts: Post[];
  @Input() itemGroups: any;
  @Input() itemTypes: any;
  @Input() taxRateTypes: any;
  @Input() taxRates: any;
  @Input() itemStates: any;
  @Input() priceCompositionType: any;
  @Input() users: User[];
  @Input() posts: any;
  @Input() customCodesDS: any;
  @Input() actions:any;
  
  @Input() barCodeIntervals: any;
  @Input() barCodeItemAllocations: any;
  @Input() caenSpecializationsDS: any;

  @Output() visible: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;
  @ViewChild('secondValidationGroup') secondValidationGroup: DxValidatorComponent;
  @ViewChild('partnerDropdownSearch') partnerDropdownSearch: PartnerDropdownSearchComponent;
  @ViewChild('itemProperties') itemProperties: ItemPropertiesComponent;
  @ViewChild('itemLogistics') itemLogistics: ItemLogisticsComponent;
  @ViewChild('itemXClientMonitoring') itemXClientMonitoring: ItemXClientMonitoringComponent;
  @ViewChild('itemEcommerce') itemEcommerce: ItemEcommerceComponent;
  @ViewChild('itemRelated') itemRelated: ItemRelatedComponent;

  @ViewChild('specDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('specToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('itemAutomation') itemAutomation: ItemAutomationComponent;
  @ViewChild('itemSpecification') itemSpecification: ItemSpecificationComponent;

  isOnSave: boolean;
  erpPopup: boolean = false;
  showHistory: boolean = false;
  item: Item;
  itemDCR: ItemDCRInfo;
  loaded: boolean = false;
  monthTurnOverPopup: boolean = false;
  itemStock: number;
  searchSupplierPriceList: any;
  customerId: string;
  searchItem: string;
  range: string;
  isTabActive: string;
  itemSmall: any;
  itemToCopy: any;
  itemTurnOver: string;
  itemERPSyncHistoryItem: any;
  itemGroupsDS: any;
  turnOver: any;
  detailsTabs: any;
  monthTurnOvers: any[] = [];
  allocations: BarCodeItemAllocation[];
  digitPattern: any = /^[0-9]*$/;
  barCodeAllocationTypes: { id: number; name: string }[] = [];
  multipleSpecializationsIds: any = [];
  singleSpecializationsId: number;
  isItemSentToCHR: boolean = false;
  isItemSentToSenior: boolean = false;
  shouldAllowCodeChange: boolean = false;
  hasERPActive: boolean = false;
  itemGroupChange: boolean = false;
  addSpecialziationsPopup: boolean = false;
  addModeMultiple: boolean = false;

  selectedItemCopyValue: number = undefined;
  openValdiationPopup: boolean = false;
  rememberCheckBox: string;

  isSGF: boolean = false;
  isCDA: boolean = false;
  isSZO: boolean = false;
  isALT: boolean = false;

  textSGF: string = '';
  textCDA: string = '';
  textSZO: string = '';
  textALT: string = '';

  itemDetailsSpecInfo: string;

  supplierPriceLists: any;
  eshopItemVisibilityData: any = [];
  isRoLangEshopItemVisible: boolean = false;

  allocatedBarCodes: any[] = [];

  constructor(private itemService: ItemService,
    private authenticationService: AuthService, private notificationService: NotificationService,
    private itemCaenCodeSpecializationService: ItemCaenCodeSpecializationService,
    private customCodeXTaxRateService: CustomCodeXtaxRateService,
    private itemTagService: ItemTagService,
    private generalOfferService: GeneralOfferService,
    private supplierPriceListService: SupplierPriceListService,
    private barCodeItemAllocationService: BarCodeItemAllocationService, private translationService: TranslateService,
    private barCodeIntervalService: BarCodeIntervalService, private caenCodeSpecializationService: CaenCodeSpecializationService,
    private eshopItemService: EshopItemService, private eshopLanguageService: EshopLanguageService, private languageService: LanguageService) {
      this.getDisplayExprTaxRate = this.getDisplayExprTaxRate.bind(this);
      this.isTabActive = this.authenticationService.getCustomerPageTitleTheme();

      this.customerId = this.authenticationService.getUserCustomerId();
      this.hasERPActive = this.authenticationService.getCustomerERPActive();

       for (let n in BarCodeAllocationTypeEnum) {
        if (typeof BarCodeAllocationTypeEnum[n] === 'number') {
          this.barCodeAllocationTypes.push({
            id: <any>BarCodeAllocationTypeEnum[n],
            name: this.translationService.instant(n)
          });
        }
      }

      this.textSGF = Constants.textSGF;
      this.textCDA = Constants.textCDA;
      this.textSZO = Constants.textSZO;
      this.textALT = Constants.textALT;
     }

  ngOnInit(): void {
    this.detailsTabs = [{
      id: 1,
      name: 'Cod de bare',
      'isActive': true
    },
    {
      id: 2,
      name: 'Caracteristici',
      'isActive': false
    },
    {
      id: 3,
      name: 'Logistica',
      'isActive': false
    },
    {
      id: 4,
      name: 'Ecommerce',
      'isActive': false
    },
    {
      id: 5,
      name: 'Sugestii',
      'isActive': false
    },
    {
      id: 6,
      name: 'Specializari',
      'isActive': false
    },
    {
      id: 7,
      name: 'Monitorizare',
      'isActive': false
    },
    {
      id: 8,
      name: 'Automatizare',
      'isActive': false
    },
    {
      id: 9,
      name: 'Specificatii',
      'isActive': false
    }];

    this.itemDetailsSpecInfo = this.translationService.instant("itemDetailsSpecInfo");
  }

  toggleMode(tab: any) {
    this.detailsTabs.find(x => x.isActive).isActive = false;
    tab.isActive = true;

    if (tab.id === 1 && !this.isOnSave && this.selectedItem) {
      this.getBarCodeAllocations(this.selectedItem.id);
   }

    if (tab.id === 2 && !this.isOnSave && this.selectedItem) {
      // this.itemProperties.loadData();
    }

    if (tab.id === 3 && !this.isOnSave && this.selectedItem) {
      this.itemLogistics.loadData();
    }

    if (tab.id === 4 && !this.isOnSave && this.selectedItem) {
      this.itemEcommerce.loadData();
    }

    if (tab.id === 5 && !this.isOnSave && this.selectedItem) {
      this.itemRelated.loadData();
    }

    if (tab.id === 6 && !this.isOnSave && this.selectedItem) {
      this.refreshSpecDataGrid();
    }

    if (tab.id === 7 && !this.isOnSave && this.selectedItem) {
      this.itemXClientMonitoring.loadData();
    }

    if (tab.id === 9 && !this.isOnSave && this.selectedItem) {
      this.itemSpecification.loadData();
    }
  }

  async loadItemData() {
    this.getAllItems();

    this.item = new Item();
    this.itemDCR = new ItemDCRInfo();
    this.allocations = [];

    this.isItemSentToCHR = false;
    this.isItemSentToSenior = false;

    this.searchItem = null;
    this.itemToCopy = null
    this.itemTurnOver = null;

    if (this.dataGrid && this.gridToolbar) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
    
    if (this.selectedItem) {
      this.isOnSave = false;
      this.itemGroupChange = false;
      this.itemStock = 0;
      this.item = this.selectedItem;
      
      this.isItemSentToCHR = (this.item.syncERPExternalId && this.item.syncERPExternalId.length) ? true : false;
      this.isItemSentToSenior = (this.item.syncSeniorExternalId) ? true : false;

       this.allocations = (this.barCodeItemAllocations && this.barCodeItemAllocations.filter(x => x.itemId === this.item.id).length > 0) ?
       this.barCodeItemAllocations.filter(x => x.itemId === this.item.id) : [];

       if (this.item.stateId == 1){
        this.getSupplierList();
       }

      this.refreshSpecDataGrid();
      this.getItemERPSyncHistoryItems(this.item.id); 
      this.getItemTurnover(this.item.id);
      this.getItemTags(this.item.id);
      this.getItemStock(this.item.id);
      this.getItemDCR(this.item.id);
      this.getSupplierPriceLists();
      this.getEshopItemVisibilityData(this.item.id);
      this.getBarCodeAllocations(this.item.id);
    } else {
      this.item.turnOver = 1;
      this.item.isValid = true;
      this.item.isActive = true;

      this.item.hasBbd =  true;
      this.item.hasLot =  true;
      this.item.hasMgf =  true;
      this.item.hasSn =  false;

      this.item.caenCodeSpecializations = [];
      this.item.measurementUnitId = this.measurementUnits.find(x => x.isImplicit)?.id;
      this.isOnSave = true;
    }
    this.checkIfItemIsOnInvoice();
  }

  async checkIfItemIsOnInvoice() {
    if (this.isOnSave) {
      return false;
    }
    if (!this.isOnSave && this.item.id) {
      await this.itemService.CheckIfItemIsOnInvoiceAsync(this.item.id).then(result => {
        this.shouldAllowCodeChange = result;
      })
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.itemGroups && this.itemGroups) {
      this.itemGroupsDS = {
        paginate: true,
        pageSize: 15,
        store: this.itemGroups
      };
    }
  }

  backToItem() {
    if (this.item && this.item.id && this.item.caenCodeSpecializations.filter(x => x.id == 0 || x.id == null || x.id == undefined).length > 0) {
      this.notificationService.alert('top', 'center', 'Atentie! Aveti specializari nesalvate! Datele se vor pierde daca nu apasati pe butonul de salvare inainte!',
      NotificationTypeEnum.Red, true);
    } else {
      this.item = null;
      this.itemGroupsDS = {
        paginate: true,
        pageSize: 15,
        store: this.itemGroups
      };
      this.toggleMode(this.detailsTabs[0]);
      this.visible.emit();
    }
  }

  async getBarCodeAllocations(itemId: number) {
    await this.barCodeItemAllocationService.getByItemIdAsync(itemId).then(result => {
      if (result && result.length > 0) {
        this.allocatedBarCodes = result;
      } else {
        this.allocatedBarCodes = [];
      }
    })
  }

  refreshItemGroupDataSource() {
    this.itemGroupsDS = {
      paginate: true,
      pageSize: 15,
      store: this.itemGroups
    };
  }

  canUpdate() {
    if (this.selectedItem && this.selectedItem.siteId !== Number(this.authenticationService.getUserSiteId())) {
      return false;
    }
    return true;
  }

  async insertERP(itemId, isSeniorERP : boolean = false) {
    if(this.item && this.validationGroup.instance.validate().isValid && this.partnerDropdownSearch.validate() && this.validateCodeFour() && itemId) {
     let userPost = null;
      if (this.posts && this.posts.length && this.users && this.users.length) {
        let user = this.users.find(x => x.userName === this.authenticationService.getUserUserName());
        if (user) {
          userPost = this.posts.find(x => Number(x.id) === user.postId);
        }
      }
      this.loaded = true;
      await this.itemService.insertToErpAsync(itemId, this.authenticationService.getUserUserName(), userPost ? userPost.code : null, isSeniorERP).then(async result => {
        if (result) {
          this.notificationService.alert('top', 'center', 'Produse - Datele au fost trimise catre ERP!',
          NotificationTypeEnum.Green, true);
          await this.getItemERPSyncHistoryItems(itemId).then(x =>
          {
            this.isItemSentToSenior = isSeniorERP;
            this.isItemSentToCHR = !isSeniorERP;
          }
          );
        } else {
          this.loaded = false;
          this.erpPopup = false;
          this.notificationService.alert('top', 'center', `Produse - Datele nu au fost trimise catre ERP! A aparut o eroare!`,
          NotificationTypeEnum.Red, true);
            return;
        }
        this.loaded = false;
        this.erpPopup = false;
      }).catch(error => { 
        this.loaded = false;
        this.erpPopup = false;
        this.notificationService.alert('top', 'center', 'Produse - Datele nu au fost trimise catre ERP! A aparut o eroare!',
        NotificationTypeEnum.Red, true); });

    }
    else {
      this.erpPopup = false;
      this.notificationService.alert('top', 'center', 'Produse - Datele nu au fost trimise catre ERP! Verificati toate campurile partenerului.',
      NotificationTypeEnum.Red, true);
        return;
    }
  }

  async customCodeChange(e) {
    if (e && e.value) {
      await this.customCodeXTaxRateService.getCustomCodeXTaxRateByIdVATAsync(e.value).then(i => {
        if (i && i.length > 0) {
          let ctx = i.find(c => c.countryId === Number(this.authenticationService.getUserCustomerCountryId()));
          if (ctx) {
            this.item.taxRateVATType = ctx.taxRateVatTypeId;
          }
        }
      });
    }
  }

  async itemTypeChangeEvent(e) {
    if (e && e.value) {
      let isStockable = this.itemTypes.find(i => i.id === e.value)?.isStockable;
      this.item.isStockable = isStockable;

      if (this.item.isStockable && this.item.itemGroupId) {
        let find = this.itemGroups.find(x => x.id === this.item.itemGroupId);
        this.item.customCodeId = (find && find.customCodeId) ? find.customCodeId : this.item.customCodeId;
      }
    }
  }

  getDisplayExprMU(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  getDisplayExprProposedBy(item: Post) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  getDisplayExprTaxRate(item) {
    if (!item) {
        return '';
      }
      return item.name;
  }
  
  getDisplayExprItemGroupCategory(item) {
    if (!item) {
        return '';
      }
      return item.catCode + ' - ' + item.firstName;
  }

  getDisplayExprItemGroupCode(item) {
    if (!item) {
        return '';
      }
      return item.grpCode + ' - ' + item.firstName;
  }

getDisplayExpCustomCode(item) {
  if (!item) {
      return '';
    }
    return item.cnKey + ' - ' + item.nameRO;
}

  getSecondDisplayExpr(item) {
    if (!item) {
      return '';
    }

    return item.code + ' - ' + item.description;
  }

  getDisplayExprPartners(item) {
    if (!item) {
       return '';
     }
     return item.partner.code + ' - ' + item.partner.name;
  }

  async getItemERPSyncHistoryItems(itemId) {
    await this.itemService.getByItemId(itemId).then(x => {
      this.itemERPSyncHistoryItem = x;
    });
  }

  async getItemTurnover(itemId) {
    this.itemTurnOver = null;
    this.turnOver = null;
    await this.itemService.getItemTurnover(itemId).then(turnOver => {
      if (turnOver) {
        this.turnOver = turnOver;
        this.itemTurnOver = Math.round(turnOver[0].rulajReal3M) + ' / ' + Math.round(turnOver[0].rulajReal6M) + ' / ' + Math.round(turnOver[0].rulajReal9M) + ' / ' + Math.round(turnOver[0].rulajReal12M);
      }
    });
  }

  async getItemTags(itemId) {
    await this.itemTagService.getByItemIdAsync(itemId).then(tags => {
      if (tags && tags.length > 0) {
        this.item.tagIds = tags.map(x => x.tagId);
        this.item.existingTagIds = tags.map(x => x.tagId);
      } else {
        this.item.tagIds = [];
        this.item.existingTagIds = [];
      }
    });
  }

  async getItemStock(itemId) {
    await this.itemService.getItemStock(itemId).then(result => {
      this.itemStock = result ? result : 0;
    });
  }

  async getItemDCR(itemId) {
    await this.itemService.getItemDCRAsync(itemId).then(result => {
      if (result) {
        this.itemDCR = result;
      }
    });
  }

  async getEshopItemVisibilityData(itemId): Promise<any> {
    this.eshopItemVisibilityData = [];
    await this.languageService.getAllLanguagesAsync().then(async languages => {
      if (languages && languages.length > 0) {
     
        await this.eshopLanguageService.getByCustomerIdAsync().then(async eshopLanguages => {
          if (eshopLanguages && eshopLanguages.length > 0) {
         
            let availableLanguages = languages.filter(f => eshopLanguages.map(m => m.languageId).includes(f.id));  

            await this.eshopItemService.getAllByItemIdAsync(itemId).then(eshopItems => {
              if (eshopItems && eshopItems.length > 0) {
                eshopItems.forEach(m => {
                  
                  if (availableLanguages.find(f => f.id === m.eshopLanguageId)) {
                    this.eshopItemVisibilityData.push({
                      languageId: m.eshopLanguageId,
                      languageName: availableLanguages.find(f => f.id === m.eshopLanguageId).name,
                      isVisibleEshop: m.productOnline === 1
                    });
                  }
                })

                if (this.eshopItemVisibilityData.find(f => f.languageId === 138 && f.isVisibleEshop)) {
                  this.isRoLangEshopItemVisible = true;
                } else {
                  this.isRoLangEshopItemVisible = false;
                }             
              }
            });
          }
        });        
      }
    });
  }

  async getMonthTurnover() {
    this.monthTurnOvers = [];
    await this.itemService.getItemMonthTurnover(this.item.id).then(turnOver => {
      if (turnOver && turnOver.length > 0) {
        this.monthTurnOverPopup = true;
        this.monthTurnOvers = turnOver;
      }
    });
  }

  canFixTurnover() {
    if (this.turnOver && this.turnOver.length > 0) {
      if (this.item.turnOver > 1.2 * Math.round(this.turnOver[0].rulajReal3M)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  fixTurnover() {
    this.item.turnOver = Math.round(1.1 * this.turnOver[0].rulajReal3M);
  }

  getAllItems() {
    this.itemService.getAllItemSmallAsync().then(items => {
      if (items && items.length > 0) {
        this.itemSmall = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      } else {
        this.itemSmall = {
          paginate: true,
          pageSize: 15,
          store: []
        };
      }
    });
  }

  itemCopyValueChange(e: any) {
    if (e && e.value) {
      this.selectedItemCopyValue = e.value;
    }
  }

  copySpecializationsClicked() {
    if (this.selectedItemCopyValue !== undefined) {
      this.itemCaenCodeSpecializationService.getAllByItemId(this.selectedItemCopyValue).then(items => {
        if (items && items.length > 0) {
          if (!this.item.caenCodeSpecializations) {
            this.item.caenCodeSpecializations = items;
            this.item.caenCodeSpecializations.forEach(x => x.id = 0 );
          } else {
            this.item.caenCodeSpecializations = [];
            items.forEach(val => {
              if (!this.item.caenCodeSpecializations.includes(val)) {
                val.id = 0;
                this.item.caenCodeSpecializations.push(val);
              }
            })
          }
        }
      });
    }
  }

  itemStateValueChange(e) {
    if (e && e.value == 1) {
      this.getSupplierList();
    }
    this.prefixNames()
  }

  priceCompositionTypeChange(e) {
    if (e && e.value) {
      this.prefixNames()
    }
  }

  prefixNames() {
    if (this.item) {
      if (this.item.nameRO) {
        this.item.nameRO = this.updateName(this.item.priceCompositionType, this.item.stateId, this.item.nameRO);
      }
      if (this.item.nameENG) {
        this.item.nameENG = this.updateName(this.item.priceCompositionType, this.item.stateId, this.item.nameENG);
      }
      if (this.item.shortNameRO) {
        this.item.shortNameRO = this.updateName(this.item.priceCompositionType, this.item.stateId, this.item.shortNameRO);
      }
      if (this.item.shortNameENG) {
        this.item.shortNameENG = this.updateName(this.item.priceCompositionType, this.item.stateId, this.item.shortNameENG);
      }
    }
  }

  removePrefix(str) {
    if (str.startsWith("**")) {
      return str.replace(/^\*\*/, '').trim();
    } else if (str.startsWith("__")) {
      return str.replace(/^__/, '').trim();
    } else if (str.startsWith("..")) {
      return str.replace(/^\.\./, '').trim();
    } else {
      return str.trim();
    }
  }

  updateName(priceCompositionType, stateId, name) {
    let n = this.removePrefix(name);
    if (priceCompositionType === 1) {
      if (stateId === null || stateId === undefined || stateId === 3) {
        n = '..' + n;
      } else if (stateId === 1) {
        n = '**' + n;
      } else if (stateId === 2) {
        n = '__' + n;
      }
    } else if (priceCompositionType === 2) {
      if (n.startsWith('__')) {
        n = n.substring(2);
      } else if (n.startsWith('**')) {
        n = n.substring(2);
      } else if (n.startsWith('..')) {
        n = n.substring(2);
      }
      if (stateId === 1) {
        n = '**' + n;
      } else if (stateId === 2) {
        n = '__' + n;
      }
    }
    return n;
  }

  getSupplierList() {
    if (this.item && this.item.id && this.item.partnerId) {
      this.supplierPriceListService.getSupplierPriceListsByPartnerId(this.item.partnerId).then(x => {
        if (x && x.length == 1) {
          this.searchSupplierPriceList = x[0];
        }
      })
    }
  }

  async saveData() {
    if (this.item && this.item.caenCodeSpecializations && this.item.caenCodeSpecializations.length == 0) {
      this.notificationService.alert('top', 'center', 'Specializari - Produsul trebuie sa contina cel putin o specializare!',
      NotificationTypeEnum.Red, true)
      return;
    }  
    if (this.item && this.validationGroup.instance.validate().isValid && this.partnerDropdownSearch.validate() && this.validateCodeFour()) {
      this.loaded = true;
      let item = new Item();
      item = this.item;
      item.customerId = Number(this.customerId);
      item.siteId = Number(this.authenticationService.getUserSiteId());
      
      if (item && item.customerId) {
        await this.itemService.createItemAsync(item).then(r => {
          if (r) {
            this.item.id = r.id;
            this.item.rowVersion = r.rowVersion;
            this.item.isActive = true;

            // this.itemProperties.loadData();
            this.itemEcommerce.loadData();

            //Tags
            let createTags: ItemXTag[] = [];
            item.tagIds?.forEach(id => {
              const t = new ItemXTag();
              t.itemId = Number(r.id);
              t.tagId = id;
              createTags.push(t);
            });
            if (createTags && createTags.length > 0) {
              this.itemTagService.createMultipleAsync(createTags).then(() => {
              });
            }

            //CaenCodeSpecialization
            if (this.item.caenCodeSpecializations && this.item.caenCodeSpecializations.length > 0) {
              this.item.caenCodeSpecializations.forEach(x => x.itemId = this.item.id );
              this.itemCaenCodeSpecializationService.syncItemCaenCodeSpecializationAsync(this.item.caenCodeSpecializations).then(() => {

                this.refreshSpecDataGrid();
                this.notificationService.alert('top', 'center', 'Specializari - Datele au fost salvate cu succes!',
                  NotificationTypeEnum.Green, true)
              });
            }        

            // Item Automation
            if (this.item.id) {
              this.itemAutomation.onSaveProduct(this.item.id);
            }

            this.isOnSave = false;
          } else {
            this.notificationService.alert('top', 'center', 'Produse - Datele nu au fost inserate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          }
          this.loaded = false;
        });
      }
    }
  }

  saveSpecializations() {
    if (this.item.caenCodeSpecializations && this.item.caenCodeSpecializations.length > 0) {
      let gridValid: boolean = true;

      this.item.caenCodeSpecializations.forEach(x => { 
        if (this.checkIfMultipleTrueStates(x)) {
          const specialization = this.caenSpecializationsDS.store.find(cs => cs.id === x.caenCodeSpecializationId);

          this.notificationService.alert('top', 'center', `Specializarea '${specialization.code} - ${specialization.description}' trebuie sa aiba bifata doar una dintre casutele SGF/CDA/SZO.`,
          NotificationTypeEnum.Red, true);
          gridValid = false;
          return;
        } else {
          x.itemId = this.item.id;
        }
      });

      if (gridValid) {
        this.loaded = true;
        this.itemCaenCodeSpecializationService.syncItemCaenCodeSpecializationAsync(this.item.caenCodeSpecializations).then(() => {
          this.refreshSpecDataGrid();
          this.notificationService.alert('top', 'center', 'Specializari - Datele au fost salvate cu succes!',
            NotificationTypeEnum.Green, true)
            this.loaded = false;
        });
      } else {
        this.notificationService.alert('top', 'center', 'Specializari - Modificarile nu au fost salvate!',
          NotificationTypeEnum.Red, true);
        return;
      }
    }    
  }

  private checkIfMultipleTrueStates(caenCodeSpecialization): boolean {
    const states: boolean[] = [];
    states.push(caenCodeSpecialization.sgf);
    states.push(caenCodeSpecialization.cda);
    states.push(caenCodeSpecialization.szo);

    return states.filter(s => s == true).length > 1;
  }

  addMultipleSpecialziationsDataEvent() {
    this.multipleSpecializationsIds = [];
    this.isSGF = false;
    this.isCDA = false;
    this.isSZO = false;
    this.isALT = false;
    this.addSpecialziationsPopup = true;
    this.addModeMultiple = true;
  }

  addSingleSpecializationDataEvent() {
    this.singleSpecializationsId = -1;
    this.isSGF = false;
    this.isCDA = false;
    this.isSZO = false;
    this.isALT = false;
    this.addSpecialziationsPopup = true;
    this.addModeMultiple = false;
  }

  addSpecCheckboxChanged(event: any, checkboxName) {
    const checkboxes: { sgf: boolean, cda: boolean, szo: boolean } = { sgf: this.isSGF, cda: this.isCDA, szo: this.isSZO };

    if (this.checkIfMultipleTrueStates(checkboxes)) {
      this.isSGF = false;
      this.isCDA = false;
      this.isSZO = false;

      switch (checkboxName) {
        case 'SGF':
          this.isSGF = true;
          break;
        case 'CDA':
          this.isCDA = true;
          break;
        case 'SZO':
          this.isSZO = true;
        default:
          break;
      }
    };
  }

  stateCheckboxChanged(event: any, name) {
    if (event && event.previousValue == true && event.value == false) {
      this.openValdiationPopup = true;
      this.rememberCheckBox = name;
    }
  }

  closeValidationPopup(state: boolean) {
    if (state && this.rememberCheckBox) {
      switch (this.rememberCheckBox) {
        case 'BBD':
          this.item.hasBbd = true;
          break;
        case 'LOT':
          this.item.hasLot = true;
          break;
        case 'MGF':
          this.item.hasMgf = true;
        case 'S/N':
          this.item.hasSn = true;
        }    
    }
    this.openValdiationPopup = false;
  }

  addSpecsInGrid() {
    if (this.item.caenCodeSpecializations && this.item.caenCodeSpecializations.length == 0) {
      this.item.caenCodeSpecializations = [];
    }

    this.multipleSpecializationsIds.forEach(val => {
              if (!this.item.caenCodeSpecializations.find(x => x.caenCodeSpecializationId == val)) {
                let newSpec = new CaenCodeSpecializationXItem();
                newSpec.id = 0;
                newSpec.caenCodeSpecializationId = val;
                newSpec.alt = this.isALT;
                newSpec.cda = this.isCDA;
                newSpec.szo = this.isSZO;
                newSpec.sgf = this.isSGF;

                if (this.checkIfMultipleTrueStates(newSpec)) {
                  newSpec.cda = false;
                  newSpec.szo = false;
                  newSpec.sgf = false;
                }

                this.item.caenCodeSpecializations.push(newSpec);
              }
            });

            this.addSpecialziationsPopup = false;       
  }

  addSpecInGrid() {
    if (this.item.caenCodeSpecializations && this.item.caenCodeSpecializations.length == 0) {
      this.item.caenCodeSpecializations = [];
    }

    if (!this.item.caenCodeSpecializations.find(x => x.caenCodeSpecializationId == this.singleSpecializationsId)) {
      let newSpec = new CaenCodeSpecializationXItem();
      newSpec.id = 0;
      newSpec.caenCodeSpecializationId = this.singleSpecializationsId;
      newSpec.alt = this.isALT;
      newSpec.cda = this.isCDA;
      newSpec.szo = this.isSZO;
      newSpec.sgf = this.isSGF;

      if (this.checkIfMultipleTrueStates(newSpec)) {
        newSpec.cda = false;
        newSpec.szo = false;
        newSpec.sgf = false;
      }

      this.item.caenCodeSpecializations.push(newSpec);
    }

    this.addSpecialziationsPopup = false;
  }

  partnerChange(e) {
    if (e) {
      this.item.partnerId = e;
      this.getSupplierList();
    }
  }

  async updateData() {
    if (this.item && this.item.caenCodeSpecializations && this.item.caenCodeSpecializations.length == 0) {
      this.notificationService.alert('top', 'center', 'Specializari - Produsul trebuie sa contina cel putin o specializare!',
      NotificationTypeEnum.Red, true)
      return;
    }  

    if (this.item && this.itemStock && !this.item.isActive) {
      this.notificationService.alert('top', 'center', 'Nu se poate inactiva produsul deoarece are stoc!', NotificationTypeEnum.Red, true);
      return;
    }

    if (this.item && this.validationGroup.instance.validate().isValid && this.partnerDropdownSearch.validate() && this.validateCodeFour()) {
      this.loaded = true;
      let item = new Item();
      item = this.item;
      //If item is in stock should be valid
      item.isValid = (this.itemStock > 0) ? true : item.isValid;
      if (item && item.customerId) {

        //Tags
        if (item.existingTagIds) {
          const existingTagIds = item.existingTagIds;
          let deleteTags: ItemXTag[] = [];
          existingTagIds.filter(x => !item.tagIds.includes(x)).forEach(id => {
            const t = new ItemXTag();
            t.itemId = item.id;
            t.tagId = id;
            deleteTags.push(t);
          });
          if (deleteTags.length > 0) {
            await this.itemTagService.deleteMultipleAsync(deleteTags);
          }
          let createTags: ItemXTag[] = [];
          item.tagIds.filter(x => !existingTagIds.includes(x)).forEach(id => {
            const t = new ItemXTag();
            t.itemId = Number(item.id);
            t.tagId = id;
            createTags.push(t);
          });
          if (createTags.length > 0) {
            await this.itemTagService.createMultipleAsync(createTags);
          }
        }

        //supplierPriceList
        if (item.stateId == 1 && item.partnerId != null && this.searchSupplierPriceList && this.searchSupplierPriceList.purchaseDuration > 0) {
          await this.supplierPriceListService.updateSupplierPriceListAsync(this.searchSupplierPriceList);
        }

        //CaenCode Specialization
            if (this.item.caenCodeSpecializations && this.item.caenCodeSpecializations.length > 0) {
              this.item.caenCodeSpecializations.forEach(x => x.itemId = item.id );
              this.itemCaenCodeSpecializationService.syncItemCaenCodeSpecializationAsync(this.item.caenCodeSpecializations).then(() => {

                this.refreshSpecDataGrid();
                this.notificationService.alert('top', 'center', 'Specializari - Datele au fost salvate cu succes!',
                  NotificationTypeEnum.Green, true)
              });
            }   
        
        // Item Automation
        this.itemAutomation.onUpdateProduct();
        
        await this.itemService.updateItemAsync(item).then(async r => {
          this.loaded = false;
          if (r) {
            this.item.rowVersion = r.rowVersion;
            //this.itemProperties.saveItemProperties(item.id);
            this.itemEcommerce.saveItemEcommerce(r.id);

            if (r.itemGroupId) {
              this.generalOfferService.UpdateByItemGroupIdAsync(r.itemGroupId);
            }

            this.notificationService.alert('top', 'center', 'Produse - Datele au fost modificate cu succes!',
              NotificationTypeEnum.Green, true);

            if (r.syncERPExternalId || r.syncSeniorExternalId) {
              let userPost = null;
              if (this.posts && this.posts.length && this.users && this.users.length) {
                let user = this.users.find(x => x.userName === this.authenticationService.getUserUserName());
                if (user) {
                  userPost = this.posts.find(x => Number(x.id) === user.postId);
                }
              }
              await this.itemService.updateToErpAsync(item.id, this.authenticationService.getUserUserName(), userPost ? userPost.code : null).then(async chrRes => {
                  if (chrRes) {
                    this.notificationService.alert('top', 'center', 'Udpdate ERP - Produsul a fost updatat!',
                      NotificationTypeEnum.Green, true)
                  } else {
                    this.notificationService.alert('top', 'center', 'Udpdate ERP - Produsul nu fost updatat!',
                      NotificationTypeEnum.Red, true)
                  }
                });
              }

          } else {
            this.notificationService.alert('top', 'center', 'Produse - Datele nu au fost modificate!A aparut o eroare! Verificati Codul / DenumireRO / Denumire ENG sa fie unice!',
              NotificationTypeEnum.Red, true)
          }
        });
      }
    } 
  }

  validateCodeFour(): boolean {
    let i = this.itemGroups.find(x => x.id === this.item.itemGroupId);
    if (i) {
      let code = i.code.substring(0, 4);
      if (this.item.code.indexOf(code) !== -1) {
        return true;
      } else 
      {
        this.notificationService.alert('top', 'center', 'Produse - Primele 4 caractere din codul produsului trebuie sa corespunda cu primele 4 din cod produs grupat!',
        NotificationTypeEnum.Red, true)
        return false;
      }
    }
    return false;
  }

  getItemCreated(item: any, isCreate: boolean) {
    if (isCreate && item.created && item.createdBy && this.users && this.users.length) {
      let user = this.users.find(u => u.id === item.createdBy)
      if (user) {
        return this.translationService.instant(`Creat in data de ${formatDate(item.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre ${user.firstName}/${user.lastName}.`);
      }
    }
    if (!isCreate && item.modified && item.modifiedBy && this.users && this.users.length) {
      let user = this.users.find(u => u.id === item.modifiedBy)
      if (user) {
        return this.translationService.instant(`Ultima modificare in data de ${formatDate(item.modified, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre ${user.firstName}/${user.lastName}.`);
      }
    }
    return '';
  }

  getItemERPSyncHistory(itemId) {
    if (itemId) {
      if (this.itemERPSyncHistoryItem) {
          return this.translationService.instant(`Creat in ERP in data de ${formatDate(this.itemERPSyncHistoryItem.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre ${this.itemERPSyncHistoryItem.postName}/${this.itemERPSyncHistoryItem.userName}.`);
      }
    }
    return '';
  }

  itemTextChanged(e) {
    if (e && e.value && e.value.length >= 4) {
      this.itemGroupsDS = {
        paginate: true,
        pageSize: 15,
        store: this.itemGroups.filter(x => x.code.includes(e.value.substring(0, 4)))
      };
    } 
  }

  async copyItemEvent() {
    if (this.itemToCopy) {
      await this.itemService.getItemByItemId(this.itemToCopy).then(result => {
        if (result) {
          this.item.nameRO = result.nameRO;
          this.item.nameENG = result.nameENG;
          this.item.shortNameRO = result.shortNameRO;
          this.item.shortNameENG = result.shortNameENG;

          this.item.measurementUnitId = result.measurementUnitId;
          this.item.itemTypeId = result.itemTypeId;
          this.item.turnOver = result.turnOver;
          this.item.taxRateVATType = result.taxRateVATType;

          this.item.customCodeId = result.customCodeId;
          this.item.isStockable = result.isStockable;
          this.item.partnerId = result.partnerId;
          this.item.isValid = result.isValid;
        }
      })
    }
  }

  itemGroupChanged(e) {
    if (e && e.value) {
      let find = this.itemGroups.find(x => x.id === e.value);
      this.item.priceCompositionType = find ? find.priceCompositionType : null;

      if (find && this.itemGroupChange) {
        this.item.itemGroupId = find.id;
        this.item.nameRO = find.nameRO;
        this.item.nameENG = find.nameENG;
        this.item.shortNameRO = find.shortNameRO;
        this.item.shortNameENG = find.shortNameENG;  
        if (this.item.isStockable) {
          this.item.customCodeId = find.customCodeId ? find.customCodeId : null;
        }
      }
    } 
    this.itemGroupChange = true;
  }
 
  getRange(from: number, to: number): string {
    return from.toString() + ' - ' + to.toString();
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  onInitNewRow(data) {
    if (data && data.data) {
    }
  }

  public displayDataEvent(e) {
  }

  openERPPopup() {
    this.erpPopup = true;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  deleteSpecRow(e) {
    if (e && e.data && e.data.caenCodeSpecializationId) {
      this.item.caenCodeSpecializations = this.item.caenCodeSpecializations.filter(x => x.caenCodeSpecializationId != e.data.caenCodeSpecializationId);
    }
  }

  refreshSpecDataGrid() {
    if (this.item && this.item.id) {
      this.item.caenCodeSpecializations = [];
      this.itemCaenCodeSpecializationService.getAllByItemId(this.item.id).then(items => {
        if (items && items.length > 0) {
          this.item.caenCodeSpecializations = items;
        }
      });
    }
  }

  insertRow(e) {
    if (e && e.data && e.data.caenCodeSpecializationId) {
      if (this.checkIfMultipleTrueStates(e.data)) {
        this.notificationService.alert('top', 'center', `Specializarea introdusa trebuie sa aiba bifata doar una dintre casutele SGF/CDA/SZO.`,
          NotificationTypeEnum.Red, true);
        e.cancel = true;
      }

      if (this.item.caenCodeSpecializations.filter(x => x.caenCodeSpecializationId == e.data.caenCodeSpecializationId).length > 0) {
        this.notificationService.alert('top', 'center', 'Specializarea este deja introdusa! A aparut o eroare!',
          NotificationTypeEnum.Red, true)
       e.cancel = true;
      }
    }
  }

  onRowSpecUpdated(e) {
    if (e && e.newData && e.newData.caenCodeSpecializationId != e.oldData.caenCodeSpecializationId) {
      const itemUpdating = { ...e.oldData, ...e.newData };
      if (this.checkIfMultipleTrueStates(itemUpdating)) {
        e.key.sgf = e.newData.sgf !== undefined ? e.newData.sgf : false;
        e.key.cda = e.newData.cda !== undefined ? e.newData.cda : false;
        e.key.szo = e.newData.szo !== undefined ? e.newData.szo : false;
      }

      if (this.item.caenCodeSpecializations.filter(x => x.caenCodeSpecializationId == e.newData.caenCodeSpecializationId).length > 0) {
        this.notificationService.alert('top', 'center', 'Specializarea este deja introdusa! A aparut o eroare!',
          NotificationTypeEnum.Red, true)
       e.cancel = true;
      }
    }
  }

  onCellPrepared(e: any) {
    if (e) {
      if (e.rowType == "header") {
        const cellElement = e.cellElement;

        // if (e.cellElement.ariaLabel === "Column SGF") {
        //   cellElement.setAttribute("title", this.textSGF);
        //   $(cellElement).tooltip({
        //     placement: 'bottom'
        //   });
        // }

        // if (e.cellElement.ariaLabel === "Column CDA") {
        //   cellElement.setAttribute("title", this.textCDA);
        //   $(cellElement).tooltip({
        //     placement: 'bottom'
        //   });
        // }

        // if (e.cellElement.ariaLabel === "Column SZO") {
        //   cellElement.setAttribute("title", this.textSZO);
        //   $(cellElement).tooltip({
        //     placement: 'bottom'
        //   });
        // }

        // if (e.cellElement.ariaLabel === "Column ALT") {
        //   cellElement.setAttribute("title", this.textALT);
        //   $(cellElement).tooltip({
        //     placement: 'bottom'
        //   });
        // }

      }
    }
  }

  getSupplierPriceLists() {
    if (this.item && this.item.id && this.item.partnerId) {
      this.supplierPriceListService.getSupplierPriceListsByPartnerId(this.item.partnerId).then(x => {
        this.supplierPriceLists = x;
      });
    }
  }
}
