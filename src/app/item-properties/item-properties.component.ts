import { Component, Input, OnInit } from '@angular/core';
import { Brand } from 'app/models/brand.model';
import { Closing } from 'app/models/closing.model';
import { Color } from 'app/models/color.model';
import { Country, SmallCountry } from 'app/models/country.model';
import { Cut } from 'app/models/cut.model';
import { DeliveryTime } from 'app/models/deliverytime.model';
import { Item } from 'app/models/item.model';
import { ItemProperties } from 'app/models/itemproperties.model';
import { Material } from 'app/models/material.model';
import { Measure } from 'app/models/measure.model';
import { Shape } from 'app/models/shape.model';
import { Size } from 'app/models/size.model';
import { AuthService } from 'app/services/auth.service';
import { BrandService } from 'app/services/brand.service';
import { ClosingService } from 'app/services/closing.service';
import { ColorService } from 'app/services/color.service';
import { CountryService } from 'app/services/country.service';
import { CutService } from 'app/services/cut.service';
import { DeliverytimeService } from 'app/services/deliverytime.service';
import { ItemPropertiesService } from 'app/services/item-properties.service';
import { MaterialService } from 'app/services/material.service';
import { MeasureService } from 'app/services/measure.service';
import { ShapeService } from 'app/services/shape.service';
import { SizeService } from 'app/services/size.service';

@Component({
  selector: 'app-item-properties',
  templateUrl: './item-properties.component.html',
  styleUrls: ['./item-properties.component.css']
})
export class ItemPropertiesComponent implements OnInit {
@Input() item: Item;

selectedItemProperties: ItemProperties;
isOnSave: boolean;
isOwner: boolean;
countriesDS: any;
countries: SmallCountry[] = [];
brands: Brand[] = [];
shapes: Shape[] = [];
sizes: Size[] = [];
colors: Color[] = [];
cuts: Cut[] = [];
closings: Closing[] = [];
measures: Measure[] = [];
materials: Material[] = [];
deliveryTimes: DeliveryTime[] = [];

  constructor(private itemPropertiesService: ItemPropertiesService,
    private countryService: CountryService,
    private brandService: BrandService,
    private shapeService: ShapeService,
    private sizeService: SizeService,
    private colorService: ColorService,
    private cutService: CutService,
    private closingService: ClosingService,
    private measureService: MeasureService,
    private materialService: MaterialService,
    private deliveryTimeService: DeliverytimeService,
    private authenticationService: AuthService
    ) {
      this.authenticationService.currentUserSubject.subscribe(token => {
        if (token) {
          if (this.authenticationService.isUserOwner()) {
            this.isOwner = true;
          }
        }
      });
     }

  ngOnInit(): void {
  }

  async loadData() {
    Promise.all([this.getItemProperties(), this.getBrands(), this.getShapes(), this.getSizes(), this.getColors(), this.getCuts(),
      this.getClosing(), this.getMeasures(), this.getMaterials(), this.getDeliveryTimes(), this.getCountries()]).then(x => {
   });
  }

  async getItemProperties() {
    await this.itemPropertiesService.getItemPropertiesAsync(this.item.id).then(item => {
      if (item) {
        this.selectedItemProperties = item;
        this.isOnSave = false;
      } else {
        this.selectedItemProperties = new ItemProperties();
        this.isOnSave = true;
      }
    })
  }

  async getBrands() {
    if (this.isOwner) {
      await this.brandService.getAllBrandsAsync().then(brandItems => {
        this.brands = brandItems;
      });
    } else {
      await this.brandService.getBrandsByCustomerIdAsync().then(brandItems => {
        this.brands = brandItems;
      });
    }
  }

  async getShapes() {
    if (this.isOwner) {
      await this.shapeService.getAllShapesAsync().then(i => {
        this.shapes = i;
      });
    } else {
      await this.shapeService.getAllShapesByCustomerIdAsync().then(i => {
        this.shapes = i;
      });
    }
  }

  async getSizes() {
    if (this.isOwner) {
      await this.sizeService.getAllSizesAsync().then(i => {
        this.sizes = i;
      });
    } else {
      await this.sizeService.getAllSizesByCustomerIdAsync().then(i => {
        this.sizes = i;
      });
    }
  }

  async getColors() {
    if (this.isOwner) {
      await this.colorService.getAllColorsAsync().then(i => {
        this.colors = i;
      });
    } else {
      await this.colorService.getAllColorsByCustomerIdAsync().then(i => {
        this.colors = i;
      });
    }
  }

  async getCuts() {
    if (this.isOwner) {
      await this.cutService.getAllCutsAsync().then(i => {
        this.cuts = i;
      });
    } else {
      await this.cutService.getAllCutsAsync().then(i => {
        this.cuts = i;
      });
    }
  }

  async getClosing() {
    if (this.isOwner) {
      await this.closingService.getAllClosingsAsync().then(i => {
        this.closings = i;
      });
    } else {
      await this.closingService.getAllClosingsByCustomerIdAsync().then(i => {
        this.closings = i;
      });
    }
  }

  async getMeasures() {
    if (this.isOwner) {
      await this.measureService.getAllMeasuresAsync().then(i => {
        this.measures = i;
      });
    } else {
      await this.measureService.getAllMeasuresByCustomerIdAsync().then(i => {
        this.measures = i;
      });
    }
  }

  async getMaterials() {
    if (this.isOwner) {
      await this.materialService.getAllMaterialsAsync().then(i => {
        this.materials = i;
      });
    } else {
      await this.materialService.getAllMaterialsByCustomerIdAsync().then(i => {
        this.materials = i;
      });
    }
  }

  async getDeliveryTimes() {
    if (this.isOwner) {
      await this.deliveryTimeService.getAllDeliveryTimesAsync().then(i => {
        this.deliveryTimes = i;
      });
    } else {
      await this.deliveryTimeService.getAllDeliveryTimesByCustomerIdAsync().then(i => {
        this.deliveryTimes = i;
      });
    }
  }

  async getCountries() {
    await this.countryService.getAllSmallCountriesAsync().then(items => {
      this.countries = items;
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries
      };
    });
  }

  async saveItemProperties(itemId: number) {

    if (this.isOnSave) {
      if (this.selectedItemProperties && Object.keys(this.selectedItemProperties).filter(x => this.selectedItemProperties[x]).length > 0) {
        this.selectedItemProperties.itemId = itemId;
        await this.itemPropertiesService.createItemPropertiesAsync(this.selectedItemProperties).then(item => { if (item) {
          this.isOnSave = false;
        } })
      }
    } else {
      if (this.selectedItemProperties && Object.keys(this.selectedItemProperties).filter(x => this.selectedItemProperties[x]).length > 0) {
      await this.itemPropertiesService.updateItemPropertiesAsync(this.selectedItemProperties).then(item => {
      })
    }
    }
  }
}
