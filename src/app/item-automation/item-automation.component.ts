import { Component, Input, OnInit } from '@angular/core';
import { ItemAutomation } from 'app/models/item-automation.model';
import { Item } from 'app/models/item.model';
import { AuthService } from 'app/services/auth.service';
import { ItemAutomationService } from 'app/services/item-automation.service';

@Component({
  selector: 'app-item-automation',
  templateUrl: './item-automation.component.html',
  styleUrls: ['./item-automation.component.css']
})
export class ItemAutomationComponent implements OnInit {
  
  //#region Fields

  @Input() item: Item;
  
  itemAutomation: ItemAutomation;

  //#endregion

  //#region Hooks

  constructor(
    private authenticationService: AuthService,
    private itemAutomationService: ItemAutomationService,
  ) { }

  ngOnInit(): void {
    this.setItemAutomation();
  }

  //#endregion

  //#region Public methods

  onSaveProduct(itemId: number) {
    this.createItemAutomation(itemId);
  }

  onUpdateProduct() {
    this.itemHasAutomationInDB().then(r => {
      if (r) {
        this.updateItemAutomation();
      } else {
        this.createItemAutomation(this.item.id);
      }
    });
  }

  //#endregion

  //#region Private methods

  private createItemAutomation(itemId: number) {
    this.itemAutomation.itemId = itemId;
    this.itemAutomationService.createAsync(this.itemAutomation).then(r => {
      this.itemAutomation = r;
    });
  }

  private async getItemAutomation() {
    if (this.item.id) {
      await this.itemAutomationService.getByItemId(this.item.id).then(r => {
        this.itemAutomation = r;
      });
    }
  }

  private async itemHasAutomationInDB() {
    let response = false;
    await this.itemAutomationService.getByItemId(this.item.id).then(r => {
      if (r) {
        response = true;
      }
    })
    return response;
  }

  private setDefaultItemAutomation() {
    this.itemAutomation = {
      intern: true,
      intraCommunity: true,
      external: true,
      authorizationClient: false,
      ansvsa: false,
      humanFarmacy: false,
      extractionCode: false,
      created: new Date(),
      modified: new Date(),
      createdBy: this.authenticationService.getUserId(),
      modifiedBy: this.authenticationService.getUserId(),
      customerId: +this.authenticationService.getUserCustomerId(),
      siteId: this.authenticationService.getUserSiteId(),
    }
  }

  private setItemAutomation() {
    this.getItemAutomation().then(r => {
      if (!this.itemAutomation) {
        this.setDefaultItemAutomation();
      }
    });
  }

  private updateItemAutomation() {
    this.itemAutomationService.updateAsync(this.itemAutomation);
  }

  //#endregion
}