import { Component, OnInit, Input, Output, EventEmitter, ViewChild, SimpleChanges, ElementRef } from '@angular/core';
import { Partner } from 'app/models/partner.model';
import { IPageActions } from 'app/models/ipageactions';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { Country } from 'app/models/country.model';
import { County } from 'app/models/county.model';
import { City } from 'app/models/city.model';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { DxValidatorComponent, DxDataGridComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { PartnerService } from 'app/services/partner.service';
import { NotificationService } from 'app/services/notification.service';
import { GoogleGeocodingService } from 'app/services/google-geocoding.service';
import { GeocoderStatus } from '@agm/core';
import { CaenCodeService } from 'app/services/caen-code.service';
import CustomStore from 'devextreme/data/custom_store';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { Constants } from 'app/constants';
import { PageActionService } from 'app/services/page-action.service';
import { PositionTypeEnum } from 'app/enums/positionTypeEnum';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { SupplierTypeEnum } from 'app/enums/supplierTypeEnum';
import { PartnerActivityAllocation } from 'app/models/partneractivityallocation.model';
import { PartnerActivity } from 'app/models/partneractivity.model';
import { CaenCodeSpecializationPartnerLocationService } from 'app/services/caen-code-specialization-partner-location.service';
import { CaenCodeSpecializationPartnerActivityAllocationService } from 'app/services/caen-code-specialization-partner-activity-allocation.service';
import { CaenCodeSpecializationXPartnerLocation } from 'app/models/caencodespecializationxpartner.model';
import { PartnerClientCreditControl } from 'app/models/partnerclientcreditcontrol.model';
import { PartnerSupplierCreditControl } from 'app/models/partnersuppliercreditcontrol.model';
import { PartnerClientCreditControlService } from 'app/services/partner-client-credit-control.service';
import { PartnerSupplierCreditControlService } from 'app/services/partner-supplier-credit-control.service';
import * as _ from 'lodash';
import { PartnerLocationSalePriceListService } from 'app/services/partner-location-sale-price-list.service';
import { SalePriceListService } from 'app/services/sale-price-list.service';
import { PartnerFinancialInfoComponent } from 'app/partner-financial-info/partner-financial-info.component';
import { SalePriceList } from 'app/models/salepricelist.model';
import { User } from 'app/models/user.model';
import { Language } from 'app/models/language.model';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { ERPSyncOptions } from 'app/models/erp-sync-options.model';
import { PartnerERPSyncHistoryService } from 'app/services/partner-erp-sync-history.service';
import { PartnerERPSyncHistory } from 'app/models/partner-erp-sync-history.model';
import { DecimalPipe, formatDate } from '@angular/common';
import { PartnerXTag } from 'app/models/partnerxtag.model';
import { Tag } from 'app/models/tag.model';
import { PartnerTagService } from 'app/services/partner-tag.service';
import { ItemGroupCategory } from 'app/models/itemgroupcategory.model';
import { PartnerLocationXItemGroupCategory } from 'app/models/partnerlocationxitemgroupcategory.model';
import { PartnerLocationItemGroupCategoryService } from 'app/services/partner-location-item-group-category.service';
import { ItemGroupCode } from 'app/models/itemgroupcode.model';
import { PartnerLocationItemGroupCodeService } from 'app/services/partner-location-item-group-code.service';
import { PartnerLocationXItemGroupCode } from 'app/models/partnerlocationxitemgroupcode.model';
import { ItemGroupCategoryCodeService } from 'app/services/item-group-category-code.service';
import { PartnerNameEquivalenceComponent } from 'app/partner-name-equivalence/partner-name-equivalence.component';
import { PartnerLocationContactComponent } from 'app/partner-location-contact/partner-location-contact.component';
import { PartnerLocationScheduleComponent } from 'app/partner-location-schedule/partner-location-schedule.component';
import { PartnerContractDetailsComponent } from 'app/components/partner-contract-details/partner-contract-details.component';
import { PartnerObservationsComponent } from 'app/partner-observations/partner-observations.component';
import { Clipboard } from '@angular/cdk/clipboard';
import { ClientTypeEnum } from 'app/enums/clientTypeEnum';
import { PartnerLocationSecondaryActivityAllocationService } from 'app/services/partner-location-secondary-activity-allocation.service';
import { PartnerLocationXSecondaryActivityAllocation } from 'app/models/partnerlocationxsecondaryactivityallocation.model';
import { PartnerWebsiteComponent } from 'app/partner-website/partner-website.component';
import { PartnerXItemGroupCodeService } from 'app/services/partner-x-item-group-code.service';
import { SalePriceListFilterVM } from 'app/models/salepricelistfilter.model';
import { PartnerItemsComponent } from 'app/partner-items/partner-items.component';
import { PartnerSpecializationsComponent } from 'app/partner-specializations/partner-specializations.component';
import { PartnerSalesPriceListComponent } from 'app/partner-sales-price-list/partner-sales-price-list.component';
import { environment } from 'environments/environment';
import { PartnerStatusHistoryService } from 'app/services/partner-status-history.service';
import { PartnerStatusHistory } from 'app/models/partner-status-history.model';
import { PartnerStatusEnum } from 'app/enums/partnerStatusEnum';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { YearlyTurnoverComponent } from 'app/yearly-turnover/yearly-turnover.component';
import { CompetitorTypeEnum } from 'app/enums/competitorTypeEnum';
import { PersonTypeEnum } from 'app/enums/personTypeEnum';
import { PartnerSubscriptionComponent } from 'app/partner-subscription/partner-subscription.component';
import { CreditIncreaseRequestBtnEmailDetails, PartnerFinancialInfoVM } from 'app/models/partnerfinancialinfo.model';
import { DiscountGrid } from 'app/models/discount-grid.model';
import { PartnerCIPCheckVM } from 'app/models/partnercipcheck.model';
import { HelperService } from 'app/services/helper.service';
import { PartnerFinancialInfoService } from 'app/services/partner-financial-info.service';
import { PartnerContractService } from 'app/services/partner-contract.service';
import { DiscountGridService } from 'app/services/discount-grid.service';
import { PaymentInstrumentService } from 'app/services/payment-instrument.service';
import { PartnerCipCheckService } from 'app/services/partner-cip-check.service';
import { DiscountGridDetailsService } from 'app/services/discount-grid-details.service';
import { PartnerReviewService } from 'app/services/partner-review.service';
import * as moment from 'moment';
import { CountyService } from 'app/services/county.service';
import { PartnerAuthorizationComponent } from 'app/partner-authorization/partner-authorization.component';
import { SupplierPriceList } from 'app/models/supplierpricelist.model';
import { SupplierPriceListService } from 'app/services/supplier-price-list.service';

@Component({
selector: 'app-partner-details',
  templateUrl: './partner-details.component.html',
  styleUrls: ['./partner-details.component.css']
})
export class PartnerDetailsComponent implements OnInit {
  @Input() selectedPartner: Partner;
  @Input() tags: Tag[];
  @Input() partnerLegalForms: any;
  @Input() partnerStatuses: any;
  @Input() addressTypes: any;
  @Input() clientTypes: any;
  @Input() supplierTypes: any;
  @Input() competitorTypes: any;
  @Input() personTypes: any;
  @Input() posts: any;
  @Input() publicInstitutions: any;

  @Input() users: User[];
  @Input() cities: City[];
  @Input() citiesDS: any;

  @Input() counties: County[];
  @Input() countiesDS: any;

  @Input() countries: Country[];
  @Input() countriesDS: any;
  @Input() language: Language[];

  @Input() partnerActivity: PartnerActivity[];
  @Input() partnerActivityAllocation: PartnerActivityAllocation[];

  @Input() itemGroupCategories: ItemGroupCategory[];
  @Input() itemGroupCodes: ItemGroupCode[];

  @Input() messagingApps: any[];
  @Input() departments: any[];
  @Input() paymentInstruments: any;
  @Input() currencies: any;
  @Input() caenSpecializationsDS: any;
  @Output() visible: EventEmitter<any> = new EventEmitter<any>();
  @Output() isOnSavePartnerLocationOutput: EventEmitter<any> = new EventEmitter<any>();
  @Output() initialPartnerLocationOutput: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('popupContent', { static: false }) popupContent: ElementRef;

  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;
  @ViewChild('secondValidationGroup') secondValidationGroup: DxValidatorComponent;

  @ViewChild('partnerDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('partnerGridToolbar') gridToolbar: GridToolbarComponent;

  @ViewChild('locationContactComponent') locationContactComponent: PartnerLocationContactComponent;
  @ViewChild('partnerContractDetails') partnerContractDetails: PartnerContractDetailsComponent;
  @ViewChild('partnerObservationsComponent') partnerObservationsComponent: PartnerObservationsComponent;
  @ViewChild('financialInfoComponent') financialInfoComponent: PartnerFinancialInfoComponent;
  @ViewChild('yearlyTurnoverComponent') yearlyTurnoverComponent: YearlyTurnoverComponent;
  @ViewChild('partnerNameEquivalenceComponent') partnerNameEquivalenceComponent: PartnerNameEquivalenceComponent;
  @ViewChild('partnerLocationScheduleComponent') partnerLocationScheduleComponent: PartnerLocationScheduleComponent;
  @ViewChild('partnerWebsite') partnerWebsiteComponent: PartnerWebsiteComponent;
  @ViewChild('partnerItemsComponent') partnerItemsComponent: PartnerItemsComponent;
  @ViewChild('partnerSalesPriceListComponent') partnerSalesPriceListComponent: PartnerSalesPriceListComponent;
  @ViewChild('partnerSpecializationsComponent') partnerSpecializationsComponent: PartnerSpecializationsComponent;
  @ViewChild('partnerSubscription') partnerSubscriptionComponent: PartnerSubscriptionComponent;
  @ViewChild('partnerAuthorization') partnerAuthorizationComponent: PartnerAuthorizationComponent;

  partner: Partner;
  partnerClientCreditControl: PartnerClientCreditControl;
  partnerSupplierCreditControl: PartnerSupplierCreditControl;
  opavPaymentInstrumentId: number;

  actions: IPageActions;
  partnerLocations: PartnerLocation[];
  isOnSave: boolean;
  showHistory: boolean = false;
  isOnSavePartnerLocation: boolean;
  popupVisible: boolean;
  showPartnerLocationSchedulePopup: boolean;
  tabError: boolean;

  selectedRows: any[];
  selectedRowIndex = -1;

  supplierPosts: any[]
  salesPosts: any[];

  rowIndex: any;
  selectedCountryId: number;
  selectedCountyId: number;
  loaded: boolean;
  isPartnerSentToERP: boolean = false;
  isPartnerSentToSenior: boolean = false;
  withoutFiscalCode: boolean;
  withoutTradeRegisterNumber: boolean;
  erpPopup: boolean = false;
  seniorPopup: boolean = false;
  specialMentionsPopup: boolean = false;

  selectedPartnerLocation: PartnerLocation = new PartnerLocation();
  caenGridDataSource: any;
  salePriceListDS: any;
  salePriceList: SalePriceList[];
  supplierSalePriceLists: SupplierPriceList[] = [];
  partnerSearchDS: Partner[] = [];
  caenGridBoxValue: number[];
  detailsTabs: any;
  customerId: string;
  searchPartner: string;
  isTabActive: string;
  groupedText: string;
  isOwner: boolean = false;
  isAdmin: boolean = false;
  hasERPActive: boolean = false;
  initialPartnerLocation: PartnerLocation;
  partnerERPSyncHistoryItems: PartnerERPSyncHistory[] = [];
  
  didCopy: boolean = false;
  rawCaenCodes: any;
  sortedCaenSpecializationsDS: any; 
  isSpecializationInvalid: boolean;
  isSalesPriceListInvalid: boolean;
  partnerStatusHistory: string;
  originalPartnerStatusId: number;
  implicitPartnerLocation: PartnerLocation;
  partnersForTurnover: any;
  supplierId: number;
  customNumberFormat = '#';
  creditIncreaseRequestBtnEmailDetails: CreditIncreaseRequestBtnEmailDetails;
  yearlyTurnOvers: any[] = [];
  currentMonthValue: number;
  previousMonthValue: number;
  sumOfLastThreeMonths: number;
  productConventionGrid: DiscountGrid;
  discountGrids: DiscountGrid[] = [];
  salePriceLists: SalePriceList[] = [];
  shouldDisplayCreditIncreaseRequestEmailTemplate: boolean;
  lastCIPCheck: PartnerCIPCheckVM;
  partnerFinancialInfo: PartnerFinancialInfoVM[] =[];
  partnerCIPCheck: PartnerCIPCheckVM[] =[];
  isCreditIncreaseRequestPopupOpen: boolean;
  gmtCounties: County[] = [];
  implicitSupplierPostId: number;
  
  constructor(
    private translationService: TranslateService,
    private partnerService: PartnerService,
    private authenticationService: AuthService,
    private partnerTagService: PartnerTagService,
    private partnerLocationService: PartnerLocationService,
    private notificationService: NotificationService,
    private googleGeocodingService: GoogleGeocodingService,
    private caenCodeService: CaenCodeService,
    private pageActionService: PageActionService,
    private partnerLocationItemGroupCategoryService: PartnerLocationItemGroupCategoryService,
    private partnerLocationItemGroupCodeService: PartnerLocationItemGroupCodeService,
    private partnerLocationSecondaryActivityAllocationService: PartnerLocationSecondaryActivityAllocationService,
    private itemGroupCategoryCodeService: ItemGroupCategoryCodeService,
    private partnerClientCreditControlService: PartnerClientCreditControlService,
    private partnerSupplierCreditControlService: PartnerSupplierCreditControlService,
    private caenCodeSpecializationPartnerLocationService: CaenCodeSpecializationPartnerLocationService,
    private caenCodeSpecializationPartnerActivityAllocationService: CaenCodeSpecializationPartnerActivityAllocationService,
    private partnerLocationSalePriceListService: PartnerLocationSalePriceListService,
    private salePriceListService: SalePriceListService,
    private partnerLocationContactService: PartnerLocationContactService,
    private partnerERPSyncHistoryService: PartnerERPSyncHistoryService,
    private clipboard: Clipboard,
    private partnerXItemGroupCodeService: PartnerXItemGroupCodeService,
    private partnerStatusHistoryService: PartnerStatusHistoryService,
    private helperService: HelperService, private financialInfoService: PartnerFinancialInfoService, private decimalPipe: DecimalPipe,
    private partnerContractService: PartnerContractService, private discountGridService: DiscountGridService, 
    private discountGridDetailsService: DiscountGridDetailsService, private paymentInstrumentService: PaymentInstrumentService,
    private partnerReviewsService: PartnerReviewService, private cipCheckService: PartnerCipCheckService, private countyService: CountyService,
    private partnerLocationXItemGroupCategoryService: PartnerLocationItemGroupCategoryService,
    private supplierPriceListService: SupplierPriceListService) {


    this.initialPartnerLocation = new PartnerLocation();
    this.initialPartnerLocation.caenCodeSpecializationIds = [];
    this.initialPartnerLocation.salePriceListIds = [];
    this.initialPartnerLocation.supplierSalePriceListIds = [];
    this.initialPartnerLocation.itemGroupCategoryIds = [];
    this.initialPartnerLocation.itemGroupCodeIds = [];
    this.salePriceList = [];

    this.getDisplayExprItemGroupCategories = this.getDisplayExprItemGroupCategories.bind(this);
    this.getDisplayExprItemGroupCodes = this.getDisplayExprItemGroupCodes.bind(this);
    this.getDisplayExprAddressTypes = this.getDisplayExprAddressTypes.bind(this);
    this.getDisplayExprPartnerActivity = this.getDisplayExprPartnerActivity.bind(this);
    this.getDisplayExprSalePriceLists = this.getDisplayExprSalePriceLists.bind(this);
    
    this.isTabActive = this.authenticationService.getCustomerPageTitleTheme();
    this.isOwner = this.authenticationService.isUserOwner();
    this.isAdmin = this.authenticationService.isUserAdmin();
    this.hasERPActive = this.authenticationService.getCustomerERPActive();
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();
    this.customerId = this.authenticationService.getUserCustomerId();
  }

  ngOnInit(): void {
    this.detailsTabs = [{
      id: 1,
      name: 'Date locatii',
      'isActive': true
    },
    {
      id: 2,
      name: 'Credit Control',
      'isActive': false
    },
    {
      id: 3,
      name: 'Date de contact',
      'isActive': false
    },
    {
      id: 4,
      name: 'Recenzii',
      'isActive': false
    },
    {
      id: 5,
      name: 'Contract',
      'isActive': false
    },
    {
      id: 6,
      name: 'Liste de pret',
      'isActive': false
    },
    {
      id: 7,
      name: 'Bonitate',
      'isActive': false
    },
    {
      id: 8,
      name: 'Date Financiare',
      'isActive': false
    },
    {
      id: 9,
      name: 'Rulaj',
      'isActive': false
    },
    {
      id: 11,
      name: 'Specializari',
      'isActive': false
    },
    {
      id: 12,
      name: 'Afiliere',
      'isActive': false
    },
    {
      id: 13,
      name: 'Observatii',
      'isActive': false
    },
    {
      id: 14,
      name: 'Produse',
      'isActive': false
    },
    {
      id: 15,
      name: 'Websites',
      'isActive': false
    },
    {
      id: 16,
      name: 'Abonare',
      'isActive': false
    },
    {
      id: 17,
      name: 'Autorizatii',
      'isActive': false
    }
    
    ];
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.caenSpecializationsDS) {
      this.sortedCaenSpecializationsDS = [...this.caenSpecializationsDS.store];
    }
 }

  toggleMode(tab: any) {
    this.tabError = false;
    this.detailsTabs.find(x => x.isActive).isActive = false;
    tab.isActive = true;

    if (tab.id === 1 && !this.isOnSave && this.selectedPartner) {
      this.loadCaenCodeSpecializationsForPartnerLocations();
    }

    if (tab.id === 3 && !this.isOnSave && this.selectedPartner) {
      this.locationContactComponent.getPartnerLocationContacts();
    }

    if (tab.id === 5 && !this.isOnSave && this.selectedPartner) {
      this.partnerContractDetails.loadData();
    }

    if (tab.id === 6 && !this.isOnSave && this.selectedPartner && this.selectedPartner.isClient) {
      this.partnerSalesPriceListComponent.getActivitiesAndSalePriceList();
    }

    if (tab.id === 7 && !this.isOnSave && this.selectedPartner) {
      this.financialInfoComponent.getPartnerFinancialInfo();
    }
    if (tab.id === 9 && !this.isOnSave && this.selectedPartner) {
      this.yearlyTurnoverComponent.getData();
    }
    if (tab.id === 11 && !this.isOnSave && this.selectedPartner) {
      this.partnerSpecializationsComponent.getActivitiesAndSpecializations();
    }
    if (tab.id === 13 && !this.isOnSave && this.selectedPartner) {
      this.partnerObservationsComponent.loadData();
    }
    if (tab.id === 15 && !this.isOnSave && this.selectedPartner) {
      this.partnerWebsiteComponent.loadData();
    }
    if (tab.id === 16 && !this.isOnSave && this.selectedPartner) {
      this.partnerSubscriptionComponent.loadData();
    }
    if (tab.id === 17 && !this.isOnSave && this.selectedPartner) {
      this.partnerAuthorizationComponent.loadData();
    }
  }

  loadCaenCodeSpecializationsForPartnerLocations() {
    this.caenCodeSpecializationPartnerLocationService.getAllCaenCodeSpecializationPartnerLocationByPartnerAsync(this.selectedPartner.id).then(items => {
      if (items) {
        this.partnerLocations.forEach(u => {
          const i = items.filter(x => x.partnerLocationId === u.id)
            .map(x => x.caenCodeSpecializationId);
          if (i) {
            u.caenCodeSpecializationIds = [...i];
            u.existingCaenCodeIds = [...i];
          }
        });
      }
    });
  }

  loadItemGroupCategoryByPartnerLocationId(partnerLocationId: number) {
    this.partnerLocationItemGroupCategoryService.getAllPartnerLocationItemGroupCategoryByPartnerLocationAsync(partnerLocationId).then(items => {
      if (items) {
        this.selectedPartnerLocation.itemGroupCategoryIds = items.map(x => x.itemGroupCategoryId);
        this.selectedPartnerLocation.existingItemGroupCategoryIds = items.map(x => x.itemGroupCategoryId);
      }
    });
  }

  loadItemGroupCodeByPartnerLocationId(partnerLocationId: number) {
    this.partnerLocationItemGroupCodeService.getAllPartnerLocationItemGroupCodeByPartnerLocationAsync(partnerLocationId).then(items => {
      if (items) {
        this.selectedPartnerLocation.itemGroupCodeIds = items.map(x => x.itemGroupCodeId);
        this.selectedPartnerLocation.existingItemGroupCodeIds = items.map(x => x.itemGroupCodeId);
      }
    });
  }

  loadSecondaryActivityAllocationByPartnerLocationId(partnerLocationId: number) {
    this.partnerLocationSecondaryActivityAllocationService.getAllByPartnerLocationIdAsync(partnerLocationId).then(items => {
      if (items) {
        this.selectedPartnerLocation.partnerSecondaryActivityAllocationIds = items.map(x => x.partnerActivityAllocationId);
        this.selectedPartnerLocation.existingPartnerSecondaryActivityAllocationIds = items.map(x => x.partnerActivityAllocationId);
      }
    });
  }

  async loadData() {
    this.partnerLocations = [];
    this.caenGridBoxValue = [];

    this.partner = new Partner();
    this.opavPaymentInstrumentId = this.paymentInstruments.find(x => x.code === 'OPA' || x.code === 'OPAV')?.id;
    this.partnerClientCreditControl = new PartnerClientCreditControl(this.opavPaymentInstrumentId);
    this.partnerSupplierCreditControl = new PartnerSupplierCreditControl(this.opavPaymentInstrumentId);

    this.searchPartner = null;
    this.initialPartnerLocation = new PartnerLocation();
    this.initialPartnerLocation.isCorrespondence = false;
    this.initialPartnerLocation.isOffice = false;
    this.initialPartnerLocation.isHome = false;
    this.initialPartnerLocation.isRegisteredOffice = false;
    this.initialPartnerLocation.isDeposit = false;
    this.setSupplierAndSalesPosts();
    this.getSalePriceListData();
    this.getSupplierSalePriceListData();
    await this.caenCodeService.getAllCaenCodesAsync().then(async caenCodes => {
      this.caenGridDataSource = new CustomStore({
        loadMode: 'raw',
        key: 'id',
        load: function () {
          return caenCodes;
        }
      });
      this.rawCaenCodes = caenCodes;
      if (this.selectedPartner) {
        this.isOnSave = false;
        this.partner = this.selectedPartner;
        this.isPartnerSentToERP = (this.partner.syncERPExternalId && this.partner.syncERPExternalId.length) ? true : false;
        this.isPartnerSentToSenior = (this.partner.syncSeniorExternalId) ? true : false;
        this.withoutTradeRegisterNumber = this.partner.tradeRegisterNumber ? false : true;
        this.withoutFiscalCode = this.partner.fiscalCode ? false : true;
        this.caenGridBoxValue.push(this.partner.caenCodeId);
        this.originalPartnerStatusId = this.partner.partnerStatusId;
        this.partnersForTurnover = {};
        this.partnersForTurnover.store = [this.selectedPartner];
        this.supplierId = this.selectedPartner.id;
        this.partner.competitor = this.partner.competitor || CompetitorTypeEnum.No;
        await this.getSearchPartner();
        await this.getCounties(); // needed for location GMT 
        await this.getPartnerLocations();
        await this.getCreditControl(this.partner);
        await this.getPartnerERPSyncHistoryItems(this.partner.id);
        await this.getPartnerTags(this.partner.id);
        await this.getPartnerXItemGroupCodes();
        await this.getPartnerStatusHistory();
        await this.getPartnerFinancialInfo();

        this.implicitPartnerLocation = this.partnerLocations.find(i => i.isImplicit);

        if (!this.isOnSave && this.selectedPartner) {
          this.loadCaenCodeSpecializationsForPartnerLocations();
        }

        if ((!this.isPartnerSentToERP || !this.isPartnerSentToSenior) && this.hasUndefinedLocations()) {
          this.notificationService.alert('top', 'center', 'Acest partener are una dintre locatii incorecte - UNDEFINED -! Pentru a se putea trimite in ERP corecteaza localitatea pe baza campului de Descriere Locatie!',
          NotificationTypeEnum.Red, true);
        }
        
      } else {
        this.withoutFiscalCode = false;
        this.withoutTradeRegisterNumber = false;
        this.isOnSave = true;
        this.partner.isPrincipalActivityUniqueForAllLocations = true;
        this.partner.partnerStatusId = PartnerStatusEnum.Functioning;
      }

      this.checkForImplicitSupplierAgent();
    });
    setTimeout(() => {
      this.setGridInstances();
    }, 200);
  }

  async getCounties() {
    await this.countyService.getAllCountysAsync().then(items => {
      this.gmtCounties = items;
    });
  }

  async getPartnerFinancialInfo() {
    this.partnerFinancialInfo = [];
    if (this.partner && this.partner.id) {
      await this.financialInfoService.getByPartnerIdAsync(this.partner.id).then(items => {
        if (items) {
          this.partnerFinancialInfo = items;
        } else {
          this.partnerFinancialInfo = [];
        }
      });
      await this.cipCheckService.getByPartnerIdAsync(this.partner.id).then(items => {
        if (items) {
          this.partnerCIPCheck = items;
          this.lastCIPCheck = items[0];
        } else {
          this.partnerCIPCheck = [];
          this.lastCIPCheck = null;
        }
      });
    }
  }

  async getPartnerERPSyncHistoryItems(partnerId) {
    await this.partnerERPSyncHistoryService.getByPartnerId(partnerId).then(x => {
      this.partnerERPSyncHistoryItems = x;
    });
  }

  async getPartnerLocations() {
    await this.partnerLocationService.getPartnerLocationsIncludingInactiveByPartnerID(this.partner.id).then(locations => {
      this.partnerLocations = locations;
      this.partnerLocations.forEach(pl => {
        pl.salePriceListIds = pl.partnerLocationXSalePriceList.map(x => x.salePriceListId)
        pl.supplierSalePriceListIds = pl.partnerLocationXSupplierPriceList.map(x => x.supplierSalePriceListId)

        let gmtOffset = this.gmtCounties.find(f => f.id === pl.countyId)?.gmtOffset;
        let gmt = this.computeLocalTime(gmtOffset);
        pl.gmt = gmt;
      });
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries
      };
    });
  }

  async getCreditControl(partner: any) {
    if (partner.isClient) {
      await this.partnerClientCreditControlService.getPartnerClientCreditControlAsync(partner.id).then(clientCreditControl => {
        if (clientCreditControl) {
          this.partnerClientCreditControl = clientCreditControl;
        } else {
          this.partnerClientCreditControl = new PartnerClientCreditControl(this.opavPaymentInstrumentId);
        }
      });
    }
    if (partner.isSupplier) {
      await this.partnerSupplierCreditControlService.getPartnerSupplierCreditControlAsync(partner.id).then(supplierCreditControl => {
        if (supplierCreditControl) {
          this.partnerSupplierCreditControl = supplierCreditControl;
        } else {
          this.partnerSupplierCreditControl = new PartnerSupplierCreditControl(this.opavPaymentInstrumentId);
        }
      });
    }
  }

  async getSalePriceListData() {
    if (this.isOwner) {
      await this.salePriceListService.getAllSalePriceListsWithProductConventionsAsync().then(i => {
        this.salePriceList = i;
      });
    } else {
      await this.salePriceListService.getSalePriceListsAsyncByIDWithProductConventions().then(i => {
        this.salePriceList = i;
      });
    }
  }

  async getSupplierSalePriceListData() {
    if (this.isOwner) {
      await this.supplierPriceListService.getAllSupplierPriceListsAsync().then(i => {
        this.supplierSalePriceLists = i;
      });
    } else {
      await this.supplierPriceListService.getSupplierPriceListsAsyncByID().then(i => {
        this.supplierSalePriceLists = i;
      });
    }
  }

  async getPartnerTags(partnerId) {
    await this.partnerTagService.getByPartnerIdAsync(partnerId).then(tags => {
      if (tags && tags.length > 0) {
        this.partner.tagIds = tags.map(x => x.tagId);
        this.partner.existingTagIds = tags.map(x => x.tagId);
      } else {
        this.partner.tagIds = [];
        this.partner.existingTagIds = [];
      }
    });
  }

  async getSearchPartner() {
    if (this.selectedPartner && this.selectedPartner.partnerLikeId) {
      this.partnerService.getByPartnerId(this.selectedPartner.partnerLikeId).then(partners => {
        this.partnerSearchDS = partners;
      })
    }
  }

  setSupplierAndSalesPosts() {
    if (this.departments && this.departments.length > 0) {
      const deps = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Supply);
      if (deps && deps.length > 0 && deps[0].offices) {
        const off = deps.map(x => x.offices).reduce((x, y) => x.concat());
        if (off && off.some(x => x.posts.length > 0)) {
          this.supplierPosts = off.map(x => x.posts)
            .reduce((x, y) => x.concat(y));
        }
      }
      const salesDeps = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Sales);
      if (salesDeps && salesDeps.length > 0 && salesDeps[0].offices) {
        const off = salesDeps.map(x => x.offices).reduce((x, y) => x.concat());
        if (off && off.some(x => x.posts.length > 0)) {
          this.salesPosts = off.map(x => x.posts)
            .reduce((x, y) => x.concat(y));
        }
      }
    }
  }

  onLocationStreetChangeEvent(data: any) {
    if (data && data.street) {
      data.name = data.street + (data.streetNumber ? ' ' + data.streetNumber : '');
  }
  }

  onInitialStreetLocationNameChangeEvent(data: any) {
    if (data && data.street) {
      data.name = data.street + (data.streetNumber ? ' ' + data.streetNumber : '');
  }
  }

  supplierValueChange(data: any) {
    if (data && data.previousValue === true && data.value === false) {
      this.partnerSupplierCreditControl = new PartnerSupplierCreditControl(this.opavPaymentInstrumentId);
    }
  }
  clientValueChange(data: any) {
    if (data && data.previousValue === true && data.value === false) {
      this.partnerClientCreditControl = new PartnerClientCreditControl(this.opavPaymentInstrumentId);
    }
  }

  getDirectoryPath() {
    if (this.selectedPartner && this.partnerLocations && this.partnerLocations.length > 0) {
      let i = this.partnerLocations.find(i => i.isImplicit);
      let c = i ? this.countries.find(c => c.id === i.countryId)?.code : null;

      if (c) {
        //TODO: Remove the server IP
        return '\\' + '\\192.168.0.79\\V-Import\\Furnizori\\' + c + this.selectedPartner.code + this.selectedPartner.name;
      }
    }
  }

  withoutFiscalCodeChange(data: any) {
    if (data && data.value) {
      this.partner.fiscalCode = null;
    }
  }

  withoutTradeCodeChange(data: any) {
    if (data && data.value) {
      this.partner.tradeRegisterNumber = null;
    }
  }

  getDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.description;
  }

  getSecondDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.description;
  }

  getDisplayExprSalePriceLists(item) {
    if (!item) {
      return '';
    }
    let c = (this.currencies && this.currencies.length > 0) ? this.currencies.find(i => i.id === item.currencyId) : null;
    return item.code + ' - ' + item.name  + ' - ' + c?.name;
  }

  getDisplayExprPartnerActivity(value: any) {
    if (value && this.partnerActivity) {
      const item = this.partnerActivity.find(x => x.id === value.partnerActivityId);
      if (item) {
        return value.saleZoneCode + ' - ' + item.name + ' - ' + value.partnerActivityClassification;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  canExport() {
    return this.actions.CanExport;
  }

  cityChange(data: any) {
    if (data && data.value) {

      if (this.isOnSave && this.initialPartnerLocation) {
        let city = this.cities.find(x => x.id === data.value);
        this.initialPartnerLocation.countryId = city.countryId;
        this.initialPartnerLocation.countyId = city.countyId;
       
        let country = this.countries.find(x => x.id === city.countryId);
        let lang = country ? this.language.find(y => y.id === country.firstLanguageId) : null;
        this.initialPartnerLocation.languageId = lang ? lang.id : null;
      }

      if (this.isOnSavePartnerLocation || this.selectedPartnerLocation) {
        let city = this.cities.find(x => x.id === data.value);
        this.selectedPartnerLocation.countryId = city.countryId;
        this.selectedPartnerLocation.countyId = city.countyId;
       
        let country = this.countries.find(x => x.id === city.countryId);
        let lang = country ? this.language.find(y => y.id === country.firstLanguageId) : null;
        this.selectedPartnerLocation.languageId = lang ? lang.id : null;
      }
    }
  }

  private async getLocationFieldsFromGeocodeAPI(address: any) {
    let stringAddress = `${address.address} ${address.city} ${address.county} ${address.country}`;
    return this.googleGeocodingService.getGeolocationsFromAddress(stringAddress);
  }

  partnerSearchValueChange(e) {
    if (e && e.value && e.value.length > 3) {
      let sf = new PartnerSearchFilter();
      sf.name = e.value;
      sf.isActive = true;
      this.partnerService.getPartnersByFilter(sf).then(items => {
        this.partnerSearchDS = items
      });
    }
  }

  backToPartner() {
    this.partner = null;
    this.initialPartnerLocation = new PartnerLocation();
    this.visible.emit(false);

    this.detailsTabs.forEach(dt => {
      dt.isActive = false;
    })

    this.detailsTabs[0].isActive = true;
  }

  setGridInstances() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  partnerActivityChanged(e: any) {
    if (this.isOnSave && e && e.value) {
      let pa = this.partnerActivityAllocation.find(y => y.id === e.value);
      if (pa) {
        this.salePriceListDS = this.salePriceList.filter(x => x.salePriceListXProductConventionList && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId));
        if (this.initialPartnerLocation.countryId) {
          let currency = 1;
          currency = this.initialPartnerLocation.countryId == 33 ? 19 : currency;
          currency = this.initialPartnerLocation.countryId == 99 ? 61 : currency;
          this.initialPartnerLocation.salePriceListIds = [];
          this.initialPartnerLocation.salePriceListIds = this.salePriceList.filter(x => x.salePriceListXProductConventionList && x.currencyId == currency && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId) && x.isImplicit && x.status).map(y => y.id);
          }
      }
      this.initialPartnerLocation.caenCodeSpecializationIds = [];

      this.caenCodeSpecializationPartnerActivityAllocationService.getAllCaenCodeSpecializationPartnerActivityAllocationAsync().then(i => {
        if (i) {
          this.initialPartnerLocation.caenCodeSpecializationIds = i.filter(x => x.partnerActivityAllocationId === e.value).map(y => y.caenCodeSpecializationId);
        }
      });
    }
  }

  async selectedPartnerActivityChanged(e: any) {
    if (e && e.value && this.partnerActivityAllocation && this.partnerActivityAllocation.length) {
      let pa = this.partnerActivityAllocation.find(y => y.id === e.value);
      if (pa) {
        if (this.isOnSavePartnerLocation) {
          this.salePriceListDS = this.salePriceList.filter(x => x.salePriceListXProductConventionList && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId));
          if (this.selectedPartnerLocation.countryId) {
            let currency = 1;
            currency = this.selectedPartnerLocation.countryId == 33 ? 19 : currency;
            currency = this.selectedPartnerLocation.countryId == 99 ? 61 : currency;
            this.selectedPartnerLocation.salePriceListIds = [];
            this.selectedPartnerLocation.salePriceListIds = this.salePriceList.filter(x => x.salePriceListXProductConventionList && x.currencyId == currency && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId) && x.isImplicit && x.status).map(y => y.id);
          }
         
        } else {
          let salePriceListFilter = new SalePriceListFilterVM();
          salePriceListFilter.productConventionIds = [pa.productConventionId];
          salePriceListFilter.currencyIds = [this.selectedPartnerLocation.baseCurrencyId, this.selectedPartnerLocation.alternativeCurrencyId];
  
          await this.salePriceListService.getSalePriceListByFilter(salePriceListFilter).then(items => {
            if (items && items.length > 0) {
              this.salePriceListDS = items;
            } else {
              this.salePriceListDS = this.salePriceList;
            }
          })
        }
      }
    }
    if (this.isOnSavePartnerLocation && e && e.value) {
      this.selectedPartnerLocation.caenCodeSpecializationIds = [];
      this.caenCodeSpecializationPartnerActivityAllocationService.getAllCaenCodeSpecializationPartnerActivityAllocationAsync().then(i => {
        if (i) {
          this.selectedPartnerLocation.caenCodeSpecializationIds = i.filter(x => x.partnerActivityAllocationId === e.value).map(y => y.caenCodeSpecializationId);
        }
      });
    }
  }

  addExtraSpecialziations() {
    if (this.selectedPartnerLocation.partnerActivityAllocationId) {
      this.caenCodeSpecializationPartnerActivityAllocationService.getAllCaenCodeSpecializationPartnerActivityAllocationAsync().then(i => {
        if (i) {
          let filteredSpecs = i.filter(x => x.partnerActivityAllocationId === this.selectedPartnerLocation.partnerActivityAllocationId).map(y => y.caenCodeSpecializationId);

          if (this.selectedPartnerLocation.partnerSecondaryActivityAllocationIds && this.selectedPartnerLocation.partnerSecondaryActivityAllocationIds.length > 0) { 
            let secondFilteredSpecs = i.filter(x => this.selectedPartnerLocation.partnerSecondaryActivityAllocationIds.includes(x.partnerActivityAllocationId) ).map(y => y.caenCodeSpecializationId);
            filteredSpecs.push(...secondFilteredSpecs);
          }

          if (filteredSpecs && filteredSpecs.length > 0) {
            filteredSpecs.forEach(spec => {
              if (!this.selectedPartnerLocation.caenCodeSpecializationIds.includes(spec)) {
                this.selectedPartnerLocation.caenCodeSpecializationIds.push(spec);
              }
            });
            this.notificationService.alert('top', 'center', 'Partener - Specializari adaugate!',
            NotificationTypeEnum.Green, true)

          } else {
            this.notificationService.alert('top', 'center', 'Partener - Nu au fost gasite specializari pentru aceasta activitate de partener! Trebuie selectata o alta activitate sau introduse manual specializarile!',
            NotificationTypeEnum.Red, true)
          }
        }
      });
    } else {
      this.notificationService.alert('top', 'center', 'Partener - Nu au putut sa fie adaugate specialziari! Locatia nu are o activitate selectata!',
      NotificationTypeEnum.Red, true)
    }
  }

  selectedPartnerItemGroupCategoryChanged(e: any) {
    if (e && e.value && this.isOnSavePartnerLocation) {
      this.selectedPartnerLocation.itemGroupCodeIds = [];
      this.itemGroupCategoryCodeService.getAll().then(i => {
        if (i) {
          this.selectedPartnerLocation.itemGroupCodeIds = i.filter(x => e.value.includes(x.itemGroupCategoryId)).map(y => y.itemGroupCodeId);
        }
      });
    }
  }

  initialPartnerLocationItemGroupCategoryChanged(e: any) {
    if (this.isOnSave && e && e.value) {
      this.selectedPartnerLocation.itemGroupCodeIds = [];
      this.itemGroupCategoryCodeService.getAll().then(i => {
        if (i) {
          this.initialPartnerLocation.itemGroupCodeIds = i.filter(x => e.value.includes(x.itemGroupCategoryId)).map(y => y.itemGroupCodeId);
        }
      });
    }
  }

  async saveData() {
    if (!this.initialPartnerLocation.name || !this.initialPartnerLocation.description || !this.initialPartnerLocation.countryId
      || !this.initialPartnerLocation.countyId || !this.initialPartnerLocation.street || !this.initialPartnerLocation.addressTypeId || !this.initialPartnerLocation.zipCode) {
      this.tabError = true;
      this.notificationService.alert('top', 'center', 'Partener - Datele nu au fost inserate! Nu ati completat tab-ul de locatie implicita!',
        NotificationTypeEnum.Red, true)
    }

    if (!this.initialPartnerLocation.caenCodeSpecializationIds || this.initialPartnerLocation.caenCodeSpecializationIds.length === 0) {
      this.isSpecializationInvalid = true;  
    } else {
      this.isSpecializationInvalid = false;
    }

    if ((!this.initialPartnerLocation.salePriceListIds || this.initialPartnerLocation.salePriceListIds.length === 0) && this.initialPartnerLocation.partnerActivityAllocationId 
      && this.partner.isClient && this.initialPartnerLocation.caenCodeSpecializationIds && this.initialPartnerLocation.caenCodeSpecializationIds.length > 0)  {
      this.isSalesPriceListInvalid = true;
    } else {
      this.isSalesPriceListInvalid = false;
    }
    
    if (this.validationGroup.instance.validate().isValid && this.isSpecializationInvalid) {
      this.tabError = true;
      this.notificationService.alert('top', 'center', 'Partener - Datele nu au fost inserate! Nu ati completat tab-ul de Specializari!',
        NotificationTypeEnum.Red, true)
      return;
    }

    if (this.validationGroup.instance.validate().isValid && this.isSalesPriceListInvalid) {
      this.tabError = true;
      this.notificationService.alert('top', 'center', 'Partener - Datele nu au fost inserate! Nu ati completat tab-ul de Liste de Pret!',
        NotificationTypeEnum.Red, true)
      return;
    }

    if (this.validationGroup.instance.validate().isValid && (this.partner.isClient || this.partner.isSupplier)
      && this.validateWithoutFiscalORRegisterCodes(this.partner)) {
      this.tabError = false;
      let item = new Partner();
      item = this.partner;
      item.customerId = Number(this.customerId);
      item.siteId = Number(this.authenticationService.getUserSiteId());
      item.caenCodeId = this.caenGridBoxValue[0];
      if (item) {
        if (item.personType !== 1) {
          item.partnerLegalFormId = null;
          item.fiscalCode = null;
          item.tradeRegisterNumber = null;
        }
        item.initialPartnerLocation = this.initialPartnerLocation;
        item.initialPartnerLocation.isHome = item.personType == 2;
        const addressForGoogleAPI = {
          address: item.initialPartnerLocation.street + ' ' + item.initialPartnerLocation.streetNumber,
          city: this.cities.find(x => x.id === item.initialPartnerLocation.cityId).name,
          county: this.counties.find(x => x.id === item.initialPartnerLocation.countyId).name,
          country: this.countries.find(x => x.id === item.initialPartnerLocation.countryId).name,
        };
        this.loaded = true;
        await this.getLocationFieldsFromGeocodeAPI(addressForGoogleAPI).then(async result => {
          if (result.status === GeocoderStatus.OK) {
            item.initialPartnerLocation.zipCode = item.initialPartnerLocation.zipCode ?? result.results[0].address_components.find(x => x.types?.find(x => x === 'postal_code'))?.long_name;
            item.initialPartnerLocation.latitude = Number(result.results[0].geometry.location.lat);
            item.initialPartnerLocation.longitude = Number(result.results[0].geometry.location.lng);
          }
          else {
            this.notificationService.alert('top', 'center', 'Locatie Partener - Codul postal, latitudinea si longitudinea nu au putut fi populate! A aparut o eroare!',
              NotificationTypeEnum.Red, true);
          }
          item.clientType = item.isClient ? ClientTypeEnum.Possible : ClientTypeEnum.None;
          item.supplierType = item.isSupplier ? SupplierTypeEnum.Possible : SupplierTypeEnum.None;
          await this.partnerService.createPartnerAsync(item).then(async r => {
            if (r) {

              if (r && r.initialPartnerLocation && r.initialPartnerLocation.id && this.partner.personType === 2) {
                //Create PartnerLocationContact
                let item = new PartnerLocationContact();
                item.partnerLocationId = r.initialPartnerLocation.id;
                item.phoneNumber = this.partner.phone;
                item.firstName = this.partner.name;
                item.lastName = "-";
                item.position = 13;
                item.partnerId = r.id;
                item.isActive = true;
                if (item) {
                   this.partnerLocationContactService.createPartnerLocationContactAsync(item);
                }
              }

              if (this.partner.competitor == 1 && this.partner.excludedItemGroupCodeIds && this.partner.excludedItemGroupCodeIds.length > 0) {
                this.savePartnerXItemGroupCode(r.id);
              }

               //Tags
              if (item.tagIds) {
                let createTags: PartnerXTag[] = [];
                item.tagIds.forEach(id => {
                  const t = new PartnerXTag();
                  t.partnerId = Number(r.id);
                  t.tagId = id;
                  createTags.push(t);
                });
                if (createTags && createTags.length > 0) {
                  this.partnerTagService.createMultipleAsync(createTags).then(() => {
                  });
                }
              }

              this.initialPartnerLocationOutput.emit(this.initialPartnerLocation);
 
              //CaenCodeSpecialization
              if (r.initialPartnerLocation.id && this.initialPartnerLocation.caenCodeSpecializationIds &&  this.initialPartnerLocation.caenCodeSpecializationIds.length > 0) {
                let createSpecializationIds: CaenCodeSpecializationXPartnerLocation[] = [];
                this.initialPartnerLocation.caenCodeSpecializationIds?.forEach(id => {
                  const c = new CaenCodeSpecializationXPartnerLocation();
                  c.partnerLocationId = Number(r.initialPartnerLocation.id);
                  c.caenCodeSpecializationId = id;
                  createSpecializationIds.push(c);
                });
                if (createSpecializationIds && createSpecializationIds.length > 0) {
                  await this.caenCodeSpecializationPartnerLocationService.createMultipleCaenCodeSpecializationPartnerLocationAsync(createSpecializationIds)
                }

                // Create PartnerLocationXItemGroupCategory
                if (this.initialPartnerLocation.itemGroupCategoryIds &&  this.initialPartnerLocation.itemGroupCategoryIds.length > 0) { 
                  let createItemGroupCategoryIds: PartnerLocationXItemGroupCategory[] = [];
                  this.initialPartnerLocation.itemGroupCategoryIds?.forEach(id => {
                    const c = new PartnerLocationXItemGroupCategory();
                    c.partnerLocationId = Number(r.initialPartnerLocation.id);
                    c.itemGroupCategoryId = id;
                    createItemGroupCategoryIds.push(c);
                  });
                  if (createItemGroupCategoryIds && createItemGroupCategoryIds.length > 0) {
                    this.partnerLocationItemGroupCategoryService.createMultiplePartnerLocationItemGroupCategoryAsync(createItemGroupCategoryIds);
                  }
                }
          
                 // Create PartnerLocationXItemGroupCode
                 if (this.initialPartnerLocation.itemGroupCodeIds &&  this.initialPartnerLocation.itemGroupCodeIds.length > 0) {
                  let createItemGroupCodeIds: PartnerLocationXItemGroupCode[] = [];
                  this.initialPartnerLocation.itemGroupCodeIds?.forEach(id => {
                    const c = new PartnerLocationXItemGroupCode();
                    c.partnerLocationId = Number(r.initialPartnerLocation.id);
                    c.itemGroupCodeId = id;
                    createItemGroupCodeIds.push(c);
                  });
                  if (createItemGroupCodeIds && createItemGroupCodeIds.length > 0) {
                    this.partnerLocationItemGroupCodeService.createMultiplePartnerLocationItemGroupCodeAsync(createItemGroupCodeIds);
                  }
                }

                if (this.initialPartnerLocation.salePriceListIds && (this.partner.isClient)) {
                  await this.partnerLocationSalePriceListService.updateMultipleAsync(Number(r.initialPartnerLocation.id), this.initialPartnerLocation.salePriceListIds, null);
                }
              }

              await this.saveCreditControl(r.id, item);
              this.notificationService.alert('top', 'center', 'Partener - Datele au fost inserate cu succes!',
                NotificationTypeEnum.Green, true);
              this.loaded = false;
              window.location.href = environment.SRMPrimaware + '/' + Constants.partner + '?id=' + r.id;
            } else {
              this.notificationService.alert('top', 'center', 'Partener - Datele nu au fost inserate! A aparut o eroare!',
                NotificationTypeEnum.Red, true)
                this.loaded = false;
            }
          });
        });
      }
    } else {
      if (!this.partner.isClient && !this.partner.isSupplier) {
        this.notificationService.alert('top', 'center', 'Partener - Partenerul trebuie sa fie Client sau Furnizor! A aparut o eroare!',
          NotificationTypeEnum.Red, true)
      }
    }
  }

  validateWithoutFiscalORRegisterCodes(partner: Partner): boolean {
    if (partner.personType === 1 && ((!this.withoutFiscalCode && !partner.fiscalCode) ||
      (!this.withoutTradeRegisterNumber && !partner.tradeRegisterNumber))) {
      this.notificationService.alert('top', 'center', 'Partener - Partenerul trebuie sa contina Cod Fiscal sau Numar Registrul Comertului daca nu a fost semnata bifa de Fara! A aparut o eroare!',
        NotificationTypeEnum.Red, true)
      return false;
    } else {
      return true;
    }
  }

  async updateData() {
    if (!this.validateExistingLocations()) {
      this.notificationService.alert('top', 'center', 'Partener - Obligatoriu ca Bifa de implicit sa fie pe locatia de sediu social pentru PJ / respectiv pe locatie de domiciliu pentru PF. Sau cel putin una dintre locatii are Judetul sau Orasul invalide!',
        NotificationTypeEnum.Red, true)
    } else {
      if (this.validationGroup.instance.validate().isValid && (this.partner.isClient || this.partner.isSupplier)
        && this.validateWithoutFiscalORRegisterCodes(this.partner)) {
        let item = new Partner();
        item = this.partner;
        item.caenCodeId = this.caenGridBoxValue[0];
        if (item) {
          if (item.personType !== 1) {
            item.partnerLegalFormId = null;
            item.fiscalCode = null;
            item.tradeRegisterNumber = null;
          }
          item.clientType = item.isClient ? ClientTypeEnum.Possible : ClientTypeEnum.None;
          item.supplierType = item.isSupplier ? SupplierTypeEnum.Possible : SupplierTypeEnum.None;
          this.loaded = true;
          await this.partnerService.updatePartnerAsync(item).then(async r => {
            if (r) {
              this.partner.rowVersion = r.rowVersion;
              this.notificationService.alert('top', 'center', 'Partener - Datele au fost modificate cu succes!',
                NotificationTypeEnum.Green, true);

                await this.saveCreditControl(item.id, item).then(h => { 
                  this.getCreditControl(item.id);
                  this.loaded = false;
                });
                
              //Tags
              if (item.existingTagIds) {
                const existingTagIds = item.existingTagIds;
                let deleteTags: PartnerXTag[] = [];
                existingTagIds.filter(x => !item.tagIds.includes(x)).forEach(id => {
                  const t = new PartnerXTag();
                  t.partnerId = item.id;
                  t.tagId = id;
                  deleteTags.push(t);
                });
                if (deleteTags.length > 0) {
                  await this.partnerTagService.deleteMultipleAsync(deleteTags);
                }
                let createTags: PartnerXTag[] = [];
                item.tagIds.filter(x => !existingTagIds.includes(x)).forEach(id => {
                  const t = new PartnerXTag();
                  t.partnerId = Number(item.id);
                  t.tagId = id;
                  createTags.push(t);
                });
                if (createTags.length > 0) {
                  await this.partnerTagService.createMultipleAsync(createTags);
                }
              }

              this.initialPartnerLocationOutput.emit(this.initialPartnerLocation);

              if (this.partner.competitor == 0 || this.partner.competitor == 2) {
                this.partner.excludedItemGroupCodeIds = [];
              }
              
              this.updatePartnerXItemGroupCode();

              if (this.originalPartnerStatusId !== this.partner.partnerStatusId) {
                let partnerStatusHistory = new PartnerStatusHistory();
                partnerStatusHistory.partnerStatusId = this.partner.partnerStatusId;
                partnerStatusHistory.previousPartnerStatusId = this.originalPartnerStatusId;
                partnerStatusHistory.partnerId = this.partner.id;
                partnerStatusHistory.postId = +this.authenticationService.getUserPostId();
                partnerStatusHistory.userId = +this.authenticationService.getUserId();
                await this.partnerStatusHistoryService.createPartnerStatusHistory(partnerStatusHistory).then(async () => {
                  await this.getPartnerStatusHistory();
                });
              }

              
              if (r.syncERPExternalId || r.syncSeniorExternalId) {
                var options = new ERPSyncOptions();
                options.partnerId = r.id;
                options.isUpdate = true;
                options.isSeniorErp = (this.partner.syncSeniorExternalId) ? true : false;
                if (this.posts && this.posts.length && this.users && this.users.length) {
                  let user = this.users.find(x => x.userName === this.authenticationService.getUserUserName());
                  if (user) {
                    var userPost = this.posts.find(x => Number(x.id) === user.postId);
                    options.historyPostName = userPost.code;
                  }
                }
                
                options.historyUserName = this.authenticationService.getUserUserName();
                await this.partnerService.upsertToCharismaAsync(options).then(async x => {

                  this.notificationService.alert('top', 'center', 'Partener - Datele au fost trimise catre ERP! ' + x,
                    NotificationTypeEnum.Yellow, true);

                  await this.getPartnerERPSyncHistoryItems(this.partner.id).then(x => {this.isPartnerSentToERP = true; 
                    if (options.isSeniorErp) {
                      this.isPartnerSentToSenior = true;
                    };
                  });
                });
              }

            } else {
              this.notificationService.alert('top', 'center', 'Partener - Datele nu au fost modificate! A aparut o eroare!',
                NotificationTypeEnum.Red, true)
                this.loaded = false;
            };
          });
        }
      } else {
        if (!this.partner.isClient && !this.partner.isSupplier) {
          this.notificationService.alert('top', 'center', 'Partener - Partenerul trebuie sa fie Client sau Furnizor! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
      }
    }
  }

  activatePartnerLocationData() {
    if (this.selectedPartnerLocation) {
      this.partnerLocationService.activatePartnerLocationAsync(this.selectedPartnerLocation.id).then(response => {
        if (response) {
          this.notificationService.alert('top', 'center', 'Locatie Partener - Locatia a fost activata!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Locatie Partener - Locatia nu au fost activata!', NotificationTypeEnum.Red, true)
        }
        this.refreshDataGrid();
        this.popupVisible = false;
      });
    }
  }

  async onPartnerLocationRowUpdated(e) {
    let item = new PartnerLocation();
    item = e.data;

    if (item.isRegisteredOffice && item.isActive == false) {
      this.notificationService.alert('top', 'center', 'Locatie Partener - Nu se poate inactiva locatia de sediu social!',
        NotificationTypeEnum.Red, true)
        this.refreshDataGrid();
        return;
    }

    if (item) {
      await this.partnerLocationService.updatePartnerLocationAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Locatie Partener - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Locatie Partener - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };
        this.popupVisible = false;
        this.refreshDataGrid();
      });
    }
  }

  //TODO Remove this line when all Code='INVALID' records from DB, are gone
  validateExistingLocations(): boolean {
    let ok = true;
    this.partnerLocations.forEach(pl => {
      if (!this.counties.map(c => c.id).includes(pl.countyId) || !this.cities.map(c => c.id).includes(pl.cityId)) {
        ok = false;
      }
      if (this.partner.personType == 1 && pl.isRegisteredOffice == true && pl.isImplicit == false) {
        ok = false;
      }
      if (this.partner.personType == 2 && pl.isHome == true && pl.isImplicit == false) {
        ok = false;
      }
    });
    return ok;
  }
  
  copySpecialMentionsFromPopup(popupContent: ElementRef) {
    const popupContentText = popupContent.nativeElement.innerText;
    this.clipboard.copy(popupContentText);
    this.didCopy = true;
  }

  async onSpecialMentionsClicked()
  {
    this.openSpecialMentionsPopup();
  }

  onPopupHidden(e: any) {
    this.didCopy = false;
  }

  async okSpecialMentionsButtonClicked() {
    this.specialMentionsPopup = false;
  }

  private openSpecialMentionsPopup(): void {
    this.specialMentionsPopup = true;
  }

  async saveCreditControl(partnerId: any, item: Partner) {
    if (partnerId && item.isClient && !_.isEmpty(this.partnerClientCreditControl)) {
      if (!this.partnerClientCreditControl.id) {
        this.partnerClientCreditControl.partnerId = partnerId;
        await this.partnerClientCreditControlService.createPartnerClientCreditControlAsync(this.partnerClientCreditControl).then(y => {
          if (y) {
            this.partnerClientCreditControl.id = y.id;
            if (y.rowVersion) {
              this.partnerClientCreditControl.rowVersion = y.rowVersion;
            }
          }
        });
      } else {
        await this.partnerClientCreditControlService.updatePartnerClientCreditControlAsync(this.partnerClientCreditControl).then(y => {
          if (y) {
            this.partnerClientCreditControl.rowVersion = y.rowVersion;
          }
        });
      }
    }
    if (partnerId && item.isSupplier && !_.isEmpty(this.partnerSupplierCreditControl)) {
      if (!this.partnerSupplierCreditControl.id) {
        this.partnerSupplierCreditControl.partnerId = partnerId;
        await this.partnerSupplierCreditControlService.createPartnerSupplierCreditControlAsync(this.partnerSupplierCreditControl).then(y => {
          if (y) {
            this.partnerSupplierCreditControl.id = y.id;
            if (y.rowVersion) {
              this.partnerSupplierCreditControl.rowVersion = y.rowVersion;
            }
          }
        });
      } else {
        await this.partnerSupplierCreditControlService.updatePartnerSupplierCreditControlAsync(this.partnerSupplierCreditControl).then(y => {
          if (y) {
            this.partnerSupplierCreditControl.rowVersion = y.rowVersion;
          }
        });
      }
    }
  }

  public addPartnerLocation() {
    this.popupVisible = true;
    this.selectedPartnerLocation = new PartnerLocation();
    this.isOnSavePartnerLocation = true;
  }

  public refreshDataGrid() {
    this.getPartnerLocations().then(x =>  {
      this.loadCaenCodeSpecializationsForPartnerLocations();
      this.dataGrid.instance.refresh();
    });
  }

  public partnerLocationHidden($event: any) {
    this.refreshDataGrid();
  }

  openERPPopup() {
    if (this.partner.isClient && this.partnerClientCreditControl && !this.partnerClientCreditControl.id) {
      this.notificationService.alert('top', 'center', 'Partener - Nu este creat credit control. Inainte de a se trimite partenerul la ERP trebuie sa apasati pe butonul de actualizare/ modifica pentru a se crea credit control!',
          NotificationTypeEnum.Red, true);
          return;
    }
    if (this.partner.isSupplier && this.partnerSupplierCreditControl && !this.partnerSupplierCreditControl.id) {
      this.notificationService.alert('top', 'center', 'Partener - Nu este creat credit control. Inainte de a se trimite partenerul la ERP trebuie sa apasati pe butonul de actualizare/ modifica pentru a se crea credit control!',
          NotificationTypeEnum.Red, true);
          return;
    }
    this.erpPopup = true;
  }

  openSeniorPopup() {
    if (this.partner.isClient && this.partnerClientCreditControl && !this.partnerClientCreditControl.id) {
      this.notificationService.alert('top', 'center', 'Partener - Nu este creat credit control. Inainte de a se trimite partenerul la ERP trebuie sa apasati pe butonul de actualizare/ modifica pentru a se crea credit control!',
          NotificationTypeEnum.Red, true);
          return;
    }
    if (this.partner.isSupplier && this.partnerSupplierCreditControl && !this.partnerSupplierCreditControl.id) {
      this.notificationService.alert('top', 'center', 'Partener - Nu este creat credit control. Inainte de a se trimite partenerul la ERP trebuie sa apasati pe butonul de actualizare/ modifica pentru a se crea credit control!',
          NotificationTypeEnum.Red, true);
          return;
    }
    this.seniorPopup = true;
  }

  public async openDetails(row: any) {
    this.selectedPartnerLocation = row;

    this.loadItemGroupCategoryByPartnerLocationId(this.selectedPartnerLocation.id);
    this.loadItemGroupCodeByPartnerLocationId(this.selectedPartnerLocation.id);
    this.loadSecondaryActivityAllocationByPartnerLocationId(this.selectedPartnerLocation.id);

    this.isOnSavePartnerLocation = false;
    this.isOnSavePartnerLocationOutput.emit(this.isOnSavePartnerLocation);
    this.popupVisible = true;
  }

  public async updatePartnerLocationData(): Promise<void> {
    let valid = true;

    if (this.secondValidationGroup.instance.validate().isValid && valid) {
      let item = new PartnerLocation();
      item = this.selectedPartnerLocation;

      if (this.partnerLocations.length > 0 && this.selectedPartnerLocation.isRegisteredOffice == true && this.partnerLocations.filter(x => x.isRegisteredOffice == true).length > 1) {
        this.notificationService.alert('top', 'center', 'Locatie Partener - Nu se pot crea 2 sedii sociale! Sterge bifa de pe locatia care este deja sediu social',
          NotificationTypeEnum.Red, true);
        return;
      } 
      if ((this.partner.personType == 1 && item.isRegisteredOffice != true && item.isCorrespondence != true && item.isDeposit != true && item.isOffice != true) || 
          (this.partner.personType == 2 && (item.isHome != true))) {
          this.notificationService.alert('top', 'center', 'Locatie Partener - Locatia trebuie sa contina cel putin o bifa de sediu social / depozit / birou / corespondenta!',
          NotificationTypeEnum.Red, true);
        return;
      } 

      if (item) {
        const addressForGoogleAPI = {
          address: item.street + ' ' + item.streetNumber,
          city: this.cities.find(x => x.id === item.cityId).name,
          county: this.counties.find(x => x.id === item.countyId).name,
          country: this.countries.find(x => x.id === item.countryId).name,
        };
        this.loaded = true;
        this.getLocationFieldsFromGeocodeAPI(addressForGoogleAPI).then(result => {
          item.partnerId = Number(this.partner.id);
          if (result.status === GeocoderStatus.OK) {
            item.zipCode = item.zipCode ?? result.results[0].address_components.find(x => x.types?.find(x => x === 'postal_code'))?.long_name;
            item.latitude = Number(result.results[0].geometry.location.lat);
            item.longitude = Number(result.results[0].geometry.location.lng);
          }
          else {
            this.notificationService.alert('top', 'center', 'Locatie Partener - Codul postal, latitudinea si longitudinea nu au putut fi populate! A aparut o eroare!',
              NotificationTypeEnum.Red, true);
          }

          this.partnerLocationService.updatePartnerLocationAsync(item).then(r => {
            if (r) {
              item.rowVersion = r.rowVersion;
              this.notificationService.alert('top', 'center', 'Locatie Partener - Datele au fost modificate cu succes!',
                NotificationTypeEnum.Green, true)

              this.refreshDataGrid();
              this.popupVisible = false;
              this.loaded = false;
            } else {
              this.loaded = false;
              this.notificationService.alert('top', 'center', 'Locatie Partener - Datele nu au fost modificate! A aparut o eroare!',
                NotificationTypeEnum.Red, true)
            }
          });
        });
      }
    }
  }

  hasTwoImplicitLocations() {
    if (this.partnerLocations && this.partnerLocations.length > 0 && this.selectedPartnerLocation.id) {
      return this.partnerLocations.filter(x => x.isImplicit).length === 2;
    } else {
      if (this.selectedPartnerLocation.isImplicit) {
        return true;
      }
    }
  }

  public async savePartnerLocationData(): Promise<void> {
    let valid = true;

    if (this.secondValidationGroup.instance.validate().isValid && valid) {
      let item = new PartnerLocation();
      item = this.selectedPartnerLocation;

      if (this.partnerLocations.length > 0 && this.selectedPartnerLocation.isRegisteredOffice == true && this.partnerLocations.filter(x => x.isRegisteredOffice == true).length >= 1) {
        this.notificationService.alert('top', 'center', 'Locatie Partener - Nu se pot crea 2 sedii sociale! Sterge bifa de pe locatia care este deja sediu social',
          NotificationTypeEnum.Red, true);
        return;
      } 

      if ((this.partner.personType == 1 && item.isRegisteredOffice != true && item.isCorrespondence != true && item.isDeposit != true && item.isOffice != true) || 
          (this.partner.personType == 2 && (item.isHome != true))) {
          this.notificationService.alert('top', 'center', 'Locatie Partener - Locatia trebuie sa contina cel putin o bifa de sediu social / depozit / birou / corespondenta!',
          NotificationTypeEnum.Red, true);
        return;
      } 

      if (item) {
        const addressForGoogleAPI = {
          address: item.street + ' ' + item.streetNumber,
          city: this.cities.find(x => x.id === item.cityId).name,
          county: this.counties.find(x => x.id === item.countyId).name,
          country: this.countries.find(x => x.id === item.countryId).name,
        };
        this.loaded = true;
        this.getLocationFieldsFromGeocodeAPI(addressForGoogleAPI).then(result => {
          item.partnerId = Number(this.partner.id);
          if (result !== undefined && result !== null && result.status === GeocoderStatus.OK) {
            item.zipCode = item.zipCode ?? result.results[0].address_components.find(x => x.types?.find(x => x === 'postal_code'))?.long_name;
            item.latitude = Number(result.results[0].geometry.location.lat);
            item.longitude = Number(result.results[0].geometry.location.lng);
          }
          else {
            this.notificationService.alert('top', 'center', 'Locatie Partener - Codul postal, latitudinea si longitudinea nu au putut fi populate! A aparut o eroare!',
              NotificationTypeEnum.Red, true);
          }

          let implicitLocation = null;
          if (this.partnerLocations && this.partner && this.partner.id) {
            implicitLocation = this.partnerLocations.find(i => i.isImplicit);

            if (implicitLocation && this.partner.isPrincipalActivityUniqueForAllLocations) {
              item.partnerActivityAllocationId = implicitLocation.partnerActivityAllocationId;
            } 
          }

          this.partnerLocationService.createPartnerLocationAsync(item).then(async r => {
            if (r) {
              if (r.id) {
                this.selectedPartnerLocation.rowVersion = r.rowVersion;

                let createSpecializationIds: CaenCodeSpecializationXPartnerLocation[] = [];
                let createPartnerSecondaryActivityAllocationIds: PartnerLocationXSecondaryActivityAllocation[] = [];
                let createItemGroupCategoryIds: PartnerLocationXItemGroupCategory[] = [];
                let createItemGroupCodeIds: PartnerLocationXItemGroupCode[] = [];
                
                if (implicitLocation && this.partner.isPrincipalActivityUniqueForAllLocations) {
                  await this.caenCodeSpecializationPartnerLocationService.getAllCaenCodeSpecializationPartnerLocationByPartnerLocationAsync(implicitLocation.id).then(items => {
                    if (items) {

                      items.forEach(id => {
                        const c = new CaenCodeSpecializationXPartnerLocation();
                        c.partnerLocationId = Number(r.id);
                        c.caenCodeSpecializationId = id.caenCodeSpecializationId;
                        createSpecializationIds.push(c);
                      });
                      this.caenCodeSpecializationPartnerLocationService.createMultipleCaenCodeSpecializationPartnerLocationAsync(createSpecializationIds);
                    }
                  })

                  await this.partnerLocationSecondaryActivityAllocationService.getAllByPartnerLocationIdAsync(implicitLocation.id).then(async items => {
                    if (items) {
                      
                        items.forEach(id => {
                        const c = new PartnerLocationXSecondaryActivityAllocation();
                        c.partnerLocationId = Number(r.id);
                        c.partnerActivityAllocationId = id.partnerActivityAllocationId;
                        createPartnerSecondaryActivityAllocationIds.push(c);
                      });
                      if (createPartnerSecondaryActivityAllocationIds && createPartnerSecondaryActivityAllocationIds.length > 0) {
                        await this.partnerLocationSecondaryActivityAllocationService.createMultipleAsync(createPartnerSecondaryActivityAllocationIds);
                      }
                    }
                  })

                  await this.partnerLocationItemGroupCategoryService.getAllPartnerLocationItemGroupCategoryByPartnerLocationAsync(implicitLocation.id).then(async items => {
                    if (items) {
                      items.forEach(id => {
                        const c = new PartnerLocationXItemGroupCategory();
                        c.partnerLocationId = Number(r.id);
                        c.itemGroupCategoryId = id.itemGroupCategoryId;
                        createItemGroupCategoryIds.push(c);
                      });
                      if (createItemGroupCategoryIds && createItemGroupCategoryIds.length > 0) {
                        await this.partnerLocationItemGroupCategoryService.createMultiplePartnerLocationItemGroupCategoryAsync(createItemGroupCategoryIds);
                      }
                    }
                  })

                  await this.partnerLocationItemGroupCodeService.getAllPartnerLocationItemGroupCodeByPartnerLocationAsync(implicitLocation.id).then(async items => {
                    if (items) {
                      items.forEach(id => {
                        const c = new PartnerLocationXItemGroupCode();
                        c.partnerLocationId = Number(r.id);
                        c.itemGroupCodeId = id.itemGroupCodeId;
                        createItemGroupCodeIds.push(c);
                      });
                      if (createItemGroupCodeIds && createItemGroupCodeIds.length > 0) {
                        await this.partnerLocationItemGroupCodeService.createMultiplePartnerLocationItemGroupCodeAsync(createItemGroupCodeIds);
                      }
                    }
                  })             
                }      
                
              }
              this.notificationService.alert('top', 'center', 'Locatie Partener - Datele au fost inserate cu succes!',
                NotificationTypeEnum.Green, true)
              this.getPartnerLocations();
              this.popupVisible = false;
              this.loaded = false;
            } else {
              this.loaded = false;
              this.notificationService.alert('top', 'center', 'Locatie Partener - Datele nu au fost inserate! A aparut o eroare!',
                NotificationTypeEnum.Red, true)
            }
          });
        });
      }
    }
  }

  public deleteRecords(data: any) {
    this.partnerLocationService.deletePartnerLocationAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Locatie Partener - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Locatie Partener - Datele nu au fost sterse!', NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.partner).then(result => {
      this.actions = result;
    });
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  canDelete() {
    return this.actions.CanDelete;
  }

  canUpdatePartner() {
    if (this.selectedPartner && this.selectedPartner.siteId !== Number(this.authenticationService.getUserSiteId())) {
      return false;
    }
    return true;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  isPublicInstitution(): boolean {
    if (this.partnerLegalForms && this.partnerLegalForms.length > 0 && this.partner.partnerLegalFormId) {
      let c = this.partnerLegalForms.find(x => x.id === this.partner.partnerLegalFormId)?.code;
      if (c && c === 'INS') {
        return true;
      } else return false;
    } else return false;
  }

  caenGridBoxDisplayExpr(item) {
    return item && `${item.code} - ${item.description}`;
  }

  getDisplayExprCountries(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  getDisplayExprCounties(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  getDisplayExprCities(item) {
    if (!item) {
      return '';
    }
    return item.name + ' - ' + item.countyName + ' - ' + item.countryName;
  }

  getDisplayExprPartnerActivityTypes(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  getDisplayExprPartnerStatus(item) {
    if (!item) {
      return '';
    }
    return item.name;
  }

  getDisplayExprPostsName(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  getDisplayExprAddressTypes(item) {
    if (!item) {
      return '';
    }
    if (this.translationService.currentLang === 'RO') {
      return item.code + ' - ' + item.nameRO;
    }
    else {
      return item.code + ' - ' + item.nameENG;
    }
  }

  getDisplayExprItemGroupCategories(item) {
    if (!item) {
      return '';
    }
    let p = this.posts?.find(p => p.id === item.implicitPostId);
    return item.catCode + ' - ' + item.firstName + (p ? ' - ' + p.code : '');
  }

  getDisplayExprItemGroupCodes(item) {
    if (!item) {
      return '';
    }
    return item.grpCode + ' - ' + item.firstName;
  }

  getDisplayExprPosts(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  getDisplayExprPaymentInstruments(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  getDisplayExprCurrencies(item) {
    if (!item) {
      return '';
    }
    return item.name + ' - ' + item.fullName;
  }

  showErpCHRPopup() {
    if (this.isOnSave || this.hasUndefinedLocations() || (this.isPartnerSentToERP) || !this.hasERPActive) {
      return false;
    } else {
      return true;
    }
  }

  showErpSeniorPopup() {
    if (this.isOnSave || this.hasUndefinedLocations() || (this.isPartnerSentToSenior) || !this.hasERPActive) {
      return false;
    } else {
      return true;
    }
  }

  hasUndefinedLocations() : boolean {
    let hasUndefinedLocations = false;
    this.partnerLocations.forEach(location => {
      if (this.cities.find(c => c.id === location.cityId)?.name.toLocaleLowerCase() == 'undefined' ||
        this.counties.find(c => c.id === location.countyId)?.name.toLocaleLowerCase() == 'undefined' ||
        this.countries.find(c => c.id === location.countryId)?.name.toLocaleLowerCase() == 'undefined') {
          hasUndefinedLocations = true;
      }
    })
    return hasUndefinedLocations;
  }

  async upsertERP(partnerId, isSeniorERP : boolean = false) {
    if(this.partner.personType == PersonTypeEnum.Legal && (this.partner.fiscalCode == null || this.partner.fiscalCode == undefined)) {
      this.notificationService.alert('top', 'center', 'Partener - Datele nu au fost trimise catre ERP! Completati obligatoriu codul fiscal!',
        NotificationTypeEnum.Red, true);
        return;
    }
    if(this.validationGroup.instance.validate().isValid 
    && (this.partner.isClient || this.partner.isSupplier) 
    && this.validateWithoutFiscalORRegisterCodes(this.partner)) {
      var options = new ERPSyncOptions();
      options.partnerId = partnerId;
      options.isSeniorErp = (isSeniorERP || this.partner.syncSeniorExternalId) ? true : false;
      options.source = 3;
      if (this.posts && this.posts.length && this.users && this.users.length) {
        let user = this.users.find(x => x.userName === this.authenticationService.getUserUserName());
        if (user) {
          var userPost = this.posts.find(x => Number(x.id) === user.postId);
          options.historyPostName = userPost.code;
        }
      }
      options.historyUserName = this.authenticationService.getUserUserName();
      this.loaded = true;
      await this.partnerService.upsertToCharismaAsync(options).then(async result => {
        this.loaded = false;
        this.erpPopup = false; this.seniorPopup = false;
        this.notificationService.alert('top', 'center', 'Partener - Datele au fost trimise catre ERP!' + result,
          NotificationTypeEnum.Yellow, true);
       
          if (options.isSeniorErp == true && result != undefined) {
            this.partner.syncSeniorExternalId = result;
            this.isPartnerSentToSenior = true;
          } 
          await this.getPartnerERPSyncHistoryItems(partnerId).then(x => this.isPartnerSentToERP = true);
      }).catch(error => { this.loaded = false;  this.erpPopup = false; this.seniorPopup = false; });
    }
    else {
      this.notificationService.alert('top', 'center', 'Partener - Datele nu au fost trimise catre ERP! Verificati toate campurile partenerului.',
      NotificationTypeEnum.Red, true);
        return;
    }
  }
  
  getPartnerERPSyncHistory(partnerId) {
    if (partnerId) {
      var historyItems = this.partnerERPSyncHistoryItems;
      if (historyItems && historyItems.length > 0) {
        if (historyItems.length === 1) {
          var historyItem = historyItems[0];
          return this.translationService.instant(`Creat in ERP in data de ${formatDate(historyItem.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre ${historyItem.postName}/${historyItem.userName}.`);
        }
        else {
          var historyItem = historyItems[historyItems.length - 1];
          return this.translationService.instant(`Modificat in ERP in data de ${formatDate(historyItem.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre ${historyItem.postName}/${historyItem.userName}.`);
        }
      }
    }
    return '';
  }

  getPartnerCreated(partner: any, isCreate: boolean) {
    if (isCreate) {
      if (partner.created && partner.createdBy && this.users && this.users.length) {
        let user = this.users.find(u => u.id === partner.createdBy)
        if (user) {
          return this.translationService.instant(`Creat in data de ${formatDate(partner.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre ${user.firstName}/${user.lastName}.`);
        }
      } else {
        return this.translationService.instant(`Creat in data de ${formatDate(partner.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre WindowsService.`);
      }
    }
    if (!isCreate) {
      if (partner.modified && partner.modifiedBy && this.users && this.users.length) {
        let user = this.users.find(u => u.id === partner.modifiedBy)
        if (user) {
          return this.translationService.instant(`Ultima modificare in data de ${formatDate(partner.modified, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre ${user.firstName}/${user.lastName}.`);
        }
      } else {
        return this.translationService.instant(`Ultima modificare in data de ${formatDate(partner.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre WindowsService.`);
      }
    }
    return '';
  }

  showPartnerLocationSchedule() {
    this.showPartnerLocationSchedulePopup = true;
    if (this.selectedPartnerLocation && this.selectedPartnerLocation.id) {
      this.partnerLocationScheduleComponent.getPartnerLocationSchedule(this.selectedPartnerLocation.id);
    }
  }

  async getPartnerXItemGroupCodes() {
    if (this.partner.id) {
      await this.partnerXItemGroupCodeService.getByPartnerId(this.partner.id).then(items => {
        this.partner.excludedItemGroupCodeIds = items.map(item => item.itemGroupCodeId);
      });
    }
  }

  async getPartnerStatusHistory() {
    if (this.partner.id) {
      await this.partnerStatusHistoryService.getLastByPartnerId(this.partner.id).then(result => {
        if (result) {
          let post = this.posts.find(f => f.id === result.postId);
          let user = this.users.find(f => f.id === result.userId);

          let currentPartnerStatusName = this.translationService.instant(PartnerStatusEnum[result.partnerStatusId]);
          let previousPartnerStatusName = this.translationService.instant(PartnerStatusEnum[result.previousPartnerStatusId]);

          this.partnerStatusHistory = `Ultima modificare de stare din ${previousPartnerStatusName} in ${currentPartnerStatusName} la data ${formatDate(result.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre ${post?.companyPost} ${user?.firstName} ${user?.lastName}`;
        } else {
          this.partnerStatusHistory = 'Nu are istoric de schimbari agent';
        }
      });
    }
  }

  async updatePartnerXItemGroupCode() {
    if (this.partner.id) {
      this.partnerXItemGroupCodeService.update(this.partner.excludedItemGroupCodeIds, this.partner.id);
    }
  }

  async savePartnerXItemGroupCode(partnerId: number) {
    if (partnerId) {
      this.partnerXItemGroupCodeService.create(this.partner.excludedItemGroupCodeIds, partnerId);
    }
  }

  hasWebsite() {
    if (this.partner.hasWebsite) {
      return false;
    }

    if (this.partner.website) {
      return this.partner.website.length != 0;
    }

    return false;
  }

  validateSite(event) {
    var res = event.value.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if(res == null)
      return false;
    else
      return true;
  }

  async checkForImplicitSupplierAgent()  {
    this.implicitSupplierPostId = null;
    
    if (this.partner && this.partner.isSupplier && !this.isOnSave && this.partnerLocations.length > 0) {
      var implicitPartnerLocation = this.partnerLocations.find(x => x.isImplicit);

      if (implicitPartnerLocation) {       
        await this.partnerLocationXItemGroupCategoryService.getAllPartnerLocationItemGroupCategoryByPartnerLocationAsync(implicitPartnerLocation.id).then(async result => {
          
          if (result && result.length > 0) {
            let firstPlXItemGroupCategory = result[0];
            let itemGroupCategory = this.itemGroupCategories.find(f => f.id === firstPlXItemGroupCategory.itemGroupCategoryId && f.implicitPostId);

            if (itemGroupCategory) {
              this.implicitSupplierPostId = itemGroupCategory.implicitPostId;
              this.partner.implicitSupplierAgentId = itemGroupCategory.implicitPostId;
            }
          }
        });
      }
    }
  }

  onSpecsOpen(event: any) {
    let arr = this.caenSpecializationsDS.store.slice().sort((a, b) => {
      const aSelected = this.selectedPartnerLocation.caenCodeSpecializationIds.includes(a.id);
      const bSelected = this.selectedPartnerLocation.caenCodeSpecializationIds.includes(b.id);

      if (aSelected && !bSelected) return -1;
      if (!aSelected && bSelected) return 1;
      return 0;
    });

    this.sortedCaenSpecializationsDS = {
      paginate: true,
      pageSize: 15,
      store: arr
    };
  }

  async onPrincipalActivityUniqueChange(event: any) {
    if (this.validationGroup.instance.validate().isValid && (this.partner.isClient || this.partner.isSupplier)
      && this.validateWithoutFiscalORRegisterCodes(this.partner) && event) {
      let item = new Partner();
      item = this.partner;
      item.caenCodeId = this.caenGridBoxValue[0];
      if (item) {
        if (item.personType !== 1) {
          item.partnerLegalFormId = null;
          item.fiscalCode = null;
          item.tradeRegisterNumber = null;
        }
        item.clientType = item.isClient ? ClientTypeEnum.Possible : ClientTypeEnum.None;
        item.supplierType = item.isSupplier ? SupplierTypeEnum.Possible : SupplierTypeEnum.None;
        item.isPrincipalActivityUniqueForAllLocations = event.value;
        await this.partnerService.updatePartnerAsync(item).then(async r => {
          if (r) {
            this.partner.rowVersion = r.rowVersion;
          }
        });
      }
    }
  }

  onInitialLocationTypeChange(e) {
    if (e && e.event) {
      if (this.initialPartnerLocation.isOffice) {
        this.initialPartnerLocation.addressTypeId = 5;
      }
      if (this.initialPartnerLocation.isDeposit) {
        this.initialPartnerLocation.addressTypeId = 3;
      }
      if (this.initialPartnerLocation.isHome || this.initialPartnerLocation.isCorrespondence) {
        this.initialPartnerLocation.addressTypeId = 2;
      }
      if (this.initialPartnerLocation.isRegisteredOffice) {
        this.initialPartnerLocation.addressTypeId = 1;
      }
    }
  }

  onSelectedLocationTypeChange(e) {
    if (e && e.event) {
      if (this.selectedPartnerLocation.isOffice) {
        this.selectedPartnerLocation.addressTypeId = 5;
      }
      if (this.selectedPartnerLocation.isDeposit) {
        this.selectedPartnerLocation.addressTypeId = 3;
      }
      if (this.selectedPartnerLocation.isHome || this.selectedPartnerLocation.isCorrespondence) {
        this.selectedPartnerLocation.addressTypeId = 2;
      }
      if (this.selectedPartnerLocation.isRegisteredOffice) {
        this.selectedPartnerLocation.addressTypeId = 1;
      }
    }
  }

  onPersonTypeChange(e) {
    if (e && e.value) {
      if (e.value == 2) {
        this.initialPartnerLocation.isOffice= false;
        this.initialPartnerLocation.isDeposit= false;
        this.initialPartnerLocation.isCorrespondence= false;
        this.initialPartnerLocation.isRegisteredOffice= false;

        this.initialPartnerLocation.isHome = true;
        this.initialPartnerLocation.addressTypeId = 2;
        this.initialPartnerLocation.partnerActivityAllocationId = 120;
      } else {
        this.initialPartnerLocation.isHome = false;
        this.initialPartnerLocation.isRegisteredOffice= true;
        this.initialPartnerLocation.addressTypeId = 1;
        this.initialPartnerLocation.partnerActivityAllocationId = null;
        this.initialPartnerLocation.caenCodeSpecializationIds = null;
      }
    }
  }

  registeredOfficeDisabled() {
    if (this.partnerLocations.length > 0 && this.partnerLocations.filter(x => x.isRegisteredOffice == true).length > 0 && this.partner.syncSeniorExternalId) {
      return true;
    } else return false;
  }

  async computeCreditIncreaseRequestDetails(): Promise<void> {
    this.creditIncreaseRequestBtnEmailDetails = new CreditIncreaseRequestBtnEmailDetails();

    /**
     * START PARTNER FISCAL CODE, NAME, PMW PAYMENT INSTRUMENTS, ACTIVITY DESCRIPTION, WEBSITE, TRADE REGISTER NUMBER, CAEN CODE
     */

    this.creditIncreaseRequestBtnEmailDetails.partnerFiscalCode = this.partner.fiscalCode;
    this.creditIncreaseRequestBtnEmailDetails.partnerName = this.partner.name;
    
    let paymentInstrumentsByCustomerId = await this.paymentInstrumentService.getAllPaymentInstrumentsByCustomerIdAsync();

    const filteredPaymentInstruments = paymentInstrumentsByCustomerId.filter(f => [
      Constants.creditIncreaseRequestEmailDetails.filteredPaymentInstruments.card, Constants.creditIncreaseRequestEmailDetails.filteredPaymentInstruments.advancePaymentOrder,
      Constants.creditIncreaseRequestEmailDetails.filteredPaymentInstruments.boOrCecOnTerm, Constants.creditIncreaseRequestEmailDetails.filteredPaymentInstruments.paymentOrderOnTime, 
      Constants.creditIncreaseRequestEmailDetails.filteredPaymentInstruments.boOrCecSentCompletedViaEmailOrWhatsapp, 
      Constants.creditIncreaseRequestEmailDetails.filteredPaymentInstruments.cecOnTerm, 
      Constants.creditIncreaseRequestEmailDetails.filteredPaymentInstruments.cardAndCash, 
      Constants.creditIncreaseRequestEmailDetails.filteredPaymentInstruments.boCertifiedSentByEmail, 
      Constants.creditIncreaseRequestEmailDetails.filteredPaymentInstruments.opWithBoCertified, 
      Constants.creditIncreaseRequestEmailDetails.filteredPaymentInstruments.cashOnDelivery, Constants.creditIncreaseRequestEmailDetails.filteredPaymentInstruments.cashOnTerm].includes(f.id));

    this.creditIncreaseRequestBtnEmailDetails.pmwPaymentMethods = filteredPaymentInstruments.map(item => item.name + Constants.creditIncreaseRequestEmailDetails.newLine).join(Constants.noSpaceDelimiter);
    
    let partnerImplicitLocation = this.partnerLocations?.find(f => f.isActive === true && f.isImplicit === true);

    if (partnerImplicitLocation && this.partnerActivityAllocation && this.partnerActivity) {
      const foundPartnerActivityAllocation = this.partnerActivityAllocation.find(f => f.id === partnerImplicitLocation.partnerActivityAllocationId);
      const foundPartnerActivity = this.partnerActivity.find(x => x.id === foundPartnerActivityAllocation?.partnerActivityId);

      if (foundPartnerActivity) 
        this.creditIncreaseRequestBtnEmailDetails.partnerActivityDescription = foundPartnerActivityAllocation.saleZoneCode + Constants.minusDelimiter + 
          foundPartnerActivity.name + Constants.minusDelimiter + foundPartnerActivityAllocation.partnerActivityClassification;
    }

    this.creditIncreaseRequestBtnEmailDetails.partnerWebsite = this.partner.website;
    this.creditIncreaseRequestBtnEmailDetails.partnerTradeRegisterNumber = this.partner.tradeRegisterNumber;

    let partnerCaen = this.rawCaenCodes.find(f => f.id === this.partner.caenCodeId);
    this.creditIncreaseRequestBtnEmailDetails.partnerCaen = partnerCaen && `${partnerCaen.code} ${partnerCaen.description}`;

    /**
     * END PARTNER FISCAL CODE, NAME, PMW PAYMENT INSTRUMENTS, ACTIVITY DESCRIPTION, WEBSITE, TRADE REGISTER NUMBER, CAEN CODE
     */

    /**
     * START PARTNER OFFICE ADDRESS, LOCALITY, COUNTY, COUNTRY
     */

    let partnerRegisteredOfficeAddressElements = [partnerImplicitLocation?.street, partnerImplicitLocation?.streetNumber, partnerImplicitLocation?.block, partnerImplicitLocation?.apartment, partnerImplicitLocation?.zipCode];
    this.creditIncreaseRequestBtnEmailDetails.partnerRegisteredOfficeAddress = partnerRegisteredOfficeAddressElements.filter(element => !!element).join(Constants.spaceDelimiter); 
    
    let partnerLocality = this.citiesDS.store.find(f => f.id === partnerImplicitLocation?.cityId);
    this.creditIncreaseRequestBtnEmailDetails.partnerRegisteredOfficeLocality = partnerLocality && partnerLocality.name;

    let partnerCounty = this.countiesDS.store.find(f => f.id === partnerImplicitLocation?.countyId);
    this.creditIncreaseRequestBtnEmailDetails.partnerRegisteredOfficeCounty = partnerCounty && partnerCounty.name;

    let partnerCountry = this.countriesDS.store.find(f => f.id === partnerImplicitLocation?.countryId);
    this.creditIncreaseRequestBtnEmailDetails.partnerRegisteredOfficeCountry = partnerCountry && partnerCountry.name;

    /**
     * END PARTNER OFFICE ADDRESS, LOCALITY, COUNTY, COUNTRY
     */

    /**
     * START PARTNER CREDIT CONTROL: CURRENT PAYMENT METHOD, CREDIT LIMIT, BLOCK DAYS, PAYMENT TERM
     */
    
    let creditControlPaymentMethod = filteredPaymentInstruments.find(f => f.id === this.partnerClientCreditControl.paymentInstrumentId);
    this.creditIncreaseRequestBtnEmailDetails.currentPaymentMethod = creditControlPaymentMethod && creditControlPaymentMethod.name;

    this.creditIncreaseRequestBtnEmailDetails.currentCreditLimit = this.partnerClientCreditControl.creditLimit && this.decimalPipe.transform(this.partnerClientCreditControl.creditLimit, Constants.decimalsNumberFormat).toString();
    this.creditIncreaseRequestBtnEmailDetails.currentPaymentTerm = this.partnerClientCreditControl.paymentTerm && (this.decimalPipe.transform(this.partnerClientCreditControl.paymentTerm, Constants.decimalsNumberFormat).toString() +
      Constants.spaceDelimiter + Constants.creditIncreaseRequestEmailDetails.creditControlDaysLabel);
    this.creditIncreaseRequestBtnEmailDetails.currentBlockNoOfDays = this.partnerClientCreditControl.graceTerm && (this.decimalPipe.transform(this.partnerClientCreditControl.graceTerm, Constants.decimalsNumberFormat).toString() +
      Constants.spaceDelimiter + Constants.creditIncreaseRequestEmailDetails.creditControlDaysLabel);

    /**
     * END PARTNER CREDIT CONTROL: CURRENT PAYMENT METHOD, CREDIT LIMIT, BLOCK DAYS, PAYMENT TERM
     */

    /**
     * START PARTNER LAST CREATED ACTIVE CONTRACT: CONTRACT NUMBER, CONTRACT VALIDITY, CONTRACT PAYMENT METHOD, PAYMENT TERM, CONTRACT CREDIT, VD AVERAGE LAST 3 MONTHS, VD LAST MONTH, VD CURRENT MONTH, DISCOUNT GRID
     */

    const partnerContracts = await this.partnerContractService.getPartnerContractsByPartnerID(this.partner.id);
    partnerContracts.sort((a, b) => new Date(b.created).getTime() - new Date(a.created).getTime());
  
    const lastCreatedActiveContract = partnerContracts.find(f => f.isActive === true && (new Date(f.validTo) >= new Date() || !f.validTo));
  
    if (lastCreatedActiveContract) {
      this.creditIncreaseRequestBtnEmailDetails.partnerActiveContractNumber = lastCreatedActiveContract.contractNumber;
      this.creditIncreaseRequestBtnEmailDetails.partnerActiveContractValidity = lastCreatedActiveContract.validTo && 
        moment(lastCreatedActiveContract.validTo).format(Constants.creditIncreaseRequestEmailDetails.lastCipVerificationDateFormat);
      this.creditIncreaseRequestBtnEmailDetails.contractPaymentMethod = filteredPaymentInstruments.find(f => f.id === lastCreatedActiveContract.paymentInstrumentId)?.name;
  
      PartnerContractDetailsComponent.paymentTermDictionary.forEach(e => {
        if (e.key === lastCreatedActiveContract.paymentTermId) {
          this.creditIncreaseRequestBtnEmailDetails.contractPaymentTerm = e.value;
        }
      });
  
      this.creditIncreaseRequestBtnEmailDetails.contractCredit = lastCreatedActiveContract.credit && this.decimalPipe.transform(lastCreatedActiveContract.credit, Constants.decimalsNumberFormat).toString();
    }

    const items = await this.financialInfoService.getDetailsByPartnerIdAsync(this.partner.id);  
    this.yearlyTurnOvers = (items && items.length > 0) ? items : [];

    if (this.yearlyTurnOvers && this.yearlyTurnOvers.length > 0) {
      const data = this.yearlyTurnOvers.find(x => x.year == new Date().getFullYear());

      if (data) {
        const monthValues = this.helperService.getMonthsData(data);
        const currentMonthIndex = new Date().getMonth();

        this.currentMonthValue = Math.ceil(monthValues[currentMonthIndex]);

        if (currentMonthIndex > 0) {
          this.previousMonthValue = Math.ceil(monthValues[currentMonthIndex - 1]);
        } else {
          const previousYear = this.yearlyTurnOvers.find(x => x.year == (new Date().getFullYear() - 1));
          const previousYearMonths = this.helperService.getMonthsData(previousYear);
          this.previousMonthValue = Math.ceil(Number(previousYearMonths[11]));
        }

        this.sumOfLastThreeMonths = 0;

        if (currentMonthIndex >= 3) {
          const x = (Number(monthValues[currentMonthIndex - 1]) + Number(monthValues[currentMonthIndex - 2]) + Number(monthValues[currentMonthIndex - 3])) / 3;
          this.sumOfLastThreeMonths = Math.ceil(Number(x.toFixed(2)));
        } else {
          const previousYear = this.yearlyTurnOvers.find(x => x.year == (new Date().getFullYear() - 1));
          const previousYearMonths = this.helperService.getMonthsData(previousYear);

          switch (currentMonthIndex) {
            case 2:
              const x = (Number(monthValues[1]) + Number(previousYearMonths[11]) + Number(previousYearMonths[10])) / 3;
              this.sumOfLastThreeMonths = Math.ceil(Number(x.toFixed(2)));
              break;
            case 1:
              const y = (Number(monthValues[0]) + Number(previousYearMonths[11]) + Number(previousYearMonths[10])) / 3;
              this.sumOfLastThreeMonths = Math.ceil(Number(y.toFixed(2)));
              break;
            case 0:
              const z = (Number(previousYearMonths[11]) + Number(previousYearMonths[10]) + Number(previousYearMonths[9])) / 3;
              this.sumOfLastThreeMonths = Math.ceil(Number(z.toFixed(2)));
              break;
          }
        }

        this.creditIncreaseRequestBtnEmailDetails.averageValueWithoutVatCustomerSalesLast3Months = this.sumOfLastThreeMonths &&  this.decimalPipe.transform(this.sumOfLastThreeMonths, Constants.decimalsNumberFormat).toString();
        this.creditIncreaseRequestBtnEmailDetails.valueWithoutVatCustomerSalesPastMonth = this.previousMonthValue && this.decimalPipe.transform(this.previousMonthValue, Constants.decimalsNumberFormat).toString();
        this.creditIncreaseRequestBtnEmailDetails.valueWithoutVatCustomerSalesCurrentMonth = this.currentMonthValue && this.decimalPipe.transform(this.currentMonthValue, Constants.decimalsNumberFormat).toString();

        const r = await this.partnerService.getPartnerProductConventionIdByImplicitLocationAsync(this.partner.id);

        if (r && r.productConventionId && r.currencyId) {
          const grids = await this.discountGridService.GetAllAsync();
          this.discountGrids = (grids && grids.length > 0) ? grids : null;

          this.productConventionGrid = this.discountGrids.find(pc => pc.productConventionId == r.productConventionId && pc.currencyId == r.currencyId && pc.benefitType == 1 && pc.dataType == 1 && pc.isActive == true);

          const maxValue = Math.max(this.currentMonthValue, this.previousMonthValue, this.sumOfLastThreeMonths);

          if (this.productConventionGrid) {
            const grids = await this.discountGridDetailsService.GetByDiscountGridAsync(this.productConventionGrid.id);

            if (grids && grids.length > 0) {
              const item = grids.find(grid => {
                const lowerConditionMet = (grid.fromConditionType === 1 && maxValue > grid.valueFrom) || (grid.fromConditionType === 2 && maxValue >= grid.valueFrom);
                const upperConditionMet = (grid.toConditionType === 1 && maxValue < grid.valueTo) || (grid.toConditionType === 2 && maxValue <= grid.valueTo);
                return lowerConditionMet && upperConditionMet;
              });

              const lists = await this.salePriceListService.getSalePriceListsAsyncByID();
              this.salePriceLists = (lists && lists.length > 0) ? lists : null;

              if (item && item.salePriceListId && this.salePriceLists && this.salePriceLists.length > 0) {
                const s = this.salePriceLists.find(ss => ss.id == item.salePriceListId);
                if (s)this.creditIncreaseRequestBtnEmailDetails.allocatedDiscountGridName = s.name;           
              }
            }
          }
        }
      }
    }

    /**
     * END PARTNER LAST CREATED ACTIVE CONTRACT: CONTRACT NUMBER, CONTRACT VALIDITY, CONTRACT PAYMENT METHOD, PAYMENT TERM, CONTRACT CREDIT, VD AVERAGE LAST 3 MONTHS, VD LAST MONTH, VD CURRENT MONTH, DISCOUNT GRID
     */

    /**
     * START PARTNER STRATEGIC, LAST CIP VERIFICATION NUMBER, VERIFICATION DATE, <2, >2
     */

    this.creditIncreaseRequestBtnEmailDetails.strategicPartner = this.partner.isStrategicPartner ? Constants.yesLabel : Constants.noLabel;

    this.creditIncreaseRequestBtnEmailDetails.lastCipVerificationNumber = (this.lastCIPCheck && this.lastCIPCheck.id) ? 
      Constants.creditIncreaseRequestEmailDetails.lastCipVerificationNumberPrefix + this.lastCIPCheck.id : Constants.noSpaceDelimiter;
    this.creditIncreaseRequestBtnEmailDetails.lastCipVerificationDate = this.lastCIPCheck && this.lastCIPCheck.dateOfCIPCheck && 
      moment(this.lastCIPCheck.dateOfCIPCheck).format(Constants.creditIncreaseRequestEmailDetails.lastCipVerificationDateFormat)
    let auxLastCipVerificationDateFormated = this.lastCIPCheck && this.lastCIPCheck.dateOfCIPCheck && 
      moment(this.lastCIPCheck.dateOfCIPCheck).format(Constants.creditIncreaseRequestEmailDetails.lastCipVerificationDateFormat2);
    this.creditIncreaseRequestBtnEmailDetails.lastCipVerificationDateFormated = auxLastCipVerificationDateFormated ? "/" + auxLastCipVerificationDateFormated : Constants.noSpaceDelimiter;
    let auxDateOfCIPCheck = this.lastCIPCheck && this.lastCIPCheck.dateOfCIPCheck && 
      moment(this.lastCIPCheck.dateOfCIPCheck).format(Constants.creditIncreaseRequestEmailDetails.lastCipVerificationDateFormat2);
    this.creditIncreaseRequestBtnEmailDetails.dateOfCIPCheck = auxDateOfCIPCheck ? "/" + auxDateOfCIPCheck : Constants.noSpaceDelimiter;
    this.creditIncreaseRequestBtnEmailDetails.moreThan2LastCipVerification = this.lastCIPCheck && this.lastCIPCheck.numberOfIncidentsAbove && this.lastCIPCheck.numberOfIncidentsAbove.toString();
    this.creditIncreaseRequestBtnEmailDetails.lessThan2LastCipVerification = this.lastCIPCheck && this.lastCIPCheck.numberOfIncidentsBelow && this.lastCIPCheck.numberOfIncidentsBelow.toString();

    this.creditIncreaseRequestBtnEmailDetails.vcipIncidentsSection = (this.lastCIPCheck && (this.lastCIPCheck.numberOfIncidentsAbove > 0 || this.lastCIPCheck.numberOfIncidentsBelow > 0))
      ? `>2: ${this.lastCIPCheck.numberOfIncidentsAbove || ''}, <2: ${this.lastCIPCheck.numberOfIncidentsBelow || ''}` : Constants.noSpaceDelimiter;

    /**
     * START PARTNER STRATEGIC, LAST CIP VERIFICATION NUMBER, VERIFICATION DATE, <2, >2
     */

    /**
     * START FINANCIAL INFO YEARS: CURRENT, LAST BUT ONE, SECOND TO LAST YEAR, YEAR CA, YEAR CA VD, ITEMS NUMBER, PROFITLOSS, EMPLOYEES NUMBER
     */

    let financialInfoYears = this.partnerFinancialInfo.map(item => item.year);
    financialInfoYears.sort((a, b) => b - a);

    if (financialInfoYears.length > 0 && financialInfoYears[0]) {
      let currentYearFinancialInfo = this.partnerFinancialInfo.find(f => f.year === financialInfoYears[0]);
      console.log('in', currentYearFinancialInfo.turnover)
      this.creditIncreaseRequestBtnEmailDetails.currentYear = currentYearFinancialInfo.year && currentYearFinancialInfo.year.toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearCa = currentYearFinancialInfo.ca && this.decimalPipe.transform(currentYearFinancialInfo.ca, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearCaVd = currentYearFinancialInfo.turnover && this.decimalPipe.transform(currentYearFinancialInfo.turnover, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearItemsNumber = currentYearFinancialInfo.numberOfItems && this.decimalPipe.transform(currentYearFinancialInfo.numberOfItems, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearProfitLoss = currentYearFinancialInfo.profitLoss && this.decimalPipe.transform(currentYearFinancialInfo.profitLoss, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearEmployeesNumber = currentYearFinancialInfo.numberOfEmployees && this.decimalPipe.transform(currentYearFinancialInfo.numberOfEmployees, Constants.decimalsNumberFormat).toString();
    }

    if (financialInfoYears.length > 0 && financialInfoYears[1]) {
      let lastButOneYearFinancialInfo = this.partnerFinancialInfo.find(f => f.year === financialInfoYears[1]);

      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1 = lastButOneYearFinancialInfo.year && lastButOneYearFinancialInfo.year.toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1Ca = lastButOneYearFinancialInfo.ca && this.decimalPipe.transform(lastButOneYearFinancialInfo.ca, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1CaVd = lastButOneYearFinancialInfo.turnover && this.decimalPipe.transform(lastButOneYearFinancialInfo.turnover, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1ItemsNumber = lastButOneYearFinancialInfo.numberOfItems && this.decimalPipe.transform(lastButOneYearFinancialInfo.numberOfItems, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1ProfitLoss = lastButOneYearFinancialInfo.profitLoss && this.decimalPipe.transform(lastButOneYearFinancialInfo.profitLoss, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1EmployeesNumber = lastButOneYearFinancialInfo.numberOfEmployees && this.decimalPipe.transform(lastButOneYearFinancialInfo.numberOfEmployees, Constants.decimalsNumberFormat).toString();
    }

    if (financialInfoYears.length > 0 && financialInfoYears[2]) {
      let secondToLastYearFinancialInfo = this.partnerFinancialInfo.find(f => f.year === financialInfoYears[2]);

      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2 = secondToLastYearFinancialInfo.year && secondToLastYearFinancialInfo.year.toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2Ca = secondToLastYearFinancialInfo.ca && this.decimalPipe.transform(secondToLastYearFinancialInfo.ca, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2CaVd = secondToLastYearFinancialInfo.turnover && this.decimalPipe.transform(secondToLastYearFinancialInfo.turnover, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2ItemsNumber = secondToLastYearFinancialInfo.numberOfItems && this.decimalPipe.transform(secondToLastYearFinancialInfo.numberOfItems, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2ProfitLoss = secondToLastYearFinancialInfo.profitLoss && this.decimalPipe.transform(secondToLastYearFinancialInfo.profitLoss, Constants.decimalsNumberFormat).toString();
      this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2EmployeesNumber = secondToLastYearFinancialInfo.numberOfEmployees && this.decimalPipe.transform(secondToLastYearFinancialInfo.numberOfEmployees, Constants.decimalsNumberFormat).toString();
    }

    /**
     * END FINANCIAL INFO YEARS: CURRENT, LAST BUT ONE, SECOND TO LAST YEAR, YEAR CA, YEAR CA VD, ITEMS NUMBER, PROFITLOSS, EMPLOYEES NUMBER
     */

    /**
     * START PARTNER COMPETITOR, PROBLEMS
     */

    let competitor = this.competitorTypes.find(f => f.id === this.partner.competitor);
    this.creditIncreaseRequestBtnEmailDetails.competitor = competitor?.name;

    await this.partnerReviewsService.getPartnerReviewsByPartnerID(this.partner.id).then(reviews => {
      (reviews && reviews.length > 0) ? 
        this.creditIncreaseRequestBtnEmailDetails.problems = _.orderBy(reviews.filter(f => f.description), ['created'], ['desc'])[0].description.substring(0, 400) : this.creditIncreaseRequestBtnEmailDetails.problems = Constants.noLabel;
    });

    /**
     * END PARTNER COMPETITOR, PROBLEMS
     */

    this.shouldDisplayCreditIncreaseRequestEmailTemplate = true;
  }

  async openCreditIncreaseRequestEmail() {
    this.loaded = true;
    await this.computeCreditIncreaseRequestDetails().then(x => {
      const email = "executive@vetro.ro";
      const cc = "comercial@vetro.ro";

      const subject = `Creditare/${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerFiscalCode)}/${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerName?.replace('&', '%26'))}`;

      const body = `***%0D%0A$$Instrument: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.pmwPaymentMethods)}$$Limita Credit:10000%0D%0A$$Blocare:15%0D%0A$$Verificare:12%0D%0A$$Termen de plata:30 %0D%0A *** %0D%0A%0D%0A Firma: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerName?.replace('&', '%26'))}, Tip client: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerActivityDescription)}, CUI: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerFiscalCode)}, RC: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerTradeRegisterNumber)} %0D%0A%0D%0A Site: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerWebsite)}, CAEN: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerCaen)} %0D%0A%0D%0A Adresa: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerRegisteredOfficeAddress?.replace('&', '%26'))}, Localitate: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerRegisteredOfficeLocality)}, Judet: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerRegisteredOfficeCounty)}, Tara: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerRegisteredOfficeCountry)} %0D%0A Instrument CH: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentPaymentMethod)}, Credit CH: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentCreditLimit)}, Termen CH: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentPaymentTerm)}, Nr Zile Blocare CH: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentBlockNoOfDays)} %0D%0A Contract: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerActiveContractNumber)}, Expira: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.partnerActiveContractValidity)}, Mod Plata ctr.: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.contractPaymentMethod)}, Termen ctr.: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.contractPaymentTerm)}, Credit contract: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.contractCredit)} %0D%0A%0D%0A VD Medie 3 Luni: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.averageValueWithoutVatCustomerSalesLast3Months)}, VD Luna Trecuta: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.valueWithoutVatCustomerSalesPastMonth)}, VD Luna Curenta: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.valueWithoutVatCustomerSalesCurrentMonth)}, Rulaj: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.allocatedDiscountGridName?.replace('%', '%25'))} %0D%0A%0D%0A Partener Strategic: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.strategicPartner)} %0D%0A%0D%0A Verificare CIP: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.lastCipVerificationNumber)}${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.dateOfCIPCheck)}; Incidente mai vechi de 2 ani: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.moreThan2LastCipVerification)}; Incidente mai noi de 2 ani: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.lessThan2LastCipVerification)}; Data ultim incident: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.lastCipVerificationDate)} %0D%0A%0D%0A ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYear)}: CA: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearCa)}, VD: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearCaVd)}, Articole: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearItemsNumber)}, Pf: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearProfitLoss)}, NA: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearEmployeesNumber)} %0D%0A ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1)}: CA: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1Ca)}, VD: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1CaVd)}, Articole: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1ItemsNumber)}, Pf: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1ProfitLoss)}, NA: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus1EmployeesNumber)} %0D%0A ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2)}: CA: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2Ca)}, VD: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2CaVd)}, Articole: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2ItemsNumber)}, Pf: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2ProfitLoss)}, NA: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.currentYearMinus2EmployeesNumber)} %0D%0A Competitor: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.competitor)}, Probleme: ${this.ensureValue(this.creditIncreaseRequestBtnEmailDetails.problems)} %0D%0A%0D%0A`;

      const mailtoLink = `mailto:${email}?cc=${cc}&Subject=${encodeURIComponent(subject)}&Body=${body}`;

      this.loaded = false;
      window.location.href = mailtoLink;
    })
  }
  
  private ensureValue(value: any): string {
    return value !== null && value !== undefined ? value : '';
  }

  async onCreditIncreaseRequestClick() {
    await this.getPartnerFinancialInfo();

    let hasCurrentYearCIPCheck = false;
    let hasValidFinancialInfoForPreviousYear = false;

    const currentYear = new Date().getFullYear();
    if (this.partnerCIPCheck) {

      hasCurrentYearCIPCheck = this.partnerCIPCheck.some(item => {
        if (item.dateOfCIPCheck) {
          const yearOfCIPCheck = new Date(item.dateOfCIPCheck).getFullYear();
          return yearOfCIPCheck === currentYear;
        }
        return false;
      });
    }

    if (this.partnerFinancialInfo) {
      hasValidFinancialInfoForPreviousYear = this.partnerFinancialInfo.some(item => {
        if (item.year === currentYear - 1) {
          const fields = [
            item.numberOfCars,
            item.numberOfEmployees,
            item.numberOfItems,
            item.numberOfWorkingPoints,
            item.profitLoss,
            item.turnover,
          ];
          return fields.every(field => field !== null && field !== undefined);
        }

        return false;
      });
    }

    if (hasCurrentYearCIPCheck && hasValidFinancialInfoForPreviousYear) {
      await this.openCreditIncreaseRequestEmail();
    } else {
      this.isCreditIncreaseRequestPopupOpen = true;
    }
  }

  onCancelCreditIncreaseRequest() {
    this.isCreditIncreaseRequestPopupOpen = false;
  }

  async onContinueCreditIncreaseRequest() {
    await this.openCreditIncreaseRequestEmail().then(() => {
      this.isCreditIncreaseRequestPopupOpen = false;
    })
  }

  getGMTFromOffset(offsetInSeconds: number): string {
    if (!offsetInSeconds)
      return '';

    const offsetInHours = offsetInSeconds / 3600;
  
    const sign = offsetInHours >= 0 ? '+' : '-';
    const formattedOffset = Math.abs(offsetInHours).toFixed(1);
  
    return `GMT${sign}${formattedOffset}`;
  }

  computeLocalTime(gmtOffset: number) {
    if (!gmtOffset)
      return '';

    const nowGMT = new Date(Date.UTC(
      new Date().getUTCFullYear(),
      new Date().getUTCMonth(),
      new Date().getUTCDate(),
      new Date().getUTCHours(),
      new Date().getUTCMinutes(),
      new Date().getUTCSeconds(),
      new Date().getUTCMilliseconds()
    ));

    const utcTime = new Date(nowGMT).getTime();
    const localTime = new Date(utcTime + gmtOffset * 1000);

    const [datePart, timePart] = localTime.toISOString().split('T');
    const [year, month, day] = datePart.split('-');
    const [hour, minute] = timePart.split(':');

    const dayFormatter = new Intl.DateTimeFormat('ro-RO', { weekday: 'short' });
    const dayOfWeek = dayFormatter.format(new Date(`${year}-${month}-${day}`));

    const timeFormatter = new Intl.DateTimeFormat('ro-RO', { hour: '2-digit', minute: '2-digit' });
    const formattedTime = timeFormatter.format(new Date(`${year}-${month}-${day}T${hour}:${minute}:00`));

    return `${dayOfWeek} ${formattedTime}`;
  }

  onInputZipCodeChange(event: any): void {
    const inputElement = event.event.target as HTMLInputElement;
    if (inputElement) {
      inputElement.value = inputElement.value.replace(/[^0-9]/g, '');
      this.selectedPartnerLocation.zipCode = inputElement.value;
    }
  }

  validateDigitsOnly = (e: any): boolean => {
    const value = e.value;
    // Return true if value contains only digits, otherwise false
    return /^[0-9]*$/.test(value);
};

  onInputPartnerNameChange(event: any): void {
    const inputElement = event.event.target as HTMLInputElement;
    if (inputElement) {
      inputElement.value = inputElement.value.replace(/[^a-zA-Z0-9&\-.\s]+/g, '').replace(/\s+/g, ' ');
      this.partner.name = inputElement.value;
    }
  }

  onCellPrepared(e: any) {
    if (e) {
        if (e.rowType == "header") {
            const cellElement = e.cellElement;

            if (e.cellElement.ariaLabel === "Column ERP") {
                cellElement.setAttribute("title", "Reprezinta Id-ul de legatura cu Senior ERP. Daca are valoare, atunci partenerul este legat cu Senior.");

                $(cellElement).dxTooltip({
                    target: cellElement,
                    showEvent: "mouseenter",
                    hideEvent: "mouseleave",
                    position: "bottom",
                    contentTemplate: () => $("<div>").text("Reprezinta Id-ul de legatura cu Senior ERP. Daca are valoare, atunci partenerul este legat cu Senior.")
                }).dxTooltip("instance");
            }

            if (e.column.dataField === 'isActive') {
              const cellElement = e.cellElement;
              cellElement.setAttribute("title", "Adresa se mai utilizeaza sau este inactiva. Cele Inactive nu se sincronizeaza cu Senior");
              ($(cellElement) as any).tooltip({
                placement: 'top'
              });
            }
        }
    }
}

  isItemGroupCategoryChangeOutput(event: any) {
    if (event) {
      this.checkForImplicitSupplierAgent();
    }
  }
}
