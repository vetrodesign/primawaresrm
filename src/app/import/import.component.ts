import { Component, OnInit, ViewChild, Input } from '@angular/core';
import * as xlsx from 'xlsx';
import { DxDataGridComponent } from 'devextreme-angular';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';


@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {
  @Input() importType: number;
  @Input() reqKeys: any;
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  animationVisible: boolean;
  file: File;
  buffer: any;
  headerColumns: any[];
  gridDataSource: any[];
  selectedRows: any[];
  showGenerateBtn: boolean;
  showImportBtn: boolean;
  gridColumns: any[];
  showSpinner: boolean;
  showGrid: boolean;
  showError: boolean;
  keysRequired: string;
  header: string;
  constructor(private authService: AuthService,
     private translationService: TranslateService) { }

  ngOnInit(): void {
    this.header = this.translationService.instant('general.requiredKey');
    let keys = '';
    this.reqKeys.forEach(i => {
      keys += i + ',';
    });
    this.header += keys;
  }

  onSelectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.showImportBtn = (this.selectedRows.length > 0) ? true : false;
  }


  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.response === this.translationService.instant('general.inserted')) {
         e.rowElement.style.backgroundColor = '#4caf50';
     } else {
       if (e.data && e.data.response === this.translationService.instant('general.error')) {
        e.rowElement.style.backgroundColor = '#ff9800';
       }
     }
  }

  uploadData(event) {
    this.headerColumns = [];
    this.file = null;
    this.file = event.target.files[0];
    this.readFile();
    this.showGenerateBtn = true;
    this.showError = false;
    this.showGrid = false;
    this.showImportBtn = false;
  }

  readFile() {
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.buffer = fileReader.result;
      const data = new Uint8Array(this.buffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = xlsx.read(bstr, { type: 'binary' });
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];
      this.headerColumns = xlsx.utils.sheet_to_json(worksheet, { raw: true, defval: null });
    };
    fileReader.readAsArrayBuffer(this.file);
  }

  generateDataGrid() {
    this.gridDataSource = [];
    this.gridColumns = [];
    let index = 0;
    let error = false;
    this.showImportBtn = true;
    let fileKeys = [];
    this.headerColumns.forEach(item => {
      const di = {};
      for (const propName in item) {
        if (item[propName] !== null && item[propName] !== undefined) {
          di[propName] = item[propName];
          if (index === 0) {
            fileKeys.push(propName);
            this.gridColumns.push({ dataField: propName, caption: propName });
          }
        }
      }
      index++;
      this.gridDataSource.push(di);
    });
    this.gridColumns.push({ dataField: 'response', caption: this.translationService.instant('general.response') });
    this.reqKeys.forEach(key => {
      if (fileKeys.indexOf(key) === -1) {
        error = true;
      }
    });
    if (this.gridDataSource.length > 0 && !error) {
      this.showGrid = true;
    } else {
      this.showError = true;
    }
    this.showGenerateBtn = false;
  }

   public async import() {
    this.showSpinner = true;
    this.showImportBtn = false;
    const customerId = this.authService.getUserCustomerId();
    // switch type in functie de tipul importului

  }
}
