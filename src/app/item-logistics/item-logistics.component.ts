import { Component, Input, OnInit } from '@angular/core';
import { Constants } from 'app/constants';
import { IPageActions } from 'app/models/ipageactions';
import { Item } from 'app/models/item.model';
import { ItemService } from 'app/services/item.service';
import { PageActionService } from 'app/services/page-action.service';
import { TranslateService } from 'app/services/translate';

@Component({
  selector: 'app-item-logistics',
  templateUrl: './item-logistics.component.html',
  styleUrls: ['./item-logistics.component.css']
})
export class ItemLogisticsComponent implements OnInit {
  @Input() item: Item;

  actions: IPageActions;

  logistics: any[];

  loaded: boolean = false;
  groupedText: string;

  constructor(
    private itemService: ItemService,
    private translationService: TranslateService,
    private pageActionService: PageActionService
  ) {
    this.setActions();
    this.groupedText = this.translationService.instant('groupedText');

  }

  ngOnInit(): void {
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.item).then(result => {
      this.actions = result;
    });
  }

  canExport() {
    return this.actions.CanExport;
  }

  async loadData() {
    this.loaded = true;

    await this.itemService.getItemLogisticsAsync(this.item.id).then(r => {
      if (r) {
        this.logistics = r;
        this.loaded = false;
      }
    });
  }

  formatDecimal(cellInfo: any): string {
    return Number(cellInfo.value).toFixed(2);
  }

  formatDecimal4(cellInfo: any): string {
    return Number(cellInfo.value).toFixed(4);
  }
  
}
