import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemLogisticsComponent } from './item-logistics.component';

describe('ItemLogisticsComponent', () => {
  let component: ItemLogisticsComponent;
  let fixture: ComponentFixture<ItemLogisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemLogisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemLogisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
