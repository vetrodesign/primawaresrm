import { Component, OnInit, ViewChild } from '@angular/core';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { ClientSmallSearchFilter } from 'app/models/client-small-search-filter.model';
import { Department } from 'app/models/department.model';
import { IPageActions } from 'app/models/ipageactions';
import { ItemTurnoverAnalysis, ItemTurnoverAnalysisFilter } from 'app/models/item-turnover-analysis.model';
import { Item } from 'app/models/item.model';
import { Post } from 'app/models/post.model';
import { SupplierOrigin } from 'app/models/supplier-origin.model';
import { User } from 'app/models/user.model';
import { AuthService } from 'app/services/auth.service';
import { DepartmentsService } from 'app/services/departments.service';
import { ItemTurnoverAnalysisService } from 'app/services/item-turnover-analysis.service';
import { ItemService } from 'app/services/item.service';
import { NotificationService } from 'app/services/notification.service';
import { PartnerService } from 'app/services/partner.service';
import { PostService } from 'app/services/post.service';
import { SupplierOriginService } from 'app/services/supplier-origin.service';
import { TranslateService } from 'app/services/translate';
import { UsersService } from 'app/services/user.service';
import { DxDataGridComponent } from 'devextreme-angular';
import * as moment from 'moment';

@Component({
  selector: 'app-item-turnover-analysis',
  templateUrl: './item-turnover-analysis.component.html',
  styleUrls: ['./item-turnover-analysis.component.css']
})
export class ItemTurnoverAnalysisComponent implements OnInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;

  itemTurnoverAnalysis: ItemTurnoverAnalysis[] = [];
  items: any;
  postsDS: Post[];
  posts: Post[];
  departments: Department[];
  users: User[];
  userPost: Post;
  isFromMngDepartment: boolean;
  filter: ItemTurnoverAnalysisFilter = new ItemTurnoverAnalysisFilter();
  suppliers: any;
  partners: any;
  partnersDS: any;
  sortedItemsFilter: any;
  supplierOrigin: SupplierOrigin[];
  isSupplyDirectorFlag: boolean;
  isModifiedDataSource = [{id: 1, name: 'modify', imageSrc: 'modify-manual-turnover-icon.png'}, {id: 2, name: 'dont-modify', imageSrc: 'dont-modify-manual-turnover-icon.png'}];
  isModifiedToWholePageTypeId: number;
  isModifiedPopupVisible: boolean;
  selectedIsModifiedWholePageValue: any = null;
  currentPageIndex: number;
  lastPageIndex: any = -1;

  groupedText: string;
  actions: IPageActions;
  loaded: boolean;
  isOwner: boolean;

  constructor(private itemTurnoverAnalysisService: ItemTurnoverAnalysisService, private notificationService: NotificationService, private departmentsService: DepartmentsService,
    private authService: AuthService, private translationService: TranslateService, private itemsService: ItemService, private postService: PostService, private userService: UsersService,
    private partnerService: PartnerService, private supplierOriginService: SupplierOriginService) {
    this.getDisplayExprPosts = this.getDisplayExprPosts.bind(this);
    this.authService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });

    this.setActions();
    this.groupedText = this.translationService.instant('groupedText');

    this.filter.isModified = true;
    this.filter.isAlerting = true;
    this.filter.turnoverSymbol = '>';

    this.getData();
  }

  ngOnInit(): void {

  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: false,
      CanUpdate: true,
      CanDelete: false,
      CanPrint: false,
      CanExport: false,
      CanImport: false,
      CanDuplicate: false
    };
  }

  async getData() {
    this.loaded = true;  
    await Promise.all([this.getUsers(), this.getPosts(), this.getDepartments(), this.getPartners(), this.getSupplierOrigin()]).then(async () => {

      if (this.posts && this.posts.length > 0 && this.departments && this.departments.length > 0) {
        this.userService.getByUsernameOrEmail(this.authService.getUserUserName(), null).then(user => {
          if (user && user.length > 0 && this.departments && this.departments.length > 0) {
            if (user[0] && user[0].postId) {
              this.userPost = this.posts.find(x => Number(x.id) === user[0].postId);
              let dep = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Management);
              const officesFromMngDep = (dep && dep.length > 0 ) ? dep.map(x => x.offices).reduce((x, y) => x.concat(y)) : null; 
              if (officesFromMngDep && officesFromMngDep.length && (officesFromMngDep.find(x => x.id === this.userPost.officeId) !== null)) {
                this.isFromMngDepartment = true;
              }
              
              this.postsDS = [];
              if (this.userPost) {
                if (this.userPost.isLocationSuperior || this.userPost.isGeneralManager || this.isFromMngDepartment || this.isOwner) {
                  this.postsDS = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Supply).map(x => x.offices).reduce((x, y) => x.concat(y)).map(p => p.posts).reduce((g, h) => g.concat(h));
                } else {
                  this.postsDS = [];      
                }
              }
            }
          }
        });
      }

      this.isSupplyDirectorFlag = this.isSupplyDirector();

      await this.getItemTurnoverAnalysis();
      this.loaded = false;
    })
  }

  isSupplyDirector() {
    if (this.posts && this.posts.length > 0 && this.users && this.users.length > 0) {
      let post = this.posts?.find(x => x.id === this.users.find(x => x.id === Number(this.authService.getUserId()))?.postId);
      if (post) {
        if (post.code === "DA") {
          return true;
        } else {
          return false;
        }
      }
    }
  }

  async getItemTurnoverAnalysis() {
    this.itemTurnoverAnalysis = [];
    this.loaded = true;

    await this.getItems().then(async r => {
  
      await this.itemTurnoverAnalysisService.getAllAsync().then(async analysisItems => { 
            
        let analysisItemIds = this.itemTurnoverAnalysis?.map(i => i.itemId);
  
        await this.itemTurnoverAnalysisService.getItemTurnoverAnalysisData().then(data => {
    
          this.items.store.filter(f => f.isStockable).forEach(i => {
   
            //if (!analysisItemIds?.includes(i.id)) {
              let newItemTurnoverAnalysis = new ItemTurnoverAnalysis();
              
              newItemTurnoverAnalysis.itemId = i.id;
              newItemTurnoverAnalysis.itemCode = i.code;
              newItemTurnoverAnalysis.itemName = i.nameRO;
              newItemTurnoverAnalysis.partnerId = i.partnerId;

              if (data) {
                let selectedData = data.find(f => f.itemId === i.id);

                newItemTurnoverAnalysis.itemStock = selectedData?.itemStock ? selectedData?.itemStock : 0;
                newItemTurnoverAnalysis.itemTurnover = selectedData?.itemTurnover ? selectedData?.itemTurnover : 0;
                newItemTurnoverAnalysis.itemEvaluatedTurnover = selectedData?.itemEvaluatedTurnover ? selectedData?.itemEvaluatedTurnover : 0;
                const clients = [selectedData?.clientsNo3, selectedData?.clientsNo6, selectedData?.clientsNo9, selectedData?.clientsNo12]
                  .map(c => c ? c : 0)
                  .map(c => new Intl.NumberFormat().format(c));
                newItemTurnoverAnalysis.clientsNo = clients.length ? clients.join("/") : null;
                newItemTurnoverAnalysis.salesValueCurrentMonth = selectedData?.salesValueCurrentMonth ? selectedData?.salesValueCurrentMonth : 0;
                newItemTurnoverAnalysis.salesValues1MonthAgo = selectedData?.salesValues1MonthAgo ? selectedData?.salesValues1MonthAgo : 0;
                newItemTurnoverAnalysis.salesValues2MonthAgo = selectedData?.salesValues2MonthAgo ? selectedData?.salesValues2MonthAgo : 0;
                newItemTurnoverAnalysis.salesValues3MonthAgo = selectedData?.salesValues3MonthAgo ? selectedData?.salesValues3MonthAgo : 0;
                const salesValues = [selectedData?.salesValueCurrentMonth, selectedData?.salesValues1MonthAgo, selectedData?.salesValues2MonthAgo, selectedData?.salesValues3MonthAgo]
                  .map(c => c ? c : 0)
                  .map(c => new Intl.NumberFormat().format(c));
                newItemTurnoverAnalysis.salesValuesComputed = salesValues.length ? salesValues.join("/") : null;
                newItemTurnoverAnalysis.r3 = selectedData?.r3 ? Math.ceil(selectedData?.r3) : 0;
                newItemTurnoverAnalysis.r6 = selectedData?.r6 ? Math.ceil(selectedData?.r6) : 0;
                newItemTurnoverAnalysis.r9 = selectedData?.r9 ? Math.ceil(selectedData?.r9) : 0;
                newItemTurnoverAnalysis.r12 = selectedData?.r12 ? Math.ceil(selectedData?.r12) : 0;

                let auxr3r12Max = [selectedData?.r3, selectedData?.r6, selectedData?.r9, selectedData?.r12].every(isNaN) 
                  ? null : Math.max(selectedData?.r3, selectedData?.r6, selectedData?.r9, selectedData?.r12);
                newItemTurnoverAnalysis.r3r12Max = Math.ceil(auxr3r12Max);

                let stockAlertInternal = this.supplierOrigin.find(s => s.code === 'INT');
                let stockAlertExternal = this.supplierOrigin.find(s => s.code === 'EXT');
                let stockAlertIntraCommunity = this.supplierOrigin.find(s => s.code === 'INTRACOM');

                let automaticTurnover = 0;
                let bottomMargin = 0; // todo: development for item type
                let topMargin = 0;
                let bottomMarginPercentage = 0;
                let topMarginPercentage = 0;

                switch (selectedData?.itemType) {
                  case 2: // extern
                    bottomMargin = stockAlertExternal ? Math.round((stockAlertExternal.lowerMargin / 100) * 10) / 10 : 0; // 0.9 rounded up
                    topMargin = stockAlertExternal ? Math.round((stockAlertExternal.upperMargin / 100) * 10) / 10 : 0; // 1.25 rounded up

                    bottomMarginPercentage = stockAlertExternal.lowerMargin;
                    topMarginPercentage = stockAlertExternal.upperMargin;
                    break;
                  case 3: // intracom
                    bottomMargin = stockAlertIntraCommunity ? Math.round((stockAlertIntraCommunity.lowerMargin / 100) * 10) / 10 : 0; // 0.9 rounded up
                    topMargin = stockAlertIntraCommunity ? Math.round((stockAlertIntraCommunity.upperMargin / 100) * 10) / 10 : 0; // 1.25 rounded up

                    bottomMarginPercentage = stockAlertIntraCommunity.lowerMargin;
                    topMarginPercentage = stockAlertIntraCommunity.upperMargin;
                    break;
                  case 4: // intern
                    bottomMargin = stockAlertInternal ? Math.round((stockAlertInternal.lowerMargin / 100) * 10) / 10 : 0; // 0.8 rounded up
                    topMargin = stockAlertInternal ? Math.round((stockAlertInternal.upperMargin / 100) * 10) / 10 : 0;;

                    bottomMarginPercentage = stockAlertInternal.lowerMargin;
                    topMarginPercentage = stockAlertInternal.upperMargin;
                    break;

                  default:
                    break;
                }

                newItemTurnoverAnalysis.bottomMargin = bottomMargin;
                newItemTurnoverAnalysis.topMargin = topMargin;

                newItemTurnoverAnalysis.bottomMarginPercentage = bottomMarginPercentage;
                newItemTurnoverAnalysis.topMarginPercentage = topMarginPercentage;

                if (auxr3r12Max < newItemTurnoverAnalysis.itemTurnover * bottomMargin) {
                  automaticTurnover = auxr3r12Max * 1.1;
                } else if (auxr3r12Max > newItemTurnoverAnalysis.itemTurnover * topMargin) {
                  automaticTurnover = auxr3r12Max * 1.1;
                } else {
                  automaticTurnover = newItemTurnoverAnalysis.itemTurnover;
                }

                newItemTurnoverAnalysis.automaticProposedTurnover = Math.round(automaticTurnover);
                if (newItemTurnoverAnalysis.automaticProposedTurnover <= 1) {
                  newItemTurnoverAnalysis.automaticProposedTurnover = 2;
                }

                newItemTurnoverAnalysis.manualTurnover = selectedData?.manualTurnover;

                let computedIsModified;
                if (newItemTurnoverAnalysis.automaticProposedTurnover == newItemTurnoverAnalysis.itemTurnover) {
                  computedIsModified = false;
                } else {
                  computedIsModified = true;
                }

                newItemTurnoverAnalysis.isModified = computedIsModified;

                if (selectedData?.isModified === false || selectedData?.isModified === true) {
                  newItemTurnoverAnalysis.isModified = selectedData?.isModified;
                }

                newItemTurnoverAnalysis.supplierAgentId = selectedData?.supplierAgentId;

                newItemTurnoverAnalysis.alerting = "Imediat";

                let analysisData = analysisItems.find(f => f.itemId === i.id);
                if (analysisData) {
                  newItemTurnoverAnalysis.id = analysisData.id;
                  newItemTurnoverAnalysis.created = analysisData.created;
                  newItemTurnoverAnalysis.modified = analysisData.modified;
                  newItemTurnoverAnalysis.createdBy = analysisData.createdBy;
                  newItemTurnoverAnalysis.modifiedBy = analysisData.modifiedBy;
                  newItemTurnoverAnalysis.rowVersion = analysisData.rowVersion;

                  newItemTurnoverAnalysis.manualTurnover = analysisData.manualTurnover;
                  newItemTurnoverAnalysis.isModified = analysisData.isModified;

                  if (analysisData?.isModified === false || analysisData?.isModified === true || analysisData?.manualTurnover) {
                    const today = moment();
                    const nextMonth = today.clone().add(1, 'months').endOf('month');
                    const nextMonthDate = nextMonth.format('DD.MM.YYYY');
                    newItemTurnoverAnalysis.alerting = nextMonthDate;
                  }

                  newItemTurnoverAnalysis.supplyAlertState = analysisData.supplyAlertState;
                  newItemTurnoverAnalysis.nextSupplyAlertDate = analysisData.nextSupplyAlertDate;
                }
                
                let auxIsModified;
                if (newItemTurnoverAnalysis.isModified === true) {
                  auxIsModified = 1;
                } else {
                  auxIsModified = 0;
                }

                let newTurnover = newItemTurnoverAnalysis.automaticProposedTurnover * auxIsModified;
                newItemTurnoverAnalysis.newTurnover = newTurnover;

                if (analysisData && analysisData.manualTurnover) {
                  newItemTurnoverAnalysis.newTurnover = analysisData.manualTurnover;
                }
              }
  
              this.itemTurnoverAnalysis.push(newItemTurnoverAnalysis);
            //}
          });
    
          this.itemTurnoverAnalysis = this.itemTurnoverAnalysis.filter(f => f.itemStock);

          if (this.filter.itemIds && this.filter.itemIds.length > 0) {
            this.itemTurnoverAnalysis = this.itemTurnoverAnalysis.filter(f => this.filter.itemIds.includes(f.itemId));
          }

          if (this.filter.postIds && this.filter.postIds.length > 0) {
            this.itemTurnoverAnalysis = this.itemTurnoverAnalysis.filter(f => this.filter.postIds.includes(f.supplierAgentId));
          }

          if (this.filter.supplierId) {
            this.itemTurnoverAnalysis = this.itemTurnoverAnalysis.filter(f => f.partnerId === this.filter.supplierId);
          }

          if (this.filter.isModified === true) {
            this.itemTurnoverAnalysis = this.itemTurnoverAnalysis.filter(f => f.isModified === false);
          }

          if (this.filter.isAlerting === true) {
            this.itemTurnoverAnalysis = this.itemTurnoverAnalysis.filter(f => !f.alerting || f.alerting === 'Imediat');
          }

          if (this.filter.turnoverSymbol) {
            switch (this.filter.turnoverSymbol) {
              case '>': {
                this.itemTurnoverAnalysis = this.itemTurnoverAnalysis.filter(f => f.itemTurnover > 0);
                break;
              }

              case '=': {
                this.itemTurnoverAnalysis = this.itemTurnoverAnalysis.filter(f => f.itemTurnover == 0);
                break;
              }

              case '>=': {
                this.itemTurnoverAnalysis = this.itemTurnoverAnalysis.filter(f => f.itemTurnover >= 0);
                break;
              }

              default:
                break;
            }
          }

          this.dataGrid.instance.pageIndex(this.currentPageIndex);
          this.loaded = false;
        })
      });
    });
  }

  async getSupplierOrigin() {
    if (this.isOwner) {
      await this.supplierOriginService.getAllAsync().then(items => {
        this.supplierOrigin = (items && items.length > 0) ? items : [];
      });
    } else {
      await this.supplierOriginService.getAllByCustomerIdAsync().then(items => {
        this.supplierOrigin = (items && items.length > 0) ? items : [];
      });
    }
  }

  async getItems(): Promise<any> {
    if (this.isOwner) {
      await this.itemsService.getAllItemsAsync().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };

        this.sortedItemsFilter = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    } else {
      await this.itemsService.getItemssAsyncByID().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };

        this.sortedItemsFilter = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    }
  }

  async getPosts() {
    if (this.isOwner) {
      await this.postService.getAllPostsAsync().then(items => {
        this.posts = items;
      });
    } else {
      await this.postService.getPostsByCustomerId().then(items => {
        this.posts = items;
      });
    }
  }

  async getDepartments() {
    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(items => {
        this.departments = items;
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(items => {
        this.departments = items;
      });
    }
  }

  async getUsers() {
    if (this.isOwner) {
      await this.userService.getUsersAsync().then(items => {
        this.users = items;
      });
    } else {
      await this.userService.getUserAsyncByID().then(items => {
        this.users = items;
      });
    }
  }

  public refreshData() {
    this.getData();
    this.dataGrid.instance.refresh();
  }

  async onSearchClick() {
    await this.getItemTurnoverAnalysis();
  }

  getDisplayExprItems(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  getDisplayExprPosts(item) {
    if (!item) {
      return '';
    }

    const user = this.users?.find(f => f.postId === item.id);

    if (user) {
      return item.companyPost + " - " + user.lastName + " " + user.firstName;
    }

    return item.companyPost;
  }

  onMultiTagItemChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onSuppliersItemSelected(event) {
    if (event && event.selectedItem) {
      this.filter.supplierId = event.selectedItem.id;
    }
  }

  onSuppliersFilterSearch(searchTerm) {
    if (searchTerm && searchTerm.length >= 4) {
      let searchFilter = new ClientSmallSearchFilter();
      searchFilter.nameOrCode = searchTerm;

      this.partnerService.getPartnersSmallAsync(searchFilter).then(items => {
        this.suppliers = (items && items.length > 0) ? items : [];
        this.suppliers = this.suppliers.map(m => ({ ...m, codeAndName: `${m.code} - ${m.name}` }));
      });
    } else {
      this.suppliers = [];
    }
  }

  onPartnersItemSelected(event) {
    if (event && event.selectedItem) {
      this.filter.manufacturerId = event.selectedItem.id;
    }
  }

  onPartnersFilterSearch(searchTerm) {
    if (searchTerm && searchTerm.length >= 4) {
      let searchFilter = new ClientSmallSearchFilter();
      searchFilter.nameOrCode = searchTerm;

      this.partnerService.getPartnersSmallAsync(searchFilter).then(items => {
        this.partners = (items && items.length > 0) ? items : [];
        this.partners = this.partners.map(m => ({ ...m, codeAndName: `${m.code} - ${m.name}` }));
      });
    } else {
      this.partners = [];
    }
  }

  getDisplayExprPartners(item) {
    if (!item) {
        return '';
      }
      return item.code + ' - ' + item.name;
  }

  async getPartners() {
    await this.partnerService.getAllPartnersSmallAsync().then(partners => {
      this.partnersDS = {
        paginate: true,
        pageSize: 15,
        store: partners
      }
    })
  }

  async onRowUpdated(event) {
    if (event) {
      let selectedRow = event.data;
      this.loaded = true;
      if (selectedRow.id) {
        await this.itemTurnoverAnalysisService.updateAsync(selectedRow).then(async (r) => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
          } else {
            this.notificationService.alert('top', 'center', 'Datele nu au fost salvate! A aparut o eroare!', NotificationTypeEnum.Red, true);
          }
          await this.getItemTurnoverAnalysis();
          this.loaded = false;
        })
      } else {
        await this.itemTurnoverAnalysisService.createAsync(selectedRow).then(async (r) => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
          } else {
            this.notificationService.alert('top', 'center', 'Datele nu au fost salvate! A aparut o eroare!', NotificationTypeEnum.Red, true);
          }
          await this.getItemTurnoverAnalysis();
          this.loaded = false;
        })
      }
    }
  }

  onCellPrepared(e) {
    if (e && e.rowType === 'header' && e.column.dataField === 'clientsNo') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Numar clienti diferiti in 3/6/9/12 luni");
      ($(cellElement) as any).tooltip({
        placement: 'top'
      });
    }

    if (e && e.rowType === 'header' && e.column.dataField === 'automaticProposedTurnover') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Rulaj Propus Automat dupa formula: Daca valoarea din Max(R3:R12) este mai mica decat Rulaj inmultit cu Marja Inf, atunci inmulteste Max(R3:R12) cu 1.1. Daca valoarea din Max(R3:R12) este mai mare decat Rulaj inmultit cu Marja Sup, atunci inmulteste Max(R3:R12) cu 1.1. In toate celelalte cazuri, foloseste direct valoarea din Rulaj. Rezultatul final este rotunjit in sus la cel mai apropiat numar intreg. Exceptie: cazul in care propunerea de rulaj este 1, se pune intotdeauna 2.");
      ($(cellElement) as any).tooltip({
        placement: 'top'
      });
    }

    if (e && e.rowType === 'header' && e.column.dataField === 'alerting') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Se modifica in 60 de zile fara luna curenta daca s-a intervenit manual prin coloana 'Modifica' sau 'Rulaj Propus Manual");
      ($(cellElement) as any).tooltip({
        placement: 'top'
      });
    }

    if (e && e.rowType === 'header' && e.column.dataField === 'bottomMargin') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Procent alertare inferioara fata de Rulaj Maxim. Pentru a modifica Marja de alertare la Rulaj, mergeti la Administrare -> Alertare Stoc");
      ($(cellElement) as any).tooltip({
        placement: 'top'
      });
    }

    if (e && e.rowType === 'header' && e.column.dataField === 'topMargin') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Procent alertare superioara fata de Rulaj Maxim. Pentru a modifica Marja de alertare la Rulaj, mergeti la Administrare -> Alertare Stoc");
      ($(cellElement) as any).tooltip({
        placement: 'top'
      });
    }

    if (e && e.rowType === 'header' && e.column.dataField === 'salesValuesComputed') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Cantitatea vanduta in Luna Curenta si 3 luni anterioare");
      ($(cellElement) as any).tooltip({
        placement: 'top'
      });
    }

    if (e && e.rowType === 'header' && e.column.dataField === 'itemEvaluatedTurnover') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Rulaj Evaluat din Survey pe toate sursele de date");
      ($(cellElement) as any).tooltip({
        placement: 'top'
      });
    }

    if (e && e.rowType === 'header' && e.column.dataField === 'r3r12Max') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Rulajele pe 3 luni / 6 luni / 9 luni / 12 luni, exceptand luna curenta si se calculeaza ca suma vanzarilor / numarul lunilor la care se reporteaza.");
      ($(cellElement) as any).tooltip({
        placement: 'top'
      });
    }
  }

  isMaxR(value: number, currentData: any): boolean {
    return value === Math.max(currentData.r3, currentData.r6, currentData.r9, currentData.r12);
  }  

  async onManualTurnoverChange(rowData: any) {
    let selectedRow = this.itemTurnoverAnalysis.find(f => f.itemId === rowData.itemId);

    if (selectedRow) {
      this.loaded = true;
      if (selectedRow.id) {
        await this.itemTurnoverAnalysisService.updateAsync(selectedRow).then(async (r) => {  
          let selectedItem = this.items.store.find(f => f.id === rowData.itemId);
          let item = new Item();
          item = selectedItem;
          item.turnOver = rowData.manualTurnover;

          await this.itemsService.updateItemAsync(item).then(async r => { 
            if (r) {
              this.notificationService.alert('top', 'center', 'Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
            } else {
              this.notificationService.alert('top', 'center', 'Datele nu au fost salvate! A aparut o eroare!', NotificationTypeEnum.Red, true);
            }

            await this.getItemTurnoverAnalysis();
            this.loaded = false;
          });  
        })
      } else {
        await this.itemTurnoverAnalysisService.createAsync(selectedRow).then(async (r) => {
          let selectedItem = this.items.store.find(f => f.id === rowData.itemId);
          let item = new Item();
          item = selectedItem;
          item.turnOver = rowData.manualTurnover;

          await this.itemsService.updateItemAsync(item).then(async r => { 
            if (r) {
              this.notificationService.alert('top', 'center', 'Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
            } else {
              this.notificationService.alert('top', 'center', 'Datele nu au fost salvate! A aparut o eroare!', NotificationTypeEnum.Red, true);
            }

            await this.getItemTurnoverAnalysis();
            this.loaded = false;
          }); 
        })
      }
    }
  }

  async onModifyManualTurnoverClick(rowData: any) {
    let selectedRow = this.itemTurnoverAnalysis.find(f => f.itemId === rowData.itemId);
    selectedRow.isModified = false;

    if (selectedRow) {
      this.loaded = true;
      if (selectedRow.id) {
        await this.itemTurnoverAnalysisService.updateAsync(selectedRow).then(async (r) => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
          } else {
            this.notificationService.alert('top', 'center', 'Datele nu au fost salvate! A aparut o eroare!', NotificationTypeEnum.Red, true);
          }
          await this.getItemTurnoverAnalysis();
          this.loaded = false;
        })
      } else {
        await this.itemTurnoverAnalysisService.createAsync(selectedRow).then(async (r) => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
          } else {
            this.notificationService.alert('top', 'center', 'Datele nu au fost salvate! A aparut o eroare!', NotificationTypeEnum.Red, true);
          }
          await this.getItemTurnoverAnalysis();
          this.loaded = false;
        })
      }
    }
  }

  async onDontModifyManualTurnoverClick(rowData: any) {
    let selectedRow = this.itemTurnoverAnalysis.find(f => f.itemId === rowData.itemId);
    selectedRow.isModified = true;

    if (selectedRow) {
      this.loaded = true;
      if (selectedRow.id) {
        await this.itemTurnoverAnalysisService.updateAsync(selectedRow).then(async (r) => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
          } else {
            this.notificationService.alert('top', 'center', 'Datele nu au fost salvate! A aparut o eroare!', NotificationTypeEnum.Red, true);
          }
          await this.getItemTurnoverAnalysis();
          this.loaded = false;
        })
      } else {
        await this.itemTurnoverAnalysisService.createAsync(selectedRow).then(async (r) => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
          } else {
            this.notificationService.alert('top', 'center', 'Datele nu au fost salvate! A aparut o eroare!', NotificationTypeEnum.Red, true);
          }
          await this.getItemTurnoverAnalysis();
          this.loaded = false;
        })
      }
    }
  }

  onItemsFilterOpen(event: any) {
    let arr = this.items.store.slice().sort((a, b) => {
      const aSelected = this.filter.itemIds.includes(a.id);
      const bSelected = this.filter.itemIds.includes(b.id);

      if (aSelected && !bSelected) return -1;
      if (!aSelected && bSelected) return 1;
      return a.code.localeCompare(b.code);
    });

    this.sortedItemsFilter = {
      paginate: true,
      pageSize: 15,
      store: arr
    };
  }

  onTurnoverFilterArrowClick() {
    const symbols = ['>', '=', '>='];
    const currentIndex = symbols.indexOf(this.filter.turnoverSymbol);
    this.filter.turnoverSymbol = symbols[(currentIndex + 1) % symbols.length];
  }

  onIsModifiedToWholePageChange(isModifiedDataSourceId: number) {
    if (isModifiedDataSourceId) {
      this.selectedIsModifiedWholePageValue = isModifiedDataSourceId;
      this.isModifiedToWholePageTypeId = isModifiedDataSourceId;
      this.isModifiedPopupVisible = true;
    }
  }

  onIsModifiedWholePageDropDownClosed() {
    setTimeout(() => {
      this.selectedIsModifiedWholePageValue = null;
    }, 0);
  }

  async onModifyWholePageClick() {
    const visibleRows = this.dataGrid.instance.getVisibleRows();

    let createRows = [];
    let updateRows = [];
    visibleRows.forEach((row: any)=> {
      let selectedRow = this.itemTurnoverAnalysis.find(f => f.itemId === row.data.itemId);

      if (selectedRow) {
        selectedRow.isModified = this.isModifiedToWholePageTypeId === 1 ? false : true;

        if (selectedRow.id) {
          updateRows.push(selectedRow);
        } else {
          createRows.push(selectedRow);
        }
      }
    })

    if ((createRows && createRows.length > 0) || (updateRows && updateRows.length > 0)) {
      this.loaded = true;
    
      try {
        const createPromise = createRows && createRows.length > 0 
          ? this.itemTurnoverAnalysisService.createMultipleAsync(createRows) 
          : Promise.resolve(true); 
        
        const updatePromise = updateRows && updateRows.length > 0 
          ? this.itemTurnoverAnalysisService.updateMultipleAsync(updateRows) 
          : Promise.resolve(true);
    
        const [createSuccess, updateSuccess] = await Promise.all([createPromise, updatePromise]);
    
        if (createSuccess && updateSuccess) {
          this.notificationService.alert('top', 'center', 'Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost salvate complet! A apărut o eroare!', NotificationTypeEnum.Red, true);
        }
    
        await this.getItemTurnoverAnalysis();
      } catch (error) {
        this.notificationService.alert('top', 'center', 'A apărut o eroare în timpul procesării!', NotificationTypeEnum.Red, true);
      } finally {
        this.isModifiedPopupVisible = false;
        this.loaded = false;
      }
    }   
  }

  onDontModifyWholePageClick() {
    this.isModifiedPopupVisible = false;
  }

  onContentReady(e:any): void {  
    let dataGrid = e.component;  
    let currentPageIndex = dataGrid.pageIndex();  

    if(this.lastPageIndex === -1) {  
      this.lastPageIndex = currentPageIndex;  
    } else {  
      if(this.lastPageIndex !== currentPageIndex) {  
        this.currentPageIndex = currentPageIndex;  
      }  
    }  
  } 
}
