import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { Partner } from 'app/models/partner.model';
import { AuthService } from 'app/services/auth.service';
import { NotificationService } from 'app/services/notification.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';
import * as moment from 'moment';
import * as _ from 'lodash';
import { PartnerNameEquivalence } from 'app/models/partnernameequivalence.model';
import { PartnerNameEquivalenceService } from 'app/services/partner-name-equivalence.service';
import { PartnerService } from 'app/services/partner.service';

@Component({
  selector: 'app-partner-name-equivalence',
  templateUrl: './partner-name-equivalence.component.html',
  styleUrls: ['./partner-name-equivalence.component.css']
})
export class PartnerNameEquivalenceComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() partner: Partner;

  groupedText: string;
  actions: IPageActions;
  partnerNameEquivalence: PartnerNameEquivalence[] = [];
  selectedRows: any[];
  loaded: boolean;
  selectedRowIndex = -1;
  partnersDS: any;

  partnerNameEquivalenceInfo: string;

  @ViewChild('partnerNameEquivalenceDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('partnerNameEquivalenceGridToolbar') gridToolbar: GridToolbarComponent;

  constructor(private notificationService: NotificationService,
    private authService: AuthService, private translationService: TranslateService, private partnerNameEquivalenceService: PartnerNameEquivalenceService, private partnerService: PartnerService) {
    this.setActions();
    this.groupedText = this.translationService.instant('groupedText');

    this.partnerNameEquivalenceInfo = this.translationService.instant("partnerNameEquivalenceInfo");
    this.getData();
   }

   setActions() {
    this.actions = {
      CanView: true,
      CanAdd: true,
      CanUpdate: true,
      CanDelete: true,
      CanPrint: true,
      CanExport: true,
      CanImport: false,
      CanDuplicate: false
    };
  }
  canUpdate() {
    return this.actions.CanUpdate;
  }

  canDelete() {
    return this.actions.CanDelete;
  }

  canExport() {
    return this.actions.CanExport;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.setGridInstances();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.partner) {
    }
  }

  setGridInstances() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getPartners(), this.getPartnerNameEquivalence()]).then(x => {
      this.loaded= false;
    })
  }

  async getPartnerNameEquivalence() {
    this.partnerNameEquivalenceService.getAllPartnerNameEquivalenceAsync().then(items => {
      if (items && items.length > 0) {
        this.partnerNameEquivalence = items;
      } else {
        this.partnerNameEquivalence = [];
      }
    });
  }

  refreshPartnerNameEquivalence() {
    this.getPartnerNameEquivalence();
    this.dataGrid.instance.refresh();
  }

  add() {
    this.dataGrid.instance.addRow();
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async save(partnerNameEquivalence: PartnerNameEquivalence): Promise<boolean> {
    let result = null
  
    if (partnerNameEquivalence) {
      this.loaded = true;
      result = await this.partnerNameEquivalenceService.createPartnerNameEquivalenceAsync(partnerNameEquivalence).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Echivalenta Nume Partener - Datele au fost inserate cu succes!',
           NotificationTypeEnum.Green, true)
           this.loaded = false;
           this.refreshPartnerNameEquivalence();
           return false;
        } else {
          this.notificationService.alert('top', 'center', 'Echivalenta Nume Partener - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
            this.loaded = false;
            this.refreshPartnerNameEquivalence();
            return true;
        }
      });
    } else return false;
  }

  public onRowInserting(event: any): void {
    event.cancel = true;
    let item = new PartnerNameEquivalence();
    item = event.data;
    if (item && item.partnerId) {
      event.cancel = this.save(item).then(result => { return result; })
    }
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public deleteRecords(data: any) {
    this.loaded = true;
    this.partnerNameEquivalenceService.deletePartnerNameEquivalenceAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Datele au fost sterse cu succes!', NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost sterse!', NotificationTypeEnum.Red, true)
      }
      this.loaded = false;
      this.refreshPartnerNameEquivalence();
    });
  }

  getDisplayExprPartners(partner) {
    if (!partner) {
      return '';
    }

    const partnerName: string = partner.name;
    const partnerCode: string = partner.code;

    let name: string = '(lipsa nume)';
    let code: string = '(lipsa cod)';

    if (partnerName !== undefined && partnerName !== null && partnerName !== '') {
      name = partnerName;
    }

    if (partnerCode !== undefined && partnerCode !== null && partnerCode !== '') {
      code = partnerCode;
    }

    return name + ' - ' + code;
  }

  async getPartners() {
    await this.partnerService.getAllPartnersSmallAsync().then(partners => {
      this.partnersDS = {
        paginate: true,
        pageSize: 15,
        store: partners
      }
    })
  }
}
