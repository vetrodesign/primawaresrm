import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { Partner } from 'app/models/partner.model';
import { PartnerReview } from 'app/models/partnerreview.model';
import { Post } from 'app/models/post.model';
import { User } from 'app/models/user.model';
import { AuthService } from 'app/services/auth.service';
import { NotificationService } from 'app/services/notification.service';
import { PartnerReviewService } from 'app/services/partner-review.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-partner-review',
  templateUrl: './partner-review.component.html',
  styleUrls: ['./partner-review.component.css']
})
export class PartnerReviewComponent implements OnInit, OnChanges {
  @Input() partner: Partner;
  @Input() posts: any;
  @Input() users: User[];
  @Input() actions: any;

  @ViewChild('partnerReviewsDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('reviewGridToolbar') gridToolbar: GridToolbarComponent;

  partnerReviews: PartnerReview[] = [];
  userPost: Post;
  selectedPartnerReview: PartnerReview;
  selectedRows: any[];
  selectedRowIndex = -1;
  groupedText: string;

  constructor(private partnerReviewsService: PartnerReviewService, private notificationService: NotificationService,
    private authService: AuthService, private translationService: TranslateService) {
    this.setActions();
      this.groupedText = this.translationService.instant('groupedText');
   }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.partner) {
      this.getReviews();
    }
    if (changes.posts) {
      if (this.posts && this.posts.length && this.users && this.users.length) {
        let user = this.users.find(x => x.userName === this.authService.getUserUserName());
        if (user) {
          this.userPost = this.posts.find(x => Number(x.id) === user.postId);
        }
      }
     }
  }

  async getReviews() {
    if (this.partner && this.partner.id) {
      this.partnerReviewsService.getPartnerReviewsByPartnerID(this.partner.id).then(reviews => {
        this.partnerReviews = reviews;
      });
     }
  }

  public refreshReviews() {
    this.getReviews();
    this.dataGrid.instance.refresh();
  }

  public add() {
    this.dataGrid.instance.addRow();
  }

  canExport() {
    return this.actions.CanExport;
  }

  public async onRowReviewInserting(event: any): Promise<void> {
    let item = new PartnerReview();
    item = event.data;
    item.partnerId = Number(this.partner.id);
    item.postId = this.userPost.id;
  
    if (item) {
      await this.partnerReviewsService.createPartnerReviewAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshReviews();
      });
    }
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: true,
      CanUpdate: false,
      CanDelete: false,
      CanPrint: true,
      CanExport: true,
      CanImport: false,
      CanDuplicate: false
    };
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  selectionReviewChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

}
