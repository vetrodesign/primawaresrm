import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YearlyTurnoverComponent } from './yearly-turnover.component';

describe('YearlyTurnoverComponent', () => {
  let component: YearlyTurnoverComponent;
  let fixture: ComponentFixture<YearlyTurnoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YearlyTurnoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearlyTurnoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
