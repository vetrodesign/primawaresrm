import { Component, OnInit, AfterViewInit, ViewChild, Input } from "@angular/core";
import { NotificationTypeEnum } from "app/enums/notificationTypeEnum";
import { GridToolbarComponent } from "app/helpers/grid-toolbar/grid-toolbar.component";
import { IPageActions } from "app/models/ipageactions";
import { SalesTargetXPost } from "app/models/saletargetxpost.model";
import { AuthService } from "app/services/auth.service";
import { CurrencyRateService } from "app/services/currency-rate.service";
import { NotificationService } from "app/services/notification.service";
import { SalesTargetPostService } from "app/services/sales-target-post.service";
import { TranslateService } from "app/services/translate";
import { DxDataGridComponent, DxValidatorComponent } from "devextreme-angular";


@Component({
  selector: 'app-sales-target-allocation',
  templateUrl: './sales-target-allocation.component.html',
  styleUrls: ['./sales-target-allocation.component.css']
})
export class SalesTargetAllocationComponent implements OnInit, AfterViewInit {
  @Input() salesTargets: any;
  @Input() salesGroupTargets: any;
  @Input() posts: any;
  @Input() saleTargetTypes: any;

  @ViewChild('salesTargetAllocationDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('salesTargetAllocationGridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('saleTargetAllocationValidation') saleTargetAllocationValidation: DxValidatorComponent;

  displayData: boolean;
  loaded: boolean;
  actions: IPageActions;
  salesTargetAllocationPopup: boolean;
  selectedSaleTargetAllocation: SalesTargetXPost;
  salesTargetAllocations: SalesTargetXPost[] = [];
  currencies: any[] = [];

  filterSalesTargets: any[] = [];
  selectedTargetType: number;

  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  selectedPost: any;
  groupedText: string;
  isTabActive: string;
  isOwner: boolean;
  constructor(
    private salesTargetAllocationService: SalesTargetPostService,
    private translationService: TranslateService,
    private currencyService: CurrencyRateService,
    private notificationService: NotificationService,
    private authService: AuthService) {
    this.isOwner = this.authService.isUserOwner();
    this.groupedText = this.translationService.instant('groupedText');
    this.isTabActive = this.authService.getCustomerPageTitleTheme();

    this.selectedPost = null;
    this.setActions();
    this.getTargetDisplayExpr = this.getTargetDisplayExpr.bind(this);
  }

  ngOnInit(): void {
  }

  async getData(searchAll? : boolean) {
    if (!this.selectedPost || searchAll) {
      this.selectedPost = null;
      await this.salesTargetAllocationService.getAllAsync().then(items => {
        this.salesTargetAllocations = (items && items.length >  0) ? items : [];
      });
    } else {
      await this.salesTargetAllocationService.getAllByPostIdAsync(this.selectedPost).then(items => {
        this.salesTargetAllocations = (items && items.length >  0) ? items : [];
      });
    }
    this.dataGrid.dataSource = this.salesTargetAllocations;
  }

  loadData() {
    this.getBaseCurrency();
    this.getData();
    this.setGridInstance();
  }

  async getBaseCurrency(): Promise<any> {
    await this.currencyService.getBaseCurrency().then(x => {
     this.currencies = x;
   });
 }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  ngAfterViewInit() {
    this.setGridInstance();
  }

  setGridInstance() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: true,
      CanUpdate: true,
      CanDelete: true,
      CanPrint: true,
      CanExport: true,
      CanImport: true,
      CanDuplicate: true
    };
  }

  public add() {
    this.selectedSaleTargetAllocation = new SalesTargetXPost();
    this.selectedSaleTargetAllocation.isActive = true;
    this.salesTargetAllocationPopup = true;
  }

  public refreshDataGrid() {
    this.loadData();
    this.dataGrid.instance.refresh();
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public deleteRecords(data: any) {
    this.salesTargetAllocationService.deleteAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Alocare Target - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Alocare Target - Datele nu au fost sterse!',
          NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data) {
      if (e.data.salesTargetId && this.salesTargets.find(x => x.id === e.data.salesTargetId)?.isActive === false) {
        e.rowElement.style.backgroundColor = '#ffaf82';
      } 
      if (e.data.salesGroupId && this.salesGroupTargets.find(x => x.id === e.data.salesGroupId)?.isActive === false) {
        e.rowElement.style.backgroundColor = '#ffaf82';
      } 
    }
  }

  async onRowUpdated(e) {
    let item = new SalesTargetXPost();
    item = e.data;

    if (item) {
      await this.salesTargetAllocationService.updateAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Alocare Target - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Alocare Target - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };
        this.refreshDataGrid();
      });
    }
  }

  async onPostValueChanged(e) {
    if (e && e.value && e.event) {
      await this.salesTargetAllocationService.getAllByPostIdAsync(e.value).then(items => {
        this.salesTargetAllocations = (items && items.length >  0) ? items : [];
      });
      this.dataGrid.dataSource = this.salesTargetAllocations;
    }
  }

  public async openDetails(data: any) {
    this.selectedSaleTargetAllocation = data;
    this.salesTargetAllocationPopup = true;
  }

  public async updateData(): Promise<void> {
    if (this.saleTargetAllocationValidation.instance.validate().isValid) {  
      let item = new SalesTargetXPost();
      item = this.selectedSaleTargetAllocation;
      if (item) {
        await this.salesTargetAllocationService.updateAsync(item).then(r => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Alocare Target Agent - Datele au fost modificate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Alocare Target Agent - Datele nu au fost modificate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          };
          this.salesTargetAllocationPopup = false;
          this.refreshDataGrid();
        });
      }
    }
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async saveData(): Promise<void> {
    if (this.saleTargetAllocationValidation.instance.validate().isValid) { 
      let item = new SalesTargetXPost();
      item = this.selectedSaleTargetAllocation;
      if (item && this.validateItem(item)) {
        await this.salesTargetAllocationService.createAsync(item).then(r => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Alocare Target Agent - Datele au fost inserate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Alocare Target Agent - Datele nu au fost inserate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          }
        });
      }
      this.salesTargetAllocationPopup = false;
      this.refreshDataGrid();
      }
  }

  validateItem(item: SalesTargetXPost): boolean {
    if (item && item.postId && item.salesTargetId) {
      let salesTargetCount = this.salesTargetAllocations.filter(x => x.postId === item.postId && x.salesTargetId === item.salesTargetId).length;

      if (salesTargetCount === 0) {
        return true;
      } else {
          this.notificationService.alert('top', 'center', 'Alocare Target Agent - Datele nu au fost inserate! Exista deja un target de acest tip pe postul selectat!',
            NotificationTypeEnum.Red, true)
        return false;
      }
    }
  }

  getPostDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  getTargetDisplayExpr(item) {
    if (!item) {
      return '';
    }
    let active = item.isActive ? 'Valid' : 'Invalid';
    let targetType = this.saleTargetTypes?.find(x => x.id === item.targetType)?.name;
    return item.code + ' - ' + item.name + ' - ' + active + ' - ' + targetType; 
  }

  getGroupTargetDisplayExpr(item) {
    if (!item) {
      return '';
    }
    let active = item.isActive ? 'Valid' : 'Invalid';
    return item.code + ' - ' + item.name + ' - ' + active;
  }

  onSaleGroupChange(e) {
    if (e && e.value) {
      this.filterSalesTargets = this.salesTargets.filter(x => x.saleGroupTargetId === e.value);
    }
  }

  onSaleTargetChange(e) {
    if (e && e.value) {
      this.selectedTargetType = this.filterSalesTargets.find(x => x.id === e.value)?.targetType;
    }
  }
}


