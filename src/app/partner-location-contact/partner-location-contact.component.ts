import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { PartnerContactObservationStateEnum } from 'app/enums/partnerContactObservationStateEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { County } from 'app/models/county.model';
import { Occupation } from 'app/models/occupation.model';
import { PartnerContactObservation } from 'app/models/partner-contact-observation.model';
import { Partner } from 'app/models/partner.model';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { PartnerLocationContactXMessagingApp } from 'app/models/partnerlocationcontactxmessagingapp.model';
import { PartnerLocationScheduleComponent } from 'app/partner-location-schedule/partner-location-schedule.component';
import { AuthService } from 'app/services/auth.service';
import { CountryService } from 'app/services/country.service';
import { CountyService } from 'app/services/county.service';
import { HelperService } from 'app/services/helper.service';
import { NotificationService } from 'app/services/notification.service';
import { OccupationService } from 'app/services/occupation.service';
import { PageActionService } from 'app/services/page-action.service';
import { PartnerContactObservationService } from 'app/services/partner-contact-observation.service';
import { PartnerLocationContactXMessagingAppService } from 'app/services/partner-location-contact-x-messaging-app.service';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-partner-location-contact',
  templateUrl: './partner-location-contact.component.html',
  styleUrls: ['./partner-location-contact.component.css']
})
export class PartnerLocationContactComponent implements OnInit, OnChanges {
  @Input() partner: Partner;
  @Input() actions: any;
  @Input() partnerLocations: any;
  @Input() messagingApps: any[];

  @ViewChild('partnerContactDataGrid') contactDataGrid: DxDataGridComponent;
  @ViewChild('contactGridToolbar') gridContactToolbar: GridToolbarComponent;

  @ViewChild('gridPlcmaDataGrid') gridPlcmaDataGrid: DxDataGridComponent;
  @ViewChild('plcmaGridToolbar') plcmaGridToolbar: GridToolbarComponent;

  @ViewChild('gridPartnerContactObservationDataGrid') gridPartnerContactObservationDataGrid: DxDataGridComponent;
  @ViewChild('partnerContactObservationGridToolbar') partnerContactObservationGridToolbar: GridToolbarComponent;

  @ViewChild('partnerLocationScheduleComponent') partnerLocationScheduleComponent: PartnerLocationScheduleComponent;

  partnerContacts: PartnerLocationContact[] = [];
  selectedPartnerLocationContact: PartnerLocationContact = new PartnerLocationContact();
  customerId: string;
  rowIndex: any;
  occupations: Occupation[] = [];
  selectedPlcmaRows: any[];
  selectedPlcmaRowIndex = -1;
  selectedPartnerContactObservationRows: any[];
  selectedPartnerContactObservationRowIndex = -1;
  selectedContactRows: any[];
  selectedContactRowIndex = -1;
  selectedPartnerLocationContactId: any;
  messagingAppPopupVisible: boolean;
  partnerContactObservationPopupVisible: boolean;
  showPartnerLocationSchedulePopup: boolean;
  plcma: PartnerLocationContactXMessagingApp[] = [];
  partnerContactObservation: PartnerContactObservation[] = [];
  partnerContactObservationState: { id: number; name: string }[] = [];
  groupedText: string;
  countries : any[];
  phoneNumberValidationRegExp: string = '^[0-9]{4,15}$';
  counties: County[] = [];
  constructor(private pageActionService: PageActionService,
    private partnerLocationContactService: PartnerLocationContactService,
    private notificationService: NotificationService,
    private occupationService: OccupationService,
    private translationService: TranslateService,
    private authenticationService: AuthService,
    private partnerLocationContactXMessagingAppService: PartnerLocationContactXMessagingAppService,
    private partnerContactObservationService: PartnerContactObservationService,
    private helperService: HelperService,
    private authService: AuthService,
    private countyService: CountyService,
    private countryService: CountryService) {
    this.groupedText = this.translationService.instant('groupedText');
    this.customerId = this.authenticationService.getUserCustomerId();
    this.phonePrefixExpr = this.phonePrefixExpr.bind(this);
    this.setGridInstance()
    this.setActions();
  }

  ngOnInit(): void {
    for (let n in PartnerContactObservationStateEnum) {
      if (typeof PartnerContactObservationStateEnum[n] === 'number') {
        this.partnerContactObservationState.push({
          id: <any>PartnerContactObservationStateEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
  }

  setGridInstance() {
    if (this.contactDataGrid && this.gridContactToolbar) {
      this.gridContactToolbar.dataGrid = this.contactDataGrid;
      this.gridContactToolbar.setGridInstance();
    }
  }

  setPlcmaGridInstance() {
    if (this.gridPlcmaDataGrid && this.plcmaGridToolbar) {
      this.plcmaGridToolbar.dataGrid = this.gridPlcmaDataGrid;
      this.plcmaGridToolbar.setGridInstance();
    }
  }

  setPartnerContactObservationGridInstance() {
    if (this.gridPartnerContactObservationDataGrid && this.partnerContactObservationGridToolbar) {
      this.partnerContactObservationGridToolbar.dataGrid = this.gridPartnerContactObservationDataGrid;
      this.partnerContactObservationGridToolbar.setGridInstance();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.partner).then(result => {
      this.actions = result;
    });
  }
  canUpdate() {
    return this.actions.CanUpdate;
  }

  canExport() {
    return this.actions.CanExport;
  }

  canDelete() {
    return this.actions.CanDelete;
  }

  async getPartnerLocationContacts() {
    await this.countryService.getAllCountrysAsync().then(r => { this.countries = r;  })
    await this.countyService.getAllCountysAsync().then(async items => {
      this.counties = items;
      
      this.partnerContacts = [];
      if (this.partner && this.partner.id) {
        await this.partnerLocationContactService.getPartnerLocationContactIncludingInactiveByPartnerID(this.partner.id).then(contacts => {
          if (contacts) {
            this.partnerContacts = contacts;

            this.partnerContacts.forEach(pc => {
              let partnerLocation = this.partnerLocations.find(f => f.id === pc.partnerLocationId);
              let gmtOffset = this.counties.find(f => f.id === partnerLocation?.countyId)?.gmtOffset;

              let gmt = this.computeLocalTime(gmtOffset);
              pc.gmt = gmt;
            })
          } else {
            this.partnerContacts = [];
          }
        });
      }
    });

    await this.occupationService.getAllOccupationsAsync().then(items => {
      if (items && items.length > 0) {
        this.occupations = items;
      } else {
        this.occupations = [];
      }
    });
  }
  

  async getPlcmaByPartnerLocationContactId() {
    if (this.selectedPartnerLocationContactId) {
      await this.partnerLocationContactXMessagingAppService.getByPartnerLocationContactIdAsync(this.selectedPartnerLocationContactId).then(items => {
        this.plcma = items;
      });
    }
  }

  async getPartnerContactObservationByPartnerLocationContactId() {
    if (this.selectedPartnerLocationContactId) {
      await this.partnerContactObservationService.getAllByPartnerContactIdAsync(this.selectedPartnerLocationContactId).then(items => {
        this.partnerContactObservation = items;
      });
    }
  }

  public refreshContactDataGrid() {
    this.getPartnerLocationContacts();
    this.contactDataGrid.instance.refresh();
  }

  public refreshPlcmaDataGrid() {
    this.getPlcmaByPartnerLocationContactId();
    this.contactDataGrid.instance.refresh();
  }

  public refreshPartnerContactObservationDataGrid() {
    this.getPartnerContactObservationByPartnerLocationContactId();
    this.contactDataGrid.instance.refresh();
  }

  public async openContactDetails(row: any) {
    this.selectedPartnerLocationContact = row;
    this.contactDataGrid.instance.editRow(row.rowIndex);
    this.rowIndex = row.rowIndex;
  }

  public addPartnerLocationContact() {
    this.contactDataGrid.instance.addRow();
  }

  public addPlcma() {
    this.gridPlcmaDataGrid.instance.addRow();
  }

  public addPartnerContactObservation() {
    this.gridPartnerContactObservationDataGrid.instance.addRow();
  }

  public async onRowContactUpdated(event: any): Promise<void> {
    let item = new PartnerLocationContact();
    item = event.data
    if (item && this.canUpdate() && item.isActive) {
      await this.partnerLocationContactService.updatePartnerLocationContactAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
            this.checkPhoneNumber(item);
        }
        this.refreshContactDataGrid();
      });
    } else {
      this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele nu au fost modificate! A aparut o eroare!',
        NotificationTypeEnum.Red, true);
    }
  }

  public async onRowContactInserting(event: any): Promise<void> {
    let item = new PartnerLocationContact();
    item = event.data;
    item.partnerId = Number(this.partner.id);
    item.isActive = true;
    if (item) {
      await this.partnerLocationContactService.createPartnerLocationContactAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshContactDataGrid();
      });
    }
  }

  public async onRowPlcmaInserting(event: any): Promise<void> {
    let item = new PartnerLocationContactXMessagingApp();
    item = event.data;
    item.partnerLocationContactId = Number(this.selectedPartnerLocationContactId);
    if (item) {
      await this.partnerLocationContactXMessagingAppService.createPartnerLocationContactXMessagingAppAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Contact mesagerie online - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Contact mesagerie online - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshPlcmaDataGrid();
      });
    }
  }

  public async onRowPartnerContactObservationInserting(event: any): Promise<void> {
    let item = new PartnerContactObservation();
    item = event.data;
    item.partnerContactId = Number(this.selectedPartnerLocationContactId);
    item.customerId = Number(this.authService.getUserCustomerId());
    if (item) {
      await this.partnerContactObservationService.createAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Observatii Contact Partener - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Observatii Contact Partener - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshPartnerContactObservationDataGrid();
      });
    }
  }

  public deletePlcmaRecords(data: any) {
    this.partnerLocationContactXMessagingAppService.deleteMultiplePartnerLocationContactXMessagingAppsAsync(this.selectedPlcmaRows).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Contact mesagerie online - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Contact mesagerie online - Datele nu au fost sterse!',
          NotificationTypeEnum.Red, true)
      }
      this.refreshPlcmaDataGrid();
    });
  }

  public deletePartnerContactObservationRecords(data: any) {
    this.partnerContactObservationService.deleteMultipleAsync(this.selectedPartnerContactObservationRows.map(item => item.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Observatii Contact Partener - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Observatii Contact Partener - Datele nu au fost sterse!',
          NotificationTypeEnum.Red, true)
      }
      this.refreshPartnerContactObservationDataGrid();
    });
  }


  public deleteContactRecords(data: any) {
    this.partnerLocationContactService.deletePartnerLocationContactAsync(this.selectedContactRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele nu au fost sterse!',
          NotificationTypeEnum.Red, true)
      }
      this.refreshContactDataGrid();
    });
  }

  public deleteContactRow(data: any) {
    this.contactDataGrid.instance.selectRows(data.key, false);
    this.gridContactToolbar.displayDeleteConfirmation();
  }

  selectionPlcmaChanged(data: any) {
    this.selectedPlcmaRows = data.selectedRowsData;
    this.selectedPlcmaRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  selectionPartnerContactObservationChanged(data: any) {
    this.selectedPartnerContactObservationRows = data.selectedRowsData;
    this.selectedPartnerContactObservationRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  selectionContactChanged(data: any) {
    this.selectedContactRows = data.selectedRowsData;
    this.selectedContactRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  activateContactDetails(data) {
    if (data && data.data && data.data.id) {
      this.partnerLocationContactService.activatePartnerContactAsync(data.data.id).then(response => {
        if (response) {
          this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Linia a fost activata!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Linia nu au fost activata!', NotificationTypeEnum.Red, true)
        }
        this.refreshContactDataGrid();
      });
    }
  }

  public async onPlcmaEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onPartnerContactObservationEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public openPlcma(data) {
    this.selectedPartnerLocationContactId = data.id;
    this.plcma = [];
    this.partnerLocationContactXMessagingAppService.getByPartnerLocationContactIdAsync(data.id).then(items => {
      this.plcma = items;
      this.messagingAppPopupVisible = true;
      this.setPlcmaGridInstance();
    });
  }

  public openPartnerContactObservation(data) {
    this.selectedPartnerLocationContactId = data.id;
    this.partnerContactObservation = [];
    this.partnerContactObservationService.getAllByPartnerContactIdAsync(data.id).then(items => {
      this.partnerContactObservation = items;
      this.partnerContactObservationPopupVisible = true;
      this.setPartnerContactObservationGridInstance();
    });
  }

  showPartnerLocationSchedule(data) {
    this.showPartnerLocationSchedulePopup = true;
    if (data && data.partnerLocationId) {
      this.partnerLocationScheduleComponent.getPartnerLocationSchedule(data.partnerLocationId);
    }
  }

  onEmailClick(event) {
    if (event && event.data && event.rowType === "data" && event.column.dataField == "email" && event.key.id && event.data.email) {
      window.location.href = `mailto:${event.value}`;
    }
  }

  checkPhoneNumber(item: PartnerLocationContact) {
    if (!item.phoneNumber.match(new RegExp(this.phoneNumberValidationRegExp))) {
      this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Numarul de telefon este invalid!',
        NotificationTypeEnum.Red, true)
    }
  }

  computeLocalTime(gmtOffset: number) {
    if (!gmtOffset)
      return '';

    const nowGMT = new Date(Date.UTC(
      new Date().getUTCFullYear(),
      new Date().getUTCMonth(),
      new Date().getUTCDate(),
      new Date().getUTCHours(),
      new Date().getUTCMinutes(),
      new Date().getUTCSeconds(),
      new Date().getUTCMilliseconds()
    ));

    const utcTime = new Date(nowGMT).getTime();
    const localTime = new Date(utcTime + gmtOffset * 1000);

    const [datePart, timePart] = localTime.toISOString().split('T');
    const [year, month, day] = datePart.split('-');
    const [hour, minute] = timePart.split(':');

    const dayFormatter = new Intl.DateTimeFormat('ro-RO', { weekday: 'short' });
    const dayOfWeek = dayFormatter.format(new Date(`${year}-${month}-${day}`));

    const timeFormatter = new Intl.DateTimeFormat('ro-RO', { hour: '2-digit', minute: '2-digit' });
    const formattedTime = timeFormatter.format(new Date(`${year}-${month}-${day}T${hour}:${minute}:00`));

    return `${dayOfWeek} ${formattedTime}`;
  }

  onCellPrepared(e) {
    if (e && e.rowType === 'header') {
      if (e.column.dataField === 'description') {
        const cellElement = e.cellElement;
        cellElement.setAttribute("title", "Scrie notite, lucruri care sunt utile si vrei sa ti le amintesti, despre aceasta Persoana de contact.");
        ($(cellElement) as any).tooltip({
          placement: 'top'
        });
      }
    }
  }

  phonePrefixExpr(e) {
    if (e && e.partnerLocationId != null && this.partnerLocations.length > 0 && this.countries.length > 0) {
      var pl = this.partnerLocations.find(x => x.id == e.partnerLocationId);
      if (pl && pl.countryId) {
        var c = this.countries.find(x => x.id == pl.countryId);
        if (c != null) {
          return '+' + c.phonePrefix;
        }
      }
    }
    return '';
  }
}
