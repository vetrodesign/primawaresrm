import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerDropdownSearchComponent } from './partner-dropdown-search.component';

describe('PartnerDropdownSearchComponent', () => {
  let component: PartnerDropdownSearchComponent;
  let fixture: ComponentFixture<PartnerDropdownSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerDropdownSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerDropdownSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
