import { TaxRate } from "./taxrate.model";

export class Tax {
    id: string;
    taxCode: string;
    taxName: string;
    taxType: number;
    taxDescription: string;
    taxRates: TaxRate[];
    countryId: number;
}