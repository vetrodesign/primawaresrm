export class County {
    public id: number;
    public code: string;
    public name: string;
    public phonePrefix: string;
    public countryId: number;
    public languageId: number;
    public gmtOffset: number;
}
