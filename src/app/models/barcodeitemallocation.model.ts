import { BaseDomain } from "./base-domain.model";

export class BarCodeItemAllocation extends BaseDomain{
    public itemId: number;

    public barCodeAllocationType: number;
    public barCodeIntervalId: number;
    public barCode: number;

    public barCodeKey: number;
    public ownerCode: number;
    public barCodeInterval: number;
    public barCodeIntervalString: string;

    public setIntervals() {
        if (this.barCodeInterval) {
            this.barCodeIntervalString = this.barCodeInterval.toString().length === 5 ? this.barCodeInterval.toString() : this.setStringValue(this.barCodeInterval.toString());
        }
    }

    setStringValue(value: string): string {
        let count = 5 - value.length;
        let string = '';
        for(let i = 0; i < count; i++) {
            string += '0';
        }
        return string + value;
    }

    constructor(barCodeItemAllocation?: BarCodeItemAllocation) {
        super();
        if (barCodeItemAllocation) {
            this.id = barCodeItemAllocation.id;
            this.rowVersion = barCodeItemAllocation.rowVersion
            this.itemId = barCodeItemAllocation.itemId;
            this.barCodeAllocationType = barCodeItemAllocation.barCodeAllocationType;
            
            this.barCodeIntervalId = barCodeItemAllocation.barCodeIntervalId;
            this.barCode = barCodeItemAllocation.barCode;
            this.barCodeKey = barCodeItemAllocation.barCodeKey;
            this.ownerCode = barCodeItemAllocation.ownerCode;

            this.barCodeInterval = barCodeItemAllocation.barCodeInterval;
            this.barCodeIntervalString = barCodeItemAllocation.barCodeIntervalString;
            this.barCodeIntervalString = barCodeItemAllocation.barCodeIntervalString;
        }
    }
}
