export class ItemAutomation {
    itemId?: number;
    intern: boolean;
    intraCommunity: boolean;
    external: boolean;
    authorizationClient: boolean;
    ansvsa: boolean;
    humanFarmacy: boolean;
    extractionCode: boolean;

    id?: number;
    rowVersion?: any;
    created?: Date;
    modified?: Date;
    createdBy?: number;
    modifiedBy?: number;

    customerId: number;
    siteId: number;
}