export class TaxRate {
    id: number;
    taxRateCode: string;
    taxRateDescription: string;
    rate: number;
    debit: string;
    credit: string;
    taxId: number;
    taxType: number;
    tax: any;
}