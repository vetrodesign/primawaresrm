export class CaenCodeDomain {
    public id: number;
    public code: string;
    public description: string;
}