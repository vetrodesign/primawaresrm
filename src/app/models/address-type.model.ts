export class AddressType  {
    public id: number;
    public code: string;
    public nameRO: string;
    public nameENG: string;
    public isActive: boolean;
}