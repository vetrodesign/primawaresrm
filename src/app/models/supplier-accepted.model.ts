import { BaseDomain } from "./base-domain.model";

export class SupplierAccepted extends BaseDomain {
    public partnerId: number;
    public postId: boolean;
    public state: number;
}