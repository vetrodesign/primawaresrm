import { BaseDomain } from "./base-domain.model";

export class PartnerLocationSchedule extends BaseDomain {
    partnerLocationId: number;
    dayOfTheWeek: number;
    dayOfTheWeekName: string;
    isClose: boolean;
    startTime: Date;
    endTime: Date;
}