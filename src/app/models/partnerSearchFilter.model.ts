import { PersonTypeEnum } from "app/enums/personTypeEnum";

export class PartnerSearchFilter {
    postIds: any;
    partnerType: any;
    countiesIds: any;
    citiesIds: any;
    countriesIds: any;
    languageIds: any;
    siteIds: any;
    personTypes: PersonTypeEnum;
    tagIds: any;
    fiscalCode: any;
    phone: any;
    email: any;
    name: any;
    code: any;
    caenSpecializationsIds: any;
    partnerActivityIds: any;
    productConventionIds: any;

    clientType: any;
    supplierType: any;
    
    isActive: boolean;

    nameOrCode: string;
    itemGroupCategoryIds: any;
    itemGroupCodeIds: any;
    isAssociatedItemsSelected: boolean;
    isActiveItemsSelected: boolean;
    isInactiveItemsSelected: boolean;
    itemCode: string;
    isAllTurnoverSelected: boolean;
    isWithTurnoverSelected: boolean;
    isWithoutTurnoverSelected: boolean;
    globalSearch: string;
    isActiveSuppliersSelected: boolean;
    isInactiveSuppliersSelected: boolean;
    isPossibleSuppliersSelected: boolean;
    customNameOrCode: string;
    caenCodeIds: any;

    haveSubscriptions: boolean= false;
}

export class SuppliersCountData {
    activeSuppliersCount: number;
    inactiveSuppliersCount: number;
    possibleSuppliersCount: number;
}
