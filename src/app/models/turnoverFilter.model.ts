export class TurnoverFilter {    
    supplierIds: number[];
    countryIds: number[];
    countyIds: number[];
    cityIds: number[];
    itemCodes: string[];
    itemIds: number[];
    supplierAgentIds: number[];
}

export class TurnoverHistoryFilter {
    itemId: number;
    itemTurnoverOperationId: number;
}