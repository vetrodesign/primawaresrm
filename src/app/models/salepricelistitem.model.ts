export class SalePriceListItem {
    id: string;
    salePriceListId: number;
    itemId: number;
    listPrice: number;
}
