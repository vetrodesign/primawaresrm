import { BaseDomain } from "./base-domain.model";

export class Cut extends BaseDomain {
    public code: string;
    public customerId: number;
    public isActive: boolean;
}