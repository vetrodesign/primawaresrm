export class ItemListFilter {
    code: any;
    nameRO: any;
    nameENG: any;
    shortNameRO: any;
    shortNameENG: any;

    siteIds: any;
    customCodeIds: any;
    itemGroupIds: any;
    measurementUnitIds: any;
    caenSpecializationIds: any;
    itemTypeIds: any;
    proposedByIds: any;
    supplierIds: any;
    supplierAgentIds: any;
    tagIds: any;
    barCodeText: any;
    isValid: boolean = true;
    isActive: boolean = true;
    itemTurnover: number;
    chartTurnover: string;
}

export class ItemERPSyncOptionsVM {
    itemId: number;
    historyPostName: string;
    historyUserName: string;
    isSeniorErp: boolean;
}



