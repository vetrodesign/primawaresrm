export class ItemGroupCpvCode {
    public id: number;
    public itemGroupId: number;
    public cpvCodeId: number;
}
