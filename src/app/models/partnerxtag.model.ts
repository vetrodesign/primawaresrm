import { BaseDomain } from "./base-domain.model";

export class PartnerXTag extends BaseDomain {
    partnerId: number;
    tagId: number;
}