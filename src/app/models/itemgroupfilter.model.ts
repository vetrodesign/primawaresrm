export class ItemGroupFilter {
    code: any;
    nameRO: any;
    nameENG: any;
    shortNameRO: any;
    shortNameENG: any;

    additionFrom: any;
    additionTo: any;

    customCodeIds: any;
    currencyIds: any;
    itemGroupCategoryIds: any;
    itemGroupCodeIds: any;
    cpvCodeIds: any;
}