import { BaseDomain } from "./base-domain.model";

export class Post extends BaseDomain {
    code: string;
    companyPost: string;
    isLeadershipPost: boolean;
    description: string;
    occupationCodeId: number;
    CORCode: string;
    CORPost: string;
    officeId: number;
    id: number;

    isActive: boolean;

    postXPartnerActivityAllocation: any[];
    partnerActivityAllocationIds: any;

    postXGeographicalArea: any[];
    postXGeographicalAreaIds: any;

    postXCategoryGeographicalArea: any[];
    postXCategoryGeographicalAreaIds: any;

    postXItemGroupCategory: any[];
    postXItemGroupCategoryIds: any;

    isOfficeSuperior: boolean;
    isDepartmentSuperior: boolean;
    directSuperiorPostId: number;
    isLocationSuperior: boolean;
    isGeneralManager: boolean;
}
