export class ERPSyncOptions {
    public partnerId: number;
    public isUpdate: boolean = false;
    public isSeniorErp: boolean = false;
    public historyPostName: string;
    public historyUserName: string;
    public source: number;
}