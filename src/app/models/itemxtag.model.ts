import { BaseDomain } from "./base-domain.model";

export class ItemXTag extends BaseDomain {
    itemId: number;
    tagId: number;
}