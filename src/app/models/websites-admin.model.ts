import { BaseDomain } from "./base-domain.model";

export class WebsiteAdmin extends BaseDomain {
    partnerId: number;
    partnerName: string;
    partnerLocation: string;
    websiteId: number;
    websiteName: string;
    partnerActivity: string;
    partnerIsCompetitor: number;
    partnerSalesAgentId: number;
    cA1: number;
    pF1: number;
    nA1: number;
    vD1: number;
    nP1: number;
    cA2: number;
    pF2: number;
    nA2: number;
    vD2: number;
    nP2: number;
    partnerContactPerson: string;
    websiteRequestsData: number;
    websiteApproved: boolean;
    websiteHasApi: boolean;
    websiteLastAccessed: Date;
    websiteObservations: string;
    websiteConnections: string;

    partnerWebsiteId: number;
    website: string;
    requestsData: number;
    approved: boolean;
    observations: string;
    connections: string;
}