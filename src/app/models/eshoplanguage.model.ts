import { BaseDomain } from "./base-domain.model";

export class EshopLanguage extends BaseDomain {
    customerId: number;
    languageId: number;
}