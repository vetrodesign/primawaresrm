import { BaseDomain } from "./base-domain.model";
import { PartnerLocation } from "./partnerlocation.model";

export class Partner extends BaseDomain {
    code: string;
    email: string;
    name: string;
    description: string;

    isSupplier: boolean = false;
    isClient: boolean = false;
    partnerStatusId: number;
    isVATPayer: boolean = false;

    supplierAgentId: number;
    implicitSupplierAgentId: number;
    salesAgentId: number;

    partnerLikeId: number;
    competitor: number = 0;
    isStrategicPartner: boolean = false;
    website: string;
    fax: string;
    hasWebsite: boolean = false;

    clientType: number;
    supplierType: number;
    personType: number;
    caenCodeId: number;
    syncERPExternalId: string;
    syncSeniorExternalId : string;
    publicInstitutionId: number;
    initialPartnerLocation: PartnerLocation;
    partnerLegalFormId: number;
    phone: string;
    tradeRegisterNumber: string;
    fiscalCode: string;
    isActive: boolean = false;

    tagIds: any;
    existingTagIds: any;

    excludedItemGroupCodeIds: number[];
    itemGroupCategoryObservations: string;
    itemGroupCodeObservations: string;
    isPrincipalActivityUniqueForAllLocations: boolean;
    partnerActivityStory: string;
    clientActivityStory: string;
    supplierActivityStory: string;
}

export class DuplicatePartner extends BaseDomain {
    code: string;
    countryId: number;
    duplicateCount: number;
    fiscalCode: string;
    invoice: string;
    lastContactDate: Date | null;
    normalizedFiscalCode: string;
    observations: string | null;
    partnerActivityAllocationId: number;
    partnerName: string;
    subscriptionState: string;
    syncERPExternalId: string;
    syncSeniorExternalId: string | null;
    tradeRegisterNumber: string;

    isPriority: boolean;
  }
