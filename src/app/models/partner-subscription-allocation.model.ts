import { BaseDomain } from "./base-domain.model";

export class PartnerSubscriptionAllocation extends BaseDomain {
    partnerId: number;
    partnerSubscriptionId: number;
    sms: boolean;
    email: boolean;
    whatsApp: boolean;
    approved: boolean;
    deleted: boolean = false;
    motivation: string;
    isActive: boolean;
}
