export class PartnerAuthorization {
    customerId: number;
    partnerId: number;
    authorizationTypeId: number;
    number: string;
    dateObtained: Date;
    isActive: boolean;
}