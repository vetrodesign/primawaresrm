export class CustomCodeXTaxRateCustoms {
    id: string;
    customCodeId: number;
    countryGroupId: number;
    taxRateId: any;
}

export class CustomCodeXTaxRateVAT {
    id: string;
    customCodeId: number;
    countryId: number;
    taxRateVatTypeId: any;
}

export class CustomCodeXTaxRateExcise {
    id: string;
    customCodeId: number;
    countryId: number;
    taxRateId: any;
}

export class CustomCodeXTaxRateAntiDumping {
    id: string;
    customCodeId: number;
    countryId: number;
    taxRateId: any;
}