export class Marker {
    public location: Location;
    public tooltip: Tooltip;
}

export class Tooltip {
    public isShown: boolean;
    public text: string;
}

export class Location {
    public lat: number;
    public lng: number;
}

export class Route {
    public weight: number;
    public color: string;
    public opacity: number;
    public mode: string;
    public locations: Location[];
}

