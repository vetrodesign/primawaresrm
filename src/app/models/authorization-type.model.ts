export class AuthorizationType {
    code: string;
    customerId: number;
    name: string;
}