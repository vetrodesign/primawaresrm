import { BaseDomain } from "./base-domain.model";

export class ItemEcommerce extends BaseDomain {
    itemId: number;
    store_view_code: string;
    product_type: string;
    product_website: string;
    is_in_stock: boolean;
    visibility: boolean;
    website_id: string;
    attribute_set_code: string;
    manage_stock: string;
    related_skus: string;
    crosssell_skus: string;
    upsell_skus: string;
    configurable_variations: string;
    configurable_variation_labels: string;
    last_price_update: Date;
    eshop_price: string;
    special_price_from_date: Date;
    special_price_to_date: Date;

    meta_title: string;
    meta_keywords: string;
    meta_description: string;
    base_image: string;
    small_image: string;
    thumbnail_iamge: string;

    new_from_date: string;
    new_to_date: string;
}