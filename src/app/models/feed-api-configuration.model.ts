import { BaseDomain } from "./base-domain.model";

export class FeedApiConfiguration extends BaseDomain {
    partnerId: number;
    apiKey: any;
    partnerName: string;
    isActive: boolean;
}

export class FeedApiXPartnerIp extends BaseDomain {
    feedApiConfigurationId: number;
    partnerIp: string;
}