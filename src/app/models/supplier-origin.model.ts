import { BaseDomain } from "./base-domain.model";

export class SupplierOrigin extends BaseDomain {
    public code: string;
    public name: string;
    public stockAlertMultiplier: number;
    public lowerMargin: number;
    public upperMargin: number;
    public isActive: boolean;
}