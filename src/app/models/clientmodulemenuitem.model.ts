export class ClientModuleMenuItem {
    public id: number;
    public title: string;
    public path: string;
    public icon: string;
    public subMenuId: number;
    public clientModuleId: number;
}
