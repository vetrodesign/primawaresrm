export class PartnerLocationConventionAndCurrencyResult {
    public productConventionId: number;
    public currencyId: number;
}