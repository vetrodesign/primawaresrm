export class CaenCode {
    public id: number;
    public code: string;
    public nace: string;
    public sic: string;
    public description: string;
    public sicDescription: string;
    public caenCodeSectionId: number;

    public caenCodeCategory: string;
    public caenCodeDomain: string;

    public getPropertiesArray(): string[] {
        return ['code', 'nace', 'sic', 'description', 'sicDescription'];
    }
}