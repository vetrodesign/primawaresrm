export class TurnoverSearchFilter {
    partnerId: any;
    postIds: any;
    code: any;
    name: any;
    itemIds: any;
    citiesIds: any;
    countriesIds: any;
    countiesIds: any;

    stockToBeSuppliedInternalSuppliers: number;
    stockToBeSuppliedExternalSuppliers: number;
    stockToBeSuppliedIntracommunitySuppliers: number;
}

export class TurnoverOnlyToSupplyFilter {
    isOnlyToSupply: boolean;
    onlyToSupplyOption: number;
}
