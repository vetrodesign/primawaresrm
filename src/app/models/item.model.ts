import { BaseDomain } from "./base-domain.model";
import { CaenCodeSpecializationXItem } from "./caencodespecializationxitem.model";
import { ItemProperties } from "./itemproperties.model";

export class Item extends BaseDomain {
    public code: string;
    public itemGroupId: number;
    public measurementUnitId: number;
    public customCodeId: number;
    public nameRO: string;
    public nameENG: string;
    public shortNameRO: string;
    public shortNameENG: string;
    public proposedById: number;

    public PC_ACT: string;
    public TRSP_ACT: string;
    public CMP_ACT: string;

    public partnerId: number;
    public itemTypeId: number;
    public syncERPExternalId: string;
    public syncSeniorExternalId: string;

    public taxRateVATType: number;
    public isValid: boolean = false;
    public isStockable: boolean = false;
    public turnOver: number;
    public evaluatedTurnover: number;
    public stateId: number;
    public priceCompositionType: number;

    public alertPeriod: number;
    public periodicity: Date;
    public numberOfAlerts: number;

    public caenCodeSpecializations: CaenCodeSpecializationXItem[];

    public tagIds: any;
    public existingTagIds: any;
    public isActive: boolean;

    public hasBbd : boolean;
    public hasLot : boolean;
    public hasMgf : boolean;
    public hasSn : boolean;

    public itemProperties: ItemProperties;
    public lastInvoiceDate: Date;

    // TODO: check if its okay to keep it on the frontEnd or move it on the backEnd
    public totalStock: number;
    public turnOversRs: string;
}

export class EshopVisibleItem {
    public code: string;
    public nameRO: string;
    public nameENG: string;
    public shortNameRO: string;
    public shortNameENG: string;
}

export class ItemsTurnoversCountData {
    public intervalName: string;
    public itemsCount: number
}
