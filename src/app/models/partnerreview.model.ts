import { BaseDomain } from "./base-domain.model";

export class PartnerReview extends BaseDomain
{
    public description: string;
    public postId: number;
    public partnerId: number;
    public partnerSuggestionState: number;
    public partnerReviewQuestionId: number;
    public decision: string;
    public decisionStateId: string;
}