export class CaenCodeSpecializationXPartnerLocation {
    public id: number;
    public caenCodeSpecializationId: number;
    public partnerLocationId: number;
}
