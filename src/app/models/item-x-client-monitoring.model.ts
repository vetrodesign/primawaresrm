import { BaseDomain } from "./base-domain.model";

export class ItemXClientMonitoringModel extends BaseDomain {
    itemId: number;
    partnerId: number;
    partnerLink: string;

    name: string;
    code: string;
    brand: string;

    priceWithTVA: number;
    priceWithoutTVA: number;

    conversionFactor: number;
    
    inStock: boolean;
    stock: number;

    // FE only
    partnerName: string;
    codeVetro: string;
    d0: number;
    c0: number;
    r0: number;
    totalPrice: number;
}