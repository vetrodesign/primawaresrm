import { BaseDomain } from "./base-domain.model";

export class BarCodeInterval extends BaseDomain{
    public standardType: number;
    public ownerCode: number;
    public from: number;
    public to: number;
    public start: string;
    public end: string;

    public fromString: string = ''; 
    public toString: string = '';

    public isImplicit: boolean = false;
    public countryId: number;
    public partnerId: number;
    public customerId: number;
    public partnerLocationGln: string;

    public setIntervals() {
        if (this.from) {
            this.fromString = this.from.toString().length === 5 ? this.from.toString() : this.setStringValue(this.from.toString());
        }
        if (this.to) {
            this.toString = this.to.toString().length === 5 ? this.to.toString() : this.setStringValue(this.to.toString());
        }
    }

    setStringValue(value: string): string {
        let count = 5 - value.length;
        let string = '';
        for(let i = 0; i < count; i++) {
            string += '0';
        }
        return string + value;
    }

    constructor(barCodeInterval?: BarCodeInterval) {
        super();
        if (barCodeInterval) {
            this.id = barCodeInterval.id;
            this.rowVersion = barCodeInterval.rowVersion
            this.standardType = barCodeInterval.standardType;
            this.ownerCode = barCodeInterval.ownerCode;
            this.from = barCodeInterval.from;
            this.to = barCodeInterval.to;
            this.start = barCodeInterval.start;
            this.end = barCodeInterval.end;

            this.fromString = barCodeInterval.fromString;
            this.toString = barCodeInterval.toString;

            this.isImplicit = barCodeInterval.isImplicit;
            this.countryId = barCodeInterval.countryId;
            this.partnerId = barCodeInterval.partnerId;
            this.customerId = barCodeInterval.customerId;
        }
    }
}
