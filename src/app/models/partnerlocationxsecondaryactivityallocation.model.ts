import { BaseDomain } from "./base-domain.model";

export class  PartnerLocationXSecondaryActivityAllocation extends BaseDomain {
    partnerActivityAllocationId: number;
    partnerLocationId: number;
}