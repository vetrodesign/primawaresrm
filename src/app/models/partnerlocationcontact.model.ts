export class PartnerLocationContact 
{
    public id: number;
    public firstName: string;
    public lastName: string;

    public partnerId: number;
    public partnerLocationId: number;
    public position: number;
    public isImplicit?: boolean;
    public isActive: boolean = false;
    public description: string;
    public phoneNumber: string;
    public email: string;
    public fax: string;
    public birthday: Date;
    public isAvailableBySchedule?: boolean;
    public syncSeniorExternalId: string;
    public gmt: string;
}