import { ApiLogs } from "./api-logs.model";
import { BaseDomain } from "./base-domain.model";

export class ApiOverview extends BaseDomain {
    name: string;
    description: string;
    notes: string;
    status: string;
    postId: number;
    lastRunDate: Date;
    hasError: boolean;

    apiLogs: ApiLogs[];
}