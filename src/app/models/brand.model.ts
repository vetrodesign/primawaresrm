import { BaseDomain } from "./base-domain.model";

export class Brand extends BaseDomain {
    public code: string;
    public name: string;
    public partnerId: number;
}