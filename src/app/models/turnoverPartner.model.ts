export class TurnoverPartner {    
    supplierId: number;
    countryId: number | undefined;
    countyId: number | undefined;
    cityId: number | undefined;
    supplierAgentId: number | undefined;
    supplierName: string;
    supplierWebsite: string;
    supplierAgentName: string;
    countryName: string;
    countyName: string;
    cityName: string;

    smallDataTurnoverItems: any;
}