export class PartnerLegalForm {
    id: number;
    code: string;
    name: string;
    isActive: boolean;
}