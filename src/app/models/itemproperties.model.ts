import { BaseDomain } from "./base-domain.model";

export class ItemProperties extends BaseDomain {
    itemId: number;
    visibility: number;
    baseImage : string;
    smallImage : string;
    thumbailImage : string;
    manufacturer : string;
    brandId: number;
    colorId: number;
    measureId: number;
    sizeId: number;
    shapeId: number;
    materialId: number;
    cutId: number;
    closingId: number;
    attributeSetCode : string;
}