import { BaseDomain } from "./base-domain.model";

export class ItemSpecification extends BaseDomain {
    itemId: number;
    itemImageBase64: string;
}