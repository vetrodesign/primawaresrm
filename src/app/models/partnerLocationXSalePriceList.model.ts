export class PartnerLocationXSalePriceList {
    id: any;
    partnerLocationId: number;
    salePriceListId: number;
}

export class PartnerLocationXSalePriceListUpdateMultipleVM {
    partnerLocationId: number;
    deletedSalePriceListIds: number[];
    createdSalePriceListIds: number[];
}