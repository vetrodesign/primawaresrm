import { CountryGroupXCountry } from './countrygroupxcountry.model';

export class CountryGroup {
    public id: number;
    public name: string;
    public  countryGroupXCountry: CountryGroupXCountry[];
    public countryIds: any[];
}
