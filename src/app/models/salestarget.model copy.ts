import { BaseDomain } from "./base-domain.model";

export class SalesTarget extends BaseDomain {
    code: string;
    name: string;
    description: string;

    saleGroupTargetId: number;
    targetType: number;
    customTypeText: string;

    isActive: boolean;
}