export class CountryBarCodeStandard {
    public id: number;
    public countryId: number;
    public standardType: number;
    public from: number;
    public to: number;
}
