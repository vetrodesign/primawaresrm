export class SupplierSmallSearchFilter {
    public code: string;
    public name: string;
    public fiscalCode: string;
    public supplierType: number[];
    public nameOrCode: string;
}