export class BnrRoborRequest {
    public startDate: Date;
    public stopDate: Date;
}

export class BnrRobor {
    public date: string;
    public roborOvernight: string;
    public roborTomorrowNext: string;
    public robor1W: string;
    public robor1M: string;
    public robor3M: string;
    public robor6M: string;
    public robor9M: string;
    public robor12M: string;
}

