export class TurnoverItemHistory {
    itemId: number;
    itemCode: string;
    itemName: string;
    partnerId: number;
    partnerName: string;
    caenCode: string;
    caenDescription: string;
    partnerActivity: string;
    partnerResponsableAgent: string;
    turnoverOperationId: number;
    turnoverOperationName: string;
    year: number;
    month: number;
    quantity: number;
    amount: number;
    currencyId: number;
    currencyName: string;
}
