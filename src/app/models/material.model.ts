import { BaseDomain } from "./base-domain.model";

export class Material extends BaseDomain {
    public code: string;
    public customerId: number;
    public isActive: boolean;
}