import { BaseDomain } from "./base-domain.model";

export class PaymentInstrument extends BaseDomain {
    code: string;
    name: string;
}
