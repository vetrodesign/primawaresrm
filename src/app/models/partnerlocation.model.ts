import { BaseDomain } from "./base-domain.model";

export class PartnerLocation extends BaseDomain
{
    public name: string;
    public note: string;
    public partnerId: number;
    public countryId: number;
    public countyId: number;
    public cityId: number;
    public languageId: number;
    public street: string;
    public streetNumber: string;
    public block: string;
    public level: string;
    public apartment: string;
    public zipCode: string;
    public latitude: number;
    public longitude: number;
    public description: string;
    public isActive: boolean = false;
    public addressTypeId: number;
    public isImplicit: boolean = false;
    public partnerActivityAllocationId: number;

    public gln: string;

    public caenCodeSpecializationIds: any;
    public existingCaenCodeIds: any;

    public partnerLocationXSalePriceList: any[];
    public partnerLocationXSupplierPriceList: any[];
    public salePriceListIds: any;
    public supplierSalePriceListIds: any;

    public existingItemGroupCategoryIds: any[];
    public itemGroupCategoryIds: any;

    public existingItemGroupCodeIds: any[];
    public itemGroupCodeIds: any;

    public existingPartnerSecondaryActivityAllocationIds: any[];
    public partnerSecondaryActivityAllocationIds: any;

    public webSite: string;
    public phoneNumber: string;
    public fax: string;

    public baseCurrencyId: number;
    public alternativeCurrencyId: number;

    public productConventionName: string;
    public productConventionId: number;

    public isRegisteredOffice: boolean = false;
    public isCorrespondence: boolean = false;
    public isDeposit: boolean = false;
    public isOffice: boolean = false;
    public isHome: boolean = false;

    public syncSeniorExternalId: string;
    public syncSeniorAddressExternalId: string;
    
    public gmt: string;
}