export class CaenCodeSpecializationXItem {
    public id: number;
    public caenCodeSpecializationId: number;
    public itemId: number;

    public sgf: boolean;
    public cda: boolean;
    public szo: boolean;
    public alt: boolean;
}
