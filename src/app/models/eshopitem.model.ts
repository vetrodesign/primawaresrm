import { BaseDomain } from "./base-domain.model";

export class EshopItem extends BaseDomain {
    itemId: number;
    languageId: number;
    eshopLanguageId: number;

    name: string;
    description: string;
    shortDescription: string;

    productOnline: number;
    vatId: number;

    metaTitle: string;
    metaKeywords: string;
    metaDescription: string;

    newFromDate: Date;
    newPriceToDate: Date;

    composition : string;
    components : string;
    concentration : string;
    use: string;
    useInstructions : string;
    aspect: string;
    presentation: string;
    properties: string;
    destination: string;
    benefits: string;
    efect: string;
    tehnicalDetails: string;
    itemPackagingMode: string;
    boxPackingMode : string;
    width: string;
    length: string;
    thickness: string;
    weight: string;
    height: string;
    heaviness: string;
    accessories: string;
    type: string;
    layers: string;
    numberOfUses: number;
    meltingPoint: string;
    country: string;
    storageConditions: string;
}