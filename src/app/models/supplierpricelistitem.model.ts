export class SupplierPriceListItem {
    id: string;
    supplierPriceListId: number;
    itemId: number;
    listPrice: number;
    discountedListPrice: number;
    convertedDiscountedListPrice: number;
    discount: number;
    discountAdvance: number;
    approvedTransport: number;
    brandId: number;
    supplierQuantity: number;
    minSupplierQuantity: number;
    details: string;
}
