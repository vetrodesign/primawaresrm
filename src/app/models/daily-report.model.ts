import { BaseDomain } from "./base-domain.model";

export class DailyReport extends BaseDomain {
    salesTargetXPostId: number;
    customerId: number;
    value: number;
    description: string;
    validated: boolean;
    fromDate: Date;
}


export class DailyReportFullDetails {
    saleTargetAllocationId: number;
    date: Date;
    saleTargetId: number;
    saleTargetDescription: string;
    saleTargetValue: string;
    orderNumber: number;
    value: number;
    description: string;
    validated: boolean;

    isUnderTarget:boolean;
}
