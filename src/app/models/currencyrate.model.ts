export class CurrencyRate {
    public id: number;
    public currencyDate: string;
    public baseCurrencyId: number;
    public baseCurrencyName: string;
    public currencyRatesXCustomerId: number;
    public isFavorite: boolean;
    public currencyId: number;
    public currencyName: string;
    public currencyOffer: number;
    public currencyExchange: number;
    public bnrRate: number;
    public percentageOfferDiff: number;
    public percentageExchangeDiff: number;
    public maxPercentageLevel: number;
    public minPercentageLevel: number;
    public bceRate: number;
}