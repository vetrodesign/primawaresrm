import { BaseDomain } from "./base-domain.model";

export class ItemGroup extends BaseDomain{
    public code: string;
    public nameRO: string;
    public nameENG: string;
    public shortNameRO: string;
    public shortNameENG: string;

    public pC_MAX: number;  

    public trS_MAX: number;
    public additionTrsMax: number;
    
    public addition: number;
    public additionPoint: number;
    
    public cmP_MAX: number;
    public currencyId: number;
    public itemGroupCodeId: number;
    public customCodeId: number;
    public itemGroupCategoryId: number;

    public cpvCodeIds: any;
    public itemGroupCpvCodes: any[];
    public nextCodeAvailable: string;
}
