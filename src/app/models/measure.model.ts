import { BaseDomain } from "./base-domain.model";

export class Measure extends BaseDomain {
    public code: string;
    public customerId: number;
    public isActive: boolean;
}