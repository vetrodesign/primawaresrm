import { BaseDomain } from "./base-domain.model";

export class User extends BaseDomain {
    defaultSiteId: number;
    postId: number;
    userName: string;
    firstName: string;
    lastName: string;
    email: string;
    isActive?: boolean;
    password?: string;
    token?: string;
    created: Date;
    createdBy: number;

    userRole: any[];
    roleIds: any;

    userXSite: any[];
    siteIds: any;

    profileImageBase64: string;
}
