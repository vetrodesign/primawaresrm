export class TurnoverItem {
    itemId: number;
    itemCode: string;
    itemName: string;
    acquisitionPeriodDays: number | undefined;
    acquisitionArrivalDays: number | undefined;
    currentStock: number | undefined;
    maximumStock: number | undefined;
    safetyStock: number | undefined;
    finalStock: number | undefined;
    quantityOnOrdersToArrive: number | undefined;
    sufficientMonths: number | undefined;
    quantityToBeOrdered: number | undefined;
    quantityOnDraftInvoices: number | undefined;
    quantityRemainingOnClientOrders: number | undefined;
    itemTurnover: number | undefined;
    rulajReal3M: number | undefined;
    rulajReal6M: number | undefined;
    rulajReal9M: number | undefined;
    rulajReal12M: number | undefined;

    itemDetails: TurnoverItemDetail[];
}

export class TurnoverItemDetail {
    turnoverOperationId: number;
    turnoverOperationName: string;
    year: number;
    month: number;
    quantity: number;
    amount: number;
    currencyId: number;
    currencyName: string;
}

export class TurnoverMonthsRow {
    itemId: number;
    turnoverOperationId: number;
    turnoverOperationName: string;
    Total: number;
    currencyId: number;
    currencyName: string;
    monthsData: TurnoverMonths[];
    NoOfPartners: number;
}

export class TurnoverMonths {
    month: Date;
    quantity: number;
    amount: number;
}

export class TurnoverHistoryMonthsRow {
    itemId: number;
    turnoverOperationId: number;
    turnoverOperationName: string;
    Total: number;
    currencyId: number;
    currencyName: string;
    monthsData: TurnoverMonths[];
    partnerId: number;
    partnerName: string;
    partnerActivity: string;
    caenCode: string;
    caenDescription: string;
    partnerResponsableAgent: string;
}

export class TurnoverPartnerMonthQuantityFilter {
    itemId: number;
    partnerId: number;
    month: number;
    year: number;
}

export class TurnoverPartnerMonthQuantity {
    documentNo: number;
    documentDate: Date;
    quantity: number;
    pu: number;
}

export class ItemPrices {
    cmp: number;
    d0: number;
    c0: number;
    r0: number;
}
