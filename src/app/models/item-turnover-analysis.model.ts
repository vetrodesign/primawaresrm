import { BaseDomain } from "./base-domain.model";

export class ItemTurnoverAnalysis extends BaseDomain {
    public itemId: number;
    public manualTurnover?: number;
    public isModified: boolean;
    public alerting: string;
    public supplyAlertState: number;
    public nextSupplyAlertDate: Date;

    public itemCode: string;
    public itemName: string;
    public itemStock: number;
    public itemTurnover: number;
    public itemEvaluatedTurnover: number;
    public clientsNo: string;
    public salesValueCurrentMonth: number;
    public salesValues1MonthAgo: number;
    public salesValues2MonthAgo: number;
    public salesValues3MonthAgo: number;
    public salesValuesComputed: string;
    public r3: number;
    public r6: number;
    public r9: number;
    public r12: number;
    public r3r12Max: number;
    public automaticProposedTurnover: number;
    public newTurnover: number;
    public itemType: number;
    public supplierAgentId: number;
    public partnerId: number;
    public bottomMargin: number;
    public topMargin: number;
    public bottomMarginPercentage: number;
    public topMarginPercentage: number;
}

export class ItemTurnoverAnalysisFilter {
    public itemIds: number[];
    public postIds: number[];
    public supplierId: number;
    public manufacturerId: number;
    public isModified: boolean;
    public isAlerting: boolean;
    public turnoverSymbol: string;
}