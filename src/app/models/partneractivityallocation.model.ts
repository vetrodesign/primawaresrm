export class PartnerActivityAllocation {
    id: number;
    partnerActivityId: number;
    partnerActivityClassification: string;
    isLegal: boolean;
    isIndividual: boolean;
    productConventionId: number;
    saleZoneCode: string;

    caenCodeSpecializationIds: any;
    existingCaenCodeIds: any;
}