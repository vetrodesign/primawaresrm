export class Language {
    public id: number;
    public code: string;
    public name: string;
    public culture: string;
    public customerId?: number;
    public flagClass: string;
}
