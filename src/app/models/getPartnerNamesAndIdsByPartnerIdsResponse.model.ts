export class GetPartnerNamesAndIdsByPartnerIdsResponse {
    id: number;
    name: string;
}