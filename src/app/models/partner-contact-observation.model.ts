import { BaseDomain } from "./base-domain.model";

export class PartnerContactObservation extends BaseDomain {
    public customerId: number;
    public partnerContactId: number;
    public state: number;
    public message: string;
    public isActive: boolean;
}