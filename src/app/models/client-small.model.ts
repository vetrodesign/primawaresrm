export class ClientSmall {
    id: number;
    code: string;
    name: string
    fiscalCode: string;
}