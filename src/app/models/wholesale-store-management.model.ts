import { BaseDomain } from "./base-domain.model";

export class WholesaleStoreManagement extends BaseDomain {
    public itemId: number;
    public itemCode: string;
    public itemName: any;
    public itemVolume: any;
    public itemGrossWeight: any;
    public itemTurnover: any;
    public itemTurnover3Months: any;
    public itemTurnover6Months: any;
    public itemTurnover9Months: any;
    public itemTurnover12Months: any;

    public cartonQuantity: any;
    public cartonHeight : any;
    public cartonWidth : any;
    public cartonLength : any;
    public cartonLayers : any;

    public decidedDB: any;
    public decidedBD: any;

    public itemTurnoversPercentage: any;
    public itemAcquisitionPeriodDays: any;
    public itemPalletGrossWeight: any;
    public mainManagementId: any;
    public mainManagementName: any;
    public mainManagementCurrentStock : any;
    public mainManagementPalletsNumber : any;
    public mainManagementStockSufficiencyAtTurnoverRate : any;
    public secondaryManagementId : any;
    public secondaryManagementName: any;
    public secondaryManagementCurrentStock : any;
    public secondaryManagementPalletsNumber : any;
    public secondaryManagementStockSufficiencyAtTurnoverRate : any;
    public destorageEnGrosPallets : any;
    public storagePickingPallets : any;
    isTurnoverToBeFixed: boolean;
    calculatedTurnover: number;
    orderStorage: number
    orderUnstorage: number;
    description: string;

    shouldHideDB: boolean;
    shouldHideBD: boolean;
}


export class WholesaleStoreManagementFilter  {
    mainManagementDestorageMonths: number;
    mainManagementStorageMonths: number;
    palletHeight: number;
    palletVolume: number;
}

