export class PartnerLocationXItemGroupCode {
    id: number;
    partnerLocationId: number;
    itemGroupCodeId: number;
}