import { BaseDomain } from "./base-domain.model";

export class SalesZone extends BaseDomain {
    code: string;
    name: string;
    isActive: boolean;
}