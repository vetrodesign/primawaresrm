import { BaseDomain } from "./base-domain.model";

export class DocumentTemplate extends BaseDomain {
    name: string;
    description: string;
    contentHTML: string;
    languageId: number;
    documentTemplateHeaderId: number;
    documentTemplateFooterId: number;
    documentTemplateTypeId: number;
    documentTypeId: number;

    isActive: boolean;
}