export class ClientSmallSearchFilter {
    code: string;
    name: string;
    fiscalCode: string;
    nameOrCode: string;
    email: string;
}