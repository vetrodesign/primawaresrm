import { BaseDomain } from "./base-domain.model";

export class ItemXRelated extends BaseDomain {
    relatedItemId: number;
    itemId: number;
    itemType: number;
    description: string;

    caenCodeSpecializationId: number | null | undefined;
    relatedItemSpecIds: number[];
    icon: string;
}

export enum RelatedItemType {
    Alike = 1,
    Complementary = 2,
    Superior = 3,
    Inferior = 4,
    Competitor = 5
  }