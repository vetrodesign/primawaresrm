import { BaseDomain } from "./base-domain.model";

export class ItemGroupCategory extends BaseDomain {
    catCode: string;
    firstName: string;
    secondName: string;
    primeMaterialId: number;
    implicitPostId: number;
    backupPostId: number;
}