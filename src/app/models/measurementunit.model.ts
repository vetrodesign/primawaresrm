import { BaseDomain } from "./base-domain.model";

export class MeasurementUnit extends BaseDomain {
    name: string;
    code: string;
    syncERPExternalId: any;
    isImplicit: boolean;
}
