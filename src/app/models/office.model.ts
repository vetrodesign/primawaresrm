import { BaseDomain } from "./base-domain.model";
import { Post } from "./post.model";

export class Office extends BaseDomain {
    name: string;
    description: string;
    departmentId: number;
    code: string;
    isActive: boolean;
    posts: Post[];
    orderNumber: number;
}
