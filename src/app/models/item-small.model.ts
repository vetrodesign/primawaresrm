export class ItemSmall {
    id: number;
    code: string;
    name: string;
    salePriceListId: number;
}