export class DownloadPDFResponseModel {
    emptyDataList: string[];
    pdfData: any;
}