import { BaseDomain } from "./base-domain.model";

export class PartnerERPSyncHistory extends BaseDomain {
    public partnerId: number;
    public postName: string;
    public userName: string;
}