// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiGatewayUrl: 'https://localhost/PrimawareGateway.Api',
  magentoApiUrl: 'https://vetro.ro/rest/all',
  magentoUser: 'vetro121',
  magentoPassword: 'CnA-xJ^MQ7dkMy~',
  SSOPrimaware: 'https://localhost:4200',
  MRKPrimaware: 'https://localhost:4207',
  SRMPrimaware: 'https://localhost:4209',
  googleApiKey: 'AIzaSyA3oUGpF8DOhE-0lNMMv1QL08sLFLjO-WU',
  encryptionKey: 'w7hskLVpTdH6I0MnhVls',

  microserviceApiUrl: '/marketingApi',
  //microserviceApiUrl: '/SRMApi',
  marketingApiUrl: '/marketingApi',

  useApiGateway: false,
  ssoApi: 'https://localhost/PrimawareAPI.Api/api',
  mrkApi: 'https://localhost/PrimawareMarketing.Api/api',
  commonApi: 'https://localhost/PrimawareCommon.Api/api',
  srmApi: 'https://localhost/PrimawareSRM.Api/api',
  apiPaths: {
     ssoApi: '/ssoApi',
     mrkApi: '/marketingApi',
     commonApi: '/commonApi',
     srmApi: 'srmApi',
  }
};
